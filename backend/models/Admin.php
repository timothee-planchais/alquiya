<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "admin".
 *
 * @property string $id
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $password_hash
 * @property string $auth_key
 * @property string $role
 * @property string $created_at
 * @property string $updated_at
 */
class Admin extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_PASSWORD = 'password';
    
    const ROLE_SUPER_ADMIN = 'super_admin';
    const ROLE_LEVEL_1 = 'level_1';//Admin
    
    public $password;//Contient le mdp non hashé
    public $password_repeat;//Mdp répété
    public $new_password;//Nouveau mdp
    public $new_password_repeat;//Répétition du nouveau mdp
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                //'value' => new \yii\db\Expression('NOW()'),
                'value' => date('Y-m-d H:i:s')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'firstname', 'lastname', 'role'], 'required'],
            ['email', 'unique'],
            ['email', 'email'],
            [['email'], 'string', 'max' => 250],
            ['email', 'trim'],
            [['firstname', 'lastname'], 'string', 'max' => 100],
            
            [['password', 'password_repeat'], 'required', 'on' => [self::SCENARIO_INSERT]],
            ['password', 'string', 'min' => 6, 'on' => [self::SCENARIO_INSERT]],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => [self::SCENARIO_INSERT]],
            
            [['new_password', 'new_password_repeat'], 'required', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['new_password', 'string', 'min' => 6, 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            
        ];
    }
    
    public function afterFind()
    {
        $this->password = $this->password_repeat = $this->new_password = $this->new_password_repeat = null;
        
        return parent::afterFind();
    }
    
    public function beforeSave($insert)
    {
        if($insert) 
        {
            if($this->scenario == self::SCENARIO_INSERT) {
                $this->setPassword($this->password);
            }
        }
        elseif($this->scenario == self::SCENARIO_UPDATE_PASSWORD) {
            $this->setPassword($this->new_password);
        }
        
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        $this->password = $this->password_repeat = null;
        
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function __toString()
    {
        return $this->firstname.' '.$this->lastname;
    }
    
    public function isSuperAdmin()
    {
        return $this->role == self::ROLE_SUPER_ADMIN;
    }
    
    public static function getRoles()
    {
        return [
            self::ROLE_SUPER_ADMIN => "Super admin",
            self::ROLE_LEVEL_1 => "Admin",
        ];
    }
    
    public function getTextRole()
    {
        return ArrayHelper::getValue(self::getRoles(), $this->role);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'firstname' => Yii::t('app', 'Prénom'),
            'lastname' => Yii::t('app', 'Nom'),
            'password' => Yii::t('app', "Mot de passe"),
            'password_hash' => Yii::t('app', "Mot de passe"),
            'password_repeat' => Yii::t('app', "Répéter le mot de passe"),
            'new_password' => Yii::t('app', "Nouveau mot de passe"),
            'new_password_repeat' => Yii::t('app', "Répéter le nouveau mot de passe"),
            'created_at' => Yii::t('app', 'Ajouté le'),
            'updated_at' => Yii::t('app', 'Modifié le'),
        ];
    }
    /*
     * 
     */
    
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email, $params = [])
    {
        $params['email'] = $email;
        return static::findOne($params);
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    
}
