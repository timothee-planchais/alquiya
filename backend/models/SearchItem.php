<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Item;

/**
 * SearchItem represents the model behind the search form of `common\models\Item`.
 */
class SearchItem extends Item
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'category_id', 'subcategory_id', 'nb_rents', 'featured', 'highlighted', 'condition'], 'integer'],
            [['status', 'title', 'slug', 'subtitle', 'description', 'full_address', 'short_address', 'address', 'neighborhood', 'zipcode', 'city', 'full_city', 'state', 'region','country','country_code'], 'safe'],
            [['price', 'price_weekend', 'price_week', 'price_month', 'deposit_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'category_id' => $this->category_id,
            'subcategory_id' => $this->subcategory_id,
            'status' => $this->status,
            'price' => $this->price,
            'price_weekend' => $this->price_weekend,
            'price_week' => $this->price_week,
            'price_month' => $this->price_month,
            'deposit_value' => $this->deposit_value,
            'featured' => $this->featured,
            'highlighted' => $this->highlighted,
            'country' => $this->country,
            'country_code' => $this->country_code,
            'condition' => $this->condition
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'neighborhood', $this->neighborhood])
            ->andFilterWhere(['like', 'zipcode', $this->zipcode])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'full_city', $this->full_city])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'region', $this->region]);

        if($this->full_address) {
            $query->andFilterWhere(['like', 'full_address', $this->full_address.'%', false]);
        }
        
        if($this->short_address) {
            $query->andFilterWhere(['like', 'short_address', $this->short_address.'%', false]);
        }
        
        
        return $dataProvider;
    }
}
