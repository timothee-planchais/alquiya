<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserPro;

/**
 * SearchUserPro represents the model behind the search form of `common\models\UserPro`.
 */
class SearchUserPro extends UserPro
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'nb_items', 'available_hours'], 'integer'],
            [['company_name', 'categories', 'website', 'vat_num', 'description', 'available_days'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserPro::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'nb_items' => $this->nb_items,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'vat_num', $this->vat_num])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'available_hours', $this->available_hours]);
        
        if($this->categories)
        {
            
        }
        
        
        if($this->available_days)
        {
            
        }
        
        return $dataProvider;
    }
}
