<?php

namespace backend\controllers;

use Yii;
use common\models\Order;
use backend\models\SearchOrder;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends \backend\components\Controller
{
    
    public function actionSearch()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = [];
        
        $q = Yii::$app->getRequest()->get('q');
        
        if($q)
        {
            $query = \common\models\Order::find()->select('order.id, order.code, u.email, u.lastname, u.firstname')
                    ->leftJoin('user u', 'u.id = order.user_id')
                    ->where(['or', ['like', 'order.code', $q.'%', false], ['like', 'u.email', $q.'%', false], ['like', 'u.lastname', $q.'%', false], ['like', 'u.firstname', $q.'%', false]]);
            
            $orders = $query->limit(25)->all();
            
            if($orders)
            {
                foreach($orders as $order)
                {
                    $results[] = [
                        'id' => $order->getPrimaryKey(),
                        'text' => $order->__toString(),
                    ];
                }
            }
        }
        
        return [
            'results' => $results
        ];
    }
    
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchOrder();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Order correctly added"));
            return $this->redirect('index');
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Order correctly updated"));
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionStatusPaid($id)
    {
        $model = $this->findModel($id);
        
        $model->status = Order::STATUS_PAID;
        $model->update(false, ['status']);
        
        return $this->redirect(['view', 'id' => $model->getPrimaryKey()]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Order correctly deleted"));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
