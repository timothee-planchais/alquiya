<?php

namespace backend\controllers;

use Yii;
use common\models\Subscription;
use backend\models\SearchSubscription;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SubscriptionController implements the CRUD actions for Subscription model.
 */
class SubscriptionController extends \backend\components\Controller
{

    public function actionSearch()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = [];
        
        $q = Yii::$app->getRequest()->get('q');
        
        if($q)
        {
            $query = \common\models\Subscription::find()->select('subscription.id, subscription.code, u.email, u.lastname, u.firstname')
                    ->leftJoin('user u', 'u.id = subscription.user_id')
                    ->where(['or', ['like', 'subscription.code', $q.'%', false], ['like', 'u.email', $q.'%', false], ['like', 'u.lastname', $q.'%', false], ['like', 'u.firstname', $q.'%', false]]);
            
            $subscriptions = $query->limit(25)->all();
            
            if($subscriptions)
            {
                foreach($subscriptions as $subscription)
                {
                    $results[] = [
                        'id' => $subscription->getPrimaryKey(),
                        'text' => $subscription->__toString(),
                    ];
                }
            }
        }
        
        return [
            'results' => $results
        ];
    }
    
    /**
     * Lists all Subscription models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchSubscription();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subscription model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Subscription model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subscription();
        $model->scenario = Subscription::SCENARIO_BACKEND;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Subscription correctly created"));
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Subscription model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Subscription::SCENARIO_BACKEND;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Subscription correctly updated"));
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Subscription model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Subscription correctly deleted"));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Subscription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Subscription the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscription::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
