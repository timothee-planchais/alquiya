<?php

namespace backend\controllers;

use Yii;
use backend\models\Admin;
use yii\data\ActiveDataProvider;
use backend\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdminController implements the CRUD actions for Admin model.
 */
class AdminController extends Controller
{
    
    /**
     * Lists all Admin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->checkPermission();
        
        $dataProvider = new ActiveDataProvider([
            'query' => Admin::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Admin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkPermission();
        
        $model = new Admin([
            'role' => Admin::ROLE_LEVEL_1,
            'scenario' => Admin::SCENARIO_INSERT
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Admin correctement ajouté");
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Admin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->checkPermission();
        
        $model = $this->findModel($id);

        if(Yii::$app->getRequest()->getIsPost())
        {
            if(Yii::$app->request->post('update-password')) {
                $model->scenario = Admin::SCENARIO_UPDATE_PASSWORD;
            }
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                if($model->scenario == Admin::SCENARIO_UPDATE_PASSWORD)
                    Yii::$app->session->setFlash('success', Yii::t('app', "Mot de passe de l'admin correctement modifié"));
                else
                    Yii::$app->session->setFlash('success', Yii::t('app', "Admin correctement modifié"));
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Admin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->checkPermission();
        
        if($this->findModel($id)->delete()) {
            Yii::$app->session->setFlash('success', "Admin correctement supprimé");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Admin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Admin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
