<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use backend\models\SearchUser;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends \backend\components\Controller
{
    public function actionSearch()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = [];
        
        $q = Yii::$app->getRequest()->get('q');
        
        if($q)
        {
            $query = \common\models\User::find()
                    ->where(['or', ['like', 'email', $q.'%', false], ['like', 'username', $q.'%', false], ['like', 'lastname', $q.'%', false], ['like', 'firstname', $q.'%', false]]);
            
            $users = $query->limit(25)->all();
            
            if($users)
            {
                foreach($users as $user)
                {
                    $results[] = [
                        'id' => $user->getPrimaryKey(),
                        'text' => $user->getTitle(),
                    ];
                }
            }
        }
        
        return [
            'results' => $results
        ];
    }
    
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchUser();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = User::SCENARIO_BACKEND_INSERT;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', "User correctly created");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = User::SCENARIO_BACKEND_UPDATE;

        if(Yii::$app->getRequest()->getIsPost())
        {
            if(Yii::$app->request->post('update-password')) {
                $model->scenario = User::SCENARIO_UPDATE_PASSWORD;
            }
            $model->load(Yii::$app->request->post());
            if($model->save()) {
                if($model->scenario == User::SCENARIO_UPDATE_PASSWORD)
                    Yii::$app->getSession()->setFlash('success', "Password correctly updated");
                else
                    Yii::$app->getSession()->setFlash('success', "User correctly updated");
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->delete()) {
            Yii::$app->getSession()->setFlash('success', "User correctly deleted");
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
