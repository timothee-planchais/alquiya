<?php

namespace backend\controllers;

use Yii;
use common\models\Item;
use backend\models\SearchItem;
use common\models\Image;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends \backend\components\Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        $behaviors['access']['rules'][0]['actions'][] = 'subcategories';
        $behaviors['access']['rules'][0]['actions'][] = 'upload-image';
        $behaviors['access']['rules'][0]['actions'][] = 'delete-image';
        
        return $behaviors;
    }
    
    public function actionSubcategories()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = [
            'status' => 0,
            'categories' => null
        ];
        
        $id = Yii::$app->getRequest()->post('id');
        
        $category = $this->getCategory($id);
        
        if($category)
        {
            $subcategories = $category->subCategories;
            $response['categories'] = $subcategories ? \yii\helpers\ArrayHelper::map($subcategories, 'id', 'title') : null;
            $response['status'] = 1;
        }
        
        return $response;
    }
    
    public function actionUploadImage($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = [
            'status' => 0
        ];
        
        $model = $this->findModel($id);
        
        if($model)
        {
            //$position = (int)$model->getImages()->count() + 1;
            $image = new Image([
                'item_id' => $model->getPrimaryKey(),
                //'position' => $position
            ]);
            $image->image = \yii\web\UploadedFile::getInstanceByName('tmp_images');
            if($image->save()) {
                $response['status'] = 1;
            }
        }
        
        return $response;
    }
    
    public function actionDeleteImage()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = [
            'status' => 0
        ];
        
        $id = Yii::$app->getRequest()->post('key');
        
        $image = Image::findOne($id);
        
        if($image && $image->delete()) {
            $response['status'] = 1;
        }
        
        return $response;
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchItem();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Item([
            'status' => Item::STATUS_ACTIVE,
            'featured' => 0,
            'highlighted' => 0,
            'locked' => 0,
        ]);
        $model->scenario = Item::SCENARIO_BACKEND;

        if ($model->load(Yii::$app->request->post())) 
        {
            if($model->save()) {
                Yii::$app->getSession()->setFlash('success', "Item correctly created");
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Item::SCENARIO_BACKEND;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', "Item correctly updated");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->delete()) {
            Yii::$app->getSession()->setFlash('success', "Item correctly deleted");
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
