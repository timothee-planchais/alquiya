<?php

namespace backend\controllers;

use Yii;
use common\models\StatSearch;
use backend\models\SearchStatsSearch;
use backend\components\Controller;
use yii\web\NotFoundHttpException;

/**
 * StatsSearchController implements the CRUD actions for StatSearch model.
 */
class StatSearchController extends Controller
{

    /**
     * Lists all StatSearch models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchStatsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the StatSearch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return StatSearch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StatSearch::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
