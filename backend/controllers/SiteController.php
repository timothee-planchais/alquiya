<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use common\models\User;
use common\models\Item;
use common\models\Booking;
use common\models\Order;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $cache = Yii::$app->cache;
        $duration = 3600;
        
        if($cache->exists('num_users')) {
            $numUsers = $cache->get('num_users');
        }
        else {
            $numUsers = User::find()->where(['status' => [User::STATUS_ACTIVE, User::STATUS_PENDING]])->count();
            $cache->set('num_users', $numUsers, $duration);
        }
        
        if($cache->exists('num_items')) {
            $numItems = $cache->get('num_items');
        }
        else {
            $numItems = Item::find()->where(['locked' => 0/*, 'status' => [Item::STATUS_ACTIVE]*/])->count();
            $cache->set('num_items', $numItems, $duration);
        }
        
        if($cache->exists('num_bookings')) {
            $numBookings = $cache->get('num_bookings');
        }
        else {
            $numBookings = Booking::find()->where(['status' => [Booking::STATUS_VALIDATED, Booking::STATUS_COMPLETED]])->count();
            $cache->set('num_bookings', $numBookings, $duration);
        }
        
        if($cache->exists('total_revenues')) {
            $totalRevenues = $cache->get('total_revenues');
        }
        else {
            $totalRevenues = Order::find()->where(['status' => Order::STATUS_PAID])->sum('amount');
            $cache->set('total_revenues', $totalRevenues, $duration);
        }
        
        $stats = [
            ['icon' => 'users', 'color' => 'red', 'label' => Yii::t('app', "Members"), 'value' => Yii::$app->formatter->asInteger($numUsers)],
            ['icon' => 'cube', 'color' => 'aqua', 'label' => Yii::t('app', "Items"), 'value' => Yii::$app->formatter->asInteger($numItems)],
            ['icon' => 'calendar-check-o', 'color' => 'yellow', 'label' => Yii::t('app', "Bookings"), 'value' => Yii::$app->formatter->asInteger($numBookings)],
            ['icon' => 'usd', 'color' => 'green', 'label' => Yii::t('app', "Revenues"), 'value' => Yii::$app->formatter->asCurrency($totalRevenues)],
        ];
        
        $lastUsers = User::find()->orderBy('created_at DESC')->limit(8)->all();
        
        $lastItems = Item::find()->orderBy('created_at DESC')->limit(5)->all();
        
        $lastOrders = Order::find()->orderBy('created_at DESC')->limit(8)->all();
        
        return $this->render('index', [
            'stats' => $stats,
            'lastItems' => $lastItems,
            'lastUsers' => $lastUsers,
            'lastOrders' => $lastOrders
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'guest';
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
