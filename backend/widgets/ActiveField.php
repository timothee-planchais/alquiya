<?php

namespace backend\widgets;

use yii\helpers\Html;

/**
 * Description of ActiveField
 * 
 * Nous sert pour :
 * - Ajouter une info bulle
 *
 * @author Timothee
 */
class ActiveField extends \yii\bootstrap\ActiveField
{
    public $infoOptions = ['class' => 'form-info bg-color-portal', 'data-toggle' => 'tooltip', 'data-placement' => 'left', 'title' => ''];
    
    public function render($content = null)
    {
        if ($content === null)
        {
            if (!isset($this->parts['{info}'])) {
                $this->info();
            }
        }
        
        $this->template .= "\n{info}";
        //$this->template = "{label}\n{beginWrapper}\n{input}\n{hint}\n{info}\n{error}\n{endWrapper}";
        
        return parent::render($content);
    }
    
    public function info($info = null, $options = [])
    {
        if ($info === false) {
            $this->parts['{info}'] = '';
            return $this;
        }
        
        $options = array_merge($this->infoOptions, $options);
        
        $html = '';
        if(method_exists($this->model, 'getAttributeInfo'))
        {
            $info = $this->model->getAttributeInfo($this->attribute) ? : null;

            if($info) 
            {
                $this->options['class'] .= ' has-info';
                $options['title'] = $info;
                $html = Html::tag('span', '?', $options);
            }
        }
        
        $this->parts['{info}'] = $html;
        
        return $this;
    }
    
}
