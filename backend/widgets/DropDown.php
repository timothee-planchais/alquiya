<?php

namespace backend\widgets;

use yii\helpers\Html;

/**
 * Description of DropDown
 *
 * @author Timothee
 */
class DropDown extends \yii\bootstrap\Dropdown
{
    public function init()
    {
        \yii\bootstrap\Widget::init();
        Html::addCssClass($this->options, 'treeview-menu');
    }
}
