<?php

namespace backend\widgets;

/**
 * Description of ActiveForm
 *
 * @author Timothee
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
    
    public $fieldClass = 'backend\widgets\ActiveField';
    
}
