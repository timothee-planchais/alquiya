<?php

namespace backend\widgets\grid;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Description of GridView
 *
 * @author Timothee
 */
class GridView extends \yii\grid\GridView
{
    public function init()
    {
        parent::init();
        
        $this->view->registerJs('$(function(){
            $("#tr1 .div1").width($("#tr2 > .table").width());
            $("#tr2").scroll(function(){
              $("#tr1").scrollLeft($("#tr2").scrollLeft());
            });
            $("#tr1").scroll(function(){
              $("#tr2").scrollLeft($("#tr1").scrollLeft());
            });
          });', \yii\web\View::POS_READY);
    }
    
    public function renderItems()
    {
        return '<div id="tr1"><div class="div1"></div></div><div class="table-responsive" id="tr2">'.parent::renderItems().'</div>';
    }
    
}
