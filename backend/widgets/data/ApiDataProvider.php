<?php

namespace backend\widgets\data;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\data\Sort;

/**
 * Description of ApiDataProvider
 *
 * @author Timothee
 */
class ApiDataProvider extends \yii\data\ActiveDataProvider
{
    /** @var Model */
    public $model = null;
    
    public $modelClass = null;
    
    public $apiParams = [];
    
    public $apiOrder = null;
    
    public function init()
    {
        \yii\data\BaseDataProvider::init();
        
        if($this->modelClass) {
            $this->model = new $this->modelClass;
        }
    }
    
    /**
     * @inheritdoc
     */
    protected function prepareModels()
    {
        if (!$this->model instanceof \common\components\api\ActiveRecord) {
            throw new InvalidConfigException('"modelClass" doit être de type "\common\components\api\ActiveRecord"');
        }
        
        if (($pagination = $this->getPagination()) !== false) {
            $pagination->totalCount = $this->getTotalCount();
            if ($pagination->totalCount === 0) {
                return [];
            }
            $this->apiParams['limit'] = $pagination->getOffset().','.$pagination->getLimit();
        }
        
        if (($sort = $this->getSort()) !== false) {
            //var_dump($sort->getOrders());die;
            $orders = $sort->getOrders();
            if($orders)
            {
                foreach($orders as $column => $dir) {
                    $this->apiParams['sort'] = ($dir == SORT_DESC ? '-' : '').$column;
                    break;
                }
            }
        }
        
        $modelClass = $this->modelClass;
        return $modelClass::findAll($this->getApiParams());
    }
    
    public function getApiParams($onlyColumns = false)
    {
        $apiParams = $this->apiParams;
        
        if(!$onlyColumns)
        {
            /*if (($pagination = $this->getPagination()) !== false) {
                $apiParams['limit'] = $pagination->getOffset().','.$pagination->getLimit();
            }
            else {
                $apiParams['limit'] = '0,25';
            }*/

            if (($sort = $this->getSort()) !== false) 
            {
                $apiOrder = '';
                $orders = $sort->getOrders();
                if($orders)
                {
                    foreach($orders as $attributeName => $dir) {
                        if($attributeName) {
                            $apiOrder = ($dir == SORT_DESC ? '-' : '').$attributeName;
                            break;
                        }
                    }
                }
                $apiParams['sort'] = $apiOrder;
            }
        }
        
        //var_dump($apiParams);die;
        
        return $apiParams;
    }
    
    /**
     * @inheritdoc
     */
    protected function prepareTotalCount()
    {
        if (!$this->model instanceof \common\components\api\ActiveRecord) {
            throw new InvalidConfigException('"modelClass" doit être de type "\common\components\api\ActiveRecord"');
        }
        
        $apiParams = $this->getApiParams(true);
        
        $modelClass = $this->modelClass;
        return (int) $modelClass::count($apiParams);
    }
    
    /**
     * @inheritdoc
     */
    protected function prepareKeys($models)
    {
        $keys = [];
        if ($this->key !== null) {
            foreach ($models as $model) {
                if (is_string($this->key)) {
                    $keys[] = $model[$this->key];
                } else {
                    $keys[] = call_user_func($this->key, $model);
                }
            }

            return $keys;
        } elseif ($this->model instanceof \common\components\api\ActiveRecord) {
            $modelClass = $this->modelClass;
            $pks = $modelClass::primaryKey();
            if (count($pks) === 1) {
                $pk = $pks[0];
                foreach ($models as $model) {
                    $keys[] = $model[$pk];
                }
            } else {
                foreach ($models as $model) {
                    $kk = [];
                    foreach ($pks as $pk) {
                        $kk[$pk] = $model[$pk];
                    }
                    $keys[] = $kk;
                }
            }

            return $keys;
        } else {
            return array_keys($models);
        }
    }
}
