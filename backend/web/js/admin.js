
$(document).ready(function(){
    
    $('body').layout({
        resetHeight : false
    });
    
    /*
     * LINKS
     */
    $('.goto, .scrollto').on('click', function(){
        goTo($(this).attr('href'));
        
        return false;
    });
    
});

function generateSlug($toElement, $value)
{
    $slug = $value.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, '');
    
    $($toElement).val($slug);
}

function goTo($elem)
{
    $('html, body').animate({
        scrollTop: $($elem).offset().top - $('#header').height()
    }, 500);
}

/**
 * 
 * @param {type} $selector
 * @param string $mode Modes : 'basic', 'full'
 * @returns {undefined}
 */
function initTinyMce($selector, $mode, $aditionnalOptions)
{
    if(!$mode) {
        $mode = 'full';
    }
    
    if($mode == 'full')
    {
        $options = {
            language: 'fr_FR',
            selector: $selector,
            content_css: '/css/site.css',
            height: 400,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link uploadimage charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars codemirror fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | fontselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link uploadimage',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            codemirror: {
                
            },
            fontsize_formats: "12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px"
        };
    }
    else
    {
        $options = {
            language: 'fr_FR',
            selector: $selector,
            content_css: '/css/site.css',
            height: 200,
            theme: 'modern',
            plugins: [
                'advlist autolink lists link charmap preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars codemirror fullscreen',
                'insertdatetime nonbreaking save table contextmenu directionality',
                'paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | insert | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
            toolbar2: 'preview | emoticons',
            //http://mardell.me/blog/how-to-add-custom-style-formats-to-tinymce-4-without-overriding-defaults/
            style_formats: [
                {title: 'Headers', items: [
                 {title: 'Header 1', format: 'h1'},
                 {title: 'Header 2', format: 'h2'},
                 {title: 'Header 3', format: 'h3'},
                 {title: 'Header 4', format: 'h4'},
                 {title: 'Header 5', format: 'h5'},
                 {title: 'Header 6', format: 'h6'}
                ]},
                {title: 'Blocks', items: [
                 {title: 'Paragraph', format: 'p'},
                 {title: 'Blockquote', format: 'blockquote'},
                 {title: 'Div', format: 'div'},
                 {title: 'Pre', format: 'pre'}
                ]},
            ],
            codemirror: {
                
            },
            fontsize_formats: "12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px"
        };
    }
    
    if($aditionnalOptions) {
        $options = $.extend($options,$aditionnalOptions);
    }
    
    tinymce.init($options);
    
}

function showMask()
{
    $('#mask').show();
}

function hideMask()
{
    $('#mask').hide();
}