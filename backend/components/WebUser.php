<?php

namespace backend\components;

use Yii;
use backend\models\Admin;

/**
 * Description of WebUser
 *
 * @author Timothee
 */
class WebUser extends \yii\web\User
{
    
    public function can($permissionName, $params = array(), $allowCaching = true)
    {
        if($this->isGuest) {
            return false;
        }
        
        if($this->isSuperAdmin()) {
            return true;
        }
        
        $controllerId = null;
        $actionId = null;
        if(strpos($permissionName, '/') !== false) 
        {
            $explode = explode('/', ltrim($permissionName, '/'));
            $controllerId = $explode[0];
            $actionId = $explode[1];
            if(strpos($actionId, '?')) {
                $explode = explode('?', $actionId);
                $actionId = $explode[0];
            }
        }
        
        $role = $this->getRole();
        
        if($role == Admin::ROLE_LEVEL_1)
        {
            if($controllerId == 'item') {
                return true;
            }
        }
        
        return false;
    }
    
    public function isSuperAdmin()
    {
        return !$this->isGuest && $this->getIdentity()->isSuperAdmin();
    }
    
    public function getRole()
    {
        return $this->getIdentity()->role; 
    }
    
    /**
     * 
     * @param type $autoRenew
     * @return \backend\models\Admin
     */
    public function getIdentity($autoRenew = true)
    {
        return parent::getIdentity($autoRenew);
    }
}
