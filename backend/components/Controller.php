<?php

namespace backend\components;

use Yii;
use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Description of Controller
 *
 * @author Timothee
 */
class Controller extends \yii\web\Controller
{
    /** @var Category[] */
    public $allCategories;
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => ['login', 'signup', 'request-password-reset', 'reset-password', 'error'],
                'rules' => [
                    [
                        'actions' => ['error', 'captcha'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['login', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['login', 'request-password-reset', 'reset-password'],
                        'allow' => false,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function init()
    {
        /*if(Yii::$app->request->get('lg') && array_key_exists(Yii::$app->request->get('lg'), Yii::$app->params['languages'])) {
            Yii::$app->session->set('lg', Yii::$app->request->get('lg'));
        }
        if(Yii::$app->session->has('lg')) {
            Yii::$app->language = Yii::$app->session->get('lg');
        }*/
        
        /*
         * CURRENCY
         */
        $this->view->registerJs('var CURRENCY_CODE = "'.Yii::$app->formatter->currencyCode.'";', \yii\web\View::POS_HEAD);
        
        /*
         * COUNTRIES
         */
        $this->view->registerJs('var COUNTRIES = '.\yii\helpers\Json::encode(array_keys(Yii::$app->params['countries2'])).';', \yii\web\View::POS_HEAD);
        $this->view->registerJs('var COUNTRY = "CO";', \yii\web\View::POS_HEAD);
        
        /*
         * FEES
         */
        $this->view->registerJs('var FEES = '.Yii::$app->params['item.fees'].';', \yii\web\View::POS_HEAD);
        
        return parent::init();
    }
    
    public function beforeAction($action)
    {
        /*
         * ------------
         *  CATEGORIES
         */
        
        $key = 'categories';
        
        if(Yii::$app->cache->exists($key))
        {
            $allCategories = Yii::$app->cache->get($key);
        }
        else
        {
            $allCategories = \yii\helpers\ArrayHelper::index(Category::find()->where(['active' => 1])->orderBy('position ASC')/*->andWhere('parent_id IS NULL')*/->all(), 'id');

            if($allCategories) 
            {
                
                foreach($allCategories as $category)
                {
                    if($category->parent_id) 
                    {
                        $parent = ArrayHelper::getValue($allCategories, $category->parent_id);
                        if($parent) {
                            $category->populateRelation('parent', $parent);
                        }
                    }
                }
            }
            
            Yii::$app->cache->set($key, $allCategories, 6*3600);
        }
        
        $dropDownCategories = [];
        
        if($allCategories)
        {
            foreach($allCategories as $category) 
            {
                if(!$category->parent_id) {
                    $dropDownCategories[$category->title] = ArrayHelper::map($category->subCategories, 'id', 'title');
                }
            }
        }
        
        $this->allCategories = $this->view->allCategories = $allCategories;
        $this->view->dropDownCategories = $dropDownCategories;
        
        return parent::beforeAction($action);
    }
    
    /**
     * 
     * @param integer $id
     * @return Category
     */
    public function getCategory($id)
    {
        return ArrayHelper::getValue($this->allCategories, $id);
    }
    
    public function afterAction($action, $result)
    {
        /*
         * Return URL
         */
        $actions = ['login', 'logout'];
        if(!in_array($action->id, $actions) /*$action->controller->id !== 'site'*/) {
            Yii::$app->user->setReturnUrl(Yii::$app->getRequest()->getUrl());
        } 
        
        return parent::afterAction($action, $result);
    }
    
    protected function checkPermission($permissionName = null, $params = [])
    {
        //Permission par défaut : actionController
        if($permissionName === null) {
            $permissionName = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
        }
        
        if(!Yii::$app->user->can($permissionName, $params)) {
            $this->denyAccess();
        }
    }
    
    protected function denyAccess($msg = null)
    {
        throw new \yii\web\ForbiddenHttpException($msg !== null ? $msg : Yii::t('yii', 'You are not allowed to perform this action.'));
    }
    
}
