<?php

namespace backend\components;

/**
 * Description of View
 *
 * @author Timothee
 */
class View extends \yii\web\View
{
    public $bodyClass = 'skin-blue';
    
    public $hideTitle = false;
    
    public $useBox = true;
    
    public $dropDownCategories = [];
    
    /** @var Category[] */
    public $allCategories = [];
    
}
