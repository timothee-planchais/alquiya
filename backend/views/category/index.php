<?php

use common\models\Category;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <p>
        <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Add Category'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', '<i class="fa fa-list"></i> Nested list'), ['nested'], ['class' => 'btn btn-default']) ?>
    </p>

    <?= backend\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'parent_id',
                'value' => function($model){
                    return $model->parent_id ? $model->parent->title : null;
                } 
            ],
            'title',
            'slug',
            //'description:ntext',
            'group',
            'position',
            'active:boolean',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
