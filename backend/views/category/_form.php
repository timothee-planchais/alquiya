<?php

use common\models\Category;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */

$selectedParent = null;
if($model->parent_id && $model->parent) {
    $selectedParent = $model->parent->title;
}

?>

<div class="category-form">

    <?php $form = ActiveForm::begin(Yii::$app->params['bootstrapFormConfig']); ?>

    <?= $form->field($model, 'parent_id')->widget(Select2::class, [
        'initValueText' => $selectedParent,
        'options' => [
            'placeholder' => Yii::t('app', 'Look for a category').'...',
            'multiple' => false
        ],
        'pluginOptions' => [
            'multiple' => false,
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => Url::to(['/category/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.text; }'),
            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'group')->dropDownList(Category::getGroupes(), ['prompt' => '-----']) ?>
    
    <?= $form->field($model, 'position')->input('number') ?>
    
    <?= $form->field($model, 'active')->dropDownList([1 => Yii::t('app', "Yes"), 0 => Yii::t('app', "No")]) ?>
    
    <?= $form->field($model, 'menu_active')->dropDownList([1 => Yii::t('app', "Yes"), 0 => Yii::t('app', "No")]) ?>

    <div class="form-group text-center">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
