<?php

use common\models\Category;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $categories Category[] */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;

$this->registerAssetBundle(\yii\jui\JuiAsset::className());
$this->registerJsFile('@web/js/plugins/jquery.nestedSortable.js', ['depends' => \yii\jui\JuiAsset::className()]);

/**
 * @param Category[] $categories
 * @return string
 */
function showRecursiveCategories($categories)
{
    $html = '';
    
    if($categories)
    {
        $html .= '<ol>';
        foreach($categories as $category) {
            $html .= '<li id="category_'.$category->getPrimaryKey().'" data-id="'.$category->getPrimaryKey().'">'
                    .'<div>'
                        .'<span class="item-btns">'
                            .'<a class="btn btn-xs btn-default" href="'.Url::to(['update','id'=>$category->id]).'" title="'.Yii::t('app',"Modifier").'"><span class="fa fa-edit"></span></a>'
                            .'<a class="btn btn-xs btn-danger" href="'.Url::to(['delete','id'=>$category->id]).'" title="'.Yii::t('app',"Supprimer").'" data-confirm="'.Yii::t('app', "Êtes-vous sûr de vouloir supprimer cet élément ?").'" data-method="post"><span class="fa fa-times"></span></a>'
                            .'<a class="btn btn-xs btn-success" href="'.Url::to(['create', 'parent_id' => $category->id]).'" title="'.Yii::t('app',"Ajouter un enfant").'"><span class="fa fa-plus"></span></a>'
                        .'</span>'
                        .'<span class="item-title">'
                            .($category->active ? '<i class="fa fa-check" title="Active"></i>' : '<i class="fa fa-times" title="Inactive"></i>')
                            .' '.($category->menu_active ? '<i class="fa fa-check" title="Visible menu"></i>': '<i class="fa fa-times" title="No visible menu"></i>')
                            .' &nbsp;'.$category->title
                        .'</span>'
                    .'</div>';
            if(!empty($category->subCategories))
                $html .= showRecursiveCategories($category->subCategories);
            $html .= '</li>';
        }
        $html .= '</ol>';
    }
    
    return $html;
}

?>
<div class="category-index">

    <p>
        <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Add Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p class="alert alert-success" id="positions-success" style="display:none;">Ordre correctement modifié</p>
    
    <p class="alert alert-error" id="positions-error" style="display:none;">Erreur lors de la modification de l'ordre</p>
    
    <?php if($categories): ?>
    
        <div id="categories" class="sortable-items">
            
            <?= showRecursiveCategories($categories) ?>
            
        </div>
    
    <?php endif; ?>
    
    
</div>
<?php $this->beginBlock('js'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('#categories ol').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 2,/*<?= Category::MAX_LEVEL ?>,*/
            forcePlaceholderSize: true,
            placeholder: 'placeholder'
        });
        
        $("#categories ol").on("sortstop", function( event, ui ) {
            $itemsArray = $('#categories ol').nestedSortable('toArray', {startDepthCount: 0});
            editPositions($itemsArray);
        });
        
    });
    
    function editPositions($categories)
    {
        $.ajax({
            method: "POST",
            url: "<?= Url::to(['positions']) ?>",
            data: {
                categories: $categories
            },
            dataType: "JSON"
          })
        .done(function( $data ) {
            if($data.status == 1) {
                $('#positions-success').show();
                setTimeout(function(){$('#positions-success').fadeOut(2000)}, 3000);
            }
            else {
                $('#positions-error').show();
                setTimeout(function(){$('#positions-error').fadeOut(2000)}, 3000);
            }
        });
    }
    
</script>
<?php $this->endBlock(); ?>