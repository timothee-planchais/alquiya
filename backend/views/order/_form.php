<?php

use common\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */

$selectedParent = null;
if($model->parent_id && $model->parent) {
    $selectedParent = $model->parent->__toString();
}

$selectedUser = null;
if($model->user_id && $model->user) {
    $selectedUser = $model->user->getTitle();
}

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->widget(Select2::class, [
        'initValueText' => $selectedParent,
        'options' => [
            'placeholder' => Yii::t('app', 'Look for a parent order').'...',
            'multiple' => false
        ],
        'pluginOptions' => [
            'multiple' => false,
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => Url::to(['/order/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.text; }'),
            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::class, [
        'initValueText' => $selectedUser,
        'options' => [
            'placeholder' => Yii::t('app', 'Look for a user').'...',
            'multiple' => false
        ],
        'pluginOptions' => [
            'multiple' => false,
            'allowClear' => true,
            'minimumInputLength' => 3,
            /*'language' => [
                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
            ],*/
            'ajax' => [
                'url' => Url::to(['/user/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.text; }'),
            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'type')->dropDownList(Order::getTypes()) ?>

    <?= $form->field($model, 'method')->dropDownList(Order::getMethods()) ?>

    <?= $form->field($model, 'status')->dropDownList(Order::getStatuses()) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->widget(kartik\datetime\DateTimePicker::class, [
        'options' => [
            'placeholder' => Yii::t('app', "Date"),
            'readonly' => true,
        ],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii',
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'amount')->input('number', ['step' => '0.01']) ?>
    
    <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'vta_rate')->input('number', ['step' => '0.01']) ?>

    <div class="text-center">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
