<?php

use common\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-info-circle"></i> '.Yii::t('app', "Payment").' '.$model->payment->__toString(), ['/payment/view', 'id' => $model->payment->id], ['class' => 'btn btn-info']) ?>
    </p>
    
    <?php if($model->status == Order::STATUS_WAITING_PAYMENT): ?>
        <?= Html::a('<i class="fa fa-check"></i> Marquer comme payé', ['status-paid', 'id' => $model->id], ['class' => 'btn btn-success btn-lg', 'data' => [ 'confirm' => Yii::t('app', 'Are you sure ?'),'method' => 'post']]) ?>
    <?php endif; ?>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'parent_id',
                'value' => $model->parent_id ? Html::a(Html::encode($model->parent->__toString()), ['view', 'id' => $model->parent_id]) : null,
                'format' => 'raw'
            ],
            [
                'attribute' => 'user_id',
                'value' => Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'type',
                'value' => $model->getTextType()
            ],
            [
                'attribute' => 'status',
                'value' => $model->getTextStatus()
            ],
            [
                'attribute' => 'method',
                'value' => $model->getTextMethod()
            ],
            'code',
            [
                'attribute' => 'amount',
                'value' => Yii::$app->formatter->asCurrency($model->amount, $model->currency)
            ],
            'currency',
            'vta_rate',
            'date:datetime',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
