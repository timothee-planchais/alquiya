<?php

use common\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchOrder" aria-expanded="false" aria-controls="searchOrder">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Advanced search") ?>
    </button>
    
    <div class="collapse" id="searchOrder">
        
        <hr/>
    
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

            <div class="row">
        
                <div class="col-xs-12 col-sm-6">
                
                    <?= $form->field($model, 'id') ?>

                    <?php // $form->field($model, 'parent_id') ?>

                    <?= $form->field($model, 'user_id')->widget(Select2::class, [
                        'initValueText' => $selectedUser,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Look for a user').'...',
                            'multiple' => false,
                            'id' => 'searchorder-user_id2',
                        ],
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            /*'language' => [
                                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                            ],*/
                            'ajax' => [
                                'url' => Url::to(['/user/search']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(user) { return user.text; }'),
                            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                        ],
                    ]) ?>

                </div>
                    
                <div class="col-xs-12 col-sm-6">

                    <?= $form->field($model, 'type')->dropDownList(Order::getTypes(), ['prompt' => '']) ?>

                    <?php // echo $form->field($model, 'code') ?>

                    <?= $form->field($model, 'method')->dropDownList(Order::getMethods(), ['prompt' => '']) ?>

                    <?php // echo $form->field($model, 'amount') ?>

                    <?php // echo $form->field($model, 'currency') ?>

                    <?php // echo $form->field($model, 'vta_rate') ?>

                    <?= $form->field($model, 'status')->dropDownList(Order::getStatuses(), ['prompt' => '']) ?>

                    <?php // echo $form->field($model, 'date') ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                    </div>
                    
                </div>
            
            </div>

        <?php ActiveForm::end(); ?>
        
    </div>

</div>
