<?php

use common\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchOrder */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;

$selectedUser = null;
if($searchModel->user_id && $searchModel->user) {
    $selectedUser = $searchModel->user->getTitle();
}

?>
<div class="order-index">
    
    <?= $this->render('_search', ['model' => $searchModel, 'selectedUser' => $selectedUser]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add order'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            $class = 'warning';
            
            if($model->status == Order::STATUS_PAID) {
                $class = 'success';
            }
            elseif($model->status == Order::STATUS_PAYMENT_CANCELLED || $model->status == Order::STATUS_PAYMENT_ERROR) {
                $class = 'danger';
            }
            return ['class' => $class];
        },
        'columns' => [
            'id',
            //'parent_id',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return Html::a($model->user->__toString(), ['/user/view', 'id' => $model->user_id]);
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->getTextType();
                },
                'filter' => Order::getTypes()
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getTextStatus();
                },
                'filter' => Order::getStatuses()
            ],
            [
                'attribute' => 'method',
                'value' => function($model) {
                    return $model->getTextMethod();
                },
                'filter' => Order::getMethods()
            ],
            'code',
            [
                'attribute' => 'amount',
                'value' => function($model) {
                    return Yii::$app->formatter->asCurrency($model->amount, $model->currency);
                }
            ],
            //'currency',
            //'vta_rate',
            'date:datetime',
            'created_at:datetime',
            //'updated_at:datetime',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
