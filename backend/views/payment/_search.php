<?php

use common\models\Payment;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchPayment" aria-expanded="false" aria-controls="searchPayment">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Advanced search") ?>
    </button>
    
    <div class="collapse" id="searchPayment">
        
        <hr/>
    
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

            <div class="row">
        
                <div class="col-xs-12 col-sm-6">
                
                    <?= $form->field($model, 'id') ?>

                    <?= $form->field($model, 'user_id')->widget(Select2::class, [
                        'initValueText' => $selectedUser,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Look for a user').'...',
                            'multiple' => false,
                            'id' => 'searchpayment-user_id2',
                        ],
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            /*'language' => [
                                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                            ],*/
                            'ajax' => [
                                'url' => Url::to(['/user/search']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(user) { return user.text; }'),
                            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                        ],
                    ]) ?>

                    <?= $form->field($model, 'order_id')->widget(Select2::class, [
                        'initValueText' => $selectedOrder,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Look for an order').'...',
                            'multiple' => false,
                            'id' => 'searchpayment-order_id2',
                        ],
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            /*'language' => [
                                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                            ],*/
                            'ajax' => [
                                'url' => Url::to(['/user/search']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(user) { return user.text; }'),
                            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                        ],
                    ]) ?>

                    <?= $form->field($model, 'reference_code') ?>

                    <?= $form->field($model, 'transaction_id') ?>

                    <?= $form->field($model, 'state_pol')->dropDownList(Payment::getStatesPol(), ['prompt' => '']) ?>
                    
                </div>
                    
                <div class="col-xs-12 col-sm-6">

                    <?php // echo $form->field($model, 'amount') ?>

                    <?php // echo $form->field($model, 'currency') ?>

                    <?= $form->field($model, 'response_code_pol') ?>

                    <?= $form->field($model, 'reference_pol') ?>

                    <?= $form->field($model, 'payment_method_id') ?>

                    <?= $form->field($model, 'payment_method_type') ?>

                    <?php // echo $form->field($model, 'transaction_date') ?>

                    <?= $form->field($model, 'response_message_pol') ?>

                    <?php // echo $form->field($model, 'created_at') ?>

                    <?php // echo $form->field($model, 'updated_at') ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                    </div>
                    
                </div>
                
            </div>

        <?php ActiveForm::end(); ?>

    </div>
        
</div>
