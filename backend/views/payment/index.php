<?php

use common\models\Payment;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchPayment */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Payments');
$this->params['breadcrumbs'][] = $this->title;

$selectedUser = null;
if($searchModel->user_id && $searchModel->user) {
    $selectedUser = $searchModel->user->getTitle();
}

$selectedOrder = null;
if($searchModel->order_id && $searchModel->order) {
    $selectedOrder = $searchModel->order->__toString();
}

?>
<div class="payment-index">
    
    <?= $this->render('_search', ['model' => $searchModel, 'selectedUser' => $selectedUser, 'selectedOrder' => $selectedOrder]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Payment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            $class = 'danger';
            
            if(empty($model->state_pol) || $model->state_pol == Payment::STATE_POL_PENDING) {
                $class = 'warning';
            }
            elseif($model->isSuccess()) {
                $class = 'success';
            }
            
            return ['class' => $class];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return Html::a($model->user->__toString(), ['/user/view', 'id' => $model->user_id]);
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'order_id',
                'value' => function($model) {
                    return Html::a($model->order->__toString(), ['/order/view', 'id' => $model->order_id]);
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'order_id',
                    'initValueText' => $selectedOrder,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for an order').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/order/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'state_pol',
                'value' => function($model) {
                    return $model->getTextStatePol();
                },
                'filter' => Payment::getStatesPol()
            ],
            'reference_code',
            'transaction_id',
            'amount',
            //'currency',
            //'response_code_pol',
            //'reference_pol',
            //'payment_method_id',
            [
                'attribute' => 'payment_method_type',
                'value' => function($model) {
                    return $model->getTextMethodType();
                },
                'filter' => Payment::getMethodTypes()
            ],
            'transaction_date',
            //'response_message_pol',
            'created_at:datetime',
            //'updated_at',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
