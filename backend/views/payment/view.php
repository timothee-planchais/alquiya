<?php

use common\models\Payment;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Payment */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="payment-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-info-circle"></i> '.Yii::t('app', "Order").' '.$model->order->__toString(), ['/order/view', 'id' => $model->order_id], ['class' => 'btn btn-info']) ?>
    </p>
    
    <?php if(empty($model->state_pol) || $model->state_pol == Payment::STATE_POL_PENDING): ?>
        <p></p>
    <?php endif; ?>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'order_id',
                'value' => Html::a(Html::encode($model->order->__toString()), ['/order/view', 'id' => $model->order_id]),
                'format' => 'raw'
            ],
            'reference_code',
            'transaction_id',
            [
                'attribute' => 'state_pol',
                'value' => $model->getTextStatePol()
            ],
            'amount',
            'currency',
            'response_code_pol',
            'reference_pol',
            'payment_method_id',
            [
                'attribute' => 'payment_method_type',
                'value' => $model->getTextMethodType()
            ],
            'transaction_date',
            'response_message_pol',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
