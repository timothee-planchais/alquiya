<?php

use common\models\ReportItem;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchReportItem */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Report Items');
$this->params['breadcrumbs'][] = $this->title;

$selectedUser = null;
if($searchModel->user_id && $searchModel->user) {
    $selectedUser = $searchModel->user->getTitle();
}

$selectedItem = null;
if($searchModel->item_id && $searchModel->item) {
    $selectedItem = $searchModel->item->title;
}

?>
<div class="report-item-index">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            $class = '';
            if(!$model->is_read) {
                $class = 'info';
            }
            return ['class' => $class];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'item_id',
                'value' => function($model) {
                    return $model->item_id && $model->item ? Html::a($model->item->title, ['/item/view', 'id' => $model->item_id]) : null;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'item_id',
                    'initValueText' => $selectedItem,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for an item').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/item/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user_id && $model->user ? Html::a($model->user->__toString(), ['/user/view', 'id' => $model->user_id]) : null;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            'email:email',
            [
                'attribute' => 'reason',
                'value' => function($model){
                    return $model->getTextReason();
                },
                'filter' => ReportItem::getReasons()
            ],
            //'message:ntext',
            //'created_at:datetime',

            [
                'class' => 'backend\widgets\grid\ActionColumn',
                'updateVisible' => false
            ],
        ],
    ]); ?>
</div>
