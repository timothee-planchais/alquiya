<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ReportItem */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Report Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="report-item-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('<i class="fa fa-eye"></i> '.Yii::t('app', "See on FO"), Yii::$app->params['frontend.url'].'/i/'.$model->id,  ['class' => 'btn btn-info', 'target' => '_blank']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'item_id',
                'value' => $model->item_id && $model->item ? Html::a(Html::encode($model->item->title), ['/item/view', 'id' => $model->item_id]) : null,
                'format' => 'raw'
            ],
            [
                'attribute' => 'user_id',
                'value' => $model->user_id && $model->user ? Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]) : null,
                'format' => 'raw'
            ],
            'email:email',
            [
                'attribute' => 'reason',
                'value' => $model->getTextReason()
            ],
            'message:ntext',
            'created_at',
        ],
    ]) ?>

</div>
