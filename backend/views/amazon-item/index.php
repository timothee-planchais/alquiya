<?php

use common\models\AmazonItem;
use common\models\Category;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchAmazonItem */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Amazon Items');
$this->params['breadcrumbs'][] = $this->title;

$categories = ArrayHelper::map(Category::find()->all(), 'id', 'title');

?>
<div class="amazon-item-index">
    
    <p class="alert alert-info">
        <i class="fa fa-info-circle"></i> 
        Amazon Partenaires : 
        <a href="https://partenaires.amazon.fr/" target="_blank">https://partenaires.amazon.fr/</a>
    </p>
    
    <?= $this->render('_search', ['model' => $searchModel, 'categories' => $categories]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Amazon Item'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_id',
            [
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->image, ['width' => 75]);
                },
                'format' => 'raw'
            ],
            'title',
            'url:url',
            //'description:ntext',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
