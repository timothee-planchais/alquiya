<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AmazonItem */

$this->title = Yii::t('app', 'Create Amazon Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Amazon Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="amazon-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
