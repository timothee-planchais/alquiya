<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchAmazonItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="amazon-item-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchAmazonItem" aria-expanded="false" aria-controls="searchAmazonItem">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Recherche avancée") ?>
    </button>
    
    <div class="collapse" id="searchAmazonItem">
        
    <hr/>
    
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">
    
            <div class="col-xs-12 col-sm-6">
    
                <?= $form->field($model, 'id') ?>

                <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => '-----']) ?>

            </div>
            <div class="col-xs-12 col-sm-6">    
            
                <?= $form->field($model, 'title') ?>

                <?= $form->field($model, 'description') ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>

            </div>
            
        </div>
                
        <?php ActiveForm::end(); ?>

    </div>
    
</div>
