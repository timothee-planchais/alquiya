<?php

use common\models\Subscription;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchSubscription */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Subscriptions');
$this->params['breadcrumbs'][] = $this->title;

$selectedUser = null;
if($searchModel->user_id && $searchModel->user) {
    $selectedUser = $searchModel->user->getTitle();
}

$selectedOrder = null;
if($searchModel->order_id && $searchModel->order) {
    $selectedOrder = $searchModel->order->getTitle();
}

?>
<div class="subscription-index">

    <?= $this->render('_search', ['model' => $searchModel, 'selectedUser' => $selectedUser, 'selectedOrder' => $selectedOrder]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Subscription'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            $class = '';
            if($model->status == Subscription::STATUS_ACTIVE || $model->status == Subscription::STATUS_UPCOMING) {
                $class = 'success';
            }
            elseif($model->status == Subscription::STATUS_PENDING) {
                $class = 'warning';
            }
            return ['class' => $class];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return Html::a($model->user->__toString(), ['/user/view', 'id' => $model->user_id]);
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'order_id',
                'value' => function($model) {
                    return Html::a($model->order->__toString(), ['/order/view', 'id' => $model->order_id]);
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'order_id',
                    'initValueText' => $selectedOrder,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for an order').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/order/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            [
                'attribute' => 'item_id',
                'value' => function($model) {
                    return $model->item ? Html::a(Html::encode($model->item->title), ['/item/view', 'id' => $model->item_id]) : null;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getTextStatus();
                },
                'filter' => Subscription::getStatuses()
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->getTextType();
                },
                'filter' => Subscription::getTypes()
            ],
            [
                'attribute' => 'period',
                'value' => function($model) {
                    return $model->getTextPeriod();
                },
                'filter' => Subscription::getPeriods()
            ],
            'start_date:date',
            'end_date:date',
            'code',
            'created_at:date',
            //'updated_at:datetime',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
