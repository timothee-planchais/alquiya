<?php

use common\models\Subscription;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Subscription */
/* @var $form yii\widgets\ActiveForm */

$selectedUser = null;
if($model->user_id && $model->user) {
    $selectedUser = $model->user->getTitle();
}

?>

<div class="subscription-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::class, [
        'initValueText' => $selectedUser,
        'options' => [
            'placeholder' => Yii::t('app', 'Look for a user').'...',
            'multiple' => false
        ],
        'pluginOptions' => [
            'multiple' => false,
            'allowClear' => true,
            'minimumInputLength' => 3,
            /*'language' => [
                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
            ],*/
            'ajax' => [
                'url' => Url::to(['/user/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.text; }'),
            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
        ],
    ]) ?>
    
    <?= $form->field($model, 'order_id') ?>
    
    <?= $form->field($model, 'item_id') ?>

    <?= $form->field($model, 'type')->dropDownList(Subscription::getTypes()) ?>

    <?= $form->field($model, 'period')->dropDownList(Subscription::getPeriods()) ?>

    <?= $form->field($model, 'status')->dropDownList(Subscription::getStatuses()) ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::class, [
        'options' => [
            'placeholder' => Yii::t('app', "Start date"),
            'readonly' => true,
        ],
        //'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii',
            //'startDate' => date('Y-m-d H:i:s'),
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'end_date')->widget(DateTimePicker::class, [
        'options' => [
            'placeholder' => Yii::t('app', "End date"),
            'readonly' => true,
        ],
        //'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd hh:ii',
            //'startDate' => date('Y-m-d H:i:s'),
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <div class="text-center">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
