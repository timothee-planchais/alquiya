<?php

use common\models\Subscription;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchSubscription */
/* @var $form yii\widgets\ActiveForm */
/* @var $selectedUser \common\models\User */

?>

<div class="subscription-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchSubscription" aria-expanded="false" aria-controls="searchSubscription">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Advanced search") ?>
    </button>
    
    <div class="collapse" id="searchSubscription">
        
        <hr/>
    
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">
        
            <div class="col-xs-12 col-sm-6">
            
                <?= $form->field($model, 'id') ?>

                <?= $form->field($model, 'user_id')->widget(Select2::class, [
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false,
                        'id' => 'searchsusbcription-user_id2',
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        /*'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],*/
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.text; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                    ],
                ]) ?>
                
                <?= $form->field($model, 'order_id')->widget(Select2::class, [
                    'initValueText' => $selectedOrder,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for an order').'...',
                        'multiple' => false,
                        'id' => 'searchsubscription-order_id2',
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        /*'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],*/
                        'ajax' => [
                            'url' => Url::to(['/order/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(user) { return user.text; }'),
                        'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                    ],
                ]) ?>

                <?= $form->field($model, 'type')->dropDownList(Subscription::getTypes(), ['prompt' => '']) ?>
                
            </div>
            
            <div class="col-xs-12 col-sm-6">
                
                <?= $form->field($model, 'status')->dropDownList(Subscription::getStatuses(), ['prompt' => '']) ?>

                <?= $form->field($model, 'period')->dropDownList(Subscription::getPeriods(), ['prompt' => '']) ?>

                <?= $form->field($model, 'code') ?>

                <?php // echo $form->field($model, 'start_date') ?>

                <?php // echo $form->field($model, 'end_date') ?>

                <?php // echo $form->field($model, 'created_at') ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>
                
            </div>
            
        </div>

        <?php ActiveForm::end(); ?>
        
    </div>

</div>
