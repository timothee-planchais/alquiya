<?php

use common\models\Subscription;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Subscription */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subscriptions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="subscription-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-info-circle"></i> '.Yii::t('app', "Order").' '.$model->order->__toString(), ['/order/view', 'id' => $model->order->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'order_id',
                'value' => Html::a(Html::encode($model->order->__toString()), ['/order/view', 'id' => $model->order_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'item_id',
                'value' => $model->item ? Html::a(Html::encode(Html::encode($model->item->title)), ['/item/view', 'id' => $model->item_id]) : null,
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => $model->getTextStatus(),
            ],
            [
                'attribute' => 'type',
                'value' => $model->getTextType(),
            ],
            [
                'attribute' => 'period',
                'value' => $model->getTextPeriod(),
            ],
            'code',
            'start_date:datetime',
            'end_date:datetime',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
