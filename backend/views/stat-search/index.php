<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchStatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Stat Searches');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="stat-search-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'term',
            'num_searches',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]); ?>
</div>
