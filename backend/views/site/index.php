<?php

use common\models\User;
use common\models\Item;
use common\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Utils;
use yii\helpers\ArrayHelper;

/* @var $this \backend\components\View */
/* @var $stats array */
/* @var $lastUsers User[] */
/* @var $lastItems Item[] */
/* @var $lastOrders Order[] */

$this->title = 'Dashboard';

$this->useBox = false;

?>

<div id="dashboard">

    <div class="row">
        <?php foreach($stats as $stat): ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-<?= $stat['color'] ?>"><i class="fa fa-<?= $stat['icon'] ?>"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><?= $stat['label'] ?></span>
                        <span class="info-box-number"><?= $stat['value'] ?></span>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="row">
        
        <!-- LAST USERS -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('app', "Last users") ?></h3>
                    <div class="box-tools pull-right">
                        <!--<span class="label label-danger">8 New users</span>-->
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <?php if($lastUsers): ?>
                        <ul class="users-list clearfix">
                            <?php foreach($lastUsers as $lastUser): ?>
                                <li>
                                    <img src="<?= Url::to($lastUser->getImageUrl('300x300')) ?>" alt="<?= Html::encode($lastUser) ?>">
                                    <a class="users-list-name" href="<?= Url::to(['/user/view', 'id' => $lastUser->id]) ?>"><?= Html::encode($lastUser) ?></a>
                                    <span class="users-list-date"><?= Yii::$app->formatter->asDatetime($lastUser->created_at, 'short') ?></span>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="box-footer text-center">
                    <a href="<?= Url::to(['/user/index']) ?>" class="uppercase"><?= Yii::t('app', "View all users") ?></a>
                </div>
            </div>
        </div>

        
        <!-- LAST ITEMS -->
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('app', "Recently added items") ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?php if($lastItems): ?>
                        <ul class="products-list product-list-in-box">
                            <?php foreach($lastItems as $lastItem): ?>
                                <li class="item">
                                    <div class="product-img">
                                        <img src="<?= Url::to($lastItem->getImageUrl('300x300')) ?>" alt="<?= Html::encode($lastItem->title) ?>">
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title"><?= Html::encode($lastItem->title) ?>
                                            <span class="label label-warning pull-right"><?= Yii::$app->formatter->asCurrency($lastItem->price) ?></span></a>
                                        <p class="product-description">
                                            <?= Html::encode(Utils::limitWords($lastItem->description, 80)) ?>
                                        </p>
                                        <p class="product-date"><?= Yii::$app->formatter->asDateTime($lastItem->created_at, 'short') ?></p>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <div class="box-footer text-center">
                    <a href="<?= Url::to(['/item/index']) ?>" class="uppercase"><?= Yii::t('app', 'View all items') ?></a>
                </div>
            </div>
        </div>
        
    </div>
    
    <!-- LAST ORDERS -->
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-8">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t('app', "Last orders") ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th><?= Yii::t('app', "Order") ?></th>
                                    <th><?= Yii::t('app', 'Type') ?></th>
                                    <th><?= Yii::t('app', "Status") ?></th>
                                    <th><?= Yii::t('app', "Amount") ?></th>
                                    <th><?= Yii::t('app', "Date") ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($lastOrders): ?>
                                    <?php foreach($lastOrders as $lastOrder): ?>
                                        <tr>
                                            <td><a href="<?= Url::to(['/order/view', 'id' => $lastOrder->id]) ?>"><?= $lastOrder->code ?></a></td>
                                            <td><?= $lastOrder->getTextType().' : '.$lastOrder->getTitle() ?></td>
                                            <td><?= Yii::$app->formatter->asCurrency($lastOrder->amount) ?></td>
                                            <td>
                                                <span class="label label-<?= $lastOrder->getColorStatus() ?>"><?= $lastOrder->getTextStatus() ?></span>
                                            </td>
                                            <td><?= Yii::$app->formatter->asDatetime($lastOrder->created_at) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <a href="<?= Url::to(['/order/create']) ?>" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> <?= Yii::t('app', 'New order') ?></a>
                    <a href="<?= Url::to(['/order/index']) ?>" class="btn btn-sm btn-default btn-flat pull-right"><?= Yii::t('app', "View all orders") ?></a>
                </div>
            </div>
        </div>
    </div>
    

</div>