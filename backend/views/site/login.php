<?php
/* @var $this \backend\components\View */
/* @var $content string */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model backend\models\LoginForm */

use backend\assets\AppAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->bodyClass .= ' login-page';

$this->title = 'Connexion';

?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><img src="<?= Url::to('@web/img/logo.png') ?>?v=cna" class="img-responsive" alt="AlquiYA"/></a>
    </div>

    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?php $template1 = "{input}<span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>\n{hint}\n{error}"; ?>
            <?= $form->field($model, 'email', ['template' => $template1, 'options' => ['class' => 'form-group has-feedback']])->input('email', ['placeholder' => $model->getAttributeLabel('email'), 'autofocus' => true]) ?>

            <?php $template2 = "{input}<span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>\n{hint}\n{error}" ?>
            <?= $form->field($model, 'password', ['template' => $template2, 'options' => ['class' => 'form-group has-feedback']])->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

            <div class="row">
                <div class="col-xs-5">
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                </div>
                <div class="col-xs-7">
                    <div class="form-group">
                        <?= Html::submitButton('Se connecter', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                    </div>
                </div>
            </div>

        <?php ActiveForm::end(); ?>


        <!--<a href="<?= Url::to(['/site/request-password-reset']) ?>">I forgot my password</a>-->

    </div>

</div>

