<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

if(Yii::$app->user->getIsGuest()) {
    $this->context->layout = 'guest';
}

?>
<div id="error">

    <div id="error-code">
        <div class="container">
            <h1><?= Html::encode($this->title) ?> <i class="fa fa-warning"></i></h1>
        </div>
    </div>

    <div id="error-content">
        <div class="container">
            <p style="font-size:30px;"><?= nl2br(Html::encode($message)) ?></p>

            <?php if(empty($message)): ?>
                <p>
                    The above error occurred while the Web server was processing your request.
                </p>
                <p>
                    Please contact us if you think this is a server error. Thank you.
                </p>
            <?php endif; ?>

            <p class="text-center" style="margin-top:40px;">
                <a class="btn btn-default" href="javascript:history.back()"><i class="fa fa-arrow-left"></i> <?= Yii::t('app', 'Retour') ?></a>
                <a class="btn btn-primary" href="<?= Url::to(Yii::$app->homeUrl) ?>"><i class="fa fa-home"></i> <?= Yii::t('app', "Accueil") ?></a>
            </p>
        </div>
    </div>
</div>