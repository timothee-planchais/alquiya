<?php

use common\models\User;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchUser */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">
    
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= backend\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->getTextStatus();
                },
                'filter' => User::getStatuses()
            ],
            'type',
            [
                'attribute' => 'gender',
                'value' => function($model){
                    return $model->getTextGender();
                },
                'filter' => User::getGenders()
            ],
            'email:email',
            'username',
            'firstname',
            'lastname',
            //'image',
            //'phone',
            //'website',
            'created_at:datetime',
            //'updated_at',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
