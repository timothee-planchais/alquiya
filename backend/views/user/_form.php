<?php

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$conf = [
    'options' => ['enctype' => 'multipart/form-data']
];

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(Yii::$app->params['bootstrapFormConfig']); ?>

        <?= $form->field($model, 'status')->dropDownList(User::getStatuses()) ?>

        <?= $form->field($model, 'gender')->dropDownList(User::getGenders()) ?>

        <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'work_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
    
        <?= $form->field($model, 'image')->fileInput(['accept' => 'image/*']) ?>

        <h2 class="text-center"><?= Yii::t('app', "Your address") ?></h2>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'country')->dropDownList(User::getCountries()) ?>

        <?= $form->field($model, 'lang')->dropDownList(User::getLangs()) ?>

        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php if(!$model->isNewRecord): ?>

            <h2 class="text-center"><?= Yii::t('app', "Edit password") ?></h2>

            <?= $form->field($model, 'new_password')->passwordInput() ?>

            <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>

            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'name' => 'update-password', 'value' => 1]) ?>
            </div>

        <?php endif; ?>
        
    <?php ActiveForm::end(); ?>

</div>
