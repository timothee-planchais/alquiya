<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?= Html::a('<i class="fa fa-list"></i> '.Yii::t('app', 'Items'), ['item/index', 'SearchItem[user_id]' => $model->getPrimaryKey()], ['class' => 'btn btn-default']) ?>
        
        <?= Html::a('<i class="fa fa-calendar-check-o"></i> '.Yii::t('app', 'Reservations'), ['reservation/index', 'SearchReservation[user_id]' => $model->getPrimaryKey()], ['class' => 'btn btn-default']) ?>
        
        <?= Html::a('<i class="fa fa-star"></i> '.Yii::t('app', 'Reviews'), ['review/index', 'SearchReview[author_id]' => $model->getPrimaryKey()], ['class' => 'btn btn-default']) ?>
        
    </p>
    
    <?php if($model->userPro): ?>
        <p>
            <?= Html::a('<i class="fa fa-eye"></i> '.Yii::t('app', "Company informations"), ['/user-pro/view', 'id' => $model->userPro->id], ['class' => 'btn btn-info']) ?>
        </p>
    <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'value' => $model->getTextStatus()
            ],
            'type',
            [
                'attribute' => 'gender',
                'value' => $model->getTextGender()
            ],
            'email:email',
            'username',
            'firstname',
            'lastname',
            [
                'attribute' => 'image',
                'value' => $model->image ? Html::img($model->getImageUrl(), ['width' => 300]) : null,
                'format' => 'raw'
            ],
            'phone',
            'mobile_phone',
            'work_phone',
            'show_email:boolean',
            'show_phone:boolean',
            'website',
            'facebook_id',
            'google_id',
            [
                'attribute' => 'ip',
                'value' => function($model){
                    return long2ip($model->ip);
                }
            ],
            'isp',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
