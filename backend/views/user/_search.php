<?php

use common\models\User;
use common\components\Utils;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchUser" aria-expanded="false" aria-controls="searchUser">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Recherche avancée") ?>
    </button>
    
    <div class="collapse" id="searchUser">
        
    <hr/>

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
    
        <div class="col-xs-12 col-sm-6">
        
            <?= $form->field($model, 'id') ?>

            <?= $form->field($model, 'status')->dropDownList(User::getStatuses(), ['prompt' => '-----']) ?>

            <?php // echo $form->field($model, 'type') ?>

            <?= $form->field($model, 'gender')->dropDownList(User::getGenders(), ['prompt' => '-----']) ?>
            
        </div>

        <div class="col-xs-12 col-sm-6">

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'username') ?>
        
            <?= $form->field($model, 'firstname') ?>

            <?= $form->field($model, 'lastname') ?>

            <?php // echo $form->field($model, 'phone') ?>

            <?php // echo $form->field($model, 'facebook_id') ?>

            <?php // echo $form->field($model, 'google_id') ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>

        </div>
        
    </div>
        
    <?php ActiveForm::end(); ?>

    </div>
</div>
