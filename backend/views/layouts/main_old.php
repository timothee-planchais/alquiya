<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?' . http_build_query(['libraries' => 'places', 'language' => Yii::$app->params['google.api_language'], 'key' => Yii::$app->params['google.api_key']]));
$this->registerJsFile('@web/js/plugins/jquery.geocomplete.min.js', ['depends'=>  \yii\web\JqueryAsset::className()]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => '<i class="fa fa-users"></i> Users', 'url' => ['user/index']],
        ['label' => '<i class="fa fa-list"></i> Items', 'url' => ['item/index']],
        ['label' => '<i class="fa fa-calendar-check-o"></i> Bookings', 'url' => ['booking/index']],
        ['label' => '<i class="fa fa-star"></i> Reviews', 'url' => ['review/index']],
        ['label' => '<i class="fa fa-list-alt"></i> Categories', 'url' => ['category/index']],
        ['label' => '<i class="fa fa-money"></i> '.Yii::t('app', "Ads"), 'items' => [
            ['label' => '<i class="fa fa-amazon"></i> Amazon', 'url' => ['amazon-item/index']]
        ]],
        '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
        .'</li>'
    ];
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels' => false,
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div id="content" class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <h1 id="title"><?= Html::encode($this->title) ?></h1>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        
    </div>
</footer>

<?php $this->endBody() ?>
    
        
<?php if (isset($this->blocks['js'])): ?>
    <?= $this->blocks['js'] ?>
<?php endif; ?>
    
</body>
</html>
<?php $this->endPage() ?>
