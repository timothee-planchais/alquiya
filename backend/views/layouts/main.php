<?php
/* @var $this backend\components\View */
/* @var $content string */
/* @var $admin \backend\models\Admin */

use backend\assets\AppAsset;
use backend\components\WebUser;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?' . http_build_query(['libraries' => 'places', 'language' => Yii::$app->params['google.api_language'], 'key' => Yii::$app->params['google.api_key']]));
$this->registerJsFile('@web/js/plugins/jquery.geocomplete.min.js', ['depends'=>  \yii\web\JqueryAsset::className()]);

$admin = Yii::$app->getUser()->getIdentity();

$nbUnreadContacts = \common\models\Contact::find()->where(['is_read' => 0])->count();
$nbUnreadItemReports = \common\models\ReportItem::find()->where(['is_read' => 0])->count();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="icon" href="<?= Url::to('@web/img/logo.png') ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-black sidebar-mini">
    <?php $this->beginBody() ?>
    
    <div id="mask"></div>

    <div id="wrap" class="wrapper">

        <header class="main-header">

            <a href="<?= Url::home() ?>" class="logo">
                <span class="logo-mini"><b>AlquiYA</b> BO</span>
                <span class="logo-lg"><b>AlquiYA</b> BO</span>
            </a>

            <nav id="nav" class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Url::to('@web/img/user/empty.png') ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $admin->__toString() ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?= Url::to(['/site/logout']) ?>" class="btn btn-default btn-flat" data-method="post">Déconnexion</a>
                                </li>
                            </ul>
                        </li>
                        <?php if(Yii::$app->user->can('admin/index')): ?>
                            <li>
                                <a href="<?= Url::to(['/admin/index']) ?>"><i class="fa fa-user-secret"></i> Admins</a>
                            </li>
                        <?php endif; ?>
                        <?php if(Yii::$app->user->can('param/index')): ?>
                            <li>
                                <a href="<?= Url::to(['/param/index']) ?>"><i class="fa fa-gears"></i></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="main-sidebar">

            <section class="sidebar">

                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= Url::to('@web/img/user/empty.png') ?>" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?= $admin->__toString() ?></p>
                        <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
                    </div>
                </div>

                <?= backend\widgets\Nav::widget([
                    'encodeLabels' => false,
                    'activateParents' => true,
                    'items' => [
                        '<li class="header">'.Yii::t('app', "General").'</li>',
                        ['label' => Yii::t('app', "Users"), 'icon' => 'users', 'url' => ['/user/index']],
                        ['label' => Yii::t('app', "Companies"), 'icon' => 'building', 'url' => ['/user-pro/index']],
                        ['label' => Yii::t('app', 'Items'), 'icon' => 'cubes', 'url' => ['/item/index']],
                        ['label' => Yii::t('app', 'Bookings'), 'icon' => 'calendar-check-o', 'url' => ['/booking/index']],
                        ['label' => Yii::t('app', 'Reviews'), 'icon' => 'star', 'url' => ['/review/index']],
                        ['label' => Yii::t('app', 'Categories'), 'icon' => 'list-alt', 'url' => ['/category/index']],
                        '<li class="header">'.Yii::t('app', "Payments").'</li>',
                        
                        ['label' => Yii::t('app', 'Subscriptions'), 'icon' => 'list-alt', 'url' => ['/subscription/index']],
                        ['label' => Yii::t('app', 'Orders'), 'icon' => 'list-alt', 'url' => ['/order/index']],
                        ['label' => Yii::t('app', 'Payments'), 'icon' => 'dollar', 'url' => ['/payment/index']],
                        
                        '<li class="header">'.Yii::t('app', "Contact").'</li>',
                        ['label' => Yii::t('app', 'Contacts'), 'icon' => 'envelope', 'url' => ['/contact/index'], 'notif' => $nbUnreadContacts],
                        ['label' => Yii::t('app', 'Item reports'), 'icon' => 'exclamation-circle', 'url' => ['/report-item/index'], 'notif' => $nbUnreadItemReports],
                        
                        '<li class="header">'.Yii::t('app', "Stats").'</li>',
                        ['label' => Yii::t('app', "Searches"), 'icon' => 'bar-chart', 'url' => ['/stat-search/index']],
                        
                        '<li class="header">'.Yii::t('app', "Ads").'</li>',
                        ['label' => Yii::t('app', "Amazon"), 'icon' => 'amazon', 'url' => ['/amazon-item/index']],
                    ],
                ]) ?>
            </section>
        </aside>

        <div class="content-wrapper">
            <section class="content-header">
                <?php if(!$this->hideTitle): ?>
                    <h1 id="title"><?= $this->title ?>&nbsp;</h1>
                <?php endif; ?>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </section>

            <section id="content" class="content container-fluid">

                <?= Alert::widget() ?>
                
                <?php if($this->useBox): ?>
                    <div class="box box-primary"><div class="box-body">
                <?php endif; ?>
                
                <?= $content ?>
                        
                <?php if($this->useBox): ?>
                    </div></div>
                <?php endif; ?>

            </section>
            
        </div>
        
        <footer class="main-footer">
            <p class="text-center">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
        </footer>

    </div>

    <?php $this->endBody() ?>

    <?php if (isset($this->blocks['js'])): ?>
        <?= $this->blocks['js'] ?>
    <?php endif; ?>

</body>
</html>
<?php $this->endPage() ?>
