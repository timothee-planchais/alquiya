<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */

$this->title = $model->__toString();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?= Html::a('<i class="fa fa-star"></i> '.Yii::t('app', 'Reviews'), ['review/index', 'SearchReview[reservation_id]' => $model->getPrimaryKey()], ['class' => 'btn btn-default']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => Html::a(Html::encode($model->user->username), ['user/view', 'id' => $model->user_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'item_id',
                'value' => Html::a(Html::encode($model->item->title), ['item/view', 'id' => $model->item_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => $model->getTextStatus()
            ],
            'date_start:date',
            'date_end:date',
            'price:currency',
            'total:currency',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
