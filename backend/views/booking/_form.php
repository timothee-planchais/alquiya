<?php

use common\models\Booking;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */
/* @var $form yii\widgets\ActiveForm */

$selectedUser = null;
if($model->user_id && $model->user) {
    $selectedUser = $model->user->getTitle();
}

?>

<div class="reservation-form">

    <?php $form = ActiveForm::begin(Yii::$app->params['bootstrapFormConfig']); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::class, [
        'initValueText' => $selectedUser,
        'options' => [
            'placeholder' => Yii::t('app', 'Look for a user').'...',
            'multiple' => false
        ],
        'pluginOptions' => [
            'multiple' => false,
            'allowClear' => true,
            'minimumInputLength' => 3,
            /*'language' => [
                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
            ],*/
            'ajax' => [
                'url' => Url::to(['/user/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.text; }'),
            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
        ],
    ]) ?>

    <?= $form->field($model, 'item_id')->input('number') ?>

    <?= $form->field($model, 'status')->dropDownList(Booking::getStatuses(), ['disabled' => true]) ?>

    <?= $form->field($model, 'date_start')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => Yii::t('app', "Select a date")],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy/mm/dd'
        ]
    ]) ?>

    <?= $form->field($model, 'date_end')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => Yii::t('app', "Select a date")],
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy/mm/dd'
        ]
    ]) ?>
    
    <?php $template = '<div class="input-group"> {input} <div class="input-group-addon">'.Yii::$app->formatter->currencyCode.'</div> </div>'; ?>

    <?= $form->field($model, 'price', ['inputTemplate' => $template])->input('number') ?>

    <?= $form->field($model, 'total', ['inputTemplate' => $template])->input('number') ?>

    <div class="form-group text-center">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
