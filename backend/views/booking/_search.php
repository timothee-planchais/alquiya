<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchBooking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reservation-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchBooking" aria-expanded="false" aria-controls="searchBooking">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Recherche avancée") ?>
    </button>
    
    <div class="collapse" id="searchBooking">
        
    <hr/>
    
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">
    
            <div class="col-xs-12 col-sm-6">
    
                <?= $form->field($model, 'id') ?>

                <?= $form->field($model, 'user_id') ?>

                <?= $form->field($model, 'item_id') ?>
                
            </div>
            
            <div class="col-xs-12 col-sm-6">

                <?= $form->field($model, 'status')->dropDownList(Booking::getStatuses()) ?>

                <?= $form->field($model, 'date_start')->input('date') ?>

                <?= $form->field($model, 'date_end')->input('date') ?>

                <?php // echo $form->field($model, 'price') ?>

                <?php // echo $form->field($model, 'total') ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>
            </div>

        </div>
            
        <?php ActiveForm::end(); ?>

    </div>
    
</div>
