<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Booking */

$this->title = Yii::t('app', 'Add Booking');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
