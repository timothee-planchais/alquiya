<?php

use common\models\Booking;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchBooking */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bookings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reservation-index">
    
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Booking'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \backend\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return Html::a(Html::encode($model->user->username), ['user/view', 'id' => $model->user_id]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'item_id',
                'value' => function($model) {
                    return Html::a(Html::encode($model->item->title), ['item/view', 'id' => $model->item_id]);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getTextStatus();
                },
                'filter' => Booking::getStatuses()
            ],
            [
                'attribute' => 'date_start',
                'format' => 'date',
                'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null]
            ],
            [
                'attribute' => 'date_end',
                'format' => 'date',
                'filterInputOptions' => ['type' => 'date', 'class' => 'form-control', 'id' => null]
            ],
            'price:currency',
            'total:currency',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => false
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'datetime',
                'filter' => false
            ],

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
