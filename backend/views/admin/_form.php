<?php

use backend\models\Admin;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Admin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-form">

    <?php $form = ActiveForm::begin(Yii::$app->params['bootstrapFormConfig']); ?>
    
        <?= $form->field($model, 'role')->dropDownList(Admin::getRoles()) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

        <?php if($model->isNewRecord): ?>
    
            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'password_repeat')->passwordInput() ?>

        <?php endif; ?>

        <div class="text-center">
            <?= Html::submitButton(Yii::t('app', 'Valider'), ['class' => 'btn btn-success btn-lg']) ?>
        </div>

        <?php if(!$model->isNewRecord): ?>

            <h2 class="text-center">Mot de passe</h2>

            <?= $form->field($model, 'new_password')->passwordInput() ?>
            <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>

            <div class="form-group text-center">
                <?= Html::submitButton('<i class="fa fa-edit"></i> '.Yii::t('app', "Modifier le mot de passe"), ['class' => 'btn btn-warning btn-lg', 'name' => 'update-password', 'value' => 1]) ?>
            </div>
    
        <?php endif; ?>

    <?php ActiveForm::end(); ?>

</div>
