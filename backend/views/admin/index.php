<?php

use yii\helpers\Html;
use backend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Admins');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Ajouter un admin'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'attribute' => 'role',
                'value' => function($model) {
                    return $model->getTextRole();
                },
            ],
            'email:email',
            'firstname',
            'lastname',
            //'created_at',
            //'updated_at',

            [
                'class' => 'backend\widgets\grid\ActionColumn',
                'viewVisible' => false
            ],
        ],
    ]); ?>
</div>
