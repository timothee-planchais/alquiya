<?php

use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Item */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$images = $model->images;

?>
<div class="item-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?= Html::a('<i class="fa fa-calendar-check-o"></i> '.Yii::t('app', 'Reservations'), ['reservation/index', 'SearchReservation[item_id]' => $model->getPrimaryKey()], ['class' => 'btn btn-default']) ?>
        
        <?= Html::a('<i class="fa fa-eye"></i> '.Yii::t('app', "See on FO"), Yii::$app->params['frontend.url'].'/i/'.$model->id,  ['class' => 'btn btn-info', 'target' => '_blank']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => '<a href="'.Url::to(['/user/view', 'id' => $model->user_id]).'">'.$model->user->__toString().'</a>',
                'format' => 'raw'
            ],
            'locked:boolean',
            [
                'attribute' => 'status',
                'value' => $model->getTextStatus()
            ],
            'featured:boolean',
            'highlighted:boolean',
            'bump_date:datetime',
            'title',
            'slug',
            [
                'attribute' => 'category_id',
                'value' => $model->category->title
            ],
            [
                'attribute' => 'subcategory_id',
                'value' => $model->subcategory_id ? $model->subcategory->title : null
            ],
            'subtitle',
            'description:ntext',
            [
                'attribute' => 'delivery',
                'value' => $model->getTextDelivery()
                ,
            ],
            [
                'attribute' => 'condition',
                'value' => $model->getTextCondition()
            ],
            /*[
                'label' => Yii::t('app', "Image"),
                'value' => Html::img($model->getImageUrl(), ['class' => 'img-responsive']),
                'format' => 'raw'
            ],*/
            'address',
            'lat',
            'lng',
            'short_address',
            'on_quote:boolean',
            [
                'attribute' => 'pricing_type',
                'value' => $model->getTextPricingType()
            ],
            'price:currency',
            'price_weekend:currency',
            'price_week:currency',
            'price_month:currency',
            'deposit_value:currency',
            'price_new:currency',
            [
                'label' => Yii::t('app', "Nb views"),
                'value' => $model->getNbViews()
            ],
            'nb_rents',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    
    <hr/>
    
    <h2>Images</h2>

    <?php if($images): ?>
    
    <div class="row">
        
        <?php foreach($images as $image): ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <img src="<?= $image->getImageUrl() ?>" alt="" height="250"/>
        </div>
        <?php endforeach; ?>
        
    </div>
    
    <?php endif; ?>
    
</div>
