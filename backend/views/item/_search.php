<?php

use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchItem */
/* @var $categories array */
/* @var $subCategories array */
/* @var $form yii\widgets\ActiveForm */

$selectedUser = null;
if($model->user_id && $model->user) {
    $selectedUser = $model->user->getTitle();
}

?>

<div class="item-search well">

    <button class="btn btn-default" type="button" data-toggle="collapse" data-target="#searchItem" aria-expanded="false" aria-controls="searchItem">
        <i class="fa fa-search-plus"></i> <?= Yii::t('app', "Recherche avancée") ?>
    </button>
    
    <div class="collapse" id="searchItem">
        
        <hr/>

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="row">

            <div class="col-xs-12 col-sm-6 col-md-4">

                <?= $form->field($model, 'id') ?>

                <?= $form->field($model, 'user_id')->widget(Select2::class, [
                        'initValueText' => $selectedUser,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Look for a user').'...',
                            'multiple' => false
                        ],
                        'pluginOptions' => [
                            'multiple' => false,
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            /*'language' => [
                                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                            ],*/
                            'ajax' => [
                                'url' => Url::to(['/user/search']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(user) { return user.text; }'),
                            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
                        ],
                    ]) ?>

                <?= $form->field($model, 'status')->dropDownList(Item::getStatuses(), ['prompt' => '-----']) ?>

                <?= $form->field($model, 'title') ?>

            </div>

            <div class="col-xs-12 col-sm-6 col-md-4">

                <?= $form->field($model, 'category_id')->dropDownList($categories, ['prompt' => '-----']) ?>

                <?= $form->field($model, 'subcategory_id')->dropDownList($subCategories, ['prompt' => '-----']) ?>

                <?= $form->field($model, 'subtitle') ?>

                

            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-4">

                <?= $form->field($model, 'featured')->dropDownList([0 => Yii::t('app', "No"), 1 => Yii::t('app', "Yes")], ['prompt' => '-----']) ?>

                <?= $form->field($model, 'highlighted')->dropDownList([0 => Yii::t('app', "No"), 1 => Yii::t('app', "Yes")], ['prompt' => '-----']) ?>
                
                <?= $form->field($model, 'full_city') ?>
                
                <?php // echo $form->field($model, 'description') ?>

                <?php // echo $form->field($model, 'address') ?>

                <?php // echo $form->field($model, 'price') ?>

                <?php // echo $form->field($model, 'price_weekend') ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
                </div>
                
            </div>

        <?php ActiveForm::end(); ?>

        </div>
    
    </div>
</div>
