<?php

use common\models\Item;
use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Item */
/* @var $form yii\widgets\ActiveForm */

$selectedUser = null;
if($model->user_id && $model->user) {
    $selectedUser = $model->user->getTitle();
}

$user = $model->user;

$isPro = $user->isPro();

?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::class, [
        'initValueText' => $selectedUser,
        'options' => [
            'placeholder' => Yii::t('app', 'Look for a user').'...',
            'multiple' => false
        ],
        'pluginOptions' => [
            'multiple' => false,
            'allowClear' => true,
            'minimumInputLength' => 3,
            /*'language' => [
                'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
            ],*/
            'ajax' => [
                'url' => Url::to(['/user/search']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(user) { return user.text; }'),
            'templateSelection' => new JsExpression('function (user) { return user.text; }'),
        ],
    ]) ?>
    
    <?= $form->field($model, 'locked', ['inline' => true])->radioList(\common\components\Utils::getYesNo()) ?>
    
    <?= $form->field($model, 'status')->dropDownList(Item::getStatuses()) ?>
    
    <?= $form->field($model, 'featured', ['inline' => true])->radioList(common\components\Utils::getYesNo()) ?>
    
    <?= $form->field($model, 'highlighted', ['inline' => true])->radioList(common\components\Utils::getYesNo()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->dropDownList($this->dropDownCategories) ?>

    <?= $form->field($model, 'subcategory_id')->dropDownList([]) ?>

    <?= $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'short_address', ['template' => "{input}\n{error}"])->hiddenInput() ?>
    <?= Html::activeHiddenInput($model, 'address') ?>
    <?= Html::activeHiddenInput($model, 'neighborhood') ?>
    <?= Html::activeHiddenInput($model, 'zipcode') ?>
    <?= Html::activeHiddenInput($model, 'city') ?>
    <?= Html::activeHiddenInput($model, 'full_city') ?>
    <?= Html::activeHiddenInput($model, 'region') ?>
    <?= Html::activeHiddenInput($model, 'state') ?>
    <?= Html::activeHiddenInput($model, 'country') ?>
    <?= Html::activeHiddenInput($model, 'country_code') ?>
    <?= Html::activeHiddenInput($model, 'lat') ?>
    <?= Html::activeHiddenInput($model, 'lng') ?>

    <?= $form->field($model, 'full_address')->textInput(['maxlength' => true/*, 'placeholder' => Yii::t('app', "Type an address")*/]) ?>

    <div id="address-img" class="form-group" style="display:none;">
        <div class="col-sm-9 col-sm-offset-3"><img src="" class="img-responsive" alt="" width="100%"/></div>
    </div>
    
    <?php if($isPro): ?>
        
        <div class="row">
            <!--<div class="col-xs-12 col-sm-6">
                <?php // $form->field($model, 'on_quote', ['inline' => true])->radioList(\common\components\Utils::getYesNo()) ?>
            </div>-->
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'pricing_type', ['inline' => true])->radioList(Item::getPricingTypes()) ?>
            </div>
        </div>

    <?php endif; ?>
    
    <div id="item-prices">
    
        <?php $template = '<div class="input-group"> {input} <div class="input-group-addon">'.Yii::$app->formatter->currencyCode.'</div> </div>'; ?>

        <?= $form->field($model, 'price'/*, ['inputTemplate' => $template]*/)->input('number', ['step' => '0.001']) ?>

        <?= $form->field($model, 'price_weekend'/*, ['inputTemplate' => $template]*/)->input('number', ['step' => '0.001']) ?>

        <?= $form->field($model, 'price_week'/*, ['inputTemplate' => $template]*/)->input('number', ['step' => '0.001']) ?>

        <?= $form->field($model, 'price_month'/*, ['inputTemplate' => $template]*/)->input('number', ['step' => '0.001']) ?>
    
    </div>

    <?= $form->field($model, 'deposit_value'/*, ['inputTemplate' => $template]*/)->input('number', ['step' => '0.001']) ?>

    <?= $form->field($model, 'price_new'/*, ['inputTemplate' => $template]*/)->input('number', ['step' => '0.001']) ?>
        
    <?= $form->field($model, 'delivery')->checkboxList(Item::getDeliveries(), ['prompt' => '']) ?>
    
    <?= $form->field($model, 'condition')->dropDownList(Item::getConditions(), ['prompt' => '']) ?>
    
    <?php
    /**
     * http://demos.krajee.com/widget-details/fileinput
     * http://plugins.krajee.com/file-advanced-usage-demo
     */

    if($model->isNewRecord)
    {
        /*
         * FILE INPUT
         */
        echo $form->field($model, 'tmp_images[]')->widget(FileInput::class, [
            'pluginOptions' => [
                'showCaption' => false,
                'showRemove' => true,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' => Yii::t('app', "Select pictures"),
                'maxFileCount' => 5,
                'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
            ],
            'options' => ['accept' => 'image/*', 'multiple' => true]
        ]);
    }
    else
    {
        /*
         * AJAX UPLOAD AND DELETE
         */
        $initialPreview = [];
        $initialPreviewConfig = [];

        $images = $model->images;

        if($images)
        {
            foreach($images as $image)
            {
                $initialPreview[] = $image->getImageUrl();
                $initialPreviewConfig[] = ['key' => $image->getPrimaryKey()];
            }
        }

        echo FileInput::widget([
            'name' => 'tmp_images',
            'options'=>[
                'multiple'=>true
            ],
            'pluginOptions' => [
                'uploadUrl' => Url::to(['/item/upload-image', 'id' => $model->getPrimaryKey()]),
                'deleteUrl' =>  Url::to(['/item/delete-image']),
                'uploadExtraData' => [
                    //'id' => $model->getPrimaryKey()
                ],
                'initialPreview' => $initialPreview,
                'initialPreviewConfig' => $initialPreviewConfig,
                'initialPreviewAsData' => true,
                'overwriteInitial' => false,
                'maxFileCount' => 5,
                'allowedFileExtensions' => ['jpg', 'png', 'jpeg']
            ]
        ]);
    }

    ?>

    <div class="text-center">
        <?= Html::submitButton(Yii::t('app', 'Valider'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        <?php if($isPro): ?>
            togglePricingType()
            $('#item-pricing_type').on('change', function(){
                togglePricingType();
            });
        <?php endif; ?>
        
        <?php if(!$model->isNewRecord): ?>
            updatePrices();
        <?php endif; ?>
        
        $('#item-price').on('keyup', function(){
            updatePrices();
        });
        
        loadSubcategories();
        
        $('#item-category_id').on('change', function(){
            loadSubcategories();
        });
        
        /*
         * 
         */
        var options = {"types":[/*"(regions)", "geocode", "establishment"*/],"componentRestrictions":{country: COUNTRY}};
        
        $('#item-full_address').on('keyup', function(){
            if($('#item-full_address').val().length <= 1) {
                $('#item-lat').val('');
                $('#item-lng').val('');
                $('#item-short_address, #item_address').val('');
                hideAddressImg();
            }
        });
        
        $('#item-full_address').geocomplete(options).bind("geocode:result", function(event, $result){
            if($result) 
            {
                var $lat = $result.geometry.location.lat();
                var $lng = $result.geometry.location.lng();
                var $address = $result.formatted_address;
                
                var $neighborhood, $zipcode, $city, $region, $state, $country;
                $.each($result.address_components, function(i, $row){
                    if($row.types[0] == 'postal_code') {
                        $zipcode = $row.long_name;
                    }
                    else if($row.types[0] == 'locality') {
                        $city = $row.long_name;
                    }
                    else if($row.types[0] == 'administrative_area_level_2') {
                        $state = $row.long_name;
                    }
                    else if($row.types[0] == 'administrative_area_level_1') {
                        $region = $row.long_name;
                    }
                    else if($row.types[0] == 'country') {
                        $country = $row.long_name;
                        $('#item-country_code').val($row.short_name);
                    }
                    else if($row.types[0] == 'neighborhood') {
                        $neighborhood = $row.long_name;
                    }
                });
                if(!$city) {
                    $city = $state;
                }
                $('#item-address').val($result.name);
                $('#item-lat').val($lat);
                $('#item-lng').val($lng);
                $('#item-neighborhood').val($neighborhood);
                $('#item-zipcode').val($zipcode);
                $('#item-city').val($city);
                $('#item-state').val($state);
                $('#item-region').val($region);
                $('#item-country').val($country);
                var $short_address = ($neighborhood ? $neighborhood+', ' : '')+$city/*+($zipcode ? ', '+$zipcode : '')*/+($region ? ', '+$region : '');
                var $full_city = $city+($region ? ', '+$region : '')+($country ? ', '+$country : '');
                $('#item-short_address').val($short_address);
                $('#item-full_city').val($full_city);
                $('.field-item-short_address').removeClass('has.error').find('.help-block-error').hide();
            }
        });
        
        /*detailsPrice();
    
        $('#item-price').on('blur', function(){
            detailsPrice();
        });*/
        
    });

    function loadSubcategories()
    {
        var $elemCategories = $('#item-category_id');
        var $elemSubCategories = $('#item-subcategory_id');
        
        var $categoryId = $elemCategories.val();
        $elemSubCategories.empty();
        
        if($categoryId)
        {
            $.ajax({
                url: "<?= Url::to(['subcategories']) ?>",
                method: "POST",
                data: { 
                    id: $categoryId
                },
                dataType: "json"
            }).done(function( $response ) {
                if($response.status == 1 && $response.categories)
                {
                    $elemSubCategories.append($('<option></option>').val('').html('<?=  Yii::t('app', "Select a subcategory") ?>'));
                    $.each($response.categories, function($index, $value) {          
                        $elemSubCategories.append($('<option></option>').val($index).html($value));
                    });
                    $('.field-item-subcategory_id').show();
                }
                else {
                    $('.field-item-subcategory_id').hide();
                }
            });
        }
    }
    
    function togglePricingType()
    {
        var $val = $('#item-pricing_type input:checked').val();
        
        if($val == '<?= Item::PRICING_TYPE_REQUEST ?>') {
            $('#item-prices').hide();
        }
        else {
            $('#item-prices').show();
        }
    }
    
    function updatePrices()
    {
        var $price = parseInt($('#item-price').val());
        $('#item-price_weekend').attr('placeholder', $price*2);
        $('#item-price_week').attr('placeholder', $price*5);
        $('#item-price_month').attr('placeholder', $price*15);
    }
    
    function hideAddressImg()
    {
        $('#address-img').find('img').attr('src', '');
        $('#address-img').slideUp(400);
    }

</script>
<?php $this->endBlock(); ?>