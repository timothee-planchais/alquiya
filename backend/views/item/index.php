<?php

use common\models\Item;
use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchItem */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Items');
$this->params['breadcrumbs'][] = $this->title;

$categories = ArrayHelper::map(Category::find()->where('parent_id IS NULL')->all(), 'id', 'title');
$subCategories = ArrayHelper::map(Category::find()->where('parent_id IS NOT NULL')->all(), 'id', 'title');

?>
<div class="item-index">

    <?= $this->render('_search', ['model' => $searchModel, 'categories' => $categories, 'subCategories' => $subCategories]); ?>

    <p>
        <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Create Item'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= backend\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]);
                },
                'format' => 'raw',
                'filter' => false
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->getTextStatus();
                },
                'filter' => Item::getStatuses()
            ],
            [
                'attribute' => 'category_id',
                'value' => 'category.title',
                'filter' => $categories
            ],
            'title',
            /*[
                'label' => Yii::t('app', "Image"),
                'value' => function($model){
                    return $model->firstImage ? Html::img($model->firstImage->getImageUrl(), ['width' => 200, 'style' => 'max-height:130px;']) : '';
                },
                'format' => 'raw'
            ],*/
            //'subcategory_id',
            //'subtitle',
            'short_address',
            'price:currency',
            //'price_weekend',
            //'price_week',
            //'price_month',
            //'deposit_value',
           [
               'attribute' => 'nb_rents',
               'filter' => false
           ],
            [
                'attribute' => 'created_at',
                //'format' => 'datetime',
                'value' => function($model){
                    return Yii::$app->formatter->asDateTime($model->created_at, 'short');
                },
                'filter' => false
            ],
            //'updated_at:datetime',

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        var options = {"types":["(cities)"]/*,"componentRestrictions":{country: 'CO'}*/};

        $('#searchitem-full_city').geocomplete(options).bind("geocode:result", function(event, $result){
            
        });
        
    });

</script>
<?php $this->endBlock(); ?>