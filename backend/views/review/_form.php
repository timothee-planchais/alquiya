<?php

use common\models\Review;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(Yii::$app->params['bootstrapFormConfig']); ?>

    <?= $form->field($model, 'author_id')->input('number') ?>

    <?= $form->field($model, 'reservation_id')->input('number') ?>

    <?= $form->field($model, 'user_id')->input('number') ?>

    <?= $form->field($model, 'note')->dropDownList(Review::getPossibleNotes()) ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <div class="form-group text-center">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
