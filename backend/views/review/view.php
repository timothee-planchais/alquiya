<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Review */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-view">

    <p class="clearfix">
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'author_id',
                'value' => Html::a($model->author, ['user/view', 'id' => $model->author_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'reservation_id',
                'value' => Html::a($model->reservation->id, ['reservation/view', 'id' => $model->reservation_id]),
                'format' => 'raw'
            ],
            [
                'attribute' => 'user_id',
                'value' => $model->user_id ? Html::a($model->user, ['user/view', 'id' => $model->user_id]) : null,
                'format' => 'raw'
            ],
            [
                'attribute' => 'note',
                'value' => $model->note.'/5'
            ],
            [
                'attribute' => 'text',
                'value' => nl2br(Html::encode($model->text)),
                'format' => 'raw'
            ],
            'created_at:datetime',
        ],
    ]) ?>

</div>
