<?php

use common\models\Review;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchReview */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Reviews');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Review'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= \backend\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'author_id',
            'booking_id',
            'user_id',
            [
                'attribute' => 'note',
                'value' => function($model){
                    return $model->note.'/5';
                },
                'filter' => Review::getPossibleNotes()
            ],
            [
                'attribute' => 'text',
                'value' => function($model){
                    return nl2br(Html::encode($model->text));
                },
                'format' => 'raw',
                'filter' => false
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => false
            ],

            ['class' => 'backend\widgets\grid\ActionColumn'],
        ],
    ]); ?>
</div>
