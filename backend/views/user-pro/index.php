<?php

use common\models\UserPro;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchUserPro */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Companies');
$this->params['breadcrumbs'][] = $this->title;

$selectedUser = null;
if($searchModel->user_id && $searchModel->user) {
    $selectedUser = $searchModel->user->getTitle();
}

?>
<div class="user-pro-index">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add a company'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model){
                    return Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]);
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            'company_name',
            //'categories',
            'website:url',
            //'vat_num',
            //'description:ntext',
            //'nb_items',
            //'available_days',
            //'available_hours',
            //'logo',
            [
                'class' => 'backend\widgets\grid\ActionColumn'
            ],
        ],
    ]); ?>
</div>
