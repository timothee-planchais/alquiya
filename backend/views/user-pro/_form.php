<?php

use common\models\UserPro;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\UserPro */
/* @var $form yii\widgets\ActiveForm */

$selectedUser = null;
if($model->user_id && $model->user) {
    $selectedUser = $model->user->getTitle();
}

$selectedCategories = null;
if($model->getAllCategories()) {
    foreach($model->getAllCategories() as $category) {
        $selectedCategories[$category->id] = $category->title;
    }
}

$conf = [
    'options' => ['enctype' => 'multipart/form-data']
];

?>

<div class="user-pro-form">

    <?php $form = ActiveForm::begin($conf); ?>

        <?= $form->field($model, 'user_id')->widget(Select2::class, [
            'initValueText' => $selectedUser,
            'options' => [
                'placeholder' => Yii::t('app', 'Look for a user').'...',
                'multiple' => false
            ],
            'pluginOptions' => [
                'multiple' => false,
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::to(['/user/search']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(user) { return user.text; }'),
                'templateSelection' => new JsExpression('function (user) { return user.text; }'),
            ],
        ]) ?>

        <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'categories')->widget(Select2::class, [
            'initValueText' => $selectedCategories,
            'options' => [
                'placeholder' => Yii::t('app', 'Look for categories').'...',
                'multiple' => true
            ],
            'pluginOptions' => [
                'multiple' => true,
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Url::to(['/category/search']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                'templateResult' => new JsExpression('function(user) { return user.text; }'),
                'templateSelection' => new JsExpression('function (user) { return user.text; }'),
            ],
        ]) ?>

        <?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'vat_num')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'nb_items')->input('number') ?>

        <?= $form->field($model, 'available_days', ['inline' => true])->checkboxList(UserPro::getAvailableDays()) ?>

        <?= $form->field($model, 'available_hours')->textInput() ?>

        <?= $form->field($model, 'logo')->fileInput(['accept' => 'image/*']) ?>

        <div class="text-center">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
