<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserPro */

$this->title = Yii::t('app', 'Update company : {name}', [
    'name' => $model->company_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Companies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->company_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="user-pro-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
