<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\UserPro */

$this->title = $model->company_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Pros'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-pro-view">

    <p>
        <?= Html::a('<i class="fa fa-times"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => Html::a(Html::encode($model->user->__toString()), ['/user/view', 'id' => $model->user_id]),
                'format' => 'raw'
            ],
            'company_name',
            [
                'attribute' => 'categories',
                'value' => $model->getNiceCategories()
            ],
            'website:url',
            'vat_num',
            'description:ntext',
            'nb_items',
            [
                'attribute' => 'available_days',
                'value' => $model->getNiceAvailableDays()
            ],
            'available_hours',
            [
                'attribute' => 'logo',
                'value' => Html::img($model->getLogoUrl(), ['width' => 250]),
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
