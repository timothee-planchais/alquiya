<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchUserPro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-pro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'categories') ?>

    <?= $form->field($model, 'website') ?>

    <?php // echo $form->field($model, 'vat_num') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'nb_items') ?>

    <?php // echo $form->field($model, 'available_days') ?>

    <?php // echo $form->field($model, 'available_hours') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
