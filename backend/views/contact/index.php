<?php

use common\models\Contact;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchContact */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contacts');
$this->params['breadcrumbs'][] = $this->title;

$selectedUser = null;
if($searchModel->user_id && $searchModel->user) {
    $selectedUser = $searchModel->user->getTitle();
}

?>
<div class="contact-index">
    
    <?php // $this->render('_search', ['model' => $searchModel, 'selectedUser' => $selectedUser]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            $class = '';
            if(!$model->is_read) {
                $class = 'info';
            }
            return ['class' => $class];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'user_id',
                'value' => function($model) {
                    return $model->user_id && $model->user ? Html::a($model->user->__toString(), ['/user/view', 'id' => $model->user_id]) : null;
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'user_id',
                    'initValueText' => $selectedUser,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Look for a user').'...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'multiple' => false,
                        'allowClear' => true,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'En attente de résultats...'; }"),
                        ],
                        'ajax' => [
                            'url' => Url::to(['/user/search']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(model) { return model.text; }'),
                        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                    ],
                ])
            ],
            'email:email',
            'name',
            [
                'attribute' => 'category',
                'value' => function($model){
                    return $model->getTextCategory();
                },
                'filter' => Contact::getCategories()
            ],
            'subject',
            //'message:ntext',
            'created_at:datetime',
            //'updated_at',

            [
                'class' => 'backend\widgets\grid\ActionColumn',
                'updateVisible' => false
            ],
        ],
    ]); ?>
</div>
