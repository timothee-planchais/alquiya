<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SpectrumAsset extends AssetBundle
{
    public $sourcePath = '@bower/spectrum';
    
    public $css = [
        'spectrum.css'
    ];
    
    public $js = [
        'spectrum.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset'
    ];
    
    /*public $jsOptions = [
        'position' => View::POS_END
    ];*/
}
