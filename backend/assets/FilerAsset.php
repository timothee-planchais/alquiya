<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FilerAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/js/plugins/jquery.filer';
    
    public $css = [
        'css/jquery.filer.css'
    ];
    
    public $js = [
        'js/jquery.filer.min.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset'
    ];
    
    /*public $jsOptions = [
        'position' => View::POS_END
    ];*/
}
