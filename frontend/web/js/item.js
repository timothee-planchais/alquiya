var $date_start, $date_end;

$(document).ready(function(){
    
    if($('#booking-date_range').length)
    {
        /*
         * BOOKING
         * @see http://www.daterangepicker.com/#events
         */
        /*$date_start = moment();
        $date_end = moment();//moment(new Date()).add(1,'days');
        updateBookingDetails();*/
        //console.log($('#booking-date_range').data('daterangepicker').startDate);
        $('#booking-date_range').on('apply.daterangepicker', function(ev, picker) {
            $date_start = picker.startDate;
            $date_end = picker.endDate;
            updateBookingDetails();
        });
    }
    
    /*
     * CAROUSEL
     */
    $('#item-images').owlCarousel({
        loop: true,
        autoplay: false,
        nav: true,
        items: 1,
        autoHeight: true
    });
    
    /*
     * SHARE
     */
    share();
    
});

function updateBookingDetails()
{
    var $elemBookingDetails = $('#item #booking-details');
    var $elemTotalPrice = $('#booking-total .value');
    var $elemDiscountAmount = $('#booking-discount .value');
    
    var $duration = 0;
    var $totalPrice = 0;
    var $discountAmount = 0;
    
    //console.log($date_start);
    
    if($date_start && $date_end && $date_start <= $date_end)
    {
        $elemBookingDetails.show();
        
        $duration = getDuration();
        $totalPrice = getTotalPrice($duration);
        $discountAmount = getDiscountAmount($duration, $totalPrice);
        
        $('#booking-duration-days .value').text($duration);
        $('#booking-duration-price').text(asCurrency($duration * $price));
        $elemTotalPrice.text(asCurrency($totalPrice));
        if($discountAmount > 0) {
            $('#booking-discount').show();
            $elemDiscountAmount.text(asCurrency($discountAmount));
        }
        else {
            $('#booking-discount').hide();
        }
    }
    else if($date_start > $date_end) {
        
    }
    else {
       $elemBookingDetails.hide();
    }
}

function getDuration()
{
    //var $diff = ($date_end - $date_start) / (1000 * 3600 * 24) + 1;
    var $diff = $date_end.diff($date_start, 'days') + 1;
    
    return $diff;
}

function getTotalPrice($duration)
{
    var $dailyPrice = $price;
    
    if($price_month && $duration >= 30) {
        $dailyPrice = $price_month / 30;
    }
    if($price_week && $duration >= 7) {
        $dailyPrice = $price_week / 7;
    }
    if($price_weekend && $duration >= 3) {
        $dailyPrice = $price_weekend / 3;
    }
    
    //return ($duration * $dailyPrice).toFixed(0);
    return $duration * $dailyPrice;
}

function getDiscountAmount($duration, $totalPrice)
{
    return ($duration * $price) - $totalPrice;
}

function initMap($lat, $lng)
{
    var $location = new google.maps.LatLng($lat, $lng);

    var $mapOptions = {
        center: $location,
        zoom: 12,
        zoomControl: false,
        disableDoubleClickZoom: true,
        //mapTypeControl: true,
        scaleControl: false,
        scrollwheel: true,
        panControl: false,
        streetViewControl: false,
        draggable : true,
        overviewMapControl: false,
        overviewMapControlOptions: {
            opened: false,
        },
    };

    var $marker = new google.maps.Marker({
        position: $location,
        map: $map,
        //icon: URL_MARKER,
        //title: $title,
        //desc: $desc
    });

    var $map = new google.maps.Map(document.getElementById('item-map-map'), $mapOptions);

    $marker.setMap($map);
}

function share()
{
    $('.btn-share').on('click', function(event){
        event.preventDefault();
        $link = $(this).attr('data-url');
        
        $done = false;
        $isFb = false;
        $isTw = false;
        $isGo = false;
        
        if($(this).hasClass('btn-facebook')) {
            $isFb = true;
        }
        else if($(this).hasClass('btn-twitter')) {
            $isTw = true;
        }
        
        $title = $('#item #title').text();
        $desc = $('#item #item-description p').text();
        
        /**
         * http://stackoverflow.com/questions/23116001/facebook-like-and-share-button-with-callback
         * https://developers.facebook.com/docs/javascript/reference/FB.ui
         */
        if($isFb)
        {
            FB.ui({
                method: 'share',
                title: $title,
                description: $desc,
                href: $link,
            },
            function (response) {
                if (response && !response.error_code) {
                } else {
                }
            });
        }
        /*
         * https://www.webniraj.com/2012/09/11/twitter-api-tweet-button-callbacks/
         */
        else
        {
            /*if($isFb) {
                if($isMobile) {
                    $url = "https://touch.facebook.com/dialog/feed?app_id="+$facebookApplicationId+"&link="+encodeURIComponent($link)+"&redirect_uri="+encodeURIComponent(document.URL);
                }
                else {
                    $url = "https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent($link);
                }
            }*/
            if($isTw) {
                $text = $(this).attr('data-text');
                $via = $(this).attr('data-via');
                $url = "https://"/*+($isMobile ? "mobile." : "")*/+"twitter.com/intent/tweet?url="+encodeURIComponent($link)+"&text="+$text+"&via="+$via;
            }
            else {
                $url = "https://plus.google.com/share?url="+encodeURIComponent($link);
            }

            $width = 600;
            $height = 450;
            $left = window.screen.width/2-($width/2+10);
            $top = window.screen.height/2-($height/2+50);
            window.open($url,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width='+$width+',height='+$height+',left='+$left+',top='+$top+',screenX='+$left+',screenY='+$top+',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no');
            
        }
        
        return false;
    });
}