var $notificationsOffset = 10;
$(document).ready(function(){
    
    /*if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(getCurrentLocation);
    }*/
    
    $('.btn-wishlist').on('click', function(){
        wishList($(this).data('id'), $(this).hasClass('saved'));
    });
    
    setMobileCategoriesMenu();
    
    $('#categories ul li a[href="'+window.location.pathname+'"]').parents('li').addClass('active');
    
    $('#li-notifications').on('click', '.notification', function(e){
        readNotification($(this).data('id'));
    });
    
    $('#li-notifications #notifications-load').on('click', function(e){
        e.stopPropagation();
        loadNotifications();
    });
    
    /*
     * ANALYTICS EVENTS
     * @see https://developers.google.com/analytics/devguides/collection/gtagjs/events
     */
    $('#home #home-items-search form').on('beforeSubmit', function(event){
        //ga('send', 'event', 'Home', 'search', 'Home : search btn');
        gtag('event', 'search', {
            'event_category': 'Home',
            'event_label': 'Home : search btn'
        });
    });
    $('#items-search form').on('beforeSubmit', function(event){
        //ga('send', 'event', 'Items', 'search', 'Items list : search btn');
        gtag('event', 'search', {
            'event_category': 'Items',
            'event_label': 'Items list : search btn'
        });
    });
    $('#categories .nav a, #categories-mobile .nav a').on('click', function(){
        var $id = $(this).data('id');
        //ga('send', 'event', 'Categories banner', 'click', 'Categories banner : click', $id);
        gtag('event', 'click', {
            'event_category': 'Categories banner',
            'event_label': 'Categories banner : click'+$id
        });
    });
    
});

function initSearchItem()
{
    /*var options = {"types":[],"componentRestrictions":{country: COUNTRY}};
        
    $('#searchitem-search_address').on('keyup', function(){
        if($('#searchitem-search_address').val().length <= 1) {
            $('#searchitem-lat').val('');
            $('#searchitem-lng').val('');
        }
    });

    $('#searchitem-search_address').geocomplete(options).bind("geocode:result", function(event, $result){
        if($result) 
        {
            var $lat = $result.geometry.location.lat();
            var $lng = $result.geometry.location.lng();
            var $address = $result.formatted_address;
            $('#searchitem-lat').val($lat);
            $('#searchitem-lng').val($lng);
        }
    });*/
    
    var options = {"types":["(cities)"],"componentRestrictions":{country: COUNTRY}};
        
    $('#search_city').on('keyup', function(){
        if($('#search_city').val().length <= 1) {
            $('#lat, #lng, #city').val('');
        }
    });

    $('#search_city').geocomplete(options).bind("geocode:result", function(event, $result){
        if($result) 
        {
            //console.log($result);
            var $neighborhood, $zipcode, $city, $region, $state, $country;
            var $lat = $result.geometry.location.lat();
            var $lng = $result.geometry.location.lng();
            $.each($result.address_components, function(i, $row){
                if($row.types[0] == 'locality') {
                    $city = $row.long_name;
                }
                else if($row.types[0] == 'administrative_area_level_2') {
                    $state = $row.long_name;
                }
                else if($row.types[0] == 'neighborhood') {
                    $neighborhood = $row.long_name;
                }
            });
            if(!$city) {
                $city = $state;
            }
            $('#lat').val($lat);
            $('#lng').val($lng);
            //$('#searchitem-neighborhood').val($neighborhood);
            $('#city').val($city);
            //$('#searchitem-state').val($state);
        }
    });
}

function loadNotifications()
{
    $('#notifications-load').prop('disabled', true);
    $.ajax({
        url: "/notification/load",
        method: "POST",
        data: {
            offset: $notificationsOffset,
        },
        dataType: "html"
    }).done(function( $html ) 
    {
        if($html != '0')
        {
            $notificationsOffset += $($html).length;
            $('#notifications-load').before($html);
            $('#notifications-load').prop('disabled', false);
        }
        
        if($html == '0' || ($html && $($html).length < 5)) {
            $('#notifications-load').hide();
        }
    });
}

function readNotification($id)
{
    $.ajax({
        url: "/notification/read",
        method: "POST",
        async : false,
        data: {
            id: $id
        },
        dataType: "html"
    }).done(function( $response ) 
    {
        $('#notifications .notification[data-id="'+$id+'"]').removeClass('unread');
    });
}

function wishList($itemId, $remove)
{
    var $obj = $('.btn-wishlist[data-id="'+$itemId+'"]');
    
    $obj.addClass('disabled');
    
    $.ajax({
        url: "/account/update-wishlist",
        method: "POST",
        data: { 
            id: $itemId,
            remove: $remove ? 1 : 0
        },
        dataType: "json"
    }).done(function( $response ) {
        if($response.status == 1)
        {
            if($remove)
            {
                $obj.removeClass('saved');
            }
            else {
                $obj.addClass('saved');
            }
        }
        $obj.removeClass('disabled');
    });
    
}

function setMobileCategoriesMenu()
{
    $("#categories-mobile").mmenu({
        "extensions": [
           "pagedim-black",//http://mmenu.frebsite.nl/documentation/extensions/page-dim.html
           "fx-panels-slide-0",//http://mmenu.frebsite.nl/documentation/extensions/effects.html
           //"multiline",//http://mmenu.frebsite.nl/documentation/extensions/multiline.html
           //"popup",//http://mmenu.frebsite.nl/documentation/extensions/popup.html
           //"position-bottom",//http://mmenu.frebsite.nl/documentation/extensions/positioning.html
        ],
        //"autoHeight": true,//http://mmenu.frebsite.nl/documentation/extensions/popup.html
        "counters": true,
        //http://mmenu.frebsite.nl/documentation/addons/navbars.html
        /*"navbars": [
           {
              "position": "bottom",
              "content": [
                 "<a class='fa fa-envelope' href='#/'></a>",
                 "<a class='fa fa-twitter' href='#/'></a>",
                 "<a class='fa fa-facebook' href='#/'></a>"
              ]
           }
           {
               "position": "top",
               "content": [
                  "breadcrumbs",
                  "close"
               ]
            }
        ]*/
        //http://mmenu.frebsite.nl/documentation/addons/set-selected.html
        "setSelected": {
            "hover": true,
            "parent": true
         }
     });
}

/*
 * @see https://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl#Locale_identification_and_negotiation
 */
function number_format($value, $decimals/*, $dec_point = "." , $thousands_sep = "," */)
{
    //$value = $value.toFixed($decimals);
    if(CURRENCY_CODE == 'COP')
    {
        return $value.toLocaleString('es-CO', {maximumFractionDigits:$decimals});
    }
    else if(CURRENCY_CODE == 'PEN')
    {
        return $value.toLocaleString('es-PE', {maximumFractionDigits:$decimals});
    }
    
    return $value.toLocaleString();
}

function asCurrency($value)
{
    var $decimals = 0;
    if(CURRENCY_CODE == 'COP')
    {
        return '$'+number_format($value, $decimals);
    }
    else if(CURRENCY_CODE == 'PEN')
    {
        return 'S/.'+number_format($value, $decimals);
    }
    
    return $value.toLocaleString();
}

function getCurrentLocation(position){
    var $lat = position.coords.latitude;
    var $lng = position.coords.longitude;
    //console.log($lat, $lng);
}