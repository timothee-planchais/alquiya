<?php


namespace frontend\models;

use Yii;
use common\models\User;
use yii\base\Model;

class ResendVerificationEmailForm extends Model
{
    /**
     * @var string
     */
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_PENDING],
                'message' => Yii::t('app', 'There is no user with this email address')
            ],
        ];
    }

    /**
     * Sends confirmation email to user
     *
     * @return bool whether the email was sent
     */
    public function sendEmail()
    {
        $user = User::findOne([
            'email' => $this->email,
            'status' => User::STATUS_PENDING
        ]);

        if ($user === null) {
            return false;
        }

        return Yii::$app
            ->mailer
            ->compose('emailVerify', ['user' => $user])
            ->setFrom([Yii::$app->params['contactFromEmail'] => Yii::$app->params['contactFromName']])
            ->setTo($this->email)
            ->setSubject("Account tegistration on AlquiYa")
            ->send();
    }
}
