<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\UserPro;
use common\components\Utils;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $type;
    public $gender;
    public $username;
    public $email;
    public $firstname;
    public $lastname;
    public $phone, $mobile_phone, $work_phone;
    public $website;
    
    public $password;
    public $password_repeat;
    
    public $address;
    public $address2;
    public $zipcode;
    public $city;
    public $country;
    
    public $accept_terms;
    
    //PRO
    public $pro_company_name;
    public $pro_categories;
    public $pro_website;
    public $pro_vta_num;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'gender', 'username', 'email', 'firstname', 'lastname', /*'phone', 'address', 'zipcode', 'city',*/ 'country', 'password', 'password_repeat'], 'required'],
            
            ['type', 'in', 'range' => array_keys(User::getTypes())],
            
            [['username', 'email'], 'trim'],
            [['username', 'email', 'firstname', 'lastname', 'address', 'address2', 'city'], 'string', 'min' => 2, 'max' => 255],
            
            ['gender', 'in', 'range' => array_keys(User::getGenders())],
            
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This username has already been taken.')],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This email address has already been taken.')],

            [['phone', 'mobile_phone', 'work_phone'], 'safe'],
            
            [['website'], 'url'],
            
            [['password', 'password_repeat'], 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', "The repeat password must be the same")],
            
            ['zipcode', 'string', 'min' => 5, 'max' => 7],
            
            ['country', 'in', 'range' => array_keys(User::getCountries())],
            
            //['zipcode', 'match', 'pattern' => Utils::REGEX_POSTAL_CODE],
            
            
            [['pro_company_name', 'pro_categories'], 'required', 
                'when' => function($model){ return $model->type == User::TYPE_USER_PRO; },
                'whenClient' => 'function(attribute, value) { return $("#signupform-type").val() != "'.User::TYPE_USER_PRO.'";}'
            ],
            [['pro_categories'], 'safe', 'when' => function($model){ return $model->type == User::TYPE_USER_PRO; }],
            ['pro_company_name', 'string', 'max' => 100, 'when' => function($model){ return $model->type == User::TYPE_USER_PRO; }],
            [['pro_website'], 'url', 'when' => function($model){ return $model->type == User::TYPE_USER_PRO; }],
            [['pro_website'], 'string', 'max' => 255,'when' => function($model){ return $model->type == User::TYPE_USER_PRO; }],
            [['pro_vta_num'], 'string', 'max' => 50,'when' => function($model){ return $model->type == User::TYPE_USER_PRO; }],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User([
            'status' => User::STATUS_PENDING,
            'username' => $this->username,
            'email' => $this->email,
            'gender' => $this->gender,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'phone' => $this->phone,
            'address' => $this->address,
            'address2' => $this->address2,
            'zipcode' => $this->zipcode,
            'city' => $this->city,
            'country' => $this->country,
            'lang' => 'es',
            'show_phone' => (int)$this->type == User::TYPE_USER_PRO,
            'show_email' => 0,
            'ip' => Utils::getUserIP(),
            'isp' => Utils::getUserISP(),
        ]);
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        
        if($user->save(false)) 
        {
            if($this->type == User::TYPE_USER_PRO)
            {
                $userPro = new UserPro([
                    'user_id' => $user->getPrimaryKey(),
                    'company_name' => $this->pro_company_name,
                    'categories' => $this->pro_categories,
                    'website' => $this->pro_website,
                    'vat_num' => $this->pro_vta_num
                ]);
                $userPro->save(false);
            }
            
            //Envoyer un email de confirmation
            Yii::$app->mailer->compose('signup', ['user' => $user])
            ->setTo($user->email)
            ->setFrom([Yii::$app->params['contactFromEmail'] => Yii::$app->params['contactFromName']])
            ->setSubject(Yii::t('app', "Your signup on {name}", ['name' => 'AlquiYa']))
            ->send();
            
            return $user;
        }
        
        return null;
    }
    
    public function attributeInfos()
    {
        $user = new User();
        return $user->attributeInfos();
    }
    
    public function getAttributeInfo($attribute)
    {
        return \yii\helpers\ArrayHelper::getValue($this->attributeInfos(), $attribute);
    }
    
    public function attributeLabels()
    {
        $user = new User();
        $labels = $user->attributeLabels();
        
        $labels['type'] = Yii::t('app', "You are");
        $labels['accept_terms'] = Yii::t('app', 'I accept the <a href="/terms.html" target="_blank">terms and conditions</a> and the <a href="/privacy.html" target="_blank">policy of privacy</a>');
        
        $labels['pro_company_name'] = Yii::t('app', "Company name");
        $labels['pro_categories'] = Yii::t('app', "Sector of activity");
        $labels['pro_website'] = Yii::t('app', "Website");
        $labels['pro_vta_num'] = Yii::t('app', "VAT number");
        
        return $labels;
    }
}
