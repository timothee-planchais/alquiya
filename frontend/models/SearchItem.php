<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Item;
use common\models\User;
use DateTime;

/**
 * SearchItem represents the model behind the search form of `common\models\Item`.
 */
class SearchItem extends Item
{
    public $search, $search_address, $search_city;
    
    public $radius = null;
    
    public $price_min, $price_max;
    
    public $date_range, $date_start, $date_end;
    
    public $user_type/* = [User::TYPE_USER,User::TYPE_USER_PRO]*/;
    
    public $bigCategoryId;
    
    public $isFeatured = false;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'string', 'max' => 100],
            [['search_city', 'search_address'], 'string', 'max' => 200],
            [['category_id', 'subcategory_id', 'condition'], 'integer'],
            [['user_type', 'title', 'subtitle', 'description', 'lat', 'lng', 'city'], 'safe'],
            [['price_min', 'price_max'], 'number'],
            //['radius', 'default', 'value' => 30],
            /*['radius', 'required', 'when' => function($model){
                return !empty($this->search_address);
            }, 'whenClient' => "function (attribute, value) {
                return $('#searchitem-search_address').val() != '';
            }"]*/
            
            ['date_range', 'safe'],
            [['date_start', 'date_end'], 'date', 'format' => 'php:d/m/Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function formName()
    {
        return '';
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Item::findActive();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'enableMultiSort' => false,
                'defaultOrder' => [
                    'bump_date' => SORT_DESC,
                    'created_at' => SORT_DESC,
                    'title' => SORT_ASC
                ],
                'attributes' => ['bump_date', 'created_at', 'title', 'price', 'item.distance']
            ],
            /*'pagination' => [
                'defaultPageSize' => 20
            ]*/
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($this->search_address && $this->lat && $this->lng)
        {
            $query->select('*, ( 6371 * acos( cos( radians(:lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(:lng) ) + sin( radians(:lat) ) * sin( radians( lat ) ) ) ) AS distance')
                    ->groupBy('id')
                    ->having(['<=', 'item.distance', $this->radius])
                    ->params(['lat' => $this->lat, 'lng' => $this->lng]);
            if(empty(Yii::$app->getRequest()->get('sort'))) {
                $dataProvider->sort->setAttributeOrders(['item.distance' => SORT_ASC], false);
            }
        }
        
        if($this->price_min)
        {
            $query->andWhere(['>=', 'item.price', $this->price_min]);
        }
        if($this->price_max)
        {
            $query->andWhere(['<=', 'item.price', $this->price_max]);
        }
        
        if($this->search)
        {
            $query->andFilterWhere(['or', ['like', 'item.title', $this->search.'%', false]]);
        }
        
        if($this->bigCategoryId && empty($this->category_id) && empty($this->subcategory_id))
        {
            $query->andWhere(['in', 'category_id', (new \yii\db\Query)->select('id')->from('category')->where(['parent_id' => $this->bigCategoryId])]);
        }
        
        if($this->search_city) {
            $query->andWhere(['like', 'item.full_city', $this->search_city]);
        }
        elseif($this->city) {
            $query->andWhere(['like', 'item.city', $this->city]);
        }
        
        if($this->date_start && $this->date_end)
        {
            $oDateStart = DateTime::createFromFormat('d/m/Y', $this->date_start);
            $oDateEnd = DateTime::createFromFormat('d/m/Y', $this->date_end);
            $niceDateStart = $oDateStart->format('Y-m-d');
            $niceDateEnd = $oDateEnd->format('Y-m-d');
            $query1 = (new \yii\db\Query)->select('item_id')->from('booking')
                    ->where(['status' => \common\models\Booking::STATUS_VALIDATED])
                    ->andWhere(['or', 'date_start BETWEEN :start AND :end', 'date_end BETWEEN :start AND :end'])
                    ->addParams(['start' => $niceDateStart, 'end' => $niceDateEnd]);
            $query->andWhere(['not in', 'item.id', $query1]);
        }

        if($this->user_type)
        {
           $query->leftJoin('user u', 'u.id = item.user_id')->andWhere(['u.type' => $this->user_type]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'item.category_id' => $this->category_id,
            'item.subcategory_id' => $this->subcategory_id,
            'item.condition' => $this->condition
        ]);

        $query->andFilterWhere(['like', 'item.title', $this->title])
            ->andFilterWhere(['like', 'item.subtitle', $this->subtitle]);
        
        if($this->isFeatured)
        {
            $query->andWhere(['item.featured' => 1])->limit(4)->orderBy('RAND()');
            $dataProvider->sort = false;
            $dataProvider->pagination = false;
        }
        
        return $dataProvider;
    }
    
    public static function getSort($withDistance = false)
    {
        $sorts = [
            //'-featured' => Yii::t('app', "Relevance"),
            '-bump_date' => Yii::t('app', "Lastest"),
            'bump_date' => Yii::t('app', "Less recent"),
            'price' => Yii::t('app', "Cheaper"),
            '-price' => Yii::t('app', "More expensive"),
            'title' => Yii::t('app', "Title")
        ];
        
        if($withDistance) {
            $sorts =['distance' => Yii::t('app', "Distance")] + $sorts;
        }
        
        return $sorts;
    }
    
    public function attributeLabels()
    {
        return [
            'search' => Yii::t('app', "What product do you want to rent ?"/*"Describe the product you are looking for"*/),
            'search_city' => Yii::t('app', "In what city ?"),
            'search_address' => Yii::t('app', "Next to"),
            'radius' => Yii::t('app', "Distance"),
            'title' => Yii::t('app', 'Title'),
            'category_id' => Yii::t('app', 'Categoría'),
            'subcategory_id' => Yii::t('app', 'Subcategory'),
            'subtitle' => Yii::t('app', 'Subtitle'),
            'address' => Yii::t('app', 'Address'),
            'price' => Yii::t('app', 'Price'),
            'price_min' => Yii::t('app', "From"),
            'price_max' => Yii::t('app', "To"),
            'date_range' => Yii::t('app', "Dates"),
            'condition' => Yii::t('app', "Condition"),
            //'user_type' => Yii::t('app', "")
        ];
    }
}
