<?php

namespace frontend\models;

use Yii;
use common\models\Message;

/**
 * Description of Conversation
 *
 * @author Timothee
 */
class Conversation extends \yii\base\Model
{
    public $id;
    
    /** @var \common\models\User */
    public $user;
    
    /** @var Message[] */
    protected $messages;
    
    /** @var \common\models\Item */
    public $item;
    
    /** @var \common\models\Booking */
    public $booking;
    
    /** @var Message */
    protected $last_message;
    
    protected $title = '';
    
    protected $date;
    
    public function getTitle()
    {
        if(empty($this->title) && !empty($this->messages)) {
            $this->title = $this->getLastMessage()->subject;
        }
        
        return $this->title;
    }
    
    public function getDate()
    {
        if(empty($this->date) && !empty($this->messages)) {
            $this->date = $this->getLastMessage()->created_at;
        }
        
        return $this->date;
    }
    
    /**
     * 
     * @return Message
     */
    public function getLastMessage()
    {
        if(empty($this->last_message) && $this->messages) {
            $this->last_message = $this->messages[count($this->messages)-1];
        }
        
        return $this->last_message;
    }
    
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;
    }
    
    /**
     * 
     * @return Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    /**
     * 
     * @param integer $userId
     * @return self[]
     */
    public static function getAll($userId = null)
    {
        $conversations = [];
        
        //$ids = Message::find()->select()->where(['author_id' => $user->getPrimaryKey()])->orWhere(['user_id' => $user->getPrimaryKey()])->all();
        
        /* @var $messages Message[] */
        $messages = Message::find()->where(['author_id' => Yii::$app->getUser()->getId()])->orWhere(['user_id' => Yii::$app->getUser()->getId()])->orderBy('created_at ASC')->all();
        
        if($messages)
        {
            foreach($messages as $message)
            {
                if($message->author_id == Yii::$app->getUser()->getId()) {
                    $conversationId = $message->user_id;
                }
                else {
                    $conversationId = $message->author_id;
                }
                
                if(isset($conversations[$conversationId])) {
                    $conversation = $conversations[$conversationId];
                    $conversation->addMessage($message);
                }
                else {
                    $conversation = new self([
                        'id' => $conversationId,
                        'user' => \common\models\User::findOne($conversationId)
                    ]);
                    $conversation->addMessage($message);
                    $conversations[$conversationId] = $conversation;
                }
            }
        }
        
        return $conversations;
    }
    
    /**
     * 
     * @param integer $id
     * @return self
     */
    public static function getOne($id)
    {
        $conversations = self::getAll();
        
        if($conversations) {
            foreach($conversations as $conversation) {
                if($conversation->id == $id) {
                    return $conversation;
                }
            }
        }
        
        return null;
    }
    
}
