<?php

namespace frontend\models;

use Yii;
use DateTime;

/**
 * Description of Booking
 *
 * @author Timothee
 */
class Booking extends \common\models\Booking
{
    const SCENARIO_ITEM = 'item';
    
    public $date_range;
    
    public function behaviors()
    {
        return [
            [
                'class' => \kartik\daterange\DateRangeBehavior::className(),
                'attribute' => 'date_range',
                'dateStartAttribute' => 'date_start',
                'dateStartFormat' => false,//'d/m/Y',
                'dateEndAttribute' => 'date_end',
                'dateEndFormat' => false,//'d/m/Y',
                'separator' => Yii::$app->params['datarange.separator']
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['date_start', 'date_end'], 'required', 'message' => Yii::t('app', "This field is required")],
            [['date_start', 'date_end'], 'date', 'format' => 'php:d/m/Y'],
            [['date_range'], 'required', 'message' => Yii::t('app', "This field is required"), 'on' => self::SCENARIO_ITEM],
            [['date_range'], 'match', 'pattern' => '/^.+\s\ \s.+$/'],
            ['refusal_reason', 'string', 'max' => 255, 'on' => self::SCENARIO_REFUSE],
            ['accept_terms', 'integer', 'on' => self::SCENARIO_CREATE],
            ['accept_terms', 'required', 'requiredValue' => 1, 'message' => Yii::t('app', "You have to accept the terms and conditions"), 'on' => self::SCENARIO_CREATE]
        ];
    }
    
    
    public function attributeInfos()
    {
        return [
            
        ];
    }
    
    public function getAttributeInfo($attribute)
    {
        return \yii\helpers\ArrayHelper::getValue($this->attributeInfos(), $attribute);
    }
    
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        
        $labels['date_range'] = Yii::t('app', "Dates");
        
        return $labels;
    }
}
