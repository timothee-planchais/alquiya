<?php

namespace frontend\assets;

/**
 * Description of OwlAsset
 *
 * @author Timothee
 */
class FullCalendarAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/fullcalendar/dist';
    
    public $css = [
        YII_DEBUG ? 'fullcalendar.css' : 'fullcalendar.min.css'
    ];
    
    public $js = [
        YII_DEBUG ? 'fullcalendar.js' : 'fullcalendar.min.js',
    ];
    
    public $depends = [
        'frontend\assets\MomentAsset'
    ];
}
