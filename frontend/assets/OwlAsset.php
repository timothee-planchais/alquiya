<?php

namespace frontend\assets;

/**
 * Description of OwlAsset
 *
 * @author Timothee
 */
class OwlAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/owl.carousel/dist';
    
    public $css = [
        YII_DEBUG ? 'assets/owl.carousel.css' : 'assets/owl.carousel.min.css'
    ];
    
    public $js = [
        YII_DEBUG ? 'owl.carousel.js' : 'owl.carousel.min.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
