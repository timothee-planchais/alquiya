<?php

namespace frontend\assets;

/**
 * Description of OwlAsset
 *
 * @author Timothee
 */
class MmenuAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/jquery.mmenu/dist';
    
    public $css = [
        'jquery.mmenu.all.css'
    ];
    
    public $js = [
        'jquery.mmenu.all.js'
    ];
    
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
