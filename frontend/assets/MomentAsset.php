<?php

namespace frontend\assets;

/**
 * Description of OwlAsset
 *
 * @author Timothee
 */
class MomentAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/moment';
    
    public $js = [
        'moment.js',
    ];
    
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
