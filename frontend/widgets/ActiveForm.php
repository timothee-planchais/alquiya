<?php

namespace frontend\widgets;

/**
 * Description of ActiveForm
 *
 * @author Timothee
 */
class ActiveForm extends \yii\bootstrap\ActiveForm
{
    
    public $fieldClass = 'frontend\widgets\ActiveField';
    
}
