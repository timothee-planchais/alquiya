<?php

namespace frontend\widgets\grid;

use Yii;
use yii\helpers\Html;

/**
 * Description of ActionColumn
 *
 * @author Timothee
 */
class ActionColumn extends \yii\grid\ActionColumn
{
    public $viewVisible = true;
    public $updateVisible = true;
    public $deleteVisible = true;
    
    public $contentOptions = ['class' => 'action-column'];
    
    /**
     * Initializes the default button rendering callbacks.
     */
    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view']) && $this->viewVisible) {
            $this->buttons['view'] = function ($url, $model, $key) {
                $options = [
                    'title' => Yii::t('yii', 'View'),
                    'aria-label' => Yii::t('yii', 'View'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-info'
                ];
                return Html::a('<span class="fa fa-info-circle"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['update']) && $this->updateVisible) {
            $this->buttons['update'] = function ($url, $model, $key) {
                $options = [
                    'title' => Yii::t('yii', 'Update'),
                    'aria-label' => Yii::t('yii', 'Update'),
                    'data-pjax' => '0',
                    'class' => 'btn btn-default'
                ];
                return Html::a('<span class="fa fa-edit"></span>', $url, $options);
            };
        }
        if (!isset($this->buttons['delete']) && $this->deleteVisible) {
            $this->buttons['delete'] = function ($url, $model, $key) {
                $options = [
                    'title' => Yii::t('yii', 'Delete'),
                    'aria-label' => Yii::t('yii', 'Delete'),
                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                    'class' => 'btn btn-danger'
                ];
                return Html::a('<span class="fa fa-times"></span>', $url, $options);
            };
        }
    }
    
    protected function renderDataCellContent($model, $key, $index)
    {
        return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
            $name = $matches[1];
            if (isset($this->buttons[$name])) {
                $url = $this->createUrl($name, $model, $key, $index);

                if($name == 'view' || $name == 'update' || $name == 'delete') 
                {
                    $attribute = $name.'Visible';
                    if($this->$attribute instanceof \Closure) {
                        $visible = call_user_func($this->$attribute, $url, $model, $key);
                    }
                    else {
                        $visible = $this->$attribute;
                    }
                }
                else {
                    $visible = true;
                }
                if(!$visible) {
                    return '';
                }
                
                return call_user_func($this->buttons[$name], $url, $model, $key);
            } else {
                return '';
            }
        }, $this->template);
    }
    
}
