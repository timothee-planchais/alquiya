<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Html;

/**
 * Description of ActiveField
 *
 * @author Timothee
 */
class ActiveField extends \yii\bootstrap\ActiveField
{
    public $template = "{label} {info}\n{input}\n{hint}\n{error}";
    public $inlineCheckboxListTemplate = "{label} {info}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}\n{hint}";
    public $inlineRadioListTemplate = "{label} {info}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}\n{hint}";
    
    public $infoOptions = ['class' => 'form-info bg-primary', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => ''];
    
    public function render($content = null)
    {
        if ($content === null)
        {
            if (!isset($this->parts['{info}'])) {
                $this->info();
            }
        }
        
        //$this->template .= "\n{info}";
        //$this->template = "{label}\n{beginWrapper}\n{input}\n{hint}\n{info}\n{error}\n{endWrapper}";
        
        return parent::render($content);
    }
    
    public function info($info = null, $options = [])
    {
        if ($info === false) {
            $this->parts['{info}'] = '';
            return $this;
        }
        
        $options = array_merge($this->infoOptions, $options);
        
        $html = '';
        $info = method_exists($this->model, 'getAttributeInfo') && $this->model->getAttributeInfo($this->attribute) ? $this->model->getAttributeInfo($this->attribute) : null;
        
        if($info) 
        {
            $this->options['class'] .= ' has-info';
            $options['title'] = $info;
            $html = Html::tag('span', '?', $options);
        }
        
        $this->parts['{info}'] = $html;
        
        return $this;
    }
}
