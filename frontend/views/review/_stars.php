<?php

/* @var $this \frontend\components\View */
/* @var $note integer */

$roundedNote = round($note);

?>

<span class="stars text-secondary" title="<?= round($note, 1) ?>">
    <?php for($i=1;$i<=5;$i++): ?>
        <span class="★ <?= $roundedNote >= $i ? 'active' : '' ?>"><i class="fa fa-star"></i></span>
    <?php endfor; ?>
</span>
