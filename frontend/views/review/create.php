<?php

use common\models\Review;
use common\models\Booking;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;


/* @var $this frontend\components\View */
/* @var $booking Booking */
/* @var $model common\models\Review */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Add a review');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bookings'), 'url' => ['owner-rents']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="review">

    <?php $form = ActiveForm::begin(Yii::$app->params['bootstrapFormConfig']); ?>
    
    <div class="form-group field-review-note required">
        
        <label class="control-label col-sm-3" for="review-note"><?= $model->getAttributeLabel('note') ?></label>
        
        <div class="col-sm-9">
            <div id="stars" class="text-center">
                <?php for($i=5;$i>=1;$i--): ?>
                    <span class="★" data-value="<?= $i ?>" title="<?= $i ?>"><i class="fa fa-star"></i></span>
                <?php endfor; ?>
            </div>
    
            <?= $form->field($model, 'note', ['template' => "{input}\n{error}", 'options' => ['class' => 'text-left']])->hiddenInput() ?>
        </div>

    </div>

    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>

    <div class="text-right">
        <?= Html::submitButton(Yii::t('app', 'Publish'), ['class' => 'btn btn-lg btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        $('#review #stars .★').on('click', function(){
            var $note = $(this).data('value');
            $('#review #stars .★').removeClass('active');
            $('#review #stars .★[data-value="'+$note+'"]').addClass('active');
            $('#review-note').val($note);
        });
        
    });
    
</script>
<?php $this->endBlock(); ?>