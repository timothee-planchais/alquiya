<?php

use common\models\Review;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this frontend\components\View */
/* @var $review Review */
/* @var $fromItemView bool */

$author = $review->author;
        
$item = $review->booking->item;

?>

<?php  ?>
<div class=" review <?= !empty($fromItemView) ? 'item-review' : '' ?> media">
    <div class="media-left">
        <img src="<?= Url::to($author->getImageUrl('300x300')) ?>" alt="<?= $author ?>" class="media-object img-circle"/>
    </div>
    <div class="media-body">
        <div class="review-header">
            <span class="review-date text-sanfrancisco pull-right"><?= Yii::$app->formatter->asDate($review->created_at, 'long') ?></span>
            <p><a class="review-author text-sanfrancisco" href="<?= Url::to($author->getProfileUrl()) ?>"><?= Html::encode($author) ?></a></p>
            <p class="review-note"><?= $this->render('/review/_stars', ['note' => $review->note]) ?></p>
        </div>
        <p class="review-text"><?= nl2br(Html::encode($review->text)) ?></p>
        <?php if(empty($fromItemView) && !empty($item)): ?>
            <p class="review-item">
                <?= Yii::t('app', "About the product") ?> <a href="<?= Url::to($item->getUrl()) ?>"><?= Html::encode($item->title) ?></a>
            </p>
            <?php endif; ?>
    </div>
    <div class="media-right text-right">
    </div>
</div>