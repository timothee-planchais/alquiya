<?php

/* @var $this \frontend\components\View */
/* @var $user \common\models\User */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
\frontend\assets\MmenuAsset::register($this);

$user = null;
if(!Yii::$app->user->isGuest) {
    $user = Yii::$app->getUser()->getIdentity();
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head itemscope itemtype="http://schema.org/WebSite" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139554735-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-139554735-1');
    </script>
    <meta name="google-site-verification" content="9gIARf1icosDuVW2YWzmO0aa0ozHAQ86Netm_lmDGXw" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="canonical" href="<?= Yii::$app->getRequest()->getAbsoluteUrl() ?>" itemprop="url"/>
    <link rel="icon" href="<?= Url::to('@web/img/favicon.png') ?>">
    <title itemprop="name"><?= 'AlquiYa - '.Html::encode($this->title) ?></title>
    <meta name="description" content="Alquiler de productos entre particularios">
    <meta name="keywords" content="alquiler,productos,particularios,gratis">
    <meta property="og:site_name" content="AlquiYa" />
    <meta property="og:url" content="<?= Url::current([], true) ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?= 'AlquiYa - '.Html::encode($this->title) ?>" />
    <meta property="og:description" content="Alquiler de productos entre particularios. Gana dinero alquiando tus objetos. Alquila el producto que necesites a otras personas"/>
    <meta property="og:image" content="<?= Url::to('@web/img/logo.png', true) ?>" />
    <meta property="og:locale" content="<?= Yii::$app->language ?>" />
    <meta property="fb:app_id" content="<?= Yii::$app->params['facebook.app_id'] ?>" />
    <meta property="fb:admins" content="1450723444" />
    <link rel="publisher" href="<?= Yii::$app->params['facebook.url'] ?>" />
    <meta name="twitter:widgets:csp" content="on">
    <meta name="twitter:site" content="@<?= Yii::$app->params['twitter.account'] ?>">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>

<body class="<?= str_replace('/','_',Yii::$app->controller->action->getUniqueId()) ?>">
    
<?php $this->beginBody() ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.12&appId=<?= Yii::$app->params['facebook.app_id'] ?>&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrap">
    
    <header id="header">
        <!--<a href="#categories-mobile" id="categories-mobile-toggle" class="fa fa-bars text-primary categories-mobile-toggle"></a>-->
        <a href="#categories-mobile" id="categories-mobile-toggle" class="btn btn-secondary categories-mobile-toggle"><i class="fa fa-bars"></i> <?= Yii::t('app', "Categories") ?></a>
        <a href="#wrap" id="categories-mobile-toggle-close" class="btn btn-secondary categories-mobile-toggle"><i class="fa fa-close"></i></a>
        <?= $this->render('_menu', ['user' => $user]) ?>
    </header>
    
    <div id="page-top" class="container">

        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        
        <?= Alert::widget() ?>
        
        <?php if($this->showTitle): ?>
            <h1 id="title" class="text-primary"><?= Html::encode($this->title) ?></h1>
        <?php endif; ?>

    </div>
    
    <div id="content" class="<?= $this->usePageContent ? 'page-content' : '' ?>">
        <div class="<?= $this->useContainer ? 'container' : '' ?>">
            <?= $content ?>
        </div>
    </div>
</div>

<?= $this->render('_footer', ['user' => $user]) ?>

<?php $this->endBody() ?>
    
<?php if (isset($this->blocks['js'])): ?>
    <?= $this->blocks['js'] ?>
<?php endif; ?>
    
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    
</body>
</html>
<?php $this->endPage() ?>
