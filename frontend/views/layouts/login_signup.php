<?php

use yii\helpers\Url;
use yii\bootstrap\Nav;

/* @var $this \frontend\components\View */
/* @var $content string */

$this->usePageContent = false;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div id="login_signup">

        <p class="text-right" style="margin-bottom:15px;text-decoration:underline;">
            <?php if(Yii::$app->controller->action->id == 'login'): ?>
                <a href="<?= Url::to(['signup']) ?>"><?= Yii::t('app', "I don't have an account, register") ?></a>
            <?php else: ?>
                <a href="<?= Url::to(['login']) ?>"><?= Yii::t('app', "I already have an account, signin") ?></a>
            <?php endif; ?>
        </p>
            
        <?= yii\bootstrap\Nav::widget([
            'items' => [
                [
                    'label' => Yii::t('app', "Login"),
                    'url' => ['/site/login']
                ],
                [
                    'label' => Yii::t('app', "Signup"),
                    'url' => ['/site/signup'],
                ]
            ],
            'options' =>  ['class' => 'nav-tabs']
        ]) ?>
        <div class="tab-content">
            <?= $content ?>
        </div>
        
    </div>

<?php $this->endContent() ?>