<?php

use yii\bootstrap\Nav;

/* @var $this \frontend\components\View */
/* @var $content string */

$this->usePageContent = false;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div id="account">

        <?= yii\bootstrap\Nav::widget([
            'items' => [
                [
                    'label' => Yii::t('app', "My account"),
                    'url' => ['/account/index'],
                    'options' => ['class' => 'hidden-xs']
                ],
                [
                    'label' => Yii::t('app', "Edit my informations"),
                    'url' => ['/account/update']
                ],
                [
                    'label' => Yii::t('app', "My items"),
                    'url' => ['/account/item/index'],
                ],
                [
                    'label' => Yii::t('app', "Bookings"),
                    'url' => ['/booking/owner-rents'],
                    'options' => ['class' => 'hidden-xs']
                ],
                [
                    'label' => Yii::t('app', "Messages"),
                    'url' => ['/account/message/index'],
                    'options' => ['class' => 'hidden-xs']
                ],
                [
                    'label' => Yii::t('app', "Subscriptions"),
                    'url' => ['/account/subscription/index'],
                ],
                [
                    'label' => Yii::t('app', "Orders"),
                    'url' => ['/account/order/index'],
                ],
            ],
            'options' =>  ['class' => 'nav-tabs']
        ]) ?>
        <div class="tab-content">
            <?= $content ?>
        </div>
        
    </div>

<?php $this->endContent() ?>