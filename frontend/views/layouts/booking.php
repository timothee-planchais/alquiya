<?php

use yii\bootstrap\Nav;

/* @var $this \frontend\components\View */
/* @var $content string */

$this->usePageContent = false;

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div id="rents">

        <?= yii\bootstrap\Nav::widget([
            'items' => [
                [
                    'label' => Yii::t('app', "As owner"),
                    'url' => ['booking/owner-rents']
                ],
                [
                    'label' => Yii::t('app', "As renter"),
                    'url' => ['booking/rents'],
                ]
            ],
            'options' =>  ['class' => 'nav-tabs']
        ]) ?>
        <div class="tab-content">
            <?= $content ?>
        </div>
        
    </div>

<?php $this->endContent() ?>