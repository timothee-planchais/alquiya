<?php

use common\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Menu;

/* @var $this frontend\components\View */
/* @var $user \common\models\User */

/**
 * @param Category[] $categories
 */
function getRecursiveItems($categories)
{
    $items = [];
    
    if($categories)
    {
        foreach($categories as $j => $category) 
        {
            if($category->menu_active)
            {
                /*if($category->position == 1 && !empty($category->parent_id)) {
                    $items[] = [
                        'label' => Yii::t('app', "See all"),
                        'url' => '#'
                    ];
                }*/
                
                $item = [
                    'label' => $category->title,
                    'url' => $category->getUrl()
                ];

                if($category->menu_subCategories) {
                    $item['items'] = getRecursiveItems($category->menu_subCategories);
                }

                $items[] = $item;
            }
        }
    }
    
    return $items;
}

$curUrl = '';

if ($this->beginCache('menu_categories', ['duration' => 7*24*3600])) 
{

?>


    <?php if($this->bigCategories): ?>

        <div id="categories" class="hidden-xs bg-primary">
            <div class="container">
                
                <ul class="nav nav-pills">

                    <?php foreach($this->bigCategories as $category): ?>

                        <?php 
                        $category->menu_subCategories = $category->getSubCategories()->where(['active'=>1,'menu_active'=>1])->orderBy('position ASC')->all(); 
                        ?>

                        <li class="<?= $category->menu_subCategories ? 'has-submenu' : '' ?>">
                            <a href="<?= Url::to($category->getUrl()) ?>"><?= $category->title ?></a>

                            <?php if($category->menu_subCategories): ?>

                                <ul class="submenu">

                                    <div class="row">

                                        <?php foreach($category->menu_subCategories as $i => $subCategory): ?>
                                            <?php $subCategory->menu_subCategories = $subCategory->getSubCategories()->where(['active'=>1,'menu_active'=>1])->orderBy('position ASC')->all(); ?>
                                            <?php if(($i) % 5 == 0): ?>
                                                <!--</div><div class="row">-->
                                            <?php endif; ?>
                                            <div class="col-sm-3 col-md-2 <?= $i+1 ?>">
                                                <li>
                                                    <a href="<?= Url::to($subCategory->getUrl()) ?>"><?= $subCategory->title ?></a>
                                                    <?php if($subCategory->menu_subCategories): ?>
                                                        <ul>
                                                            <?php foreach($subCategory->menu_subCategories as $subCategory2): ?>
                                                                <li>
                                                                    <a href="<?= Url::to($subCategory2->getUrl()) ?>" title="<?= $subCategory2->title ?>"><?= $subCategory2->title ?></a>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </li>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>

                                </ul>

                            <?php endif; ?>

                        </li>

                    <?php endforeach; ?>
                </ul>

            </div>
        </div>

        <nav id="categories-mobile">
            <?= Menu::widget([
                    'options' => ['class' => ''],
                    'items' => getRecursiveItems($this->bigCategories),
                ]);
            ?>
        </nav>

    <?php endif; ?>

<?php

$this->endCache();

}

?>