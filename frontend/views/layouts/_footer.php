<?php

use common\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $categories Category[] */

?>

<footer id="footer">
    <div class="container">
        
        <div class="row">
            
            <!--<div class="col-xs-6 col-sm-4 col-md-3">
                
                <h3 class="text-primary text-sanfrancisco"><?= Yii::t('app', "Top cities") ?></h3>
                
                <ul>
                    <?php foreach(Yii::$app->params['footer.cities'] as $city => $coords): ?>
                        <li><a href="<?= Url::to(['/item/index', 'SearchItem' => ['lat' => $coords['lat'], 'lng' => $coords['lng'], 'radius' => $coords['radius']]]) ?>"><?= $city ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>-->
            
            <div class="col-xs-6 col-sm-4 col-md-3">
                
                <h3 class="text-primary text-sanfrancisco"><?= Yii::t('app', "Informations") ?></h3>
                
                <ul>
                    <li><a href="<?= Url::to(['page/contact']) ?>"><?= Yii::t('app', "Contact") ?></a></li>
                    <li><a href="<?= Url::to(['page/about']) ?>"><?= Yii::t('app', "About") ?></a></li>
                    <li><a href="<?= Url::to(['page/help']) ?>"><?= Yii::t('app', "Help") ?></a></li>
                    <li><a href="<?= Url::to(['/page/terms']) ?>"><?= Yii::t('app', "Terms and Conditions") ?></a></li>
                    <li><a href="<?= Url::to(['/page/privacy']) ?>"><?= Yii::t('app', "Privacy") ?></a></li>
                </ul>
                
            </div>
            
            <div class="col-xs-6 col-sm-4 col-md-3">
                
                <h3 class="text-primary text-sanfrancisco"><?= Yii::t('app', "Servicies") ?></h3>
                
                <ul>
                    <li><a href="<?= Url::to(['/account/item/create']) ?>"><?= Yii::t('app', "Publish a product") ?></a></li>
                    <!--<li><a href="<?= Url::to(['/account/item/index']) ?>"><?= Yii::t('app', "My items") ?></a></li>-->
                    <li><a href="<?= Url::to(['/item/index']) ?>"><?= Yii::t('app', "All the items") ?></a></li>
                    <li><a href="#"><?= Yii::t('app', "Rent more often") ?></a></li>
                    <li><a href="<?= Url::to(['/page/pro']) ?>"><?= Yii::t('app', "AlquiYa for companies") ?></a></li>
                    <li><a href="<?= Url::to(['/page/rent-contract']) ?>"><?= Yii::t('app', "Rent contract") ?></a></li>
                </ul>
                
            </div>
            
            <div class="col-xs-6 col-sm-4 col-md-3">
                
                <h3 class="text-primary text-sanfrancisco"><?= Yii::t('app', "Categories") ?></h3>
                
                <?php $categories = common\models\Category::find()->where(['active' => 1])->andWhere('parent_id IS NULL')->orderBy('position ASC')->all(); ?>
                
                <?php if($categories): ?>
                
                    <ul>
                    <?php foreach($categories as $category): ?>
                        <li><a href="<?= Url::to($category->getUrl()) ?>"><?= Html::encode($category->title) ?></a></li>
                    <?php endforeach; ?>
                    </ul>
                
                <?php endif; ?>
                
            </div>
        
            <div class="col-xs-6 col-sm-4 col-md-3">
            
                <h3><?= Yii::t('app', "Follow us") ?></h3>
                
                <ul>
                    <li id="footer-follow">
                        <a href="<?= Yii::$app->params['facebook.url'] ?>"><i class="fa fa-facebook-f"></i></a> &nbsp;
                        <a href="<?= Yii::$app->params['instagram.url'] ?>"><i class="fa fa-instagram"></i></a> &nbsp;
                        <a href="<?= Yii::$app->params['twitter.url'] ?>"><i class="fa fa-twitter"></i></a>
                    </li>
                </ul>
                
                <!--<ul style="margin-top:15px;">
                    <li><?= Html::dropDownList('country', 'COL', Yii::$app->params['countries'], ['class' => 'form-control']) ?></li>
                </ul>-->

            </div>
        
        </div>
        
        <p class="text-center">Copyright © 2018-<?= date('Y') ?> AlquiYa</p>
            
    </div>
</footer>