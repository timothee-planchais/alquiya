<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this frontend\components\View */
/* @var $user \common\models\User */

NavBar::begin([
    'brandLabel' => false,//Yii::$app->name,
    'brandImage' => '@web/img/logo.png',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'id' => 'nav', 
        'class' => 'navbar-default',
    ],
    'screenReaderToggleText' => null
]);

/*
 * ITEMS
 */
$rightMenuItems = [
    ['label' => Yii::t('app', "Home"), 'url' => Yii::$app->homeUrl, 'options' => ['class' => 'visible-xs']],
    ['label' => '<i class="fa fa-plus"></i> '.Yii::t('app', "Publish a product"), 'url' => ['/account/item/create'], 'options' => ['id' => 'li-publish'], 'linkOptions' => ['class' => 'bg bg-secondary']]
];

/*
 * Languages
 */
$languagesItems = [];
$languages = Yii::$app->params['languages'];
$curLanguage = '';
$curLanguageText = '';

foreach($languages as $lg => $name) {
    if($lg == Yii::$app->language) {
        $curLanguage = $lg;
        $curLanguageText = $languages[$lg];
    }
    else {
        $languagesItems[] = ['label' => '<img src="'.Url::to('@web/img/flags/'.$lg.'.png').'" alt="'.$lg.'" title="'.$name.'"/> <span>'.$name.'</span>', 'url' => Url::current(['lg' => $lg])];
    }
}

/*
 * 
 */
if(Yii::$app->user->isGuest) 
{
    //$rightMenuItems[] = ['label' => Yii::t('app', "Signup"), 'url' => ['/site/signup'], 'options' => ['id' => 'li-signup']];
    $rightMenuItems[] = ['label' => Yii::t('app', "Login or signup"), 'url' => ['/site/login'], 'options' => ['id' => 'li-login']];
} 
else 
{
    /*
     * NOTIFICATIONS
     */
    $nbNotificiations = $user->getCountUnreadNotifications();
    $notifications = $user->getNotifs(null);
    
    $notifsItems = [];
    if($notifications) {
        foreach($notifications as $notification) {
            $notifsItems[] = [
                'label' => $notification->getText().'<small class="text-right text-muted">'.$notification->getNiceDate().'</small>',
                'url' => Url::to($notification->getUrl()), 
                'options' => ['data-id' => $notification->id, 'class' => 'notification '.($notification->is_read ? 'read' : 'unread')]];
        }
        if(count($notifications) >= 10) {
            $notifsItems[] = '<li id="notifications-load"><button class="btn btn-primary btn-block"><i class="fa fa-refresh"></i> Load more</button></li>';
        }
    }
    
    $rightMenuItems[] = [
        'options' => ['id' => 'li-notifications'],
        'label' => '<i class="fa fa-bell"></i> '.($nbNotificiations>0 ? '<span class="label label-primary text-sanfrancisco">'.$nbNotificiations.'</span>' : ''),
        'items' => $notifsItems
    ];
    
    /*
     * USER
     */
    $rightMenuItems[] = [
        'label' => Html::img($user->getImageUrl('300x300')).' '.Html::encode($user->__toString()), 
        'items' => [
            ['label' => '<i class="fa fa-user"></i> '.Yii::t('app', "Account"), 'url' => ['/account/index']],
            ['label' => '<i class="fa fa-list"></i> '.Yii::t('app', "My items"), 'url' => ['/account/item/index']],
            ['label' => '<i class="fa fa-calendar-check-o"></i> '.Yii::t('app', "Bookings"), 'url' => ['/booking/owner-rents']],
            ['label' => '<i class="fa fa-list"></i> '.Yii::t('app', "Subscriptions"),'url' => ['/account/subscription/index']],
            [
                'label' => '<i class="fa fa-envelope-o"></i> '.Yii::t('app', "My messages").( $this->nbNewMessages > 0 ? ' <span class="badge bg-secondary">'.$this->nbNewMessages.'</span>' : ''), 
                'url' => ['/account/message/index']
            ],
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                '<i class="fa fa-sign-out"></i> '.Yii::t('app', 'Logout'),
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        ],
        'options' => ['id' => 'li-user']
    ];
}

/*$rightMenuItems [] = [
    'options' => ['id' => 'li-lg'],
    'label' => '<img src="'.Url::to('@web/img/flags/'.$curLanguage.'.png').'" alt="'.$curLanguageText.'" title="'.$curLanguageText.'"/>',
    'items' => $languagesItems
];*/

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $rightMenuItems,
    'encodeLabels' => false
]);
NavBar::end();

?>

<?= $this->render('_menu_categories') ?>