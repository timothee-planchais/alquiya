<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\WebUser;
use yii\bootstrap\ActiveForm;

/* @var $this \frontend\components\View */
/* @var $booking Booking */

$this->title = Yii::t('app', "Details booking").' '.$booking;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Bookings"), 'url' => ['/booking/owner-rents']];
$this->params['breadcrumbs'][] = $this->title;

$item = $booking->item;

$renterUser = $booking->user;
$ownerUser = $item->user;

$isRenter = false;
if($renterUser->getPrimaryKey() == Yii::$app->user->getId()) {
    $isRenter = true;
    $user = $ownerUser;
}
else {
    $user = $renterUser;
}

?>

<div id="booking">
    
    <div class="row">
    
        <div class="col-xs-12 col-sm-7 col-md-8">
        
            <h2 class="text-primary"><?= Yii::t('app', "Summary of your booking") ?></h2>

            <p>
                
                <strong><?= Yii::t('app', "Status") ?> :</strong> <?= $booking->getTextStatus() ?> &nbsp;
                
                <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_CANCEL, ['booking' => $booking])): ?>
            
                    <a href="<?= Url::to(['cancel', 'id' => $booking->id]) ?>" class="btn btn-danger">
                        <i class="fa fa-times"></i> <?= Yii::t('app', "Cancel") ?>
                    </a>

                <?php endif; ?>

                <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_VALIDATE, ['booking' => $booking])): ?>

                    <a href="<?= Url::to(['validate', 'id' => $booking->id]) ?>" class="btn btn-success" data-method="post" data-confirm="<?= Yii::t('yii', 'Are you sure you want to validate the booking ?') ?>">
                        <i class="fa fa-check"></i> <?= Yii::t('app', "Validate") ?>
                    </a>

                <?php endif; ?>
                
                <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_REFUSE, ['booking' => $booking])): ?>

                    <a href="<?= Url::to(['refuse', 'id' => $booking->id]) ?>" class="btn btn-danger">
                        <i class="fa fa-times"></i> <?= Yii::t('app', "Refuse") ?>
                    </a>

                <?php endif; ?>
                    
            </p>
            
            <?php if($booking->refusal_reason): ?>
                <p><strong><?= Yii::t('app', "Reason of refusal") ?> :</strong> <?= $booking->refusal_reason ?></p>
            <?php endif; ?>

            <p><strong><?= Yii::t('app', "Booking") ?> :</strong> <?= $booking->getFromTo() ?></p>

            <?php /* VIEW RENTER */ ?>
            <?php if($isRenter): ?>

                <p><strong><?= Yii::t('app', "Owner") ?> :</strong> <?= Html::a(Html::encode($item->user->getFullName()), $item->user->getProfileUrl()) ?></p>

                <p>
                    <strong><?= Yii::t('app', "Address") ?> :</strong> 
                    
                    <?php /*if($booking->status == Booking::STATUS_VALIDATED):*/ ?>
                        <?php /* Html::encode($item->address) */ ?>
                    <?php /*else:*/ ?>
                        <?= Html::encode($item->short_address) ?><br/>
                        <!--<span class="text-muted"><?= Yii::t('app', "The exact address will be shown when the owner will validate the booking") ?></span>-->
                    <?php /*endif;*/ ?>
                    
                </p>

            <?php /* VIEW OWNER */ ?>
            <?php else: ?>

                <p><strong><?= Yii::t('app', "Renter") ?> :</strong> <?= Html::a(Html::encode($booking->user->getFullName()), $booking->user->getProfileUrl()) ?></p>

                <p><strong><?= Yii::t('app', "Address") ?> :</strong> <?= Html::encode($booking->user->getOfficialAddress()) ?></p>

            <?php endif; ?>
                
        </div>
        
        <div class="col-xs-12 col-sm-5 col-md-4">
            
            <h2 class="text-primary"><?= Yii::t('app', "Contact {user}", ['user' => $user->username]) ?></h2>

            <div id="item-chat" class="item-bloc">
                <?php 

                $message = new common\models\Message([
                    'subject' => null
                ]);
                $template = "{input}\n{hint}\n{error}";
                ?>

                <?php $form = ActiveForm::begin(['action' => ['/account/message/send', 'userId' => $user->getPrimaryKey(), 'bookingId' => $booking->getPrimaryKey()], 'method' => 'POST']) ?>

                <?= $form->field($message, 'subject', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $message->subject]) ?>

                <?= $form->field($message, 'message', ['template' => $template])->textarea(['maxlength' => true, 'placeholder' => Yii::t('app', "Ask {user} any questions about the booking", ['user' => $user])]) ?>

                <p class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-send-o"></i> &nbsp;<?= Yii::t('app', "Send") ?></button>
                </p>

                <?php $form->end() ?>

            </div>
                
        </div>

    </div>        
           
    <h2 class="text-primary"><?= Yii::t('app', "The product") ?></h2>
    
    <div class="table-responsive" style="margin-top:15px;">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><?= Yii::t('app', "Item") ?></th>
                    <?php if(!empty($showDeposit)): ?>
                        <th><?= Yii::t('app', "Deposit") ?></th>
                    <?php endif; ?>
                    <th><?= Yii::t('app', "Price per day") ?></th>
                    <th><?= Yii::t('app', "Duration") ?></th>
                    <th><?= Yii::t('app', "Total price") ?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <a href="<?= Url::to($item->getUrl()) ?>">
                            <?= Html::img($item->getImageUrl('300x200'), ['width' => 80]) ?>
                            <?= html::encode($item->title) ?>
                        </a>
                    </td>
                    <td><?= $booking->deposit_value ? Yii::$app->formatter->asCurrency($booking->deposit_value) : '' ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($booking->getDailyPrice()) ?></td>
                    <td><?= Yii::t('app', '{nb} days', ['nb' =>$booking->getDuration()]) ?></td>
                    <td><?= Yii::$app->formatter->asCurrency($booking->getTotalPrice()) ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    
</div>