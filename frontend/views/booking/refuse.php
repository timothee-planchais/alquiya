<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\WebUser;
use yii\bootstrap\ActiveForm;

/* @var $this \frontend\components\View */
/* @var $booking Booking */

$this->title = Yii::t('app', "Refuse booking").' '.$booking;

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Bookings"), 'url' => ['/booking/owner-rents']];
$this->params['breadcrumbs'][] = ['label' => $booking->getNum(), 'url' => ['/booking/view', 'id' => $booking->getPrimaryKey()]];
$this->params['breadcrumbs'][] = $this->title;

$item = $booking->item;

$duration = $booking->getDuration();

$renterUser = $booking->user;
$ownerUser = $item->user;

$isRenter = false;
if($renterUser->getPrimaryKey() == Yii::$app->user->getId()) {
    $isRenter = true;
    $user = $ownerUser;
}
else {
    $user = $renterUser;
}

?>

<div id="refuse-booking">
    
    <p class="clearfix">
        <a href="<?= Url::to(['/booking/view', 'id' => $booking->getPrimaryKey()]) ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> <?= Yii::t('app', "Return") ?></a>
    </p>
    
    <hr/>
    
    <!--<p class="alert alert-info">
        <i class="fa fa-info-circle"></i> 
    </p>-->
    
    <p><strong><?= Yii::t('app', "Dates") ?> :</strong> <?= $booking->getFromTo() ?></p>
    
    <p><strong><?= Yii::t('app', "Renter") ?> :</strong> <?= Html::a(Html::encode($booking->user), $booking->user->getProfileUrl()) ?></p>
    
    <p><strong><?= Yii::t('app', "Address") ?> :</strong> <?= Html::encode($booking->user->getOfficialAddress(', ')) ?></p>
    
    <?php $form = ActiveForm::begin() ?>
    
        <?= $form->field($booking, 'refusal_reason')->textarea(['maxlength' => true])->label(Yii::t('app', 'Reason of refusal')) ?>
    
        <p class="text-right">
            <button type="submit" class="btn btn-danger btn-lg"><i class="fa fa-times"></i> <?= Yii::t('app', "Refuse booking") ?></button>
        </p>

    <?php $form->end() ?>
    
</div>