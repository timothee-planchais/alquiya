<?php

use frontend\models\Booking;
use common\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\WebUser;
use yii\bootstrap\ActiveForm;

/* @var $this \frontend\components\View */
/* @var $model Booking */
/* @var $item Item */
/* @var $form ActiveForm */

$this->usePageContent = false;

$renterUser = $model->user;
$ownerUser = $item->user;

if($model->isNewRecord) 
{
    $this->title = Yii::t('app', "Book {item}", ['item' => $item->title]);
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rent of').' '.$item->title, 'url' => $item->getUrl()];
    $this->params['breadcrumbs'][] = $this->title;
    $user = $item->user;
}
else 
{
    $this->title = Yii::t('app', "Details booking").' '.$model;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Bookings"), 'url' => ['/booking/owner-rents']];
    $this->params['breadcrumbs'][] = $this->title;

    $isRenter = false;
    if($renterUser->getPrimaryKey() == Yii::$app->user->getId()) {
        $isRenter = true;
        $user = $ownerUser;
    }
    else {
        $user = $renterUser;
    }
}

$url = Url::to($item->getUrl());

?>

<div id="item-booking" class="row">
    
    <div class="col-xs-12 col-sm-7 col-md-8">
        
        <?php if($model->isNewRecord): ?>
            <p class="clearfix" style="margin-bottom:20px;">
                <a href="<?= Url::to($item->getUrl()) ?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> <?= Yii::t('app', "Cancel") ?></a>
            </p>
        <?php endif; ?>
            
        <!-- BOOKING STATUS -->
        <?php if(!$model->isNewRecord): ?>
        
            <div id="booking-status" class="well">
                <h2 class="text-primary"><?= Yii::t('app', "Status") ?></h2>
                <p id="booking-status-text"><span class="text-<?= $model->getStatusColor() ?>"><?= $model->getTextStatus() ?> </span></p>

                <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_CANCEL, ['booking' => $model])): ?>

                    <p><a href="<?= Url::to(['cancel', 'id' => $model->id]) ?>" class="btn btn-danger"><i class="fa fa-times"></i> <?= Yii::t('app', "Cancel") ?></a></p>

                <?php endif; ?>

                <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_VALIDATE, ['booking' => $model])): ?>

                    <p><a href="<?= Url::to(['validate', 'id' => $model->id]) ?>" class="btn btn-success" data-method="post" data-confirm="<?= Yii::t('app', 'Are you sure you want to validate the booking ?') ?>"><i class="fa fa-check"></i> <?= Yii::t('app', "Validate") ?></a></p>

                <?php endif; ?>

                <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_REFUSE, ['booking' => $model])): ?>

                    <p><a href="<?= Url::to(['refuse', 'id' => $model->id]) ?>" class="btn btn-danger"><i class="fa fa-times"></i> <?= Yii::t('app', "Refuse") ?></a></p>

                <?php endif; ?>

                <?php if($model->refusal_reason): ?>
                    <h2 class="text-primary"><?= Yii::t('app', "Reason of cancellation") ?></h2>
                    <p><?= nl2br(Html::encode($model->refusal_reason)) ?></p>
                <?php endif; ?>
                    
                <?php if($model->status == Booking::STATUS_VALIDATED || $model->status == Booking::STATUS_PENDING): ?>
                    <hr/>
                    <p id="booking-download-contract">
                        <?= Yii::t('app', "Rent contract") ?> : &nbsp;&nbsp;
                        <a href="<?= Url::to('@web/file/contrato-alquiler-alquiya.pdf') ?>" class="btn btn-secondary" target="_blank"><i class="fa fa-download"></i> <?= Yii::t('app', "Download") ?></a>
                    </p>
                <?php endif; ?>
                    
                <?php if($model->status == Booking::STATUS_COMPLETED && Yii::$app->getUser()->can(WebUser::PERMISSION_BOOKING_REVIEW, ['booking' => $model])): ?>
                    <hr/>
                    <p><a href="<?= Url::to(['/booking/review', 'id' => $model->getPrimaryKey()]) ?>" class="btn btn-secondary"><i class="fa fa-star-half-full"></i> Evalúo la reservación</a></p>
                <?php endif; ?>
                    
            </div>
        <?php endif; ?>
            
        <div class="page-content well">

            <div id="item-booking-top" class="media">
                <div class="media-left"><a href="<?= $url ?>"><img src="<?= Url::to($item->getImageUrl('300x200')) ?>" class="media-object" width="200" alt="<?= $item->title ?>"/></a></div>
                <div class="media-body">
                    <h2 class="text-primary"><a href="<?= $url ?>" class="text-primary"><?= Html::encode($item->title) ?></a></h2>
                    <div class="table">
                        <div class="table-cell">
                            <h3 class="text-primary"><?= Yii::t('app', "Delivery") ?></h3>
                            <p class="text-sanfrancisco"><?= ucfirst(Yii::$app->formatter->asDate($model->oStart, 'full')) ?></p>
                        </div>
                        <div class="table-cell empty"></div>
                        <div class="table-cell">
                            <h3 class="text-primary"><?= Yii::t('app', "Return") ?></h3>
                            <p class="text-sanfrancisco"><?= ucfirst(Yii::$app->formatter->asDate($model->oEnd, 'full')) ?></p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h3 class="text-primary"><?= Yii::t('app', "Contact information") ?></h3>
                    <p class="text-sanfrancisco"><a href="<?= Url::to($user->getProfileUrl()) ?>"><?= Html::encode($user->__toString()) ?></a></p>
                    <?php /*if($user->show_email): ?>
                        <p><u><a href="mailto:<?= $user->email ?>" class="text-sanfrancisco"><?= Html::encode($user->email) ?></a></u></p>
                    <?php endif;*/ ?>
                    <?php /*if($user->show_phone && $user->getPhoneNumber()): ?>
                        <p class="text-sanfrancisco"><?= Html::encode($user->getPhoneNumber()) ?></p>
                    <?php endif;*/ ?>
                    <!--<p class="text-sanfrancisco"><?= html::encode($item->short_address) ?></p>-->
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="text-primary"><?= Yii::t('app', "See map") ?></h3>
                    <div id="item-map-img" class="map-img map-squarred map-small">
                        <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?= $item->lat.','.$item->lng ?>&zoom=15&size=400x200&scale=2&maptype=roadmap&language=<?= Yii::$app->language ?>&key=<?= Yii::$app->params['google.api_key'] ?>" class="img-responsive" alt=""/>
                    </div>
                </div>
            </div>

            <!-- CREATE BOOKING -->
            <?php if($model->isNewRecord): ?>
            
                <?php $form = ActiveForm::begin() ?>

                    <?= $form->field($model, 'accept_terms')->checkbox() ?>

                    <p class="text-center">
                        <button type="submit" class="btn btn-secondary btn-lg btn-block text-uppercase"><?= Yii::t('app', "Send reservation request") ?></button>
                    </p>

                <?php $form->end() ?>
            <?php endif; ?>
                    
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-5 col-md-4 right">
        
        <div class="well">
            <div id="item-booking-details">
                <h2 class="text-primary"><?= Yii::t('app', "Total costs") ?></h2>
                <table id="booking-details" class="table">
                    <tr id="booking-duration">
                        <td id="booking-duration-days">
                            <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price) ?></span> x 
                            <span class="value text-sanfrancisco"><?= $model->getDuration() ?></span> <?= Yii::t('app', $model->getDuration() == 1 ? 'day' : 'days') ?></td>
                        <td id="booking-duration-price" class="text-right text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($model->getDurationPrice()) ?></td>
                    </tr>
                    <?php if($model->getDiscountAmount()): ?>
                        <tr id="booking-discount">
                            <td>Descuento</td>
                            <td class="text-right value text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($model->getDiscountAmount()) ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr id="booking-total">
                        <td><?= strtoupper(Yii::t('app', "Total")) ?></td>
                        <td class="text-right text-sanfrancisco value"><?= Yii::$app->formatter->asCurrency($model->getTotalPrice()) ?></td>
                    </tr>
                </table>
            </div>
        </div>
        
        <?php if($model->isNewRecord): ?>
            <div id="item-booking-steps" class="page-content well">
                <h2 class="text-primary"><?= Yii::t('app', "How to proceed ?") ?></h2>

                <h3 class="text-secondary"><?= Yii::t('app', "booking_step_2_title") ?></h3>

                <p><?= Yii::t('app', 'booking_step_2_txt') ?></p>

                <h3 class="text-secondary"><?= Yii::t('app', "booking_step_3_title") ?></h3>

                <p><?= Yii::t('app', 'booking_step_3_txt') ?></p>

                <h3 class="text-secondary"><?= Yii::t('app', "booking_step_4_title") ?></h3>

                <p><?= Yii::t('app', 'booking_step_4_txt') ?></p>

                <h3 class="text-secondary"><?= Yii::t('app', "booking_step_5_title") ?></h3>

                <p><?= Yii::t('app', 'booking_step_5_txt', ['url' => Url::to(['/page/contact'])]) ?></p>

            </div>
        
        <?php else: ?>
        
            <div id="item-chat" class="well">

                <h2 class="text-primary"><?= 'Contactar '.$user->username ?></h2>

                <?php 

                $message = new common\models\Message([
                    'subject' => null
                ]);
                $template = "{input}\n{hint}\n{error}";
                ?>

                <?php $form = ActiveForm::begin(['action' => ['/account/message/send', 'userId' => $user->getPrimaryKey(), 'bookingId' => $model->getPrimaryKey()], 'method' => 'POST']) ?>

                <?= $form->field($message, 'subject', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $message->subject]) ?>

                <?= $form->field($message, 'message', ['template' => $template])->textarea(['maxlength' => true, 'placeholder' => Yii::t('app', "Ask {user} any questions about the booking", ['user' => $user])]) ?>

                <p class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-send-o"></i> &nbsp;<?= Yii::t('app', "Send") ?></button>
                </p>

                <?php $form->end() ?>

            </div>
        
        <?php endif; ?>
        
    </div>
    
</div>