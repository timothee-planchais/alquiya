<?php

use common\models\Booking;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\grid\GridView;

/* @var $this \frontend\components\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('app', "Bookings");

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="owner-bookings">
    
    <p>
        <a href="<?= Url::to(['/account/calendar']) ?>" class="btn btn-secondary btn-lg"><i class="fa fa-calendar-check-o"></i> <?= Yii::t('app', "My calendar") ?></a>
    </p>
    
    <hr/>
    
    <?= frontend\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model) {
            $color = '#dff0d8';
            
            if($model->status == Booking::STATUS_REFUSED || $model->status == Booking::STATUS_CANCELLED) {
                $color = '#f2dede';
            }
            elseif($model->status == Booking::STATUS_PENDING) {
                $color = '#d9edf7';
            }
            
            return ['style' => 'background-color:'.$color.';'];
        },
        'columns' => [
            [
                'attribute' => 'id',
                'value' => function($model){
                    return Html::a('#'.$model->id, $model->getUrl());
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    return $model->getTextStatus();
                }
            ],
            [
                'label' => Yii::t('app', "Item"),
                'value' => function($model){
                    $html = Html::img($model->item->getImageUrl('300x200'), ['width' => '50']);
                    $html .= '<br/>'.Html::a(Html::encode($model->item->title), $model->item->getUrl());
                    return $html;
                },
                'format' => 'raw'
            ],
            [
                'label' => Yii::t('app', 'Address'),
                'value' => function($model){
                    return nl2br(Html::encode($model->user->getOfficialAddress()));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'user_id',
                'label' => Yii::t('app', "Renter"),
                'value' => function($model){
                    return Html::a($model->user->username, $model->user->getProfileUrl());
                },
                'format' => 'raw'
            ],
            [
                'label' => Yii::t('app', "Contact"),
                'value' => function($model) {
                    return $model->user->phone;
                }
            ],
            [
                'attribute' => 'date_start',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->date_start, 'short');
                }
            ],
                        [
                'attribute' => 'date_end',
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->date_end, 'short');
                }
            ],
            [
                'label' => Yii::t('app', "Duration"),
                'value' => function($model){
                    return Yii::t('app', '{nb} days', ['nb' => $model->getDuration()]);
                }
            ],
            'total:currency',
            /*[
                'attribute' => 'total',
                'value' => function($model){
                    return $model->getTotalPrice();
                },
                'format' => 'currency'
            ],
            [
                'label' => Yii::t('app', "Amount to pay to the owner"),
                'value' => function($model){
                    return $model->getTotalPrice();
                },
                'format' => 'currency'
            ], */
            //'created_at:datetime',
            [
                'class' => 'frontend\widgets\grid\ActionColumn',
                'template' => '{review} {view} {validate} {refuse} {cancel}',
                'buttons' => [
                    'review' => function ($url, $model, $key) {
                        if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_REVIEW, ['booking' => $model]))
                        {
                            $options = [
                                'title' => Yii::t('app', 'Add a review'),
                                'aria-label' => Yii::t('app', 'Add a review'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-secondary'
                            ];
                            return Html::a('<i class="fa fa-commenting-o"></i>', $url, $options);
                        }
                    },
                    'validate' => function ($url, $model, $key) {
                        if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_VALIDATE, ['booking' => $model]))
                        {
                            $options = [
                                'title' => Yii::t('app', 'Validate'),
                                'aria-label' => Yii::t('app', 'Validate'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to validate the booking ?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-success'
                            ];
                            return Html::a('<i class="fa fa-check"></i>', $url, $options);
                        }
                    },
                    'refuse' => function ($url, $model, $key) {
                        if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_REFUSE, ['booking' => $model]))
                        {
                            $options = [
                                'title' => Yii::t('app', 'Refuse'),
                                'aria-label' => Yii::t('app', 'Refuse'),
                                //'data-confirm' => Yii::t('app', 'Are you sure you want to refuse the booking ?'),
                                //'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-warning'
                            ];
                            return Html::a('<i class="fa fa-minus-circle"></i>', $url, $options);
                        }
                    },
                    'cancel' => function ($url, $model, $key) {
                        if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_BOOKING_CANCEL, ['booking' => $model]))
                        {
                            $options = [
                                'title' => Yii::t('app', 'Cancel'),
                                'aria-label' => Yii::t('app', 'Cancel'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to cancel the booking ?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                                'class' => 'btn btn-danger'
                            ];
                            return Html::a('<i class="fa fa-times"></i>', $url, $options);
                        }
                    }
                ]
            ],
        ],
    ]); ?>
    
</div>