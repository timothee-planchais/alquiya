<?php

use frontend\models\Booking;
use common\models\Payment;
use common\models\Item;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \frontend\components\View */
/* @var $payment Payment */

$this->usePageContent = false;

$user = $payment->user;
$order = $payment->order;

$this->title = Yii::t('app', "Payment order").' #'.$order->code;

?>

<div id="payu">

    <form id="pay_form" method="post" action="<?= Yii::$app->params['payu.action'] ?>" class="well">

     <input name="merchantId"    type="hidden"  value="<?= Yii::$app->params['payu.merchantId'] ?>"   >
     <input name="accountId"     type="hidden"  value="<?= Yii::$app->params['payu.accountId'] ?>" >
     <input name="description"   type="hidden"  value="<?= $payment->getDescription() ?>"  >
     <input name="referenceCode" type="hidden"  value="<?= $payment->getReferenceCode() ?>" >
     <input name="amount"        type="hidden"  value="<?= $payment->amount ?>"   >
     <input name="buyerEmail"    type="hidden"  value="<?= $user->email ?>" >
     <input name="buyerFullName"    type="hidden"  value="<?= $user->firstname.' '.$user->lastname ?>" >
     <input name="shippingAddress"    type="hidden"  value="<?= $user->address ?>" >
     <input name="shippingCity"    type="hidden"  value="<?= $user->city ?>" >
     <input name="shippingCountry"    type="hidden"  value="<?= $user->country ?>" >
     <input name="telephone"    type="hidden"  value="<?= $user->phone ?>" >
     <input name="tax"           type="hidden"  value="0"  >
     <input name="taxReturnBase" type="hidden"  value="0" >
     <input name="currency"      type="hidden"  value="<?= $payment->currency ?>" >
     <input name="signature"     type="hidden"  value="<?= $payment->getSignature() ?>"  >
     <input name="test"          type="hidden"  value="<?= Yii::$app->params['payu.test'] ?>" >
     <input name="responseUrl"    type="hidden"  value="<?= Url::to(['/payment/response', 'id' => $payment->getPrimaryKey()], true) ?>" >
     <input name="confirmationUrl"  type="hidden"  value="<?= Url::to(['/payment/confirm', 'id' => $payment->getPrimaryKey()], true) ?>" >
     <input name="lng" type="hidden"  value="<?= Yii::$app->language ?>" >

    <p class="text-primary"><?= Yii::t('app', "Redirection to the payment platform") ?></p>
    <p><img src="<?= Url::to('@web/img/waiting.gif') ?>" alt="waiting" title="Redirection" /></p>

    <!--<p><button type="submit" class="btn btn-lg btn-secondary"><?= Yii::t('app', "Pay") ?></button></p>-->

   </form>
    
</div>

<?php $this->beginBlock('js'); ?>
<script language="JavaScript">
	
    $(document).ready(function(){
        
        setTimeout(function(){
            $('#pay_form').submit();
        }, 500);
        
    });
    
</script>
<?php $this->endBlock(); ?>