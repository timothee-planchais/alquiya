<?php

use common\models\Category;
use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this frontend\components\View */
/* @var $model \frontend\models\SearchItem */
/* @var $allItems array */
/* @var $items Item[] */

$this->title = Yii::t('app', "Homepage");

$this->showTitle = false;
$this->usePageContent = false;
$this->useContainer = false;
        
$this->registerAssetBundle(frontend\assets\OwlAsset::class);

?>

<div id="home">
    
    <section id="home-top" class="home-section bg-secondary">
    
        <div class="container">

            <?= $this->render('/item/_search', ['model' => $model, 'home' => true]) ?>

        </div>
        
    </section>
        
    <section id="home-blocs" class="home-section">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="home-bloc">
                        <p><i class="fa fa-map-marker text-secondary"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_bloc_1_title") ?></h3>
                        <p><?= Yii::t('app', "home_bloc_1_txt") ?></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="home-bloc">
                        <p><i class="fa fa-dollar text-secondary"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_bloc_2_title") ?></h3>
                        <p><?= Yii::t('app', "home_bloc_2_txt") ?></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="home-bloc">
                        <p><i class="fa fa-shield text-secondary"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_bloc_3_title") ?></h3>
                        <p><?= Yii::t('app', "home_bloc_3_txt") ?></p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    
    <section id="home-rent" class="home-section">
        <p class="container"><a href="<?= Url::to(['/account/item/create']) ?>" class="btn btn-secondary btn-lg"><i class="fa fa-plus"></i> <?= Yii::t('app', "Propose my objects for rent") ?></a></p>
    </section>
    
    <section id="home-steps" class="home-section">
        <div class="container">
            <h2><?= Yii::t('app', "How does it work ?") ?></h2>
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-4">
                    <div class="home-step">
                        <p><i class="fa fa-search"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_step_1_title") ?></h3>
                        <p><?= Yii::t('app', "home_step_1_txt") ?></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="home-step">
                        <p><i class="fa fa-calendar-check-o"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_step_2_title") ?></h3>
                        <p><?= Yii::t('app', "home_step_2_txt") ?></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="home-step">
                        <p><i class="fa fa-hand-peace-o"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_step_3_title") ?></h3>
                        <p><?= Yii::t('app', "home_step_3_txt") ?></p>
                    </div>
                </div>
                
            </div>
            
        </div>
    </section>
       
    <div class="container">
    
        <div id="items" class="items">

            <?php foreach($allItems as $list): ?>
                <?php $items = $list['items']; ?>

                    <?php if($items): ?>

                        <h2 class="text-primary"><?= $list['title'] ?></h2>

                        <div class="home-items owl-carousel owl-theme">

                            <?php foreach($items as $item): ?>
                                <?= $this->render('/item/_item', ['item' => $item]) ?>
                            <?php endforeach; ?>

                        </div>

                        <!--<p class="text-right"><a href="<?= Url::to(['/item/index']) ?>" class="text-secondary"><?= Yii::t('app', "See all") ?> <i class="fa fa-chevron-right"></i></a></p>-->

                    <?php endif; ?>

            <?php endforeach; ?>
        
            <p class="text-center"><a href="<?= Url::to(['/item/index']) ?>" class="btn btn-primary btn-lg"><?= Yii::t('app', "See all the products") ?> <i class="fa fa-arrow-right"></i></a></p>

        </div>
        
    </div>
    
    <section id="home-community" class="home-section">
        <div class="container">
            
            
            
        </div>
    </section>
    
</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        $('.home-items').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                450:{
                    items:2
                },
                700:{
                    items:3
                },
                992:{
                    items:4
                }
            }
        });
        
    });

</script>
<?php $this->endBlock(); ?>