<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', "Reset password");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">

    <p class="text-center">Por favor elige tu nueva contraseña :</p>

    <div class="row" style="margin-top:30px;">
        <div class="col-lg-5 col-centered">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                <div class="text-right">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-secondary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
