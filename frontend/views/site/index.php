<?php

use common\models\Category;
use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

/* @var $this frontend\components\View */
/* @var $model \frontend\models\SearchItem */
/* @var $categories Category[] */
/* @var $allItems array */
/* @var $items Item[] */

$this->title = Yii::t('app', "Peer to peer rent");

$this->showTitle = false;
$this->usePageContent = false;
$this->useContainer = false;
        
$this->registerAssetBundle(frontend\assets\OwlAsset::class);

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?' . http_build_query(['libraries' => 'places', 'language' => Yii::$app->params['google.api_language'], 'key' => Yii::$app->params['google.api_key']]));
$this->registerJsFile('@web/js/plugins/jquery.geocomplete.min.js', ['depends'=>  \yii\web\JqueryAsset::className()]);

$this->registerJs('initSearchItem();');

$template = "{input}\n{hint}\n{error}";
$template2 = '<div class="input-group"> {input} <div class="input-group-addon">km</div> </div>';

?>

<div id="home">
    
    <!-- TOP -->
    <section id="home-top" class="home-section">
    
        <div id="home-top-inner">
        
            <div class="container">

                <!--<h2>Encuentra el producto que necesites y <u class="text-blackhand">alquilalo</u> a otras personas</h2>-->
                <h2>
                    Alquiler de objetos entre particulares
                    <small>(herramientas, cámaras, ropa, muebles, fincas, parqueaderos ...)</small>
                </h2>

                <div id="home-items-search" class="">

                    <?php $form = ActiveForm::begin(['layout' => 'inline', 'action' => ['/item/search'], 'method' => 'POST']); ?>

                    <?= Html::activeHiddenInput($model, 'lat') ?>
                    <?= Html::activeHiddenInput($model, 'lng') ?>
                    <?= Html::activeHiddenInput($model, 'city') ?>

                    <?= $form->field($model, 'search', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('search')]) ?>

                    <?php // $form->field($model, 'search_address', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('search_address')]) ?>
                    <?= $form->field($model, 'search_city', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('search_city')]) ?>

                    <?php //$form->field($model, 'radius', ['template' => $template, 'inputTemplate' => $template2])->input('number', ['placeholder' => $model->getAttributeLabel('radius')]) ?>

                    <div class="form-group text-center">
                        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-block', 'id' => 'home-btn-search']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>
            
        </div>
        
    </section>
    
    <!-- BLOCS -->
    <section id="home-blocs" class="home-section">
        <div class="container">
            
            <div class="table">

                <div class="table-cell home-bloc">
                    <div class="media">
                        <div class="media-left"><img src="<?= Url::to('@web/img/home/alquila-cerca-de-ti.png') ?>" class="media-object"/></div>
                        <div class="media-body">
                            <h3 class="text-primary">Alquila cerca de ti</h3>
                            <p>Conecta con personas que están alrededor.</p>
                        </div>
                    </div>
                </div>
                
                <div class="table-cell home-bloc">
                    <div class=" media">
                        <div class="media-left"><img src="<?= Url::to('@web/img/home/compara-precios.png') ?>" class="media-object"/></div>
                        <div class="media-body">
                            <h3 class="text-primary">Compara los precios</h3>
                            <p>Escoge la opción más conveniente para ti.</p>
                        </div>
                    </div>
                </div>
                
                <div class="table-cell home-bloc">
                    <div class="media">
                        <div class="media-left"><img src="<?= Url::to('@web/img/home/mas-opciones-ti.png') ?>" class="media-object"/></div>
                        <div class="media-body">
                            <h3 class="text-primary">Más opciones para ti</h3>
                            <p>Encuentra cientos de productos para alquilar.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    
    <!--<section id="home-steps" class="home-section">
        <div class="container">
            <h2><?= Yii::t('app', "How does it work ?") ?></h2>
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-4">
                    <div class="home-step">
                        <p><i class="fa fa-search"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_step_1_title") ?></h3>
                        <p><?= Yii::t('app', "home_step_1_txt") ?></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="home-step">
                        <p><i class="fa fa-calendar-check-o"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_step_2_title") ?></h3>
                        <p><?= Yii::t('app', "home_step_2_txt") ?></p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="home-step">
                        <p><i class="fa fa-hand-peace-o"></i></p>
                        <h3 class="text-primary"><?= Yii::t('app', "home_step_3_title") ?></h3>
                        <p><?= Yii::t('app', "home_step_3_txt") ?></p>
                    </div>
                </div>
                
            </div>
            
        </div>
    </section>-->
        
    <section id="home-rent" class="home-section">
        <div class="container text-center">
            <a href="<?= Url::to(['/account/item/create']) ?>" class="btn btn-secondary btn-lg"><i class="fa fa-check"></i> Quiero alquiler objetos y ganar dinero</a>
        </div>
    </section>
    
    <!-- CATEGORIES -->
    <?php $colors = ['#CBCBCB','#FDAC65','#EB859C','#C8BEFF','#F5D34B','#C6D053','#72D3B8']; ?>
    <section id="home-categories" class="home-section">
        <div class="container">
            
            <h2 class="home-title text-primary"><?= Yii::t('app', "Famous categories") ?></h2>
            
            <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4">
                <?php foreach($categories as $i =>$category): ?>
                    <li class="home-category">
                        <a href="<?= Url::to($category->getUrl()) ?>" style="background-color:<?= $colors[$i] ?>;">
                            <span class="home-category-img" style="background-image:url('<?= Url::to('@web/img/category/'.$category->getPrimaryKey().'.png') ?>');"></span>
                            <!--<img src="<?= Url::to('@web/img/category/'.$category->getPrimaryKey().'.png') ?>" class="img-responsive"/>-->
                            <span class="home-category-title"><?= $category->title ?> <span class="text-blackhand">!</span></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
       
    <!-- ITEMS -->
    <?php if($allItems): ?>
        <div id="home-items" class="home-section">

            <div class="container">

                <?php foreach($allItems as $list): ?>
                    <?php $items = $list['items']; ?>

                        <?php if($items): ?>
                            <div class="home-items">
                                <h2 class="home-title text-primary"><?= $list['title'] ?></h2>

                                <div class="items owl-carousel owl-theme">

                                    <?php foreach($items as $item): ?>
                                        <?= $this->render('/item/_item', ['item' => $item, 'asRow' => false]) ?>
                                    <?php endforeach; ?>

                                </div>
                            </div>
                        <?php endif; ?>

                <?php endforeach; ?>

                <!--<p class="text-center"><a href="<?= Url::to(['/item/index']) ?>" class="btn btn-secondary btn-lg"><?= Yii::t('app', "See all the productos") ?> <i class="fa fa-arrow-right"></i></a></p>-->

            </div>

        </div>
    <?php endif; ?>
        
    <div id="home-ad" class="home-section">
        <div class="container">
            <div class="alquiya-ad">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-2957507038442890"
                     data-ad-slot="3877147283"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
    
</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        $('.home-items .owl-carousel').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                450:{
                    items:2
                },
                700:{
                    items:3
                },
                992:{
                    items:4
                }
            }
        });
        
    });

</script>
<?php $this->endBlock(); ?>