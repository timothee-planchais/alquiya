<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model frontend\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', "Login");
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="login">
            
    
    <div id="login-socials">
        <div class="row">
            <div class="col-xs-12 col-sm-5 text-left">
                <p style="font-weight:500;"><?= Yii::t('app', "Login with your social networks") ?></p>
            </div>
            <div class="col-xs-12 col-sm-7 text-right">
                <a href="<?= Url::to(['site/auth', 'authclient' => 'facebook']) ?>" class="btn btn-social btn-facebook"><i class="fa fa-facebook-f"></i></a>

                <a href="<?= Url::to(['site/auth', 'authclient' => 'twitter']) ?>" class="btn btn-social btn-twitter"><i class="fa fa-twitter"></i></a>

                <a href="<?= Url::to(['site/auth', 'authclient' => 'google']) ?>" class="btn btn-social btn-google"><i class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>
    

    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <div class="text-center" style="margin-top:20px;">
            <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-block btn-secondary', 'name' => 'login-button']) ?>
        </div>

        <div class="text-center" style="margin-top:15px;font-weight:500;">
            <u><?= Html::a(Yii::t('app', "I lost my password"), ['site/request-password-reset']) ?></u>
        </div>

    <?php ActiveForm::end(); ?>
            
</div>
