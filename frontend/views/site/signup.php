<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
use kartik\select2\Select2;

use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;

$this->title = Yii::t('app', "Signup");
$this->params['breadcrumbs'][] = $this->title;

$conf = Yii::$app->params['bootstrapFormConfig'];

?>
<div id="signup">
    
    <div id="login-socials">
        <div class="row">
            <div class="col-xs-12 col-sm-5 text-left">
                <p style="font-weight:500;"><?= Yii::t('app', "Register with your social networks") ?></p>
            </div>
            <div class="col-xs-12 col-sm-7 text-right">
                <a href="<?= Url::to(['site/auth', 'authclient' => 'facebook']) ?>" class="btn btn-social btn-facebook"><i class="fa fa-facebook-f"></i></a>

                <a href="<?= Url::to(['site/auth', 'authclient' => 'twitter']) ?>" class="btn btn-social btn-twitter"><i class="fa fa-twitter"></i></a>

                <a href="<?= Url::to(['site/auth', 'authclient' => 'google']) ?>" class="btn btn-social btn-google"><i class="fa fa-google-plus"></i></a>
            </div>
        </div>
    </div>
    
    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

        <?= $form->field($model, 'type', ['inline' => true])->radioList(User::getTypes()) ?>
    
        <div class="row type-<?= User::TYPE_USER_PRO ?>" style="display:none;">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'pro_company_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'pro_website')->textInput(['maxlength' => true, 'placeholder' => 'http://']) ?>
            </div>
        </div>
    
        <div class="type-<?= User::TYPE_USER_PRO ?>" style="display:none;">
            <?= $form->field($model, 'pro_categories')->widget(Select2::class, [
                'data' => $categories,
                'options' => [
                    'placeholder' => Yii::t('app', "Please select categories"),
                    'multiple' => true
                ],
                'pluginOptions' => [
                    'multiple' => true,
                    'maximumSelectionLength' => 3,
                    'closeOnSelect' => false
                ],
            ]) ?>
        </div>
    
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'gender')->dropDownList(User::getGenders()) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'country')->dropDownList(common\components\Utils::getCountries()) ?>
            </div>
        </div>
    
        <?php // echo $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
            </div>
        </div>
    
        <div class="form-group text-center" style="margin-top:15px;">
            <div class="checkbox">
                <label for="signupform-accept_terms" style="font-size:12px;">
                    <input type="checkbox" id="contributionform-accept_terms" name="SignupForm[accept_terms]" value="1" required="required">
                    <?= $model->getAttributeLabel('accept_terms') ?>
                </label>
            </div>
        </div>

        <div class="text-center">
            <?= Html::submitButton(Yii::t('app', "Signup"), ['class' => 'btn btn-block btn-secondary', 'name' => 'signup-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        toggleType();
        $('#signupform-type').on('change', function(){
            toggleType();
        });
        
    });
    
    function toggleType()
    {
        var $type = $('#signupform-type input:checked').val();
        
        if($type == '<?= User::TYPE_USER_PRO ?>') {
            $('.type-<?= User::TYPE_USER_PRO ?>').show();
        }
        else {
            $('.type-<?= User::TYPE_USER_PRO ?>').hide();
        }
    }

</script>
<?php $this->endBlock(); ?>