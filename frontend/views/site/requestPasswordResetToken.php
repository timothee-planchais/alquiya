<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', "I lost my password");
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">

    <p class="text-center">Por favor ingresa la dirección de email que utilizaste en el proceso de registro de AlquiYa.</p>

    <div class="row" style="margin-top:35px;">
        <div class="col-lg-5 col-centered">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="text-right">
                    <?= Html::submitButton(Yii::t('app', "Send"), ['class' => 'btn btn-secondary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
