<?php

use common\models\User;
use common\models\UserPro;
use common\models\Review;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $userPro UserPro */

?>

<div id="pro-contact" class="">
        
    <h2 class="text-primary"><?= Yii::t('app', "Contact") ?></h2>

    <div class="row">
        
        <div class="col-xs-12 col-sm-6">
            <h3 class="text-secondary"><?= Yii::t('app', "Company informations") ?></h3>
            <p><strong><?= Yii::t('app', "Company") ?> :</strong> <?= Html::encode($userPro->company_name) ?></p>
            <p><strong><?= Yii::t('app', "Address") ?> :</strong> <br/>
                <?= nl2br(Html::encode($user->getNiceAddress())) ?>
            </p>
        </div>
        
        <div class="col-xs-12 col-sm-6">
            <h3 class="text-secondary"><?= Yii::t('app', "Contact the profesional") ?></h3>
            <p><a href="#" class="btn btn-primary btn-block"><?= Yii::t('app', 'Request a quote') ?></a></p>
            <p><a href="<?= Url::to(['/account/message/send', 'userId' => $user->id]) ?>" class="btn btn-primary btn-block"><?= Yii::t('app', 'Contact') ?></a></p>
            <?php if($userPro->website): ?>
                <p><a href="<?= $userPro->website ?>" class="btn btn-primary btn-block" target="_blank"><?= Yii::t('app', 'Consultar la pagina') ?></a></p>
            <?php endif; ?>
        </div>
        
    </div>

</div>