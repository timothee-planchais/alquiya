<?php

use common\models\User;
use common\models\UserPro;
use common\models\Review;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $userPro UserPro */
/* @var $reviews Review[] */

?>

<div id="reviews-list" class="">
        
    <h2 class="text-primary"><?= Yii::t('app', "Recevied reviews") ?></h2>

    <?php if($reviews): ?>

        <?php foreach($reviews as $review): ?>
            <?= $this->render('/review/_review', ['review' => $review]) ?>
        <?php endforeach; ?>

    <?php else: ?>
        <p><?= Yii::t('app', "No review") ?></p>
    <?php endif; ?>

</div>