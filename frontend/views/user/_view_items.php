<?php

use common\models\User;
use common\models\UserPro;
use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $userPro UserPro */
/* @var $items Item[] */

?>

<div id="items" class="items">
        
    <h2 class="text-primary"><?= Yii::t('app', "Items") ?></h2>

    <?php if(!empty($items)): ?>

        <div id="items-list">
            <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4">

                <?php foreach($items as $item): ?>
                    <li><?= $this->render('/item/_item', ['item' => $item]) ?></li>
                <?php endforeach; ?>

            </ul>
        </div>

    <?php else: ?>
        <p><?= Yii::t('app', "No published product") ?></p>
    <?php endif; ?>

</div>