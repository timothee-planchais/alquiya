<?php

use common\models\User;
use common\models\UserPro;
use common\models\Review;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $userPro UserPro */

?>

<div id="pro" class="">
        
    <h2 class="text-primary"><?= Yii::t('app', "Company") ?></h2>

    <h3 class="text-secondary"><?= Yii::t('app', 'Activity description') ?> :</h3>
    
    <div class="pro-bloc"><p><?= nl2br(Html::encode($userPro->description)) ?></p></div>
    
    <h3 class="text-secondary"><?= Yii::t('app', 'Sector of activity') ?> :</h3>
    
    <div class="pro-bloc"><p><?= $userPro->getNiceCategories('<br/>') ?></p></div>

</div>