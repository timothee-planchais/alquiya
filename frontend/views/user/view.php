<?php

use common\models\User;
use common\models\UserPro;
use common\models\Item;
use common\models\Review;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $userPro UserPro */
/* @var $items Item[] */
/* @var $reviews Review[] */

if(!empty($userPro)) {
    $this->title = Yii::t('app', "Pro").' : '.$userPro->company_name;
}
else {
    $this->title = Yii::t('app', "Member").' : '.$user->__toString();
}
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->showTitle = false;
$this->usePageContent = false;

$rating = $user->getRating();

?>

<div id="user-view">
    
    <div id="user-view-top" class="well page-content">
        
        <h1 id="title" class="text-primary"><?= $this->title ?></h1>
        
        <hr/>
        
        <div class="media">
            <div class="media-left">
                <?php if(!empty($userPro)): ?>
                    <img src="<?= Url::to($userPro->getLogoUrl('300x300')) ?>" class="media-object img-circle" alt="<?= $userPro->company_name ?>"/>
                <?php else: ?>
                    <img src="<?= Url::to($user->getImageUrl('300x300')) ?>" class="media-object img-circle" alt="<?= $user->username ?>"/>
                <?php endif; ?>
                <a href="<?= Url::to(['/account/message/send', 'userId' => $user->id]) ?>" class="btn btn-secondary btn-lg btn-block"><?= Yii::t('app', "Contact") ?></a>
            </div>
            <div class="media-body">
                <?php if(!empty($userPro)): ?>
                    <p><strong><?= $userPro->getAttributeLabel('categories') ?> :</strong> <?= $userPro->getNiceCategories() ?></p>
                <?php else: ?>
                    <p><strong><?= Yii::t('app', "Start date") ?> :</strong> <span class="text-sanfrancisco"><?= Yii::$app->formatter->asDate($user->created_at) ?></span></p>
                    <p><strong><?= Yii::t('app', "Rating") ?> :</strong> <?= $this->render('/review/_stars', ['note' => $rating]) ?></p>
                    <p style="margin:10px 0;font-weight:600;">
                        <?php if($user->security_email_verified): ?>
                            <i class="fa fa-check text-success"></i> <?= Yii::t('app', "Email verified") ?>
                        <?php else: ?>
                            <i class="fa fa-times text-danger"></i> <?= Yii::t('app', "Email no verified") ?>
                        <?php endif; ?>
                    </p>
                <?php endif; ?>
                <p><strong><?= Yii::t('app', "Number of published products") ?> :</strong> <span class="text-sanfrancisco"><?= $user->getItems()->where(['status' => Item::STATUS_ACTIVE, 'locked' => 0])->count() ?></span></p>
                <p><strong><?= Yii::t('app', "Address") ?> :</strong> <?= $user->getOfficialAddress() ?></p>
                
                <?php if($user->description): ?>
                    <p id="user-description"><?= nl2br(Html::encode($user->description)) ?></p>
                <?php endif; ?>
                
            </div>
        </div>
        
    </div>
    <?= yii\bootstrap\Tabs::widget([
        'options' => ['class' => 'nav nav-tabs', 'role' => 'tablist'],
        'items' => [
            [
                'label' => Yii::t('app', "Company"), 
                'content' => !empty($userPro) ? $this->render('_view_pro', ['user' => $user, 'userPro' => $userPro]) : null,
                'options' => ['id' => 'tab-pro'],
                'visible' => !empty($userPro)
            ],
            [
                'label' => Yii::t('app', "Items"), 
                'content' => $this->render('_view_items', ['items' => $items]),
                'options' => ['id' => 'tab-items']
            ],
            [
                'label' => Yii::t('app', "Recevied reviews"), 
                'content' => empty($userPro) ? $this->render('_view_reviews', ['reviews' => $reviews]) : null,
                'options' => ['id' => 'tab-reviews'],
                'visible' => empty($userPro)
            ],
            [
                'label' => Yii::t('app', "Contact"), 
                'content' => !empty($userPro) ? $this->render('_view_pro_contact', ['user' => $user, 'userPro' => $userPro]) : null,
                'options' => ['id' => 'tab-pro-contact'],
                'visible' => !empty($userPro)
            ],
        ]
    ]); ?>
    
    
    
    
    
</div>