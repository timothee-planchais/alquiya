<?php


use common\models\Contact;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this frontend\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model Contact */

$this->title = Yii::t('app', "Contact");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

?>
<div id="contact" class="page page-content">

    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

        <?php if($model->scenario == Contact::SCENARIO_GUEST): ?>
    
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
    
        <?php endif; ?>

        <?= $form->field($model, 'category')->dropDownList(Contact::getCategories(), ['prompt' => '']) ?>

        <p class="alert alert-info" data-category="2" style="display:none;">
            <i class="fa fa-info-circle"></i> Indíquenos el tipo y la versión de su navegador (Chrome, Mozilla, IE, Safari...), su sistema operativo y la dirección de la página correspondiente.
        </p>
        
        <p class="alert alert-info" data-category="4" style="display:none;">
            <i class="fa fa-info-circle"></i> Por favor, sugiera cualquier funcionalidad que le resulte útil y explique cómo esta puedria ser beneficiosa para nuestros miembros.
        </p>
        
        <p class="alert alert-info" data-category="5" style="display:none;">
            <i class="fa fa-info-circle"></i> Por favor, especifique la categoría o categorías que faltan. Después de la revisión, integraramos la nueva categoría lo antes posible.
        </p>
        
        <p class="alert alert-info" data-category="13" style="display:none;">
            <i class="fa fa-info-circle"></i> Tuvo una buena experiencia con AlquiYa, envíenos un testimonio y se presentará en la página de inicio.
        </p>
    
        <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'message')->textarea(['rows' => 6, 'maxlength' => true]) ?>

        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>
        
        <div class="text-right">
            <?= Html::submitButton('<i class="fa fa-send"></i> '.Yii::t('app', "Send"), ['class' => 'btn btn-secondary btn-lg']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        toggleCategory();
        $('#contact-category').on('change', function(){
            toggleCategory();
        });
        
    });
    
    function toggleCategory()
    {
        var $value = $('#contact-category').val();
        var $elem = $('.alert-info[data-category="'+$value+'"]');
        
        $('.alert-info').hide();
        
        if($elem.length) 
        {
            $elem.show();
        }
    }

</script>
<?php $this->endBlock(); ?>