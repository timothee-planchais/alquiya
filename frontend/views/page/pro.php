<?php

/* @var $this \frontend\components\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('app', "Pro");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

?>
<div id="pro" class="page page-content">
    
    <h2 class="text-primary">Ser visible por miles de visitantes cada mes.</h2>
    
    <p>
        AlquiYa es el sitio líder de alquiler en Colombia. Con miles de visitantes y suscriptores, AlquiYa es la plataforma líder para empresas de alquiler profesional.<br/>
        <!--La presencia en Alquiya se paga.-->
    </p>
    
    
    <h2 class="text-primary">Los beneficios de AlquiYa</h2>
    
    <p>Al suscribirse se beneficia de:</p>
    
    <ul>
        <li>El perfil de su empresa <strong>en nuestro directorio</strong> de propietarios profesionales.</li>
        <li><strong>Sus productos accesibles las 24 horas del día</strong> por nuestros visitantes.</li>
        <li><strong>Sus productos se distinguen de los otros con una etiqueta "pro"</strong></li>
        <!--<li>Visibilidad en los buscadores.</li>-->
        <li><strong>Solicitudes de cotizacion</strong> <!-- citas, presupuestos --> regular</li>
        <li>Su espacio personal para administrar sus productos, sus tarifas, sus reservaciónes, sus datos... de manera muy sencilla.</li>
        <li><strong>Un servicio de consejo y asistencia.</strong></li>
    </ul>
    
    <?php if(Yii::$app->getUser()->isGuest): ?>
        <p style="max-width:600px;margin:30px auto 0 auto;">
            <a href="<?= Url::to(['/site/signup', 'pro' => 1]) ?>" class="btn btn-secondary btn-lg btn-block" style="">
                <i class="fa fa-check"></i> Registro mi empresa en AlquiYa</a>
        </p>
    <?php endif; ?>
    
</div>
