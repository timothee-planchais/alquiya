<?php

/* @var $this \frontend\components\View */

use yii\helpers\Html;

$this->title = Yii::t('app', "Privacy");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

?>
<div id="privacy" class="page page-content">
    
    <h2 class="text-primary">Recoleccion de datos</h2>

    <p>AlquiYa solo recolecta datos personales que el Usuario nos envía en forma voluntaria y con una finalidad específica. El Usuario puede brindarnos datos como forma de registración para algún servicio, o al realizar llamados o consultas por teléfono o cualquier otro medio de comunicación. Los datos serán usados para que el Usuario de Internet, que publique información en el sitio de AlquiYa, pueda ser contactado por el interesado en adquirir bienes o servicios relacionados a la información publicada. Asimismo, los datos podrán ser usados para que AlquiYa contacte a los Usuarios a fin de brindarle información sobre cambios y/o novedades y/o requerirles información sobre la experiencia en el uso del sitio web. Esos datos no serán usados con otra finalidad distinta que aquella para la que fueron recolectados en cada oportunidad, excepto en el caso que sea requerido de otro modo por la ley. Si el Usuario ha dado permiso sus datos podrán ser compartidos con otras empresas subsidiarias o afiliadas de AlquiYa. No se requiere que el Usuario proporcione información personal como condición para usar este Sitio, a menos que sea necesario para proveerle información adicional que el Usuario solicite. Cuando el Usuario utiliza este Sitio, almacenamos datos en nuestros servidores para posibilitar la conexión y por cuestiones de seguridad. Estos datos pueden incluir el nombre de su proveedor de servicio de Internet, el sitio web que el Usuario ha usado para vincularse a este Sitio, los sitios web de AlquiYa que el Usuario ha visitado, los sitios web que el Usuario visita desde nuestro este Sitio, su dirección IP (Internet Protocol), la fecha y duración de sus visitas a este Sitio. Estos datos podrían conducir posiblemente a su identificación, pero generalmente no lo hacemos. Utilizamos los datos periódicamente para fines estadísticos, pero mantenemos el anonimato de cada usuario individual de manera tal que la persona no pueda ser identificada. Los datos personales se reúnen exclusivamente si el Usuario nos los proporciona, a través del registro, del llenado de formularios o de correos electrónicos, como parte de un pedido de mayor información, de consultas o solicitudes acerca de este Sitio y situaciones similares en las que el Usuario haya elegido proveernos esa información. Mantendremos el control de y la responsabilidad del uso de cualquier dato personal que el Usuario nos proporcione y solo los usaremos para contactarlo por vía electrónico o telefónica. Algunos o todos estos datos pueden ser almacenados o procesados en ordenadores ubicados en otras jurisdicciones, cuyas leyes de protección de la información pueden diferir de la jurisdicción del domicilio del Usuario. Los datos del Usuario podrán ser compartidos con terceras partes, así como con otras empresas subsidiarias o afiliadas de AlquiYa. Si la totalidad de los activos de AlquiYa son vendidos o fusionados con otra compañía, la sociedad adquirente podrá acceder a la información personal del Usuario sin su consentimiento. Si una parte del negocio de AlquiYa es vendido, la entidad adquirente deberá tener acceso a la información personal del Usuario sin su consentimiento. En caso de que el Usuario utilice los medios de comunicación de mensajes provistos por AlquiYa, AlquiYa podrá almacenar y acceder a dichos mensajes, hacer un seguimiento y remitir notificaciones relacionadas a dicho mensaje.</p>

    <h2 class="text-primary">Almacenamiento de datos</h2>

    <p>Los datos personales que el Usuario pudiera haber brindado a AlquiYa serán almacenados durante el tiempo requerido para la finalidad para la cual pudieran haber sido solicitados por AlquiYa o brindados por el Usuario. Esos datos se utilizarán únicamente con el propósito de proporcionarle la información o evacuar la consulta que el Usuario haya requerido o para otros propósitos para los cuales el Usuario dio su consentimiento, salvo que la ley estipule otra cosa.</p>

    <h2 class="text-primary">Derecho de acceso, rectificación y supresión</h2>

    <p>El Usuario como titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que acredite un interés legítimo al efecto. Asimismo, el Usuario tiene derecho a corregir los datos personales inexactos y a retirar el consentimiento que ha dado para su utilización a AlquiYa solicitando que los mismos sean eliminados de los registros en los cuales se encuentren almacenados. Para acceder a sus datos personales por favor comuníquese con nuestro Oficial de Privacidad y Protección de Datos Personales a través de nuestro formulario de contacto. En dichas solicitudes el titular del dato deberá, en todo caso, previamente acreditar identidad acompañando copia de documento nacional de identidad. Para consultas al departamento de email marketing, dirija su correo a legal@alquiya.com</p>

    <h2 class="text-primary">Seguridad</h2>

    <p>AlquiYa es consciente de la importancia que tienen para el Usuario la confidencialidad y seguridad de sus datos personales generados por el uso de este Sitio. Para ello, adoptamos precauciones de seguridad técnicas y de organización a fin de proteger sus datos personales de manipulación, pérdida, destrucción o acceso por personas no autorizadas. Nuestros procedimientos de seguridad experimentan revisiones continuas basadas en el desarrollo de nuevas tecnologías.</p>

    <h2 class="text-primary">Menores de edad</h2>

    <p>En vista de la importancia de proteger la privacidad de los menores de edad, nosotros no recopilamos, procesamos o utilizamos en este Sitio ninguna información relacionada con algún individuo que sepamos que es menor de edad sin el previo y verificable consentimiento de su representante legal. Tal representante legal tiene el derecho, a su solicitud, de ver la información que facilitó el menor de edad y/o solicitar que la misma sea borrada o corregida.</p>

    <h2 class="text-primary">Google Analytics</h2>

    <p>AlquiYa implementa funciones de Google Analytics basadas en la publicidad de display ("Remarketing"), para publicar anuncios online.</p>

    <p>El Usuario puede inhabilitar Google Analytics para publicidad de display y así personalizar los anuncios de la red de display de Google por medio del uso de la herramienta “Configuración de Anuncios” (para más información sobre esta herramienta, ir a <a href="https://www.google.com/intl/en/policies/technologies/ads/" target="_blank">https://www.google.com/intl/en/policies/technologies/ads/</a>).</p>

    <p>Además, se informa al Usuario que: (i) otros proveedores, incluido Google, pueden mostrar sus anuncios en sitios de Internet; y (ii) AlquiYa y otros proveedores, incluido Google, usan las cookies de origen y las cookies de terceros combinadas para informar, optimizar y publicar anuncios basándose en las visitas anteriores al sitio web.</p>

    <p>Se informa también a los Usuarios que AlquiYa debe cumplir con la Política de Publicidad de Google, que se encuentra en: <a href="https://support.google.com/adwordspolicy/answer/143465#sensitive" target="_blank">https://support.google.com/adwordspolicy/answer/143465#sensitive</a>. En especial, debe cumplir con lo dispuesto para las categorías sensibles.</p>

 
    
</div>
