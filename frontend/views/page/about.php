<?php

/* @var $this \frontend\components\View */

use yii\helpers\Html;

$this->title = Yii::t('app', "About");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

?>
<div id="about" class="page page-content">
    
    <h2 class="text-primary">AlquiYa</h2>
    
    <p>
        Fundada en 2018, <a href="http://www.alquiya.com">www.alquiya.com</a> es una <b>plataforma comunitaria</b> que ofrece, con toda confianza y seguridad, 
        un <b>nuevo servicio que conecta a las personas que desean alquilar y / o poner en alquiler cualquier tipo de Bienes</b> (herramientas, cámaras, ropa, materiales, muebles, fincas, parqueaderos ...).
    </p>
    
    <h2 class="text-primary">Observaciones y idea</h2>
    
    <p>Hicimos las siguientes observaciones :</p>
    
    <ul>
        <li><i class="fa fa-arrow-circle-right"></i> Muchos Bienes son almacenados y no utilizados</li>
        <li><i class="fa fa-arrow-circle-right"></i> Comprar se vuelve más y más caro</li>
        <li><i class="fa fa-arrow-circle-right"></i> Aparecen nuevos comportamientos de consumo y un impulso ecológico contra el consumo excesivo</li>
    </ul>
    
    <p>Y dado que no siempre es fácil tener un Bien vecino que posea la herramienta, el camión ... que necesitamos y podemos prestárnoslo ese día, la idea se ha refinado y se ha hecho realidad :</p>
    
    <p>¿Por qué no <b>conectar a las personas</b>, a través de Internet, para que puedan <b>alquilar cualquier tipo de Bienes</b> (herramientas, cámaras, ropa, materiales, muebles, fincas, parqueaderos ...)?</p>
    
    
    <h2 class="text-primary">Nuestra vision</h2>
   
    <ul>
        <li><i class="fa fa-arrow-circle-right"></i> Libertad a través del alquiler</li>
        <li><i class="fa fa-arrow-circle-right"></i> Ser enfocado en el futuro</li>
        <li><i class="fa fa-arrow-circle-right"></i> Facilitar la vida cotidiana</li>
        <li><i class="fa fa-arrow-circle-right"></i> Ser positivo y sonriendo</li>
        <li><i class="fa fa-arrow-circle-right"></i> Crear confianza y transparencia</li>
        <li><i class="fa fa-arrow-circle-right"></i> Compartir, desarrollar la convivencia, el intercambio</li>
    </ul>
    
    <p>
        ... más allá de un alquiler, <b>queremos que viva una experiencia real</b> : aprovechar de los consejos de otros miembros, 
        encontrar soluciones a los problemas de última hora (por ejemplo, una herramienta rota, un equipo roto ...) incluso el fin de semana, 
        poder probar bienes que nunca hubiera comprado, encontrar el placer de intercambiar con sus vecinos ...
    </p>
    
    
    <!--<div class="well">
        <h2 class="text-primary">Introducción</h2>
        
        <p>Alquiya.com es una plataforma comunitaria de alquiler. AlquiYa le permite a cualquier persona, individuos y profesionales, alquilar y poner en alquiler cualquier tipo de Bien.</p>
        
        <p>AlquiYa ofrece una solución para alquilar todo tipo de bienes cerca de su hogar. 
            ¡Necesita un taladro para un fin de semana, un vestido para una boda o una bicicleta durante una semana, solo busca el bien deseado y el lugar!
        </p>
        
        <p>AlquiYa es adecuado tanto para profesionales como para individuos, ya que proporciona una plataforma simple y potente para alquilar y administrar miles de objetos por cuenta.</p>
        
    </div>-->
    
</div>
