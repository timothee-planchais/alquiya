<?php
/* @var $this \frontend\components\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('app', "Help");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;
?>

<div id="help" class="page">

    <div class="well">

        <h2 class="text-primary">¿Como funciona?</h2>

    </div>

    <div class="welll">

        <?php
        $texts = [
            'Preguntas generales' => [
                '¿Que es AlquiYa?' => "<p>AlquiYa es una plataforma comunitaria que ofrece un servicio que conecta a las personas que desean alquilar y / o poner en alquiler cualquier tipo de Bienes (herramientas, cámaras, ropa, materiales, muebles, fincas, parqueaderos ...).</p>"
                    ."<p>La plataforma es segura y le permite ahorrar dinero o complementar sus ingresos a la vez que preserva el medio ambiente a través de un consumo más razonado.</p>",
                '¿Cómo funciona AlquiYa?' => "<p>Hay 2 tipos de miembros en AlquiYa: los Arrendador que ponen sus Bienes o Productos en alquiler y los Arrendatario que los alquilan.</p>"
                    ."<p>AlquiYa tiene la intención de reunir a estas personas o empresas en un solo mercado.</p>",
                '¿El registro es gratuito?' => "<p>Sí, el registro en AlquiYa es gratuito para individuos y sin compromiso.</p>",
                '¿Quién puede registrarse en AlquiYa?' => "<p>No hay condiciones especiales para el registro en AlquiYa. Así, individuos, empresas y organizaciones independientes, públicas o asociativas pueden registrarse en AlquiYa como miembros.</p>",
                '¿Qué sucede en caso de accidente (pérdida, daño, no presencia en el punto de alquiler)?' => "<p>Se establece un contrato de alquiler entre el Arrendador y el Arrendatario el día del alquiler. Las consecuencias de un incidente se rigen por este contrato y, por supuesto, por la legislación vigente. AlquiYa proporciona un contrato estandarizado para individuos con las cláusulas esenciales de un contrato de alquiler estándar.</p>",
                '¿Qué es una reservación?' => "<p>La reservación es la solicitud realizada a través de AlquiYa por un miembro Arrendatario para un alquiler en un lugar determinado durante un período específico.</p>",
                '¿Que es la duración de la cancelación de una reserva?' => "<p>La duración de la cancelación de una reservación es el tiempo concedido al Arrendatario o al Propietario para cancelar una reservación.</p>",
                '¿Cómo cancelar una reservación?' => "<p>Mientras el alquiler no haya comenzado, puede cancelarlo. Para esto, vaya a su cuenta, \"Reservaciónes\", \"como Arrendatario\", \"Cancelar\". Una vez que la fecha y hora de inicio del alquiler han pasado, ya no es posible cancelar.</p>",
                '¿Cómo puedo contactar con el soporte de AlquiYa?' => "<p>Puede ir a la sección de <a href=\"".Url::to(['page/contact'])."\">Contacto</a>.</p>",
                '¿Cómo puedo cerrar mi cuenta de miembro de AlquiYa?' => "<p>Para cancelar su suscripción de alquiya.com, simplemente háganos saber su deseo por la sección de <a href=\"".Url::to(['page/contact'])."\">Contacto</a>.</p>",
                '¿Cómo se evalúa a los miembros de AlquiYa (Arrendatarios y Arrendadores)?' => "<p>Se deben evaluar 2 elementos para ofrecer a los miembros de AlquiYa un sistema de máxima confianza :</p>"
                    ."<ul><li>El Arrendador poniendo Bienes en alquiler.</li><li>El Arrendatario que realiza alquileres de Bienes.</li></ul>"
                    ."<p>La página descriptiva de cada Producto reanudará sus evaluaciones. La página de perfil de cada miembro reanudará sus evaluaciones como Arrendador y como Arrendatario.</p>",
                '¿Puedo usar AlquiYa para proponer mis servicios en línea?' => "<p>No. AlquiYa está ahora solamente reservado para el alquiler de Bienes y Productos en línea.</p>",
            ],
            'Arrendatarios' => [
                'Yo soy arrendatario, ¿qué debo pagar?' => "<p>Reservar un objeto por la plataforma AlquiYa es gratis! El monto del alquiler y el depósito se entregarán al Propietario del objeto el día del alquiler.</p>",
                '¿Qué sucede si el propietario cancela o rechaza mi solicitud de reservación?' => "<p>El Propietario puede rechazar o cancelar una reservación. Usted será notificado y su reserva será cancelada.</p>",
                '¿Cómo hacer una pregunta al Propietario de un objeto?' => "<p>Se puede ponerse en contacto con el Propietario a través del sistema de mensajería o en la  interna de Alquiya. Atención, esto no le dispensa a reservar el Bien en línea.</p>",
                '¿Qué pasa si rompo el Bien que alquilo?' => "<p>Se establece un contrato de alquiler entre el Arrendador y el Arrendatario el día del alquiler. Las consecuencias de un incidente se rigen por este contrato y, por supuesto, por la legislación vigente.</p>",
                '¿Cómo puedo encontrar el Producto de alquiler que deseo?' => "<p>Varios métodos de búsqueda están disponibles para buscar un Bien :</p>"
                    ."<ul><li>1. El curso de las categorías presentes en el menú.</li><li>2. A través de la búsqueda rápida en la parte superior de la página que enumera los Productos</li><li>3. A través de la búsqueda presente en la página de inicio.</li></ul>",
                '¿Cuáles son los medios de pago de un alquiler?' => "<p>Los medios de pago del saldo del contrato de alquiler están definidos por el Propietario de un Bien en la descripción del Bien.<br/>"
                    ."Cualquier tipo de pago es posible, que sea por tarjeta de crédito, pago en efectivo, transferencia bancaria o Paypal, según el caso. Este pago del saldo no se realiza a través de la plataforma AlquiYa, sino fuera de línea.</p>",
                'Como Arrendatario, ¿puedo retractarme?' => "<p>Sí, se puede cancelar una reservación mientras el alquiler no haya comenzado.</p>",
                '¿Cómo puedo evaluar un Arrendador?' => "<p>El sistema de evaluación de AlquiYa lo invita a evaluar al Arrendador del Bien al final del período de alquiler.</p>",
            ],
            'Arrendadores' => [
                '¿Qué tipo de Bienes u Objetos puedo alquilar?' => "<p>AlquiYa permite a todos de alquilar cualquier tipo de Objeto, Producto o Bien. Las categorías están visibles en línea.<br/>"
                    ."El alquiler de Bienes raíces residenciales todavía no hace parte de las categorías ofrecidas, pero se puede poner en alquiler su propiedad.</p>"
                    ."<p>Es posible proponer una nueva categoría en la sección de <a href=\"".Url::to(['page/contact'])."\">contacto</a> especificando \"Propuesta de nueva categoría\".</p>",
                '¿Cómo puedo poner en alquiler mis Bienes?' => "<p>Es muy fácil, solo tiene que regístrarese y luego ir a la <a href=\"".Url::to(['/account/item/create'])."\">página de publicación</a> de un nuevo objeto y completer el formulario.</p>",
                '¿Cómo se deposita el depósito?' => "<p>El depósito se debe entregar al Propietario del Bien el día del alquiler.</p>"
                    ."<p>AlquiYa no tiene la intención de albergar los depósitos solicitados por los Propietarios. Estos deben ser entregados por el Arrendatario al Propietario de acuerdo con el procedimiento propuesto por el Propietario en la descripción del objeto. Los detalles pueden ser introducidos directamente por el Propietario en el contrato de alquiler proporcionado por AlquiYa.</p>",
                '¿Cuáles son los costos de uso de AlquiYa para el propietario?' => "<p>Los costos de uso y de registro son gratuitos, y AlquiYa no cobra comisión.</p>"
                    ."<p>Para los profesionales del alquiler, AlquiYa ofrece diferentes opciones de suscripción. Toda la información <a href=\"".Url::to(['/page/pro'])."\">aquí</a>.</p>",
                '¿Cómo fijar el precio de mi alquiler o mi depósito?' => "<p>AlquiYa no da instrucciones sobre estos dos elementos.</p>"
                    ."<p>Si usted es un particular, AlquiYa lo invita a consultar los precios vigentes en el mercado de alquiler para el producto que está alquilando.</p>"
                    ."<p>Acerca del depósito, las diferentes formas de calcularlo variarán según su grado de confianza y las características de su Bien, como su antigüedad, por ejemplo.</p>",
                '¿Qué sucede si un Bien en alquiler se rompe o no se devuelve?' => "<p>Este tipo de evento se rige por el contrato de alquiler que vincula al Arrendatario y al Arrendador durante la transacción.</p>",
                '¿Puedo rechazar una solicitud de alquiler?' => "<p>Por supuesto, bajo ciertas condiciones, es completamente posible que rechace un alquiler. En este caso, AlquiYa lo invita a hacerlo muy rápidamente y notifica su rechazo al Arrendatario.</p>",
                '¿Cuáles son los métodos de pago disponibles para facturar mi alquiler a los Arrendatarios?' => "<p>Ya que el pago de la renta se realiza exclusivamente entre el Arrendador y el Arrendatario, le corresponde al Arrendador establecer los medios de pago deseados.</p>",
                '¿Cómo hacer que mi Objeto no esté disponible por un período específico?' => "<p>Puede pausar un Objeto : no se podrá alquilar. Para hacer esto, vaya a Mi cuenta, Mis productos. Haga clic en \"Desactivar\", delante del objeto de su elección.</p>",
                '¿Cómo puedo eliminar una de mis Bienes en alquiler?' => "<p>Para hacer esto, vaya a Mi cuenta, Mis productos. Haga clic en \"Eliminar\", delante del objeto de su elección.</p>"
                    ."<p>Tenga en cuenta que no es posible eliminar un Objeto que tenga una reservación validada y a venir.</p>",
                '¿Cómo reportar ingresos de mis alquileres en AlquiYa?' => "<p>La cuestión no es si uno debe declarar sus ingresos, sino cómo declarar sus ingresos AlquiYa en su declaración anual.</p>"
                    ."<p>Como particular, en la declaración anual de impuestos, declaramos todos nuestros ingresos.<br/>"
                    ."Los ingresos de AlquiYa no escapan a esta lógica, por lo que también se requiere que declare todos sus ingresos de AlquiYa en la Administración Tributaria.</p>",
                '' => "<p></p>",
            ],
            'Mi cuenta' => [
                'No puedo conectarme a AlquiYa' => "<p>Contáctenos directamente para enviarnos su problema <a href=\"".Url::to(['/page/pro'])."\">por aquí</a>.</p>",
                'Perdí mi contraseña' => "<p>No hay problema ! Puede hacer clic en el enlace proporcionado para este propósito en el formulario de inicio de sesión o <a href=\"".Url::to(['/site/request-password-reset'])."\">aquí</a>. Recibirá un correo electrónico que le permitirá crear fácilmente una nueva contraseña.</p>",
                'No recuerdo qué dirección de correo electrónico utilicé para mi registro.' => "<p>Puede revisar sus diferentes buzones para verificar el que le escribimos la primera vez.</p>"
                    ."<p>Si no puede encontrar nuestro correo electrónico, haga clic <a href=\"".Url::to(['/site/request-password-reset'])."\">aquí</a> e ingrese la dirección de correo electrónico que podría haber sido utilizada para crear su cuenta, luego haga clic en confirmar. Si esta dirección es correcta, recibirá un correo electrónico en su buzon. Si no, lo invitamos a probar con sus otras direcciones de correo electrónico.</p>",
            ]
        ];
        $i = 1;
        ?>
        
        
        <?php foreach($texts as $title => $questions): ?>

            <h2 class="text-primary"><?= $title ?></h2>
        
            <div class="panel-group" id="accordion<?= $i ?>" role="tablist" aria-multiselectable="true">

                <?php $j = 1; ?>
                <?php foreach($questions as $question => $answer): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="q<?= $i.$j ?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion<?= $i ?>" href="#a<?= $i.$j ?>" aria-expanded="true" aria-controls="collapseOne" class="<?= $i == 1 && $j == 1 ? '' : 'collapsed' ?>">
                                    <?= $question ?>
                                </a>
                            </h4>
                        </div>
                        <div id="a<?= $i.$j ?>" class="panel-collapse collapse <?= $i == 1 && $j == 1 ? 'in' : '' ?>" role="tabpanel" aria-labelledby="q<?= $i.$j ?>">
                            <div class="panel-body well">
                                <?= $answer ?>
                            </div>
                        </div>
                    </div>
                    <?php ++$j; ?>
                <?php endforeach; ?>

            </div>
            <?php ++$i ?>
        <?php endforeach; ?>
        
    </div>


</div>
