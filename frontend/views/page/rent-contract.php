<?php

/* @var $this \frontend\components\View */

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = "Modelo de contrato de alquiler entre particulares";//Yii::t('app', "Rent contract");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

?>
<div id="rent-contract" class="page">
    
    <div class="page-content well">
    
        <p>Para acompañarle hasta la conclusión del acuerdo, ponemos a su disposición un <b>contrato de alquiler</b> hecho por <b>AlquiYa 100% gratis</b>.</p>

        <p>Este contrato incluye todos los elementos esenciales para el buen funcionamiento del alquiler :</p>

        <ul>
            <li>Recordatorio de las reglas del sentido común</li>
            <li>Descripción del Bien (estado general, consejo de uso, monto del depósito ...)</li>
            <li>Duración del alquiler</li>
            <li>Garantía y responsabilidades de cada persona</li>
        </ul>

        <p>Puede usarlo <b>para cualquier alquiler de Bienes entre particulares</b>.</p>

        <p><a href="<?= Url::to('@web/file/contrato-alquiler-alquiya.pdf') ?>" class="btn btn-secondary btn-lg btn-block"><i class="fa fa-download"></i> Descargar gratis el contrato de alquiler</a></p>
    
    </div>
        
    <div class="page-content well">
    
        <p>
            El objetivo de <b>AlquiYa</b> es simple : <b>poner en relación</b> a una persona que desea <b>alquilar uno de sus Bienes</b> con una persona que está buscando uno para un <b>plazo fijo</b> y una utilidad precisa.<br/> 
            <b>AlquiYa</b> sirve aquí como un <b>intermediario entre las dos partes.</b>
        </p>
        
        <p><b>AlquiYa no compromete su responsabilidad</b> en la entrega entre los dos tercios.</p>
        
    </div>
    
</div>

<hr/>

<div id="contract">
    
    <div class="well">
    
        <h2 style="color:#000;font-size:100px;margin:0;line-height:1.1;" class="text-center"><b>AlquiYa.com</b></h2>
        <p style="font-size:30px;margin-top:0;" class="text-center"><b>Alquiler de Bienes entre particulares</b></p>

        <p class="text-center" style="font-size:20px;margin-top:1.5cm;"><b>Las reglas del sentido común para leer y aplicar!</b></p>

        <p style="line-height:2;">
            Antes de llenar el contrato de alquiler, compruebe que puede marcar todas las casillas por debajo. Si este es el caso, su alquiler se hará en las mejores condiciones. 
            Gracias por utilizar AlquiYa, el sitio web de colaboración especializado en el alquiler de Bienes entre particulares.
        </p>

        <div class="table" style="margin-top:25px;">
            <div class="table-cell inner left">
                <h3 style="font-size:19px;">Usted es el "arrendador" del Bien :</h3>
                <ul>
                    <li><b>Llena</b> los detalles del contrato de alquiler</li>
                    <li><b>Evalua</b> qué tan bien está funcionando su Bien y establezca un precio en consecuencia</li>
                    <li>Asegúrate de que tu material sea <b>revisado</b><small>(esto aumenta significativamente su fiabilidad)</small></li>
                    <li><b>Informa</b> al Arrendatario sobre el funcionamiento de su Bien<small>(Esto puede evitar llamadas de ayuda)</small></li>
                    <li><b>Opera</b> su equipo frente al Arrendatario</li>
                    <li>Establece un <b>depósito</b> coherente y comprometese a cobrarlo solo como último recurso<small>(1)</small></li>
                    <li>Comprueba que tu material esté <b>completo</b><small>(Su responsabilidad está en juego en caso de litigio)</small></li>
                    <li>Deja <b>el manual</b> del Bien en su caja</li>
                </ul>
            </div>
            <div class="table-cell inner right">
                <h3 style="font-size:19px;"><b>Usted es el "arrendatario" del Bien :</b></h3>
                <ul>
                    <li><b>Llena</b> los detalles del contrato de alquiler a su disposición por el propietario.</li>
                    <li><b>Comprueba</b> el estado del equipo<small>(choques, ruidos, rodamientos, juego anormal, arañazos, ...)</small></li>
                    <li>Pide el favor de ver el equipo <b>funcionando</b></li>
                    <li>Lee las <b>instrucciones</b> de funcionamiento con cuidado, cada dispositivo tiene sus especificidades</li>
                    <li>Comprometese a <b>mantener</b> el equipo<small>Tendra que devolverlo en el mismo estado</small></li>
                    <!--<li>Tiene una <b>responsabilidad civil</b> con su aseguradora</li>-->
                    <li>Usted pagará con <b>transferencia bancaria o cheque</b> para dejar un registro de la transacción</li>
                </ul>
            </div>
        </div>

        <p class="text-center" style="font-weight:400;font-size:13px;margin:15px 0 0 0;">(1) Por ejemplo : el precio del compra -20%, agregando un descuento dependiendo de la antigüedad de su equipo</p>

    </div>
    
    <div class="well">
        <h2 style="color:#000;font-size:32px;margin:0;line-height:1.1;" class="text-center"><b>Contrato de alquiler estándar entre particulares</b></h2>
        
        <p style="margin-top:35px;font-size:26px;" class="text-center"><b>Entre los firmantes :</b></p>
        
        <div class="row" style="margin-top:30px;">
            <div class="col-xs-6">
                <p style="margin-bottom:20px;font-size:20px;"><b>El Arrendador :</b></p>
                <p>Nombres : ...........................................................................................</p>
                <p>Apellidos : ..........................................................................................</p>
                <p>Dirección : ..........................................................................................</p>
                <p>&nbsp;..................................................................................................................</p>
                <p>Ciudad : ...................................... &nbsp;Barrio : ......................................</p>
                <p>Celular : ...............................................................................................</p>
                <p>Email : ....................................... @ ......................................................</p>
                <p>Cedula : ...............................................................................................</p>
            </div>
            <div class="col-xs-6">
                <p style="margin-bottom:20px;font-size:20px;"><b>El Arrendatario :</b></p>
                <p>Nombres : ...........................................................................................</p>
                <p>Apellidos : ..........................................................................................</p>
                <p>Dirección : ..........................................................................................</p>
                <p>&nbsp;..................................................................................................................</p>
                <p>Ciudad : ...................................... &nbsp;Barrio : ......................................</p>
                <p>Celular : ...............................................................................................</p>
                <p>Email : ....................................... @ ......................................................</p>
                <p>Cedula : ...............................................................................................</p>
            </div>
        </div>
        
        <div class="inner" style="margin-top:25px;">
            <h3>Articulo 1 : el Bien</h3>
            
            <?php $dots = '....................................................................................................................................................................................................................................'; ?>
            <p><b>Descripción</b> del Bien alquilado (nombre, marca, tipo, número de serie, tamaño, color, potencia) :</p>
            <?php for($i=1;$i<=5;$i++): ?>
                <p><?= $dots ?></p>
            <?php endfor; ?>
            <p><b>Condición general</b> (antigueda, apariencia, daños, desgastes, defectos, ...) :</p>
            <?php for($i=1;$i<=2;$i++): ?>
                <p><?= $dots ?></p>
            <?php endfor; ?>
            <p><b>Precauciones</b> de uso :</p>
            <?php for($i=1;$i<=2;$i++): ?>
                <p><?= $dots ?></p>
            <?php endfor; ?>
            <p>Otras cláusulas especiales (restitución con el nivel de combustible, suministros, ubicación, ...) :</p>
            <?php for($i=1;$i<=3;$i++): ?>
                <p><?= $dots ?></p>
            <?php endfor; ?>
            <p style="margin-top:35px;">
                <b>Monto del depósito :</b> ........................................................ &nbsp; 
                <b>Precio diario del alquiler :</b> ........................................................
            </p>
        </div>
        
    </div>
    
    <div class="well">
        
        <div class="table">
            <div class="table-cell inner left">
                <h3>Articulo 2 : duración del alquiler</h3>
                <p>El alquiler comienza en la <b>recogida del Bien</b> : </p>
                <p>el .............................................. a las ..............................................</p>
                <p>La contratación termina con la <b>restitución del Bien</b> : </p>
                <p>el .............................................. a las ..............................................</p>
                <p style="margin-top:20px;font-size:14px;">Cualquier extensión debe ser objeto de un acuerdo por escrito entre las dos partes.</p>
            </div>
            <div class="table-cell inner right">
                <h3>Articulo 3 : garantía</h3>
                <p>
                    <b>El depósito se pagará con cheque bancario, efectivo o transferencia bancaria</b>, 
                    y se devolverá al Arrendatario a la restitución del Bien, menos cualquier daño relacionado con el artículo 4. 
                    El propietario podrá encajar el depósito en caso de que el Bien no se devuelva al Arrendador dentro de los 30 días posteriores a la fecha de devolución del Bien.
                </p>
            </div>
        </div>
        
        <div class="inner" style="margin-top:20px;">
            <h3>Articule 4 : responsabilidades</h3>
            <p>Daños, pérdidas o robos, y responsabilidades hasta la devolución del Bien.</p>
            <p>El Arrendatario se compromete a : (marcar las casillas)</p>
            <ul>
                <li><b>Respetar</b> todas las reglas de uso del Bien, cuidar el Bien y asegurar la integridad del Bien.</li>
                <li>Usar el Bien para fines legales, <b>no venderlo, no subarrendarlo, no modificarlo, no cambiarlo</b>.</li>
                <li><b>Ser el propietario exclusivo</b> del Bien y ser el único guardián del Bien.</li>
                <li>Poseer todos las <b>habilidades reglamentarias</b> para usar o mantener el Bien.</li>
                <li>Conocer todas las <b>precauciones de uso</b> y todas las reglas de seguridad para implementar.</li>
                <li>Ser el <b>único responsable de cualquier daño</b> corporal o material que el Bien podria causar.</li>
                <!--<li>Tener un <b>seguro de responsabilidad civil</b> personal.</li>-->
            </ul>
            
            <p style="margin-top:20px;">
                <b>Durante el período de alquiler</b> del Bien, el Arrendador <b>queda liberado de cualquier responsabilidad</b> por la propiedad y su uso.
                <b>Los daños</b> causado por el Arrendatario <b>serán a su cargo</b>. 
                El monto del depósito <b>no podrá exceder el valor indicado en el artículo 1</b>. 
                Sean cuales sean sus posibles reparaciones,<b> el Bien seguirá siendo propiedad del Arrendador</b>. 
                En caso de no devolución del Bien, el Arrendatario deberá <b>pagar al Arrendador</b> el valor del depósito del Bien especificado en el Artículo 1.
            </p>
            
        </div>
        
        <div class="inner" style="margin-top:20px;">
            <h3>Articulo 5 : litigios</h3>
            
            <p>
                El sitio <b>www.alquiya.com no es de ninguna manera parte de este contrato</b> y <b>no puede ser considerado responsable</b>, directo o indirecto, 
                por cualquier litigo o daño relacionado con la ejecución del contrato.
                Si, en caso de litigo, el Arrendatario y el Arrendador no encuentran una solución amistosa, los tribunales del lugar de la firma del presente contrato se mantendrán solo por competentes.
            </p>
            
        </div>
        
        <p style="margin-top:30px;">
            Hecho en .........................................................................................................., el ................................................................................................. <br/>
            en dos copias, una de las cuales se entrega a cada una de las dos partes que la reconocen.
        </p>
        
        <p class="clearfix" style="margin-top:15px;">
            <b class="pull-right">Firma del Arrendatario</b>
            <b>Firma del Arrendador</b>
        </p>
        
        <p><br/><br/></p>
        
    </div>
    
</div>
