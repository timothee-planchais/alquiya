<?php

use frontend\models\Conversation;
use common\models\Message;
use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this frontend\components\View */
/* @var $user User */
/* @var $conversation Conversation */
/* @var $form ActiveForm */
/* @var $author User */

$this->title = Yii::t('app', "Conversation with {user}", ['user' => Html::encode($user->username)]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Conversations"), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

$messages = $conversation->getMessages();

?>

<div id="conversation" class="row">
    
    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10">
        <div id="messages" class="well page-content">

            <h2 class="text-primary"><?= Yii::t('app', "Conversation") ?></h2>
            
            <?php if($messages): ?>

                <?php foreach($messages as $message): ?>
                    <?php 
                    $author = $message->author_id == $user->getPrimaryKey() ? $user : Yii::$app->getUser()->getIdentity();
                    $isAuthor = $message->author_id == Yii::$app->user->getId();
                    ?>
                    <div class="message <?= $isAuthor ? 'is-author' : '' ?> media">
                        <div class="media-left <?= $isAuthor ? 'empty' : '' ?>">
                            <?php if(!$isAuthor): ?>
                                <img src="<?= Url::to($author->getImageUrl('300x300')) ?>" class="media-object"/>
                                <p class="text-center"><a href="<?= url::to($author->getProfileUrl()) ?>"><?= Html::encode($author->username) ?></a></p>
                            <?php endif; ?>
                        </div>
                        <div class="media-body">
                            <div class="inner">
                                <p class="message-subject"><?= Html::encode($message->subject) ?></p>
                                <p class="message-text"><?= nl2br(Html::encode($message->message)) ?></p>
                                <p class="message-date text-sanfrancisco"><?= Yii::$app->formatter->asDateTime($message->created_at, 'short') ?></p>
                            </div>
                        </div>
                        <div class="media-right <?= !$isAuthor ? 'empty' : '' ?>">
                            <?php if($isAuthor): ?>
                                <img src="<?= Url::to($author->getImageUrl('300x300')) ?>" class="media-object"/>
                                <p class="text-center"><a href="<?= url::to($author->getProfileUrl()) ?>"><?= Html::encode($author->username) ?></a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>

            <?php endif; ?>
    
        </div>
        <div class="well page-content" id="new-message">
    
            <h2 class="text-primary"><?= Yii::t('app', "Message for {user}", ['user' => Html::encode($user->username)]) ?> :</h2>
            <?php 

            $message = new common\models\Message([
                'subject' => "Re: ".$conversation->getLastMessage()->subject
            ]);
            $template = "{input}\n{hint}\n{error}";
            ?>

            <?php $form = ActiveForm::begin(['action' => ['send', 'userId' => $user->id], 'method' => 'POST']) ?>

            <?= $form->field($message, 'subject', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $message->subject]) ?>

            <?= $form->field($message, 'message', ['template' => $template])->textarea(['maxlength' => true, 'placeholder' => Yii::t('app', "Respond here")]) ?>

            <p class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-send-o"></i> &nbsp;<?= Yii::t('app', "Send") ?></button>
            </p>

            <?php $form->end() ?>

        </div>
    </div>
            
     <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
        <div class="well page-content">
            <a href="<?= url::to($user->getProfileUrl()) ?>" ><img src="<?= Url::to($user->getImageUrl()) ?>" class="img-responsive" alt=""/></a>
            <p class="text-center" style="margin-top:15px;"><a href="<?= url::to($user->getProfileUrl()) ?>" ><?= Html::encode($user->username) ?></a></p>
        </div>
         <p><a href="#new-message" class="btn btn-secondary btn-block goto"><i class="fa fa-envelope"></i> <?= Yii::t('app', "New message") ?></a></p>
    </div>
    
</div>