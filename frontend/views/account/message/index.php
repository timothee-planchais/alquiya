<?php

use common\models\Message;
use frontend\models\Conversation;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this frontend\components\View */
/* @var $conversations Conversation */

$this->title = Yii::t('app', "Conversations");

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div id="conversations">
    
    <?php if(!empty($conversations)): ?>
    
        <table class="table table-striped">
        <tbody>
            <?php foreach($conversations as $conversation): ?>
            
                <?php if($conversation->user): ?>
            
                    <?php 
                    $user = $conversation->user;
                    $url = Url::to(['conversation', 'id' => $conversation->id]);
                    ?>
                    <tr class="conversation <?= $conversation->getLastMessage()->isNotRead() ? 'new-message' : '' ?>">
                        <td>
                            <a href="<?= Url::to($user->getProfileUrl()) ?>">
                                <img src="<?= url::to($user->getImageUrl('300x300')) ?>" alt="<?= $user->username ?>"/> &nbsp;
                                <?= Html::encode($user->username) ?>
                            </a>
                        </td>
                        <td>
                            <?php if($conversation->getLastMessage()->isNotRead()): ?>
                            <i class="fa fa-exclamation-circle not-read text-secondary"></i>
                            <?php endif; ?>
                            
                            <a href="<?= $url ?>"><?= $conversation->getTitle() ?></a>
                        </td>
                        <td class="text-right"><?= Yii::$app->formatter->asRelativeTime($conversation->getDate()) ?></td>
                        <td class="text-right"><a href="<?= $url ?>" class="btn btn-secondary"><i class="fa fa-envelope"></i> <?= Yii::t('app', "See") ?></a></td>
                    </tr>

                <?php endif; ?>

            <?php endforeach; ?>
        </tbody>
        </table>
            
    <?php endif; ?>
    
</div>