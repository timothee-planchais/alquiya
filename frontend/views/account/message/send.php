<?php

use common\models\Message;
use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;

/* @var $this frontend\components\View */
/* @var $model Message */

$this->title = Yii::t('app', "Send a message");
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

$template = "{input}\n{hint}\n{error}";

?>

<div id="message-send" class="well" style="max-width:700px;margin:0 auto;">
    
    <?php $form = ActiveForm::begin() ?>

        <?php if($model->item_id && $model->item): ?>
            <?= $form->field($model, 'item_id')->textInput(['disabled' => true, 'value' => $model->item->title]) ?>
        <?php endif; ?>
    
        <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'message')->textarea(['maxlength' => true]) ?>

        <p class="form-group text-center">
            <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-send-o"></i> &nbsp;<?= Yii::t('app', "Send") ?></button>
        </p>

    <?php $form->end() ?>
    
</div>