<?php

use common\models\Subscription;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchSubscription */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Subscriptions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="subscription-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model){
            $class = '';
            if($model->status == Subscription::STATUS_ACTIVE || $model->status == Subscription::STATUS_UPCOMING) {
                $class = 'success';
            }
            elseif($model->status == Subscription::STATUS_PENDING) {
                $class = 'warning';
            }
            return ['class' => $class];
        },
        'columns' => [
            [
                'attribute' => 'type',
                'value' => function($model) {
                    $html = $model->getTextType();
                    if($model->type == Subscription::TYPE_ITEM_BUMPED) {
                        $html .= '<br/><i class="text-muted" style=font-weight:400;>'.Yii::t('app', "Bumped at").' : '.Yii::$app->formatter->asDatetime($model->item->bump_date, 'short').'</i>';
                    }
                    return $html;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'item_id',
                'value' => function($model) {
                    return $model->item ? Html::a(Html::encode($model->item->title), ['/item/view', 'id' => $model->item_id]) : null;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getTextStatus();
                }
            ],
            [
                'attribute' => 'period',
                'value' => function($model) {
                    return $model->getTextPeriod();
                }
            ],
            [
                'attribute' => 'start_date',
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->start_date, 'short');
                }
            ],
            [
                'attribute' => 'end_date',
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->end_date, 'short');
                }
            ],
            'code',
            [
                'attribute' => 'order_id',
                'value' => function($model) {
                    return $model->order->__toString();
                },
            ],
            /*[
                'attribute' => 'created_at',
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->created_at, 'short');
                }
            ],*/

            /*[
                'class' => 'backend\widgets\grid\ActionColumn'
            ],*/
        ],
    ]); ?>
</div>
