<?php

use yii\helpers\Html;


/* @var $this \frontend\components\View */
/* @var $model common\models\Item */

$this->title = Yii::t('app', 'Publish a product');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-create">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user
    ]) ?>

</div>
