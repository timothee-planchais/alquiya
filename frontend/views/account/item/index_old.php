<?php

use common\models\Item;
use yii\helpers\Url;
use common\models\Category;
use yii\helpers\Html;
use \yii\helpers\ArrayHelper;
use frontend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My products');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

$categories = ArrayHelper::map(Category::find()->where('parent_id IS NULL')->all(), 'id', 'title');
//$subCategories = ArrayHelper::map(Category::find()->where('parent_id IS NOT NULL')->all(), 'id', 'title');

?>
<div id="user-items">

    <p>
        <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> '.Yii::t('app', "Publish a product")), ['create'], ['class' => 'btn btn-secondary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model) {
            $color = '';
            
            if($model->locked) {
                $color = '#ffd8d4';
            }
            elseif($model->status == Item::STATUS_INACTIVE) {
                $color = '#fcf8e3';
            }
            
            return ['style' => 'background-color:'.$color.';'];
        },
        'columns' => [
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->locked) {
                        return '<i class="fa fa-ban"></i> '.Yii::t('app', "Locked");
                    }
            
                    $html = $model->getTextStatus().'<br/>';
                    
                    if($model->status == Item::STATUS_ACTIVE) {
                        $html .= Html::a('<i class="fa fa-minus-circle"></i> '.Yii::t('app', "Inactivate"), ['inactivate', 'id' => $model->getPrimaryKey()], ['class' => 'btn btn-xs btn-danger']);
                    }
                    else {
                        $html .= Html::a('<i class="fa fa-check"></i> '.Yii::t('app', "Activate"), ['activate', 'id' => $model->getPrimaryKey()], ['class' => 'btn btn-xs btn-success']);
                    }
                    
                    return $html;
                },
                //'filter' => Item::getStatuses(),
                'format' => 'raw'
            ],
            /*[
                'attribute' => 'category_id',
                'value' => 'category.title',
                //'filter' => $categories
            ],*/
            [
                'attribute' => 'title',
                'value' => function($model){
                    $html = Html::a(Html::encode($model->title), $model->getUrl());
                    
                    $htmlSubscriptions = $model->getNiceActiveSubscriptions('');
                    if($htmlSubscriptions) {
                        $html .= '<br/><p class="item-active-subscriptions text-muted">'.$htmlSubscriptions.'</p>';
                    }
                    
                    return $html;
                },
                'format' => 'raw'
            ], 
            /*[
                'label' => Yii::t('app', "Image"),
                'value' => function($model){
                    return $model->firstImage ? Html::img($model->firstImage->getImageUrl(), ['width' => 200]) : '';
                },
                'format' => 'raw'
            ],*/
            [
                'label' => Yii::t('app', 'Address'),
                'attribute' => 'short_address',
                'value' => function($model) {
                    return nl2br(Html::encode($model->short_address));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'price',
                'format' => 'currency',
            ],
            /*[
               'attribute' => 'nb_views',
               'filter' => false
            ],
            [
               'attribute' => 'nb_rents',
               'filter' => false
            ],*/
            /*[
                'attribute' => 'bump_date',
                'value' => function($model) {
                    return Yii::$app->formatter->asDate($model->bump_date, 'short');
                },
                'filter' => false
            ],*/
            [
                'attribute' => 'created_at',
                'value' => function($model) {
                    return Yii::$app->formatter->asDate($model->created_at, 'short');
                },
                'filter' => false
            ],
            //'updated_at:datetime',

            [
                'class' => 'frontend\widgets\grid\ActionColumn',
                //'controller' => '/account/item',
                'template' => '{bump} {feature} {highlight} {update} {delete}',
                'buttons' => [
                    'bump' => function ($url, $model, $key) {
                        if($model->status == Item::STATUS_ACTIVE && !$model->locked)
                        {
                            $options = [
                                'title' => Yii::t('app', 'Bump'),
                                'aria-label' => Yii::t('app', 'Bump'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-secondary'
                            ];
                            $url = ['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $model->id, 'subscriptionType' => common\models\Subscription::TYPE_ITEM_BUMPED];
                            return Html::a('<span class="fa fa-arrow-up"></span>', $url, $options);
                        }
                    },
                    'feature' => function ($url, $model, $key) {
                        if($model->status == Item::STATUS_ACTIVE && !$model->locked)
                        {
                            $options = [
                                'title' => Yii::t('app', 'Feature'),
                                'aria-label' => Yii::t('app', 'Feature'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-secondary'
                            ];
                            $url = ['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $model->id, 'subscriptionType' => common\models\Subscription::TYPE_ITEM_FEATURED];
                            return Html::a('<span class="fa fa-certificate"></span>', $url, $options);
                        }
                    },
                    'highlight' => function ($url, $model, $key) {
                        if($model->status == Item::STATUS_ACTIVE && !$model->locked)
                        {
                            $options = [
                                'title' => Yii::t('app', 'Highlight'),
                                'aria-label' => Yii::t('app', 'Highlight'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-secondary'
                            ];
                            $url = ['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $model->id, 'subscriptionType' => common\models\Subscription::TYPE_ITEM_HIGHLIGHTED];
                            return Html::a('<span class="fa fa-star"></span>', $url, $options);
                        }
                    },
                ]
            ],
        ],
    ]); ?>
</div>
