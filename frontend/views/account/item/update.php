<?php

use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $model common\models\Item */

$this->title = Yii::t('app', 'Update item').' '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => $model->getUrl()];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div id="item-update">

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user
    ]) ?>

</div>
