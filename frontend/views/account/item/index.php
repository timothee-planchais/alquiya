<?php

use common\models\Item;
use yii\helpers\Url;
use common\models\Category;
use yii\helpers\Html;
use \yii\helpers\ArrayHelper;
use frontend\widgets\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My products');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

$items = $dataProvider->getModels();
$pagination = $dataProvider->getPagination();

$sorts = [
    '-created_at' => Yii::t('app', "Lastest"),
    'created_at' => Yii::t('app', "Less recent"),
    'price' => Yii::t('app', "Más baratos"),
    '-price' => Yii::t('app', "More expensive"),
    'title' => Yii::t('app', "Title")
];

?>
<div id="user-items">

    <div class="well">
        <p class="clearfix">
            <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> '.Yii::t('app', "Publish a product")), ['create'], ['class' => 'btn btn-secondary btn-lg']) ?>
        </p>
    </div>

    <div id="items" class="items">
        
        <div id="items-nb-results" class="clearfix">

            <div class="pull-right">
                <strong class="text-primary"><?= Yii::t('app', "Order by") ?> : </strong>&nbsp;&nbsp;

                <?php
                $sortItems = [];
                foreach($sorts as $value => $sort) {
                    $sortItems[] = ['label' => $sort, 'url' => Url::current(['sort' => $value]), 'options' => ['class' => Yii::$app->request->get('sort') == $value ? 'active' : '']];
                }
                ?>
                <?= yii\bootstrap\ButtonDropdown::widget([
                    'label' => ArrayHelper::getValue($sorts, Yii::$app->request->get('sort'), Yii::t('app', "Lastest")) ,
                    'options' => ['class' => 'btn btn-default'],
                    'dropdown' => [
                        'items' => $sortItems
                    ]
                ]) ?>
            </div>

            <p><span class="text-sanfrancisco"><?= Yii::$app->formatter->asInteger($dataProvider->getTotalCount()) ?></span> <?= Yii::t('app', "results found") ?></p>

        </div>
        
        <div id="items-actions">
            <a href="<?= Url::to(['activate-all']) ?>" class="btn btn-xs btn-default"><i class="fa fa-check-square-o"></i> <?= Yii::t('app', 'Activate all') ?></a>
            <a href="<?= Url::to(['inactivate-all']) ?>" class="btn btn-xs btn-default"><i class="fa fa-minus-square-o"></i> <?= Yii::t('app', 'Inactivate all') ?></a>
        </div>
        
        <div id="items-list">

            <?php if($items): ?>

                <?php foreach($items as $i => $item): ?>

                    <?= $this->render('/item/_item', ['item' => $item, 'asRow' => true, 'showActions' => true]) ?>

                <?php endforeach; ?>

            <?php else: ?>

            <p></p>

            <?php endif; ?>
        </div> 
    </div>

    <?=yii\widgets\LinkPager::widget([
        'pagination' => $pagination,
        'prevPageLabel' => '<i class="fa fa-chevron-left"></i> '.Yii::t('app', "Previous"),
        'nextPageLabel' => Yii::t('app', "Next").' <i class="fa fa-chevron-right"></i>'
    ]); ?>

</div>
