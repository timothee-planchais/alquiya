<?php

use common\models\Item;
use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this \frontend\components\View */
/* @var $model common\models\Item */
/* @var $form yii\widgets\ActiveForm */
/* @var $user \common\models\User */

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?' . http_build_query(['libraries' => 'places', 'language' => Yii::$app->params['google.api_language'], 'key' => Yii::$app->params['google.api_key']]));
$this->registerJsFile('@web/js/plugins/jquery.geocomplete.min.js', ['depends'=> \frontend\assets\AppAsset::class]);

//$categories = ArrayHelper::map(Category::find()->where('parent_id IS NULL')->all(), 'id', 'title');
//$subCategories = ArrayHelper::map(Category::find()->where('parent_id IS NOT NULL')->all(), 'id', 'title');

$subCategories = [];
if(!$model->isNewRecord) {
    $subCategories = ArrayHelper::map(Category::find()->where(['parent_id' => $model->category_id])->all(), 'id', 'title');
}

$this->usePageContent = false;

$isPro = $user->isPro();

?>

<div id="item-form">

    <?php $form = ActiveForm::begin(/*Yii::$app->params['bootstrapFormConfig']*/); ?>
    
    
    <?php if(!$model->isNewRecord): ?>
    
        <div class="well">
    
            <h2 class="text-primary"><?= Yii::t('app', "Status") ?></h2>

            <?= $form->field($model, 'status', ['template' => "{input}\n{error}"])->radioList(Item::getStatuses(), ['item' => Yii::$app->params['formRadioItem']]) ?>
            
        </div>
    <?php endif; ?>

    <div class="well">
        <h2 class="text-primary"><?= Yii::t('app', 'Category') ?></h2>
        
        <div class="row">
            <div class="col-sm-6">
                <?php if($model->isNewRecord): ?>
                    <?= $form->field($model, 'category_id')->dropDownList(Yii::$app->controller->getDropdownCategories()) ?>
                <?php else: ?>
                    <?= $form->field($model, 'category_id')->textInput(['value' => $model->category->title, 'disabled' => true]) ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-6">
                <?php if($model->isNewRecord): ?>
                    <?= $form->field($model, 'subcategory_id')->dropDownList($subCategories, ['prompt' => Yii::t('app', "Select a subcategory")]) ?>
                <?php elseif($model->subcategory_id): ?>
                    <?= $form->field($model, 'subcategory_id')->textInput(['value' => $model->subcategory->title, 'disabled' => true]) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
   
    <div class="well">
    
        <h2 class="text-primary"><?= Yii::t('app', "Product") ?></h2>
        
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?php //echo $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6, 'maxlength' => true]) ?>
        
        <?php
        /**
         * http://demos.krajee.com/widget-details/fileinput
         * http://plugins.krajee.com/file-advanced-usage-demo
         */

        if($model->isNewRecord)
        {
            /*
             * FILE INPUT
             */
            echo $form->field($model, 'tmp_images[]')->widget(FileInput::class, [
                'pluginOptions' => [
                    'showUpload' => false,
                    'showCaption' => false,
                    'showRemove' => true,
                    'removeClass' => 'btn btn-secondary fileinput-remove fileinput-remove-button',
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' => Yii::t('app', "Select pictures (4 max)"),
                    'maxFileCount' => Yii::$app->params['item.max_files'],
                    'allowedFileExtensions' => ['jpg', 'png', 'jpeg'],
                ],
                'options' => ['accept' => 'image/*', 'multiple' => true]
            ])/*->label(true)*/;
        }
        else
        {
            /*
             * AJAX UPLOAD AND DELETE
             */
            $initialPreview = [];
            $initialPreviewConfig = [];

            $images = $model->images;

            if($images)
            {
                foreach($images as $image) {
                    $initialPreview[] = $image->getImageUrl();
                    $initialPreviewConfig[] = ['key' => $image->getPrimaryKey()];
                }
            }

            echo FileInput::widget([
                'name' => 'tmp_images',
                'options'=>[
                    'multiple'=>true
                ],
                'pluginOptions' => [
                    'showCaption' => false,
                    'showUpload' => false,
                    'showRemove' => true,
                    'removeClass' => 'btn btn-secondary fileinput-remove fileinput-remove-button',
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseLabel' => Yii::t('app', "Select pictures (4 max)"),
                    'uploadUrl' => Url::to(['/account/item/upload-image', 'id' => $model->getPrimaryKey()]),
                    'deleteUrl' =>  Url::to(['/account/item/delete-image']),
                    'uploadExtraData' => [
                        //'id' => $model->getPrimaryKey()
                    ],
                    'initialPreview' => $initialPreview,
                    'initialPreviewConfig' => $initialPreviewConfig,
                    'initialPreviewAsData' => true,
                    'overwriteInitial' => false,
                    'validateInitialCount' => true,
                    'maxFileCount' => Yii::$app->params['item.max_files'],
                    'allowedFileExtensions' => ['jpg', 'png', 'jpeg']
                ]
            ]);
        }

        ?>
        
    </div>

    <div class="well">
    
        <h2 class="text-primary"><?= Yii::t('app', "Price") ?></h2>
        
        <?php if($isPro): ?>
        
            <div class="row">
                <!--<div class="col-xs-12 col-sm-6">
                    <?php // $form->field($model, 'on_quote', ['inline' => true])->radioList(\common\components\Utils::getYesNo()) ?>
                </div>-->
                <div class="col-xs-12 col-sm-6">
                    <?= $form->field($model, 'pricing_type', ['inline' => true])->radioList(Item::getPricingTypes()) ?>
                </div>
            </div>
        
        <?php endif; ?>

        <div id="item-prices">
        
            <p style="margin-bottom:15px;font-size:16px;font-weight:600;"><?= Yii::t('app', "Rent price").' ('.Yii::$app->formatter->currencyCode.')' ?></p>

            <div class="row form-group">
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <?= $form->field($model, 'price')->input('number', ['step' => '0.001', 'placeholder' => 0])->label(Yii::t('app', "Per day")) ?>
                    <!--<p id="item-price-total" class="price-details"><?= Yii::t('app', "Total amount") ?> : <span class="currency"><?= str_replace(0,'<span class="value">0</span>',(Yii::$app->formatter->asCurrency(0))) ?></span></p>-->
                </div>
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <?= $form->field($model, 'price_weekend')->input('number', ['step' => '0.001', 'placeholder' => 0])->label(Yii::t('app', "Per weekend (3 days)")) ?>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <?= $form->field($model, 'price_week')->input('number', ['step' => '0.001', 'placeholder' => 0])->label(Yii::t('app', "Per week (7 days)")) ?>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3">
                        <?= $form->field($model, 'price_month')->input('number', ['step' => '0.001', 'placeholder' => 0])->label(Yii::t('app', "Per month (30 days)")) ?>
                    </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?= $form->field($model, 'deposit_value')->input('number', ['step' => '0.001']) ?>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?= $form->field($model, 'price_new')->input('number', ['step' => '0.001']) ?>
            </div>
        </div>
        
    </div>
    
    <div class="well">
    
        <h2 class="text-primary"><?= Yii::t('app', "Location") ?></h2>

        <p class="text-muted">
            <?= Yii::t('app', "Please select an address") ?>. <?= Yii::t('app', "Your exact address will not be shown") ?>.
        </p>

        <?= $form->field($model, 'short_address', ['template' => "{input}\n{error}"])->hiddenInput() ?>
        <?= Html::activeHiddenInput($model, 'address') ?>
        <?= Html::activeHiddenInput($model, 'neighborhood') ?>
        <?= Html::activeHiddenInput($model, 'zipcode') ?>
        <?= Html::activeHiddenInput($model, 'city') ?>
        <?= Html::activeHiddenInput($model, 'full_city') ?>
        <?= Html::activeHiddenInput($model, 'region') ?>
        <?= Html::activeHiddenInput($model, 'state') ?>
        <?= Html::activeHiddenInput($model, 'country') ?>
        <?= Html::activeHiddenInput($model, 'country_code') ?>
        <?= Html::activeHiddenInput($model, 'lat') ?>
        <?= Html::activeHiddenInput($model, 'lng') ?>

        <?= $form->field($model, 'full_address')->textInput(['maxlength' => true/*, 'placeholder' => Yii::t('app', "Type an address")*/]) ?>

        <div id="address-img" class="form-group map-img map-squarred" style="display:none;">
            <img src="" class="img-responsive" alt="" width="100%"/>
        </div>

        
    
    </div>
    
    <div class="well">
        
        <h2 class="text-primary"><?= Yii::t('app', "Informations") ?></h2>
        
        <?= $form->field($model, 'delivery')->checkboxList(Item::getDeliveries()) ?>
        
        <?= $form->field($model, 'condition', ['inline' => true])->radioList(Item::getConditions()) ?>
        
    </div>
    
    <div class="text-right">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Publish') : Yii::t('app', "Save"), ['class' => 'btn btn-secondary btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        <?php if($isPro): ?>
            togglePricingType()
            $('#item-pricing_type').on('change', function(){
                togglePricingType();
            });
        <?php endif; ?>
        
        <?php if(!$model->isNewRecord): ?>
            updatePrices();
        <?php endif; ?>
        
        $('#item-price').on('keyup', function(){
            updatePrices();
        });
        
        <?php if(empty($subCategories)): ?>
            loadSubcategories();
        <?php endif; ?>
        $('#item-category_id').on('change', function(){
            loadSubcategories();
        });
        
        <?php if(!$model->isNewRecord && $model->lat): ?>
            showAddressImg('<?= $model->lat ?>', '<?= $model->lng ?>');
        <?php endif; ?>
        
        /*
         * 
         */
        var options = {"types":[/*"(regions)", "geocode", "establishment"*/],"componentRestrictions":{country: COUNTRY}};
        
        $('#item-full_address').on('keyup', function(){
            if($('#item-full_address').val().length <= 1) {
                $('#item-lat').val('');
                $('#item-lng').val('');
                $('#item-short_address, #item_address').val('');
                hideAddressImg();
            }
        });
        
        $('#item-full_address').geocomplete(options).bind("geocode:result", function(event, $result){
            if($result) 
            {
                var $lat = $result.geometry.location.lat();
                var $lng = $result.geometry.location.lng();
                var $address = $result.formatted_address;
                showAddressImg($lat, $lng);
                
                var $neighborhood, $zipcode, $city, $region, $state, $country;
                $.each($result.address_components, function(i, $row){
                    if($row.types[0] == 'postal_code') {
                        $zipcode = $row.long_name;
                    }
                    else if($row.types[0] == 'locality') {
                        $city = $row.long_name;
                    }
                    else if($row.types[0] == 'administrative_area_level_2') {
                        $state = $row.long_name;
                    }
                    else if($row.types[0] == 'administrative_area_level_1') {
                        $region = $row.long_name;
                    }
                    else if($row.types[0] == 'country') {
                        $country = $row.long_name;
                        $('#item-country_code').val($row.short_name);
                    }
                    else if($row.types[0] == 'neighborhood') {
                        $neighborhood = $row.long_name;
                    }
                });
                if(!$city) {
                    $city = $state;
                }
                $('#item-address').val($result.name);
                $('#item-lat').val($lat);
                $('#item-lng').val($lng);
                $('#item-neighborhood').val($neighborhood);
                $('#item-zipcode').val($zipcode);
                $('#item-city').val($city);
                $('#item-state').val($state);
                $('#item-region').val($region);
                $('#item-country').val($country);
                var $short_address = ($neighborhood ? $neighborhood+', ' : '')+$city/*+($zipcode ? ', '+$zipcode : '')*/+($region ? ', '+$region : '');
                var $full_city = $city+($region ? ', '+$region : '')+($country ? ', '+$country : '');
                $('#item-short_address').val($short_address);
                $('#item-full_city').val($full_city);
                $('.field-item-short_address').removeClass('has.error').find('.help-block-error').hide();
            }
        });
        
        /*detailsPrice();
    
        $('#item-price').on('blur', function(){
            detailsPrice();
        });*/
        
    });

    function loadSubcategories()
    {
        var $elemCategories = $('#item-category_id');
        var $elemSubCategories = $('#item-subcategory_id');
        
        var $categoryId = $elemCategories.val();
        $elemSubCategories.empty();
        
        if($categoryId)
        {
            $.ajax({
                url: "<?= Url::to(['/item/subcategories']) ?>",
                method: "POST",
                data: { 
                    id: $categoryId
                },
                dataType: "json"
            }).done(function( $response ) {
                if($response.status == 1 && $response.categories)
                {
                    $elemSubCategories.append($('<option></option>').val('').html('<?= Yii::t('app', "Select a subcategory") ?>'));
                    $.each($response.categories, function($index, $value) {          
                        $elemSubCategories.append($('<option></option>').val($index).html($value));
                    });
                    $('.field-item-subcategory_id').show();
                }
                else {
                    $('.field-item-subcategory_id').hide();
                }
            });
        }
    }
    
    function togglePricingType()
    {
        var $val = $('#item-pricing_type input:checked').val();
        
        if($val == '<?= Item::PRICING_TYPE_REQUEST ?>') {
            $('#item-prices').hide();
        }
        else {
            $('#item-prices').show();
        }
    }
    
    function updatePrices()
    {
        var $price = parseInt($('#item-price').val());
        $('#item-price_weekend').attr('placeholder', $price*2);
        $('#item-price_week').attr('placeholder', $price*5);
        $('#item-price_month').attr('placeholder', $price*15);
    }

    function showAddressImg($lat, $lng)
    {
        var $imgUrl = 'https://maps.googleapis.com/maps/api/staticmap?'
                +'center='+$lat+','+$lng
                //+'&zoom=12&size=790x250'
                +'&zoom=15&size=790x250'
                +'&scale=2'
                //+'&markers='+$lat+','+$lng /*icon:<?= urlencode(Url::to('@web/img/map-marker-shadow.png', true)) ?>|*/
                +'&language=<?= Yii::$app->language ?>'
                +'&maptype=roadmap&key=<?= Yii::$app->params['google.api_key'] ?>';
        $('#address-img').find('img').attr('src', $imgUrl);
        $('#address-img').slideDown(400);
    }
    
    function hideAddressImg()
    {
        $('#address-img').find('img').attr('src', '');
        $('#address-img').slideUp(400);
    }
    
    /*function detailsPrice()
{
    var $price = $('#item-price').val();
    var $priceWithFees = $price + ($price * FEES);
    var $elem = $('#item-price-total');
    
    if($price) {
        $elem.show();
        $elem.find('.value').text($priceWithFees);
    }
    else {
        $elem.hide();
    }
}

function detailsPriceWeekend()
{
    
}
*/

</script>
<?php $this->endBlock(); ?>