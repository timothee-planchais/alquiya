<?php

use common\models\User;
use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $items Item[] */

$this->title = Yii::t('app', "My favorites");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

?>

<div id="user-wishlist">
    
    <div id="items" class="items">
        
        <?php if(!empty($items)): ?>
        
            <div id="items-list">
                <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4">
        
                    <?php foreach($items as $item): ?>
                        <li><?= $this->render('/item/_item', ['item' => $item, 'showWishlistButton' => true]) ?></li>
                    <?php endforeach; ?>
                    
                </ul>
            </div>
                    
        <?php else: ?>
        
            <p class=""><?= Yii::t('app', "No favorite") ?></p>
            
        <?php endif; ?>
        
    </div>
    
    
</div>