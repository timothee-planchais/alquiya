<?php

use common\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\grid\GridView;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions' => function($model){
            $class = 'warning';
            
            if($model->status == Order::STATUS_PAID) {
                $class = 'success';
            }
            elseif($model->status == Order::STATUS_PAYMENT_CANCELLED || $model->status == Order::STATUS_PAYMENT_ERROR) {
                $class = 'danger';
            }
            return ['class' => $class];
        },
        'columns' => [
            //'id',
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return $model->getTextType();
                },
                'filter' => Order::getTypes()
            ],
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->getTextStatus();
                },
                'filter' => Order::getStatuses()
            ],
            /*[
                'attribute' => 'method',
                'value' => function($model) {
                    return $model->getTextMethod();
                },
                'filter' => Order::getMethods()
            ],*/
            'code',
            [
                'attribute' => 'amount',
                'value' => function($model) {
                    return Yii::$app->formatter->asCurrency($model->amount, $model->currency);
                }
            ],
            //'currency',
            //'vta_rate',
            [
                'attribute' => 'date',
                'value' => function($model) {
                    return Yii::$app->formatter->asDateTime($model->date, 'short');
                }
            ],
            [
                'class' => 'frontend\widgets\grid\ActionColumn',
                'template' => '{pay}',
                'controller' => 'account/order',
                'updateVisible' => false,
                'deleteVisible' => false,
                'viewVisible' => false,
                'buttons' => [
                    'pay' => function ($url, $model, $key) {
                        if($model->status == Order::STATUS_WAITING_PAYMENT && $model->payment)
                        {
                            $options = [
                                'title' => Yii::t('app', 'Pay'),
                                'aria-label' => Yii::t('app', 'Pay'),
                                'data-pjax' => '0',
                                'class' => 'btn btn-primary'
                            ];
                            $url = ['/payment/payu', 'id' => $model->payment->id];
                            return Html::a(Yii::t('app', "Pay"), $url, $options);
                        }
                    }
                ]
            ],
        ],
    ]); ?>
</div>
