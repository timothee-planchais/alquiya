<?php

use common\models\Order;
use common\models\Subscription;
use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \frontend\components\View */
/* @var $order Order */
/* @var $subscription Subscription */
/* @var $subscriptionPrices array */
/* @var $item Item */

$this->title = $subscription->getTextType().' '.Yii::t('app', "an item");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'My items'), 'url' => ['/account/item/index']];
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

$lastActiveSubscription = $subscription->getLastActiveSubscription();

?>
<div id="order-create">

    <div class="row">
    
        <div class="col-xs-12 col-sm-6 col-md-8 left">

            <div class="well">
                <h2 class="text-primary"><?= Yii::t('app', "How does it work ?") ?></h2>
                <p><span class="text-primary"><?= $subscription->getTextType(true) ?></span> :</p>
                
                <?php if($subscription->type == Subscription::TYPE_ITEM_BUMPED): ?>
                
                <p>
                    Tu producto subirá a las primeras posiciones del listado, una vez por día, durante el período que elegiste.
                </p><!-- posicionará al tope de la lista de resultados -->
                
                <p>
                    Al aplicar este a tu aviso, esta recibirá, en promedio, una visibilidad 5 veces mayor a la de un aviso regular.<br/>
                    Tu publicación mantendrá este beneficio por el plazo establecido al momento de la compra.
                </p>
                
                <?php elseif($subscription->type == Subscription::TYPE_ITEM_FEATURED): ?>
                
                <p>
                    Tu aviso se mostrará con la etiqueta "destacado", y entrará en una lista de anuncios que aparecerán en la primera posición aleatoriamente cada vez que se actualice la página, 
                    mostrándose en la categoría donde tienes mayor oportunidad de venta.
                </p>
                
                <p>
                    Al aplicar este a tu aviso, esta recibirá, en promedio, una visibilidad 10 veces mayor a la de un aviso regular.<br/>
                    Tu publicación mantendrá este beneficio por el plazo establecido al momento de la compra.
                </p>
                
                <?php elseif($subscription->type == Subscription::TYPE_ITEM_HIGHLIGHTED): ?>
                
                <p>
                    Al aplicar este a tu aviso, se mostrará con un color de fondo distinto de los demás, y los arrendatarios podrán identificarlo fácilmente.
                </p>
                
                <p>
                    Al aplicar este a tu aviso, esta recibirá, en promedio, una visibilidad 3 veces mayor a la de un aviso regular.<br/>
                    Tu publicación mantendrá este beneficio por el plazo establecido al momento de la compra.
                </p>
                
                <?php elseif($subscription->type == Subscription::TYPE_ITEM_PICTURES): ?>
                
                <p>
                    Al aplicar este a tu aviso, podrás añadir mas de 4 imagenes a tu aviso.
                </p>
                
                <p>
                    Tu publicación mantendrá este beneficio por siempre.
                </p>
                
                <?php endif; ?>
                
            </div>
            
            <div class="well">

                <h2 class="text-primary"><?= Yii::t('app', "Please select your period") ?></h2>

                <?= Html::beginForm('', 'post', ['class' => 'form-horizontal']) ?>
                
                <?php if(!empty($lastActiveSubscription)): ?>
                    <p class="alert alert-info">
                        <i class="fa fa-info-circle"></i> <?= Yii::t('app', "You have an active subscription for this item that ends at").' : '.Yii::$app->formatter->asDate($lastActiveSubscription->end_date, 'short') ?>
                    </p>
                <?php endif; ?>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-2" for=""><?= $subscription->getAttributeLabel('period') ?> :</label>
                    <div class="col-sm-9 col-md-10"><?= Html::activeDropDownList($subscription, 'period', Subscription::getPeriods(), ['class' => 'form-control']) ?></div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-2" for=""><?= Yii::t('app', "Dates") ?> :</label>
                    <div class="col-sm-9 col-md-10"><p class="form-control-static text-sanfrancisco" id="order-dates"></p></div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3 col-md-2"><?= Yii::t('app', "Amount") ?> :</label>
                    <div class="col-sm-9 col-md-10"><p class="form-control-static text-sanfrancisco" id="order-amount"><?= Yii::$app->formatter->asCurrency($order->amount) ?></p></div>
                </div>

                <div class="text-center">
                    <?= Html::submitButton('<i class="fa fa-check"></i> '.Yii::t('app', "Validate"), ['class' => 'btn btn-secondary btn-lg']) ?>
                </div>

                <?= Html::endForm() ?>
            </div>
            
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-4 right">
            
            <div id="order-item" class="well">
                <img src="<?= Url::to($item->getImageUrl('300x200')) ?>" class="img-responsive img-full"/>
                <p class="text-primary"><a href="<?= Url::to($item->getUrl()) ?>" class="text-primary"><?= Html::encode($item->title) ?></a></p>
                <p class="text-muted"><?= $item->getNiceCategories() ?></p>
                <p class="text-secondary"><strong class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price) ?></strong> <?= Yii::t('app', "per day") ?></p>
            </div>
            
        </div>
            
        
    </div>
    
</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">
    
    var $prices = <?= \yii\helpers\Json::encode($subscriptionPrices) ?>, $period;
    $(document).ready(function(){
        
        $period = $('#subscription-period').val();
        setPrice();
        setDates();
        $('#subscription-period').on('change', function($model) {
            $period = $(this).val();
            setPrice();
            setDates();
        });
        
        
    });
    
    function setPrice()
    {
        var $price = $prices[$period];
        $('#order-amount').text(asCurrency($price));
    }
    
    function setDates()
    {
        var $elem = $('#order-dates');
        var $start_date = '<?= Yii::$app->formatter->asDate($subscription->start_date, 'long') ?>';
        var $end_date;
        
        $elem.text('');
        
        if($period == '<?= Subscription::PERIOD_1_DAY?>') {
            $elem.text($start_date);
        }
        else if($period == '<?= Subscription::PERIOD_1_WEEK ?>') {
            $end_date = '<?= Yii::$app->formatter->asDate(date('Y-m-d',strtotime('+1 week', strtotime($subscription->start_date))), 'long') ?>';
        }
        else if($period == '<?= Subscription::PERIOD_1_MONTH ?>') {
            $end_date = '<?= Yii::$app->formatter->asDate(date('Y-m-d',strtotime('+1 month', strtotime($subscription->start_date))), 'long') ?>';
        }
        else if($period == '<?= Subscription::PERIOD_6_MONTHS ?>') {
            $end_date = '<?= Yii::$app->formatter->asDate(date('Y-m-d',strtotime('+6 months', strtotime($subscription->start_date))), 'long') ?>';
        }
        else if($period == '<?= Subscription::PERIOD_1_YEAR ?>') {
            $end_date = '<?= Yii::$app->formatter->asDate(date('Y-m-d',strtotime('+1 year', strtotime($subscription->start_date))), 'long') ?>';
        }
        
        var $text = $start_date;
        if($end_date) {
            $text += ' &nbsp;&nbsp;<i class="fa fa-arrow-right" style="font-size:14px;"></i>&nbsp;&nbsp; '+$end_date;
        }
        $elem.html($text);
    }
    
</script>
<?php $this->endBlock(); ?>