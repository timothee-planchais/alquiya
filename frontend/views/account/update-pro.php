<?php

use common\models\UserPro;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;

/* @var $this \frontend\components\View */
/* @var $model common\models\UserPro */

$this->title = Yii::t('app', "Edit company informations");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Edit my informations"), 'url' => ['/account/update']];
$this->params['breadcrumbs'][] = $this->title;

$conf['options']['enctype'] = 'multipart/form-data';
$conf['options']['id'] = 'form-folder';

?>

<div id="user-update">
    
    <?php $form = ActiveForm::begin($conf); ?>
    
        <h2 class="text-primary"><?= Yii::t('app', "Informations company") ?></h2>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'website')->textInput(['maxlength' => true, 'placeholder' => 'http://']) ?>
            </div>
        </div>

        <?= $form->field($model, 'categories')->widget(Select2::class, [
            'data' => $categories,
            'options' => [
                'placeholder' => Yii::t('app', "Please select categories"),
                'multiple' => true
            ],
            'pluginOptions' => [
                'multiple' => true,
                'maximumSelectionLength' => 3,
                'closeOnSelect' => false
            ],
        ]) ?>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'nb_items')->textInput(['maxlength' => true, 'placeholder' => 'http://']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'available_days', ['inline' => true])->checkboxList(UserPro::getAvailableDays()) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'available_hours')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <?= $form->field($model, 'logo')->widget(FileInput::class, [
                'options' => [
                    'multiple' => false
                ],
                'pluginOptions' => [
                    'showUpload' => false,
                    'showCaption' => false,
                    'showRemove' => true,
                    'removeClass' => 'btn btn-secondary fileinput-remove fileinput-remove-button',
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' => Yii::t('app', "Select picture"),
                    'initialPreview' => $model->logo ? Html::img($model->getImageFileUrl('logo'), ['class'=>'file-preview-image img-responsive', 'alt'=>' ', 'title'=>' ']) : false,
                    'initialCaption' => ''
                ],
                'options' => ['accept' => 'image/*']
            ]) ?>

         <div class="form-group text-center">
            <?= Html::submitButton('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update my informations'), ['class' => 'btn btn-secondary btn-lg']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>