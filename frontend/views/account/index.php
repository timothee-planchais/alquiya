<?php

use common\models\User;
use yii\helpers\Url;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $stats array */

$this->title = Yii::t('app', "My account");

$this->usePageContent = true;

?>

<div id="user-dashboard">
    
    <div class="row">
    
        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9 left">
            <div class="">

                <h2 class="text-primary"><?= Yii::t('app', "My account") ?></h2>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['update']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-edit"></i> <?= Yii::t('app', "Edit my informations") ?></a>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/user/view', 'id' => $user->getPrimaryKey()]) ?>" class="btn btn-secondary btn-block"><i class="fa fa-user"></i> <?= Yii::t('app', "My profile") ?></a>
                    </div>
                </div>
                
                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/account/order/index']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-list"></i> <?= Yii::t('app', "Orders") ?></a>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/account/subscription/index']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-list"></i> <?= Yii::t('app', "Subscriptions") ?></a>
                    </div>
                </div>
                
            </div>
            
            <div class="">

                <h2 class="text-primary"><?= Yii::t('app', "Items") ?></h2>    

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/account/item/create']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-plus"></i> <?= Yii::t('app', "Publish a product") ?></a>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/account/item/index']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-list"></i> <?= Yii::t('app', "My products") ?></a>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['wishlist']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-heart"></i> <?= Yii::t('app', "My favorites") ?></a>
                    </div>

                </div>
                
            </div>
            
            <div class="">

                <h2 class="text-primary"><?= Yii::t('app', "My activity") ?></h2>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/booking/owner-rents']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-calendar-check-o"></i> <?= Yii::t('app', "My bookings") ?></a>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <a href="<?= Url::to(['/account/message/index']) ?>" class="btn btn-secondary btn-block"><i class="fa fa-envelope-o"></i> <?= Yii::t('app', "My messages") ?></a>
                    </div>

                </div>
            </div>
            
             <div class="" id="user-stats">
                <h2 class="text-primary"><?= Yii::t('app', "Statistics") ?></h2>
                
                <div class="row">
                    <?php foreach($stats as $i => $stat): ?>
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <div class="user-stat" style="background-color:<?= $stat['color'] ?>;">
                                <i class="user-stat-icon <?= $stat['icon'] ?>"></i>
                                <h4 class="user-stat-title"><?= $stat['label'] ?></h4>
                                <p class="user-stat-text"><?= $stat['text'] ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
        </div>
        
        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 right">
            
            <div class="">
                
                <h2 class="text-primary"><?= Yii::t('app', "My profile") ?></h2>
                <?php $completicity = $user->getProfileCompleticity() ?>
                
                <div class="progress">
                    <div class="progress-bar progress-bar-<?= $user->getProfileCompleticityColor() ?>" role="progressbar" aria-valuenow="<?= $completicity ?>" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: <?= $completicity*100 ?>%;">
                        <?= Yii::$app->formatter->asPercent($completicity) ?>
                    </div>
                </div>
                
                <?php if($completicity < 1): ?>
                    <p class="text-left"><a href="<?= Url::to(['update']) ?>" class="text-secondary" style="text-decoration:underline;font-size:13px;"><?= Yii::t('app', "Complete my profile") ?></a></p>
                <?php endif; ?>
                    
            </div>
            
            <div id="user-dashboard-security" class="">
                
                <h2 class="text-primary"><?= Yii::t('app', "Security") ?></h2>
                
                <p class="text-primary">
                    <?php if($user->security_email_verified): ?>
                        <i class="fa fa-check text-success"></i> <?= Yii::t('app', "Email verified") ?>
                    <?php else: ?>
                        <i class="fa fa-times text-danger"></i> <?= Yii::t('app', "Email not verified") ?>
                    <?php endif; ?>
                </p>
                
                
            </div>
            
        </div>
            
    </div>
    
</div>