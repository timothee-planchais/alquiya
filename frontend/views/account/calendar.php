<?php

use common\models\User;
use common\models\Booking;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this \frontend\components\View */
/* @var $user User */
/* @var $bookings Booking[] */

$this->title = Yii::t('app', "My calendar");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "Bookings"), 'url' => ['/booking/owner-rents']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerAssetBundle(\frontend\assets\FullCalendarAsset::class);

$events = [];
if($bookings)
{
    foreach($bookings as $booking)
    {
        $event = [
            'title' => $booking->__toString().' / '.$booking->item->title.' / '.$booking->user->username,
            'url' => \yii\helpers\Url::to($booking->getUrl()),
            'start' => $booking->oStart->format('Y-m-d'),
            'end' => $booking->oEnd->format('Y-m-d'),
            'color' => ''
        ];
        $color = 'blue';//'#d9edf7';
        if($booking->status == Booking::STATUS_VALIDATED || $booking->status == Booking::STATUS_COMPLETED) {
            $color = 'green';//'#dff0d8';
        }
        elseif($booking->status == Booking::STATUS_REFUSED || $booking->status == Booking::STATUS_CANCELLED) {
            $color = 'red';//'#f2dede';
        }

        $event['color'] = $color;

        $description = '<b>'.Yii::t('app', "Product").' :</b> '.$booking->item->title
                .'<br/>'
                .'<b>'.Yii::t('app', 'Dates').' :</b> '.$booking->getFromTo()
                .'<br/>'
                .'<b>'.Yii::t('app', "Duration").' :</b> '.$booking->getDuration().' '.Yii::t('app', 'days')
                .'<br/>'
                .'<b>'.Yii::t('app', "Total").' :</b> '.Yii::$app->formatter->asCurrency($booking->total);
        $event['description'] = $description;
        
        $events[] = $event;
    }
}

?>

<div id="user-calendar">
    
    <p>
        <span class="label" style="background:blue;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; <?= Yii::t('app', "Must be confirmed") ?>
        &nbsp;&nbsp;
        <span class="label" style="background:green;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; <?= Yii::t('app', "Confirmed") ?>
        &nbsp;&nbsp;
        <span class="label" style="background:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp; <?= Yii::t('app', "Refused") ?>
    </p>
    
    <hr/>
    
    <div id="calendar"></div>
    
</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">
    
    $(document).ready(function(){
        
        /*
         * https://fullcalendar.io/docs
         * https://fullcalendar.io/docs/event-display
         * https://fullcalendar.io/docs/event-tooltip-demo
         * https://codepen.io/pen/?editors=0010
         */
        
        $('#calendar').fullCalendar({
            defaultView: 'month',
            //defaultDate: '2018-03-12',

            eventRender: function(eventObj, $el) {
                $el.popover({
                    html: true,
                    title: eventObj.title,
                    content: eventObj.description,
                    trigger: 'hover',
                    placement: 'top',
                    container: 'body'
                });
            },

            events: 
            <?= Json::encode($events) ?>
        });
        
    });
    
</script>
<?php $this->endBlock(); ?>