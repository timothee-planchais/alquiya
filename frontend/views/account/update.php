<?php

use common\models\User;
use common\models\UserPro;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;

/* @var $this \frontend\components\View */
/* @var $model User */
/* @var $modelPro common\models\UserPro */

$this->title = Yii::t('app', "Edit my informations");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

$conf['options']['enctype'] = 'multipart/form-data';
$conf['options']['id'] = 'form-folder';

?>

<div id="user-update">
    
    <p class="" style="">
        <?php if($model->isPro()): ?>
            <a href="<?= Url::to(['update-pro']) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> <?= Yii::t('app', 'Edit company informations') ?></a>
        <?php endif; ?>
        <a href="<?= Url::to(['update-password']) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> <?= Yii::t('app', 'Edit my password') ?></a>
    </p>
    
    <hr/>
    
    <?php $form = ActiveForm::begin($conf); ?>
            
        <h2 class="text-primary"><?= Yii::t('app', "Informations") ?></h2>

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'gender')->radioList(User::getGenders(), ['item' => Yii::$app->params['formRadioItem']]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?php // echo $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-xs-12 col-sm-6">
                <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
                
        <?php // $form->field($model, 'work_phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'image')->widget(FileInput::class, [
                'options' => [
                    'multiple' => false
                ],
                'pluginOptions' => [
                    'showUpload' => false,
                    'showCaption' => false,
                    'showRemove' => true,
                    'removeClass' => 'btn btn-secondary fileinput-remove fileinput-remove-button',
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' => Yii::t('app', "Select picture"),
                    'initialPreview' => $model->image ? Html::img($model->getImageFileUrl('image'), ['class'=>'file-preview-image img-responsive', 'alt'=>' ', 'title'=>' ']) : false,
                    'initialCaption' => ''
                ],
                'options' => ['accept' => 'image/*']
            ]) ?>

        <?php //echo $form->field($model, 'website')->textInput(['placeholder' => 'http://', 'maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

        <h2 class="text-primary"><?= Yii::t('app', "Your address") ?></h2>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address2')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'zipcode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'country')->dropDownList(User::getCountries()) ?>

        <h2 class="text-primary"><?= Yii::t('app', "Preferencies") ?></h2>

        <?php // $form->field($model, 'lang')->dropDownList(User::getLangs()) ?>

        <?= $form->field($model, 'show_phone', ['inline' => true])->radioList(\common\components\Utils::getYesNo()) ?>

        <?= $form->field($model, 'show_email', ['inline' => true])->radioList(\common\components\Utils::getYesNo()) ?>

         <div class="form-group text-center">
            <?= Html::submitButton('<i class="fa fa-edit"></i> '.Yii::t('app', 'Update my informations'), ['class' => 'btn btn-secondary btn-lg']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>