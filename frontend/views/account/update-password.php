<?php

use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this \frontend\components\View */
/* @var $model User */

$this->title = Yii::t('app', "Edit my password");
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', "My account"), 'url' => ['/account/index']];
$this->params['breadcrumbs'][] = $this->title;

$conf['options']['enctype'] = 'multipart/form-data';
$conf['options']['id'] = 'form-folder';

?>

<div id="user-update">
    
    <?php $form = ActiveForm::begin($conf); ?>

    <?= $form->field($model, 'new_password')->passwordInput() ?>

    <?= $form->field($model, 'new_password_repeat')->passwordInput() ?>
    
     <div class="form-group text-center">
        <?= Html::submitButton('<i class="fa fa-edit"></i> '.Yii::t('app', 'Edit'), ['name' => 'edit-password', 'value' => 1 ,'class' => 'btn btn-secondary btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>