<?php

use common\models\Item;
use common\models\Order;
use common\models\Subscription;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\components\Utils;
use yii\bootstrap\ActiveForm;

/* @var $this frontend\components\View */
/* @var $item common\models\Item */
/* @var $model common\models\Booking */
/* @var $reviews common\models\Review[] */
/* @var $similarItems Item[] */

$this->usePageContent = false;
$this->showTitle = false;

$category = $item->category;
$subcategory = $item->subcategory;

$this->title = Yii::t('app', 'Rent of').' '.$item->title;
if($category->parent) {
    $this->params['breadcrumbs'][] = ['label' => $category->parent->title, 'url' => $category->parent->getUrl()];
}
$this->params['breadcrumbs'][] = ['label' => $category->title, 'url' => $category->getUrl()];
if($subcategory) {
    $this->params['breadcrumbs'][] = ['label' => $subcategory->title, 'url' => $subcategory->getUrl()];
}
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerAssetBundle(\frontend\assets\OwlAsset::class);
$this->registerJsFile('@web/js/item.js', ['depends' => \kartik\date\DatePickerAsset::class]);


$js = '$price = '.$item->price.';'
    .'$price_weekend = '.($item->price_weekend ? : 0).';'
    .'$price_week = '.($item->price_week ? : 0).';'
    .'$price_month = '.($item->price_month ? : 0).';'
    .'var $upcomingBookingDateRanges = '.\yii\helpers\Json::encode($item->getJsUpcomingBookingDateRanges()).';';

$this->registerJs($js, \yii\web\View::POS_HEAD);

$this->registerJsFile('http://maps.google.com/maps/api/js?' . http_build_query(['key' => Yii::$app->params['google.api_key'], 'language' => Yii::$app->params['google.api_language']]));

$user = $item->user;

$userPro = null;
$isPro = false;
if($user->isPro() && $user->userPro) {
    $isPro = $user->isPro();
    $userPro = $user->userPro;
}

$images = $item->images;

$upcomingBookings = $item->getUpcomingBookings();

/*$datesDisabled = [];
if($upcomingBookings) {
    foreach($upcomingBookings as $booking) {
        $datesDisabled = array_merge($datesDisabled, $booking->getIntervalDates());
    }
}*/

?>

<div id="item" class="<?= $isPro ? 'pro' : '' ?>">
    
    <div class="row">
        
        <!-- LEFT -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 left">
            
            <div class="well">
                
                <p id="item-id" class="text-right"><?= Yii::t('app', 'Publication') ?> #<span class="text-sanfrancisco"><?= $item->getPrimaryKey() ?></span></p>
            
                <?php if($images): ?>

                    <div id="item-images" class="owl-carousel owl-theme">

                        <?php foreach($images as $image): ?>

                            <div><img src="<?= Url::to($image->getImageUrl()) ?>" alt="<?= $item->title ?>" class="img-responsive"/></div>

                        <?php endforeach; ?>

                    </div>

                <?php endif; ?>

                <h1 id="title" class="text-primary"><?= Html::encode($item->title) ?></h1>

                <div id="item-share" class="clearfix">

                    <?= $this->render('/item/_wishlist_button', ['item' => $item]); ?>

                    <a href="javascript:;" data-url="<?= Yii::$app->getRequest()->getAbsoluteUrl() ?>" class="btn btn-social btn-facebook btn-share" title="<?= Yii::t('app', "Share on {social}", ['social' => 'Facebook']) ?>"><i class="fa fa-facebook-f"></i> <span>Facebook</span></a>
                    <a href="javascript:;" data-url="<?= Yii::$app->getRequest()->getAbsoluteUrl() ?>" data-text="<?= $item->title ?>" data-via="<?= Yii::$app->params['twitter.account'] ?>" class="btn btn-social btn-twitter btn-share" title="<?= Yii::t('app', "Share on {social}", ['social' => 'Twitter']) ?>"><i class="fa fa-twitter"></i> Twitter</a>
                    <a href="javascript:;" data-url="<?= Yii::$app->getRequest()->getAbsoluteUrl() ?>" class="btn btn-social btn-google btn-share" title="<?= Yii::t('app', "Share on {social}", ['social' => 'Google+']) ?>"><i class="fa fa-google-plus"></i> Google+</a>

                </div>

                <p id="item-permalink" class="form-inline">
                    <strong><?= Yii::t('app', "Permalink") ?> :</strong> &nbsp;
                    <?= Html::textInput('', Url::to($item->getPermalink(), true), ['class' => 'form-control', 'style' => 'width:230px;', 'onclick' => 'select();']) ?>
                </p>

                <!--<h3 id="item-subtitle"><?= Html::encode($item->subtitle) ?></h3>-->

                <div id="item-description">
                    <h2><?= Yii::t('app', "Description") ?></h2>
                    <p><?= nl2br(Html::encode($item->description)) ?></p>
                </div>

                <?php if($item->delivery): ?>

                    <div id="item-delivery">
                        <h2><?= Yii::t('app', "Delivery options") ?> :</h2>
                        <ul>
                            <?php foreach($item->delivery as $delivery): ?>
                            <li>- <?= ArrayHelper::getValue(Item::getDeliveries(), $delivery) ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                <?php endif; ?>

                <hr/>
                
                <h2><?= Yii::t('app', "Reviews") ?></h2>

                <?php if($reviews): ?>
                    <div id="reviews">
                        <?php
                        $totalReviews = $item->getReviews()->select('id')->count();
                        $bars = $item->getReviewsBars();
                        $avg = $item->getReviewsAvg();
                        ?>
                        <div id="reviews-count">
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <?php if($avg): ?>
                                        <p id="reviews-total-note-value" class="text-sanfrancisco"><?= Yii::$app->formatter->asDecimal($avg) ?></p>
                                        <p id="reviews-total-note" class="review-note"><?= $this->render('/review/_stars', ['note' => $avg]) ?></p>
                                    <?php endif; ?>
                                    <p id="reviews-total-people" class="text-sanfrancisco"><?= Yii::t('app', "Rated by").' :<br/> '.Yii::$app->formatter->asInteger($totalReviews).' '.Yii::t('app', 'people') ?></p>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-8">
                                    <div id="reviews-bars">
                                        <?php foreach($bars as $i=>$value): ?>
                                            <div class="reviews-bar clearfix">
                                                <span class="text-sanfrancisco pull-left"><?= $i ?></span>
                                                <div class="progress">
                                                    <div class="progress-bar bg-secondary" title="<?= Yii::$app->formatter->asPercent($value) ?>" role="progressbar" aria-valuenow="<?= $value*100 ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?= $value*100 ?>%;opacity:<?= ($i/10*2) ?>;"></div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php endif; ?>

                <div id="reviews-list">
                    <?php if($reviews): ?>
                        <?php foreach($reviews as $review): ?>

                            <?= $this->render('/review/_review', ['review' => $review, 'fromItemView' => true]) ?>

                        <?php endforeach; ?>
                    <?php else: ?>
                        <p><?= Yii::t('app', "No review") ?></p>
                    <?php endif; ?>
                </div>
                
            </div>
            
            <div class="alquiya-ad hidden-xs">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-2957507038442890"
                     data-ad-slot="3379110373"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
                
        </div>
        
        <!-- RIGHT -->
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 right">
            
            <?php if(Yii::$app->user->can(frontend\components\WebUser::PERMISSION_ITEM_UPDATE, ['item' => $item])): ?>
                <div class="well" id="item-edit">
                    <h2 class="text-primary"><?= Yii::t('app', "My item") ?></h2>
                    <?php if(!$item->locked && YII_ENV != 'prod'): ?>
                        <p>
                            <a href="<?= Url::to(['/account/order/create', 'type' => Order::TYPE_SUBSCRIPTION, 'itemId' => $item->id, 'subscriptionType' => Subscription::TYPE_ITEM_BUMPED]) ?>" class="btn btn-secondary"><i class="fa fa-arrow-up"></i> <?= Yii::t('app', 'Bump') ?></a>
                            <a href="<?= Url::to(['/account/order/create', 'type' => Order::TYPE_SUBSCRIPTION, 'itemId' => $item->id, 'subscriptionType' => Subscription::TYPE_ITEM_FEATURED]) ?>" class="btn btn-secondary"><i class="fa fa-star"></i> <?= Yii::t('app', 'Feature') ?></a>
                        </p>
                    <?php elseif($item->locked): ?>
                        <p class="alert alert-danger"><i class="fa fa-ban"></i> <?= Yii::t('app', "Item locked") ?></p>
                    <?php endif; ?>
                        <p>
                            <a href="<?= Url::to(['/account/item/update', 'id' => $item->id]) ?>" class="btn btn-default"><i class="fa fa-edit"></i> <?= Yii::t('app', 'Update') ?></a>
                            <a href="<?= Url::to(['/account/item/delete', 'id' => $item->id]) ?>" data-method="post" data-confirm="<?= Yii::t('app', "Are you sure you want to delete the item ?") ?>" class="btn btn-danger"><i class="fa fa-times"></i> <?= Yii::t('app', 'Delete') ?></a>
                        </p>
                </div>
            <?php endif; ?>
            
            <div id="item-user" class="well">
                <div class="media">
                    <div class="media-left">
                        <a href="<?= Url::to($user->getProfileUrl()) ?>">
                            <img src="<?= url::to($user->getImageUrl('300x300')) ?>" class="media-object img-circle" title="<?= $user->__toString() ?>" alt="<?= $user->__toString() ?>"/>
                        </a>
                    </div>
                    <div class="media-body media-middle">
                        <p id="item-user-title" href="<?= Url::to($user->getProfileUrl()) ?>"><?= Html::encode($user) ?></p>
                        <?php if($user->country): ?>
                            <p id="item-user-location"><i class="fa fa-map-marker"></i> <?= ($user->city ? $user->city.', ' : '').$user->getTextCountry() ?></p>
                        <?php endif; ?>
                        <?php if(!$isPro): ?>
                            <p id="item-user-stars"><?= $this->render('/review/_stars', ['note' => $user->getRating()]) ?></p>
                        <?php endif; ?>
                        <p id="item-user-link"><a href="<?= Url::to($user->getProfileUrl()) ?>"><?= Yii::t('app', "See profile") ?></a></p>
                    </div>
                </div>
            </div>
            
            <div class="well" id="item-prices">

                <h2 class="text-primary"><?= Yii::t('app', 'Prices') ?></h2>
                
                <?php if($item->pricing_type == Item::PRICING_TYPE_REQUEST): ?>
                    <p id="item-price-request"><?= $item->getTextPricingType() ?></p>
                <?php else: ?>
                    <?php if($item->pricing_type == Item::PRICING_TYPE_FROM): ?>
                        <span id="item-price-from"><?= $item->getTextPricingType() ?></span>
                    <?php endif; ?>
                    <p id="item-price" class="item-price">
                        <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price) ?></span>
                        <?= Yii::t('app', 'per day') ?>
                    </p>
                
                    <?php if(isset($item->price_weekend)): ?>
                        <p id="item-price_weekend" class="item-price"><?= Yii::t('app', "Weekend (3 days)") ?> : <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price_weekend) ?></span></p>
                    <?php endif; ?>
                    <?php if(isset($item->price_week)): ?>
                        <p id="item-price_week" class="item-price"><?= Yii::t('app', "Week (7 days)") ?> : <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price_week) ?></span></p>
                    <?php endif; ?>
                    <?php if(isset($item->price_month)): ?>
                        <p id="item-price_weekend" class="item-price"><?= Yii::t('app', "Month (30 days)") ?> : <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price_month) ?></span></p>
                    <?php endif; ?>
                        
                <?php endif; ?>

                <?php if(!empty($item->deposit_value)): ?>
                    <p id="item-deposit"><i><?= Yii::t('app', "Deposit") ?> : <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->deposit_value) ?></span></i></p>
                <?php endif; ?>
                    
                <?php if($isPro): ?>
                    <hr/>
                    <?php if($user->show_phone && $user->getPhoneNumber()): ?>
                        <p id="pro-phone"><strong><?= Yii::t('app', "Celular") ?> :</strong> <a href="tel:$user->getPhoneNumber()"><?= Html::encode($user->getPhoneNumber()) ?></a></p>
                    <?php endif; ?>
                    <?php if($user->show_email): ?>
                        <p id="pro-email"><strong><?= Yii::t('app', "Email") ?> :</strong> <a href="mailto:<?= $user->email ?>"><?= Html::encode($user->email) ?></a></p>
                    <?php endif; ?>
                    <!--<p id="pro-btn"><a href="<?= Url::to(['/account/message/send', 'userId' => $user->id, 'itemId' => $item->id]) ?>" class="btn btn-secondary btn-lg btn-block"><?= Yii::t('app', "Contact the profesional") ?></a></p>-->
                <?php endif; ?>
                    
            </div>
            
            <?php if(!$isPro && !Yii::$app->user->can(frontend\components\WebUser::PERMISSION_ITEM_UPDATE, ['item' => $item])): ?>
            
                <div class="well" id="item-booking-form">
            

                    <h2 class="text-primary"><?= Yii::t('app', "Booking") ?></h2>
            
                    <!--<p id="item-price">
                        <span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price) ?></span>
                        <?= Yii::t('app', 'per day') ?>
                    </p>-->

                    <?php $form = ActiveForm::begin() ?>

                    <?= $form->field($model, 'date_range')->widget(kartik\daterange\DateRangePicker::class, [
                        'convertFormat' => true,
                        'startAttribute' => 'date_start',
                        'endAttribute' => 'date_end',
                        'options' => ['class' => 'form-control text-sanfrancisco'],
                        'pluginOptions'=> [
                            'locale'=>[
                                'format' => 'd/m/Y',
                                'separator' => Yii::$app->params['datarange.separator']
                            ],
                            'minDate' => date('d/m/Y'),
                            'autoUpdateInput' => true,
                            'opens' => 'left',
                            //'applyButtonClasses' => 'applyBtn btn btn-sm btn-secondary',
                            'autoApply' => true,
                            'isInvalidDate' => new yii\web\JsExpression('function(date) { 
                                return $upcomingBookingDateRanges.reduce(function(bool, range) {
                                    return bool || (date >= range.start && date <= range.end);
                                }, false); }')
                        ]
                    ]) ?>
                    
                    <table id="booking-details" class="table" style="display:none;">
                        <tr id="booking-duration">
                            <td id="booking-duration-days"><span class="text-sanfrancisco"><?= Yii::$app->formatter->asCurrency($item->price) ?></span> x <span class="value text-sanfrancisco"></span> <?= Yii::t('app', 'days') ?></td>
                            <td id="booking-duration-price" class="text-right text-sanfrancisco"></td>
                        </tr>
                        <tr id="booking-discount" style="display:none;">
                            <td>Descuento</td>
                            <td class="text-right value text-sanfrancisco"></td>
                        </tr>
                        <tr id="booking-total">
                            <td><?= strtoupper(Yii::t('app', "Total")) ?></td>
                            <td class="text-right text-sanfrancisco value"></td>
                        </tr>
                    </table>

                    <p id="booking-btn"><button type="submit" class="btn btn-secondary btn-lg btn-block"><?= Yii::t('app', "Book item") ?></button></p>

                    <?php $form->end() ?>
                    
                </div>
            
            <?php endif; ?>
            
            <?php if(!Yii::$app->user->can(frontend\components\WebUser::PERMISSION_ITEM_UPDATE, ['item' => $item])): ?>
                
                <div id="item-chat" class="well">
                    <?php 

                    $message = new common\models\Message([
                        'subject' => Yii::t('app', "Question sobre {item}", ['item' => $item->title])
                    ]);
                    $template = "{input}\n{hint}\n{error}";
                    ?>

                    <h2 class="text-primary"><?= Yii::t('app', "Send a message") ?></h2>

                    <?php $form = ActiveForm::begin(['action' => ['/account/message/send', 'userId' => $user->id, 'itemId' => $item->getPrimaryKey()], 'method' => 'POST']) ?>

                    <?= $form->field($message, 'subject', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $message->subject]) ?>

                    <?= $form->field($message, 'message', ['template' => $template])->textarea(['maxlength' => true, 'placeholder' => Yii::t('app', "Ask {user} any questions about the item", ['user' => $user])]) ?>

                    <p class="text-right">
                        <button type="submit" class="btn btn-secondary btn-lg"><?= Yii::t('app', "Send") ?></button>
                    </p>

                    <?php $form->end() ?>

                </div>

            <?php endif; ?>
            
            <div id="item-map" class="well">
            
                <h2 class="text-primary"><?= Yii::t('app', "Location") ?></h2>
                
                <!--<div id="item-map-map" style="height:180px;"></div>-->
                
                <div id="item-map-img" class="map-img map-squarred">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?= $item->lat.','.$item->lng ?>&zoom=15&size=400x400&scale=2&maptype=roadmap&language=<?= Yii::$app->language ?>&key=<?= Yii::$app->params['google.api_key'] ?>" class="img-responsive" alt=""/>
                </div>
                
                <?php if($item->short_address): ?>
                    <p style="margin-top:10px;"><?= $item->short_address ?></p>
                <?php endif; ?>
                    
            </div>
            
            <div id="item-right-ad" class="alquiya-ad hidden-xs">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-2957507038442890"
                     data-ad-slot="1961430385"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
                
            <?php if(!empty($similarItems)): ?>
                
                <div class="well">
            
                    <h2 class="text-primary"><?= Yii::t('app', "Similar products") ?></h2>

                    <div id="item-similiar-items" class="items">

                        <?php foreach($similarItems as $similarItem): ?>
                            <?php $url = Url::to($similarItem->getUrl()); ?>
                            <div class="item similar media">
                                <div class="media-left"><img src="<?= Url::to($similarItem->getImageUrl('300x300')) ?>" class="media-object"/></div>
                                <div class="media-body">
                                    <p class="item-title"><a href="<?= $url ?>"><?= Utils::limitWords(Html::encode($similarItem->title), 30) ?></a></p>
                                    <?php if($similarItem->short_address): ?>
                                        <p class="item-address"><?= Html::encode($similarItem->short_address) ?></p>
                                    <?php endif; ?>
                                    <p class="item-price text-secondary"><b><?= Yii::$app->formatter->asCurrency($similarItem->price) ?></b> <?= Yii::t('app', "per day") ?></p>
                                </div>
                            </div>

                        <?php endforeach; ?>

                    </div>
                    
                </div>
            
            <?php endif; ?>
            
            <div id="item-report" class="">
                <p class="text-primary"><?= Yii::t('app', "Is there something wrong?") ?></p>
                <p><a href="<?= Url::to(['/item/report', 'id' => $item->getPrimaryKey()]) ?>"><?= Yii::t('app', "Report this publication") ?></a></p>
            </div>
            
        </div>
        
    </div>
    
    <div class="alquiya-ad visible-xs">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-2957507038442890"
             data-ad-slot="3379110373"
             data-ad-format="auto"
             data-full-width-responsive="true"></ins>
        <script>
             (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>

</div>

<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        $date_start = $('#booking-date_range').data('daterangepicker').startDate;
        $date_end = $('#booking-date_range').data('daterangepicker').endDate;
        updateBookingDetails();
        
    });

</script
<?php $this->endBlock(); ?>