<?php

use common\models\Item;
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Utils;

/* @var $this frontend\components\View */
/* @var $item Item */
/* @var $asRow bool */
/* @var $showDistance bool */
/* @var $showWishlistButton bool */
/* @var $showActions bool */

$urlParams = [
    'start' => Yii::$app->getRequest()->get('date_start'),
    'end' => Yii::$app->getRequest()->get('date_end'),
];

$category = $this->getCategory($item->category_id);
$urlParams['categorySlug'] = $category->slug;
$bigCategory = $this->getCategory($category->parent_id);
if($bigCategory) {
    $urlParams['bigCategorySlug'] = $bigCategory->slug;
}

$subCategory = null;
if($item->subcategory_id) {
    $subCategory = $this->getCategory($item->subcategory_id);
    $urlParams['subCategorySlug'] = $subCategory->slug;
}

/*if(empty($subCategory) && $item->subcategory_id && !empty($category)) {
    $subCategories = \yii\helpers\ArrayHelper::index($category->subCategories, 'id');
    if($subCategories && !empty($subCategories[$item->subcategory_id])) {
        $subCategory = $subCategories[$item->subcategory_id];
    }
}*/

$startDate = Yii::$app->getRequest()->get('date_start');
$endDate = Yii::$app->getRequest()->get('date_end');

$urlParams['start'] = $startDate;
$urlParams['end'] = $endDate;

$totalPrice = null;
$discount = null;
if($startDate && $endDate) 
{
    $booking = new \common\models\Booking([
        'item_id' => $item->getPrimaryKey(),
        'price' => $item->price,
        'date_start' => $startDate,
        'date_end' => $endDate
    ]);
    $booking->initDatesFromInputs();
    if($item->id == 15) {
        //var_dump($booking->getDuration(), $booking->getDurationPrice(), $booking->getDiscountAmount(), $booking->getTotalPrice());die;
    }
    $totalPrice = $booking->getTotalPrice();
    $discount = $booking->getDiscountAmount();
}

$url = Url::to($item->getUrl($urlParams));

?>

<div class="item <?= !empty($asRow) ? 'is-row' : 'not-row' ?> <?= $item->featured ? 'featured' : '' ?> <?= $item->highlighted ? 'highlighted' : '' ?> <?= $item->isPro() ? 'pro' : '' ?> <?= !empty($item->locked) ? 'locked' : '' ?> <?= $item->isInactivated() ? 'inactive' : '' ?>">
    
    <div class="item-header">
        
        <?php if(!empty($showWishlistButton)): ?>
            <?= $this->render('/item/_wishlist_button', ['item' => $item, 'small' => true]) ?>
        <?php endif; ?>
        
        <?php if($item->highlighted): ?>
            <span class="item-highlighted label label-secondary" title="<?= Yii::t('app', "Highlighted") ?>"><i class="fa fa-star"></i></span>
        <?php endif; ?>
        
        <?php if(!empty($showDistance) && isset($item->distance)): ?>
            <span class="item-distance label label-primary"><?= Yii::t('app', 'at') ?> <?= number_format($item->distance, 2) ?> km</span>
        <?php endif; ?>
            
        <a href="<?= $url ?>">
            <img src="<?= url::to($item->getImageUrl('300x200')) ?>" alt="<?= $item->title ?>" title="<?= $item->title ?>" class="img-responsive item-img-300x200"/>
            <img src="<?= url::to($item->getImageUrl('300x300')) ?>" alt="<?= $item->title ?>" title="<?= $item->title ?>" class="img-responsive item-img-300x300"/>
        </a>
        
    </div>
    <div class="item-body">
        
        <?php if($item->featured): ?>
            <p class="item-featured"><span class="label label-default bg-secondary text-sanfrancisco" title="<?= Yii::t('app', "Featured") ?>"><?= Yii::t('app', "Featured") ?></span></p>
        <?php endif; ?>
        
        <p class="item-category">
            <?php if($item->isPro()): ?>
                <span class="item-pro">(Pro)</span>
            <?php endif; ?>
            <?= $category->title ?>
        </p>
        
        <h4 class="item-title" title="<?= Html::encode($item->title) ?>"><a href="<?= $url ?>"><?= Html::encode(Utils::limitWords($item->title, 45)) ?></a></h4>
        
        <?php if($item->subtitle): ?>
            <!--<h5 class="item-subtitle" title="<?= $item->subtitle ?>"><?= Html::encode(Utils::limitWords($item->subtitle, 30)) ?></h5>-->
        <?php endif; ?>
        
        <p class="item-price">
            <?php if($item->pricing_type == Item::PRICING_TYPE_REQUEST): ?>
                <span class="item-price-request"><?= $item->getTextPricingType() ?></span>
            <?php else: ?>
                <?php if($item->pricing_type == Item::PRICING_TYPE_FROM): ?>
                    <span class="item-price-from"><?= $item->getTextPricingType() ?></span>
                <?php endif; ?>
                <span class="text-secondary text-sanfrancisco value"><?= Yii::$app->formatter->asCurrency($item->price) ?></span> <?= Yii::t('app', "per day") ?>
            <?php endif; ?>
        </p>
        
        <?php if(!empty($totalPrice)): ?>
            <p class="item-price item-price-total"><?= Yii::t('app', "Total") ?> : <span class="text-secondary text-sanfrancisco value"><?= Yii::$app->formatter->asCurrency($totalPrice) ?></span></p>
        <?php endif; ?>
        
        <p class="item-address"><i class="fa fa-map-marker text-secondary"></i> <?= Html::encode($item->city ? Html::encode($item->city) : Html::encode($item->short_address)) ?></p>

        <p class="item-note text-sanfrancisco"><i class="fa fa-star text-secondary"></i> 4.5</p>
        
        <?php if(!empty($showActions)): ?>
        
            <div class="item-actions">
                <p>
                    <?php if($item->locked): ?>
                        <span class="text-danger"><i class="fa fa-ban"></i> <?= Yii::t('app', "Locked") ?></span>
                    <?php elseif($item->status == Item::STATUS_ACTIVE): ?>
                        <a class="btn btn-xs btn-default" href="<?= Url::to(['/account/item/inactivate', 'id' => $item->id]) ?>"><i class="fa fa-minus-circle"></i> <?= Yii::t('app', "Inactivate") ?></a>
                    <?php else: ?>
                        <a class="btn btn-xs btn-default" href="<?= Url::to(['/account/item/activate', 'id' => $item->id]) ?>"><i class="fa fa-check"></i> <?= Yii::t('app', "Activate") ?></a>
                    <?php endif; ?>
                </p>
                <p>
                    <a class="btn btn-default" href="<?= Url::to(['/account/item/update', 'id' => $item->id]) ?>" title="<?= Yii::t('yii', 'Update') ?>" data-pjax="0"><i class="fa fa-edit"></i> <?= Yii::t('yii', 'Update') ?></a>
                    <a class="btn btn-danger" href="<?= Url::to(['/account/item/delete', 'id' => $item->id]) ?>" title="<?= Yii::t('yii', 'Delete') ?>" data-confirm="<?= Yii::t('yii', 'Are you sure you want to delete this item?') ?>" data-method="post" data-pjax="0"><i class="fa fa-times"></i> <?= Yii::t('yii', 'Delete') ?></a>
                </p>
                <p>
                <?php
                if($item->status == Item::STATUS_ACTIVE && !$item->locked && YII_ENV != 'prod')
                {
                    $options = [
                        'title' => Yii::t('app', 'Bump'),
                        'aria-label' => Yii::t('app', 'Bump'),
                        'data-pjax' => '0',
                        'class' => 'btn btn-primary'
                    ];
                    echo Html::a('<span class="fa fa-arrow-up"></span> '.Yii::t('app', 'Bump'), ['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $item->id, 'subscriptionType' => common\models\Subscription::TYPE_ITEM_BUMPED], $options);
                
                    $options = [
                        'title' => Yii::t('app', 'Feature'),
                        'aria-label' => Yii::t('app', 'Feature'),
                        'data-pjax' => '0',
                        'class' => 'btn btn-primary'
                    ];
                    echo Html::a('<span class="fa fa-certificate"></span> '.Yii::t('app', 'Feature'), ['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $item->id, 'subscriptionType' => common\models\Subscription::TYPE_ITEM_FEATURED], $options);
                    
                    $options = [
                        'title' => Yii::t('app', 'Highlight'),
                        'aria-label' => Yii::t('app', 'Highlight'),
                        'data-pjax' => '0',
                        'class' => 'btn btn-primary'
                    ];
                    echo Html::a('<span class="fa fa-star"></span> '.Yii::t('app', 'Highlight'), ['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $item->id, 'subscriptionType' => common\models\Subscription::TYPE_ITEM_HIGHLIGHTED], $options);
                }
                ?>
                </p>
            </div>
        
        <?php endif; ?>
        
    </div>
    
    <div class="item-bottom">
        
        <p class="item-address"><i class="fa fa-map-marker text-secondary"></i> <?= Html::encode($item->city ? Html::encode($item->city) : Html::encode($item->short_address)) ?></p>

        <p class="item-note text-sanfrancisco"><i class="fa fa-star text-secondary"></i> 4.5</p>
        
        <p class="item-price">
            <?php if($item->pricing_type == Item::PRICING_TYPE_REQUEST): ?>
                <span class="item-price-request"><?= $item->getTextPricingType() ?></span>
            <?php else: ?>
                <?php if($item->pricing_type == Item::PRICING_TYPE_FROM): ?>
                    <span class="item-price-from"><?= $item->getTextPricingType() ?></span>
                <?php endif; ?>
                <span class="text-secondary text-sanfrancisco value"><?= Yii::$app->formatter->asCurrency($item->price) ?></span> <?= Yii::t('app', "per day") ?>
            <?php endif; ?>
        </p>
        
        <?php if(!empty($totalPrice)): ?>
            <p class="item-price item-price-total"><?= Yii::t('app', "Total") ?> : <span class="text-secondary text-sanfrancisco value"><?= Yii::$app->formatter->asCurrency($totalPrice) ?></span></p>
        <?php endif; ?>
        
    </div>
    
</div>