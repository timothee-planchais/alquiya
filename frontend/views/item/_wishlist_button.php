<?php

/* @var $this \frontend\components\View */
/* @var $item common\models\Item */
/* @var $user common\models\User */
/* @var $small bool */

?>

<?php if(!Yii::$app->user->getIsGuest()): ?>

    <?php 
    $user = Yii::$app->getUser()->getIdentity();
    $isInWishlist = $user->isInWishList($item->id); 
    ?>

    <button type="button" 
            class="btn btn-default btn-wishlist <?= $isInWishlist ? 'saved' : '' ?> pull-right" 
            data-id="<?= $item->id ?>" >

        <i class="fa fa-heart-o i-save"></i><i class="fa fa-heart i-saved"></i>

        <span class="txt-save" title="<?= Yii::t('app', "Add to favorites") ?>"> &nbsp;<?= Yii::t('app', "Save") ?></span>
        <span class="txt-saved" title="<?= Yii::t('app', "Remove from favorites") ?>"> &nbsp;<?= Yii::t('app', "Saved") ?></span>

    </button>

<?php endif; ?>