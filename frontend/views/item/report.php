<?php

use common\models\ReportItem;
use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this frontend\components\View */
/* @var $model ReportItem */
/* @var $item common\models\Item */

$this->title = Yii::t('app', "Make a report");
$this->params['breadcrumbs'][] = ['label' => $item->title, 'url' => $item->getUrl()];
$this->params['breadcrumbs'][] = $this->title;

$this->usePageContent = false;

$template = "{input}\n{hint}\n{error}";

?>

<div id="item-report" class="well" style="max-width:700px;margin:0 auto;">
    
    <?php $form = ActiveForm::begin() ?>

        <?php if($model->item_id && $model->item): ?>
            <?= $form->field($model, 'item_id')->textInput(['disabled' => true, 'value' => $model->item->title]) ?>
        <?php endif; ?>
    
        <?php if($model->scenario == ReportItem::SCENARIO_GUEST): ?>
            <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
        <?php endif; ?>
    
        <?= $form->field($model, 'reason')->dropDownList(ReportItem::getReasons(), ['prompt' => '']) ?>

        <?= $form->field($model, 'message')->textarea(['maxlength' => true]) ?>

        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ]) ?>
        
        <div class="text-right">
            <?= Html::submitButton('<i class="fa fa-send"></i> '.Yii::t('app', "Send"), ['class' => 'btn btn-secondary btn-lg']) ?>
        </div>

    <?php $form->end() ?>
    
</div>