<?php

use common\models\User;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\models\SearchItem;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this \frontend\components\View */
/* @var $model SearchItem */

$this->registerJsFile('http://maps.googleapis.com/maps/api/js?' . http_build_query(['libraries' => 'places', 'language' => Yii::$app->params['google.api_language'], 'key' => Yii::$app->params['google.api_key']]));
$this->registerJsFile('@web/js/plugins/jquery.geocomplete.min.js', ['depends'=>  \yii\web\JqueryAsset::className()]);

$this->registerJs('initSearchItem();');

$template = "{input}\n{hint}\n{error}";

?>

<div id="items-search" class="well">
    
    <?php $form = ActiveForm::begin(['action' => ['search'], 'method' =>'POST']); ?>
    
    <?= Html::activeHiddenInput($model, 'lat') ?>
    <?= Html::activeHiddenInput($model, 'lng') ?>
    <?= Html::activeHiddenInput($model, 'city') ?>
    <?= Html::hiddenInput('bigCategorySlug', Yii::$app->getRequest()->get('bigCategorySlug')) ?>
    <?= Html::hiddenInput('bigCategoryId', Yii::$app->getRequest()->get('bigCategoryId')) ?>
    <?= Html::hiddenInput('categorySlug', Yii::$app->getRequest()->get('categorySlug')) ?>
    <?= Html::hiddenInput('categoryId', Yii::$app->getRequest()->get('categoryId')) ?>
    <?= Html::hiddenInput('subCategorySlug', Yii::$app->getRequest()->get('subCategorySlug')) ?>
    <?= Html::hiddenInput('subCategoryId', Yii::$app->getRequest()->get('subCategoryId')) ?>
    
    <div class="row">
    
        <div class="col-xs-12 col-sm-6">
            <?= $form->field($model, 'search', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('search')]) ?>
        </div>
            
        <div class="col-xs-12 col-sm-4">
            <?= $form->field($model, 'search_city', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('search_city')]) ?>
            <?php // $form->field($model, 'search_address', ['template' => $template])->textInput(['maxlength' => true, 'placeholder' => $model->getAttributeLabel('search_address')]) ?>
            <?php //$form->field($model, 'radius', ['template' => $template, 'inputTemplate' => $template2])->input('number', ['placeholder' => $model->getAttributeLabel('radius')]) ?>
        </div>

        <div class="col-xs-12 col-sm-2 text-right hidden-xs">
            <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-block']) ?>
        </div>
        
    </div>
    
    <div class="row">
    
        <div id="search-price" class="col-xs-12 col-sm-4 col-md-4">
        
            <?php $template2 = "{input}\n{hint}\n{error}"; ?>
                <label class="control-label" for="searchitem-price_min"><?= $model->getAttributeLabel('price') ?></label>
                <div class="row">
                    <div class="col-xs-6">
                        <?= $form->field($model, 'price_min', ['template' => $template2])->input('number', ['step' => '0.001', 'placeholder' => $model->getAttributeLabel('price_min')]) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= $form->field($model, 'price_max', ['template' => $template2])->input('number', ['step' => '0.001', 'placeholder' => $model->getAttributeLabel('price_max')]) ?>
                    </div>
                </div>
            
        </div>
        
        <div class="col-xs-12 col-sm-4 col-md-3">
            <?php $template = !empty($model->date_range) ? "{label} <span class=\"date-clean text-secondary\">x</span>\n{input}\n{hint}\n{error}" : "{label} \n{input}\n{hint}\n{error}"; ?>
            <?= $form->field($model, 'date_range', ['template' => $template])->widget(kartik\daterange\DateRangePicker::class, [
                'convertFormat' => true,
                'startAttribute' => 'date_start',
                'endAttribute' => 'date_end',
                'options' => ['class' => 'form-control text-sanfrancisco'],
                'pluginOptions' => [
                    'locale'=>[
                        'format' => 'd/m/Y',
                        'separator' => '               '
                    ],
                    'minDate' => date('d/m/Y'),
                    'autoUpdateInput' => true,
                    'opens' => 'left',
                    'applyButtonClasses' => 'applyBtn btn btn-sm btn-secondary',
                    //'autoApply' => true
                ],
            ]) ?>
            
        </div>
        
        <!--<div class="col-xs-12 col-sm-4 col-md-3">
            <?php // $form->field($model, 'user_type', ['inline' => false])->checkboxList([User::TYPE_USER => 'Particulares', User::TYPE_USER_PRO => "Profesionales"])->label(false) ?>
        </div>-->
        
        <div class="col-xs-12 visible-xs"><?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-block']) ?></div>
        
    </div>

    <?php ActiveForm::end(); ?>
    
</div>