<?php

use common\models\Item;
use frontend\models\SearchItem;
use common\models\Category;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this frontend\components\View */
/* @var $bigCategory Category */
/* @var $category Category */
/* @var $subCategory Category */
/* @var $subCategories Category[] */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $items Item[] */
/* @var $featuredItems Item[] */
/* @var $model \frontend\models\SearchItem */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Rent');

if(!empty($bigCategory)) {
    $this->title = Yii::t('app', "Rent of").' '.$bigCategory->title;
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => $bigCategory->getUrl()];
}

if(!empty($category)) {
    $this->title = Yii::t('app', "Rent of").' '.$category->title;
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => $category->getUrl()];
}

if(!empty($subCategory)) {
    $this->title = Yii::t('app', "Rent of").' '.$subCategory->title;
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => $subCategory->getUrl()];
}


$this->usePageContent = false;

$pagination = $dataProvider->getPagination();

$params = Yii::$app->getRequest()->get();
$urlParams = [
    'search' => Yii::$app->getRequest()->get('search'),
    'address' => Yii::$app->getRequest()->get('address'),
    'radius' => Yii::$app->getRequest()->get('radius'),
    'lat' => Yii::$app->getRequest()->get('lat'),
    'lng' => Yii::$app->getRequest()->get('lng'),
    'search_city' => Yii::$app->getRequest()->get('search_city'),
    'city' => Yii::$app->getRequest()->get('city'),
    'price_min' => Yii::$app->getRequest()->get('price_min'),
    'price_max' => Yii::$app->getRequest()->get('price_max'),
    'date_range' => Yii::$app->getRequest()->get('date_range'),
];

?>

<?= $this->render('_search', ['model' => $model]) ?>

<hr/>

<div id="items" class="items">
    
    <div class="row">
    
        <div class="col-xs-12 col-sm-3 left hidden-xs">
            
            <div id="items-filter-categories">
                
                <?php
                if(empty($category) && empty($subCategories) && $bigCategory && $bigCategory->subCategories) {
                    $subCategories = $bigCategory->subCategories;
                }
                elseif(empty($category) && empty($subCategories)) {
                    $subCategories = $this->bigCategories;
                }
                ?>
                    
                <?php if(!empty($bigCategory)): ?>
                    <p class="text-secondary"><a href="<?= Url::to($bigCategory->getUrl($urlParams)) ?>" class="text-secondary"><?= $bigCategory->title ?></a></p>
                <?php endif; ?>
                <?php if(!empty($category)): ?>
                    <p class="text-secondary">&nbsp; > <a href="<?= Url::to($category->getUrl($urlParams)) ?>" class="text-secondary"><?= $category->title ?></a></p>
                <?php endif; ?>

                <h2 class="text-primary"><?= Yii::t('app', "Categories") ?></h2>

                <ul id="items-subcategories">
                    <?php foreach($subCategories as $subCat): ?>
                        <li class="<?= !empty($subCategory) && $subCategory->id == $subCat->id ? 'active' : '' ?>" title="<?= $subCat->title ?>">
                            <a href="<?= Url::to($subCat->getUrl($urlParams)) ?>"><?= $subCat->title ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                
            </div>
            
            <div class="alquiya-ad">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-2957507038442890"
                     data-ad-slot="8024932393"
                     data-ad-format="auto"
                     data-full-width-responsive="true"></ins>
                <script>
                     (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            
        </div>
        
        <div class="col-xs-12 col-sm-9 right">
            
            <div id="items-nb-results" class="clearfix">

                <div class="pull-right">
                    <strong class="text-primary"><?= Yii::t('app', "Order by") ?> : </strong>&nbsp;&nbsp;

                    <?php
                    //var_dump($dataProvider->getSort()->);die;
                    $sorts = SearchItem::getSort(!empty($model->radius));
                    $sortItems = [];
                    foreach($sorts as $value => $sort) {
                        $sortItems[] = ['label' => $sort, 'url' => Url::current(['sort' => $value]), 'options' => ['class' => Yii::$app->request->get('sort') == $value ? 'active' : '']];
                    }
                    ?>
                    <?= yii\bootstrap\ButtonDropdown::widget([
                        'label' => ArrayHelper::getValue($sorts, Yii::$app->request->get('sort'), Yii::t('app', "Relevance")) ,
                        'options' => ['class' => 'btn btn-default'],
                        'dropdown' => [
                            'items' => $sortItems
                        ]
                    ]) ?>
                </div>

                <p><span class="text-sanfrancisco"><?= Yii::$app->formatter->asInteger($dataProvider->getTotalCount()) + count($featuredItems) ?></span> <?= Yii::t('app', "results found") ?></p>

            </div>
            
            <div id="items-list">

                <?php if($items): ?>

                    <?php foreach($items as $i => $item): ?>
                
                        <?= $this->render('_item', ['item' => $item, 'showDistance' => false, 'asRow' => true]) ?>
                
                        <?php if((count($items) == 1 && $i == 0) || (count($items) == 2 && $i == 1) || (count($items) == 3 && $i == 2) || (count($items) >= 4 && $i == 3) || (count($items)>4 && $i==(count($items)-1))): ?>
                
                            <div class="alquiya-ad">
                                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-format="fluid"
                                     data-ad-layout-key="-f7+70+66-hh+99"
                                     data-ad-client="ca-pub-2957507038442890"
                                     data-ad-slot="8322824609"></ins>
                                <script>
                                     (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                            </div>
                
                        <?php endif; ?>
                
                    <?php endforeach; ?>

                <?php else: ?>
                
                    <div class="alquiya-ad">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-format="fluid"
                             data-ad-layout-key="-f7+70+66-hh+99"
                             data-ad-client="ca-pub-2957507038442890"
                             data-ad-slot="8322824609"></ins>
                        <script>
                             (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                
                <?php endif; ?>
            </div> 
            
            <?=yii\widgets\LinkPager::widget([
                'pagination' => $pagination,
                'prevPageLabel' => '<i class="fa fa-chevron-left"></i> '.Yii::t('app', "Previous"),
                'nextPageLabel' => Yii::t('app', "Next").' <i class="fa fa-chevron-right"></i>'
            ]); ?>
            
        </div>
                         
    </div>
    
</div>


<?php $this->beginBlock('js'); ?>
<script type="text/javascript">

    $(document).ready(function(){
        
        $('.field-date_range .date-clean, .field-booking-date_range .date-clean').on('click', function(){
            $('#date_range, #date_range-start, #date_range-end').val('');
        });
        
    });

</script>
<?php $this->endBlock(); ?>