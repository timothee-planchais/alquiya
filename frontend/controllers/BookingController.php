<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\models\Item;
use common\models\Review;
use frontend\models\Booking;
use frontend\components\WebUser;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\Payment;

/**
 * Description of BookingController
 *
 * @author Timothee
 */
class BookingController extends \frontend\components\Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['index', 'view', 'create', 'cancel', 'validate', 'refuse', 'review', 'pay', 'response'],
                'rules' => [
                    [
                        //'actions' => ['index', 'view', 'create', 'cancel', 'validate', 'refuse', 'review', 'pay', 'response'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'cancel' => ['post'],
                    'validate' => ['post'],
                    //'confirm' => ['post']
                ],
            ],
        ];
    }
    
    /**
     * 
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if($action->id == 'response') {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    
    /*
     * EN TANT QUE PROPRIETAIRE
     * Bookings of the user (like owner)
     */
    public function actionOwnerRents()
    {
        $this->layout = 'booking';
        
        $dataProvider = new ActiveDataProvider([
            'query' => $this->getUser()->getIdentity()->getBookings2(),
            'sort' => [
                'attributes' => ['id', 'date_start', 'date_end', 'price', 'total', 'created_at'],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'created_at' => SORT_DESC
                ]
            ]
        ]);
        
        return $this->render('owner_bookings', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    /*
     * EN TANT QUE LOCATAIRE
     * Bookings of the user (like renter)
     */
    public function actionRents()
    {
        $this->layout = 'booking';
        
        $dataProvider = new ActiveDataProvider([
            'query' => $this->getUser()->getIdentity()->getBookings(),
            'sort' => [
                'attributes' => ['id', 'date_start', 'date_end', 'price', 'total', 'created_at'],
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'created_at' => SORT_DESC
                ]
            ]
        ]);
        
        return $this->render('bookings', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionCreate($id)
    {
        $item = Item::findOne($id);
        
        $this->checkPermission(WebUser::PERMISSION_ITEM_VIEW, ['item' => $item]);
        
        $model = new Booking([
            'item_id' => $item->getPrimaryKey(),
            'status' => Booking::STATUS_PENDING,
            'price' => $item->price,
            'deposit_value' => $item->deposit_value
        ]);
        $model->populateRelation('item', $item);
        
        if(Yii::$app->getRequest()->get('date_start') || Yii::$app->getRequest()->get('date_end')) {
            $model->date_start = Yii::$app->getRequest()->get('date_start');
            $model->date_end = Yii::$app->getRequest()->get('date_end');
        }
        else {
            $model->load(Yii::$app->getRequest()->get());
        }
        
        if(empty($model->date_start) || empty($model->date_end)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        
        //On valide à l'arrivée
        if(!Yii::$app->getRequest()->getIsPost() && !$model->validate()) {
            return $this->redirect(['/item/view', 'id' => $item->getPrimaryKey()]);
        }
        
        $model->scenario = Booking::SCENARIO_CREATE;
        
        $model->initDatesFromInputs();
        $model->initDuration();
        
        if(!$this->getUser()->getIsGuest()) {
            $model->user_id = $this->getUser()->getId();
        }
        //Vérification minimum
        if($model->getTotalPrice() < Yii::$app->params['booking.min_total']) {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', "The amount of the reservation can not be less than {nb}", ['nb' => Yii::$app->formatter->asCurrency(Yii::$app->params['booking.min_total'])]));
            return $this->redirect(['/item/view', 'id' => $item->getPrimaryKey()]);
        }
        
        if($model->load(Yii::$app->getRequest()->post()))
        {
            if($model->save())
            {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Your booking have been correctly saved. You will be notified when the owner will accept your booking."));
                return $this->redirect(['booking/view', 'id' => $model->getPrimaryKey()]);
                //return $this->redirect(['booking/pay', 'id' => $model->getPrimaryKey()]);
            }
            else {
                //var_dump($model->getErrors());die;
            }
        }
        
        return $this->render('view', [
            'model' => $model,
            'item' => $item
        ]);
    }
    
    public function actionView($id)
    {
        $booking = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_BOOKING_VIEW, ['booking' => $booking]);
        
        $item = $booking->item;
        
        return $this->render('view', [
            'model' => $booking,
            'item' => $item
        ]);
    }
    
    public function actionReview($id)
    {
        $booking = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_BOOKING_REVIEW, ['booking' => $booking]);
        
        $author = $this->getUser()->getIdentity();
        $user = $this->getUser()->getId() == $booking->user_id ? $booking->item->user : $booking->user;
        $model = new Review([
            'author_id' => $author->getPrimaryKey(),
            'booking_id' => $booking->getPrimaryKey(),
            'user_id' => $user->getPrimaryKey(),
            'note' => 5
        ]);
        
        if($model->load(Yii::$app->getRequest()->post()) && $model->save())
        {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "Review correctly posted"));
            return $this->redirect($booking->getUrl());
        }
        
        return $this->render('/review/create', [
            'model' => $model,
            'booking' => $booking
        ]);
    }
    
    public function actionValidate($id)
    {
        $booking = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_BOOKING_VALIDATE, ['booking' => $booking]);
        
        $booking->status = Booking::STATUS_VALIDATED;
        if($booking->update(false)) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', "Booking correctly validated"));
        }
        
        return $this->redirect(['view', 'id' => $id]);
    }
    
    public function actionRefuse($id)
    {
        $booking = $this->findModel($id);
        $booking->scenario = Booking::SCENARIO_REFUSE;
        
        $this->checkPermission(WebUser::PERMISSION_BOOKING_REFUSE, ['booking' => $booking]);
        
        if($booking->load(Yii::$app->getRequest()->post()))
        {
            $booking->status = Booking::STATUS_REFUSED;
            if($booking->validate(['refusal_reason']) && $booking->update(false)) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Booking correctly refused"));
                return $this->redirect(['view', 'id' => $id]);
            }

        }
            
        return $this->render('refuse', [
            'booking' => $booking
        ]);
    }
    
    public function actionCancel($id)
    {
        $booking = $this->findModel($id);
        $booking->scenario = Booking::SCENARIO_CANCEL;
        
        $this->checkPermission(WebUser::PERMISSION_BOOKING_CANCEL, ['booking' => $booking]);
        
        if($booking->load(Yii::$app->getRequest()->post()))
        {
            $booking->status = Booking::STATUS_CANCELLED;
            if($booking->validate(['refusal_reason']) && $booking->update(false)) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Booking correctly cancelled"));
                return $this->redirect(['view', 'id' => $id]);
            }
            /*elseif($booking->hasErrors()) {
                var_dump($booking->getErrors());die;
            }*/

        }
            
        return $this->render('cancel', [
            'booking' => $booking
        ]);
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPayment($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
}
