<?php

namespace frontend\controllers;

use Yii;
use common\models\Payment;
use common\models\Order;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Description of PaymentController
 *
 * @author Timothee
 */
class PaymentController extends \frontend\components\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', '?']
                    ],
                ]
            ],
        ];
    }
    
    /**
     * 
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if($action->id == 'notification') {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    
    /**
     * Payment avec redirection
     * @see http://developers.payulatam.com/en/web_checkout/sandbox.html
     */
    public function actionPayu($id)
    {
        $payment = $this->findModel($id);
        
        //$this->checkPermission(null, ['payment' => $payment]);
        
        $payment->generateSignature();
        
        return $this->render('payu', [
            'payment' => $payment
        ]);
    }
    
    /*
     * Confirmation PayU
     * http://dev.alquiya.com/payment/confirm?id=3&merchantId=508029&merchant_name=Test+PayU+Test+comercio&merchant_address=Av+123+Calle+12&telephone=7512354&merchant_url=http%3A%2F%2Fpruebaslapv.xtrweb.com&state_pol=4&response_message_pol=APPROVED&message=APPROVED&referenceCode=nLyWH7m1Cc&reference_pol=845636784&transaction_id=2ad7c077-7b0a-46a7-8b0d-7ced63341347&description=Subscription+%3A+Reposicionado+%28One+month%29&trazabilityCode=00000000&cus=00000000&orderLanguage=es&extra1=&extra2=&extra3=&polTransactionState=4&signature=47e1aebaafbf6611852662da3d6c0e0e&response_code_pol=1&lapResponseCode=APPROVED&risk=&payment_method_id=1&lapPaymentMethod=MASTERCARD&payment_method_type=1&lapPaymentMethodType=CREDIT_CARD&installmentsNumber=1&TX_VALUE=25000.00&TX_TAX=.00&currency=COP&lng=es&pseCycle=&buyerEmail=timothee%40umcgroup.fr&pseBank=&pseReference1=&pseReference2=&pseReference3=&authorizationCode=00000000&transaction_date=2019-05-15
     */
    public function actionConfirm($id)
    {
        $payment = $this->findModel($id);
        
        $order = $payment->order;
        
        $request = Yii::$app->getRequest();
        
        $payment->state_pol = $request->get('state_pol');
        $signature = $request->get('signature');
       
        if($signature == $payment->getSignatureResponse())
        {
            $payment->response_code_pol = $request->get('response_code_pol');
            $payment->reference_pol = $request->get('reference_pol');
            $payment->payment_method_id = $request->get('payment_method_id');
            $payment->payment_method_type = $request->get('payment_method_type');
            $payment->transaction_date = $request->get('transaction_date');
            $payment->response_message_pol = $request->get('response_message_pol');
            $payment->transaction_id = $request->get('transaction_id');
            //var_dump($payment->getAttributes());die;
            
            if($payment->state_pol == Payment::STATE_POL_APPROVED)
            {
                if($payment->update(false))
                {
                    $order->status = Order::STATUS_PAID;
                    $order->update(false, ['status', 'updated_at']);
                    return 2;
                }
            }
            else
            {
                //Supprimer le paiement et le booking
                //$order->delete();
                //$payment->delete();
            }
            
            return 1;
        }
        
        return 0;
    }
    
    /*
     * Redirection vers view après confirmation
     * http://dev.alquiya.com/payment/response?id=3&merchantId=508029&merchant_name=Test+PayU+Test+comercio&merchant_address=Av+123+Calle+12&telephone=7512354&merchant_url=http%3A%2F%2Fpruebaslapv.xtrweb.com&transactionState=4&lapTransactionState=APPROVED&message=APPROVED&referenceCode=nLyWH7m1Cc&reference_pol=845636784&transactionId=2ad7c077-7b0a-46a7-8b0d-7ced63341347&description=Subscription+%3A+Reposicionado+%28One+month%29&trazabilityCode=00000000&cus=00000000&orderLanguage=es&extra1=&extra2=&extra3=&polTransactionState=4&signature=47e1aebaafbf6611852662da3d6c0e0e&polResponseCode=1&lapResponseCode=APPROVED&risk=&polPaymentMethod=11&lapPaymentMethod=MASTERCARD&polPaymentMethodType=2&lapPaymentMethodType=CREDIT_CARD&installmentsNumber=1&TX_VALUE=25000.00&TX_TAX=.00&currency=COP&lng=es&pseCycle=&buyerEmail=timothee%40umcgroup.fr&pseBank=&pseReference1=&pseReference2=&pseReference3=&authorizationCode=00000000&processingDate=2019-05-15
     */
    public function actionResponse($id = null)
    {
        $payment = null;
        if($id) {
            $payment = $this->findModel($id);
        }
        
        $request = Yii::$app->getRequest();
        $statePol = $request->get('transactionState');
        
        $order = $payment->order;
        if($payment && $statePol == Payment::STATE_POL_APPROVED)
        {
            $signature = $request->get('signature');
            $payment->state_pol = $statePol;
            $payment->reference_code = $request->get('referenceCode');
            $payment->amount = $request->get('TX_VALUE');
            $payment->currency = $request->get('currency');
            if($signature == $payment->getSignatureResponse())
            {
                if($order->type == Order::TYPE_SUBSCRIPTION)
                {
                    //$subscriptions = $order->subscriptions;
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', "Subscription correctly paid !"));
                }
                else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', "Payment accepted"));
                }
                
                return $this->redirect(['/account/order/index']);
            }
        }
        else 
        {
            $message = "Error";
            if($statePol == Payment::STATE_POL_PENDING) {
                $message = 'Pending payment';
            }
            elseif($statePol == Payment::STATE_POL_ERROR) {
                $message = 'Error';
            }
            if($statePol == Payment::STATE_POL_DECLINED) {
                $message = 'Payment rejected';
            }
            elseif($statePol == Payment::STATE_POL_DECLINED) {
                $message = 'Payment declined';
            }
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $message));
        }

        return $this->redirect(['/account/index']);
    }
    
    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
