<?php

namespace frontend\controllers\account;

use Yii;
use common\models\Message;
use common\models\User;
use common\models\Item;
use common\models\Booking;
use frontend\models\Conversation;
use frontend\components\WebUser;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Description of MessageController
 *
 * @author Timothee
 */
class MessageController extends \frontend\components\Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionSend($userId, $itemId = null, $bookingId = null)
    {
        if($userId == $this->getUser()->getId()) {
            return $this->goBack();
        }
        
        //User
        $user = $this->findUser($userId);
        
        //Item
        $item = null;
        if(!empty($itemId)) {
            $item = $this->findItem($itemId);
            if($item->user_id != $user->getPrimaryKey()) {
                $item = null;
            }
        }
        
        //Booking
        $booking = null;
        if(!empty($bookingId)) {
            $booking = $this->findBook($bookingId);
            $this->checkPermission(WebUser::PERMISSION_BOOKING_VIEW, ['booking' => $booking]);
            $item = $booking->item;
        }
        
        $model = new Message([
            'type' => null,
            'author_id' => $this->getUser()->getId(),
            'user_id' => $user->getPrimaryKey(),
            'item_id' => $item ? $item->getPrimaryKey() : null,
            'booking_id' => $booking ? $booking->getPrimaryKey() : null
        ]);
        $model->populateRelation('user', $user);
        
        if($model->load(Yii::$app->getRequest()->post()))
        {
            if($model->save()) 
            {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Message correctly sent"));
                
                if(!empty($booking)) {
                    $url = $booking->getUrl();
                }
                elseif(!empty($item)) {
                    $url = $item->getUrl();
                }
                else {
                    //$url = $user->getProfileUrl();
                    $url = ['conversation', 'id' => $user->getPrimaryKey()];
                }
                
                return $this->redirect($url);
            }
        }
        
        return $this->render('send', [
            'model' => $model
        ]);
    }
    
    /*
     * Conversations list
     */
    public function actionIndex()
    {
        $conversations = Conversation::getAll();
        
        return $this->render('index', [
            'conversations' => $conversations
        ]);
    }
    
    /*
     * Affichage de la conversation
     */
    public function actionConversation($id)
    {
        $user = $this->findUser($id);
        
        $conversation = Conversation::getOne($id);
        
        if(!$conversation) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        
        //Mark as read
        Yii::$app->db->createCommand()
            ->update('message', ['is_read' => 1], ['author_id' => $user->getPrimaryKey(), 'user_id' => Yii::$app->getUser()->getId()])
            ->execute();
        if(Yii::$app->cache->exists('user_nb_messages')) {
            Yii::$app->cache->delete('user_nb_messages');
        }
        
        return $this->render('conversation', [
            'user' => $user,
            'conversation' => $conversation
        ]);
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findItem($id)
    {
        if (($model = Item::find()->where(['id' => $id, 'status' => Item::STATUS_ACTIVE])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findBook($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
}
