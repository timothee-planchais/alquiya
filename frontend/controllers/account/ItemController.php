<?php

namespace frontend\controllers\account;

use Yii;
use common\models\Item;
use frontend\models\SearchItem;
use common\models\Category;
use frontend\models\Booking;
use common\models\Image;
use frontend\components\WebUser;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Description of ItemController
 *
 * @author Timothee
 */
class ItemController extends \frontend\components\Controller
{
    public $layout = 'account';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => Yii::$app->getUser()->getIdentity()->getItems()->orderBy('created_at DESC'),
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    /*
     * CREATE ITEM
     */
    public function actionCreate()
    {
        $this->layout = 'main';
        
        $user = $this->getUser()->getIdentity();
        
        if(!$user->isAddressCompleted()) {
            Yii::$app->session->setFlash('warning', '<a href="'.\yii\helpers\Url::to(['/account/update']).'">'.Yii::t('app', "Your profile is not completed. You must complete your address before publishing a product.").'</a>');
            return $this->redirect(['/account/update']);
        }
        
        $model = new Item([
            'user_id' => $this->getUser()->getId(),
            'status' => Item::STATUS_ACTIVE,
            'locked' => 0,
            'featured' => 0,
            'highlighted' => 0,
            'on_quote' => 0,
            'pricing_type' => Item::PRICING_TYPE_FIX
        ]);
        $model->scenario = Item::SCENARIO_FRONTEND_CREATE;
        $model->is_pro = $user->isPro();
        
        if($model->load(Yii::$app->getRequest()->post()))
        {
            if($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "Product correctly published !"));
                return $this->redirect($model->getUrl());
            }
        }
        
        return $this->render('create', [
            'model' => $model,
            'user' => $user
        ]);
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $user = $this->getUser()->getIdentity();
        
        $model->is_pro = $user->isPro();
        
        $this->checkPermission(WebUser::PERMISSION_ITEM_UPDATE, ['item' => $model]);
        
        if($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "Product correctly updated"));
            return $this->redirect($model->getUrl());
        }
        
        return $this->render('update', [
            'model' => $model,
            'user' => $user
        ]);
    }
    
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_ITEM_UPDATE, ['item' => $model]);
        
        if($model->getBookings()->andWhere(['status' => \common\models\Booking::STATUS_VALIDATED])->count() <= 0) 
        {
            if($model->delete()) {
                Yii::$app->session->setFlash('success', Yii::t('app', "Product correctly deleted"));
            }
        }
        else {
            Yii::$app->session->setFlash('danger', Yii::t('app', "You cannot delete a product that has an incoming booking"));
        }
        
        //return $this->redirect(['/account/item/index']);
        return $this->goBack();
    }
    
    public function actionInactivate($id)
    {
        $model = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_ITEM_UPDATE, ['item' => $model]);
        
        $model->status = Item::STATUS_INACTIVE;
        $model->update(false, ['status']);
        Yii::$app->session->setFlash('success', Yii::t('app', "Your product is now inactive and not visible"));
        
        return $this->goBack();
    }
    
    public function actionActivate($id)
    {
        $model = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_ITEM_UPDATE, ['item' => $model]);
        
        $model->status = Item::STATUS_ACTIVE;
        $model->update(false, ['status']);
        Yii::$app->session->setFlash('success', Yii::t('app', "Your product is now active and visible"));
        
        return $this->goBack();
    }
    
    public function actionInactivateAll()
    {
        $user = $this->getUser()->getIdentity();
        
        $models = $user->getItems()->all();
        
        if($models)
        {
            foreach($models as $model)
            {
                $model->status = Item::STATUS_INACTIVE;
                $model->update(false, ['status']);
            }
            Yii::$app->session->setFlash('success', Yii::t('app', "Your products are now inactives and not visibles"));
        }
        
        return $this->goBack();
    }
    
    public function actionActivateAll()
    {
        $user = $this->getUser()->getIdentity();
        
        $models = $user->getItems()->all();
        
        if($models)
        {
            foreach($models as $model)
            {
                $model->status = Item::STATUS_ACTIVE;
                $model->update(false, ['status']);
            }
            Yii::$app->session->setFlash('success', Yii::t('app', "Your products are now ictives and visibles"));
        }
        
        return $this->goBack();
    }
    
    public function actionUploadImage($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = [
            'status' => 0
        ];
        
        $item = $this->findModel($id);
        
        $this->checkPermission(WebUser::PERMISSION_ITEM_UPDATE, ['item' => $item]);
        
        if($item)
        {
            $nbImages = $item->getImages()->count();
            $tmpImages = \yii\web\UploadedFile::getInstancesByName('tmp_images');
            if($tmpImages)
            {
                foreach($tmpImages as $tmpImage)
                {
                    if($nbImages >= Yii::$app->params['item.max_files']) {
                        break;
                    }
                    
                    //$position = (int)$model->getImages()->count() + 1;
                    $image = new Image([
                        'item_id' => $item->getPrimaryKey(),
                        //'position' => $position
                    ]);
                    $image->image = $tmpImage;
                    $image->save();
                    $nbImages++;
                    $response['status'] = 1;
                }
            }
        }
        
        return $response;
    }
    
    public function actionDeleteImage()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = [
            'status' => 0
        ];
        
        $id = Yii::$app->getRequest()->post('key');
        
        $image = Image::findOne($id);
        
        $item = $this->findModel($image->item_id);
        $this->checkPermission(WebUser::PERMISSION_ITEM_UPDATE, ['item' => $item]);
        
        if($image && $image->delete()) {
            $response['status'] = 1;
        }
        
        return $response;
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
