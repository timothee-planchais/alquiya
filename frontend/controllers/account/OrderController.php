<?php

namespace frontend\controllers\account;

use Yii;
use common\models\Order;
use backend\models\SearchOrder;
use common\models\Item;
use common\models\Subscription;
use common\models\Payment;

/**
 * Description of OrderController
 *
 * @author Timothee
 */
class OrderController extends \frontend\components\Controller
{
    public $layout = 'account';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user = $this->getUser()->getIdentity();
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $user->getOrders()->orderBy('created_at DESC'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = Order::TYPE_SUBSCRIPTION, $itemId, $subscriptionType)
    {
        if(YII_ENV == 'prod') {
            Yii::$app->session->setFlash('info', '<i class="fa fa-info"></i> '.Yii::t('app', "This feature is not yet available"));
            return $this->goBack();
        }
        
        $this->layout = 'main';
        
        $user = $this->getUser()->getIdentity();
        
        $payment = new Payment([
            'user_id' => $user->getPrimaryKey(),
            'currency' => Yii::$app->params['currency']
        ]);
        $payment->generateReferenceCode();
        
        $order = new Order([
            'user_id' => $user->getPrimaryKey(),
            'type' => $type,
            'status' => Order::STATUS_WAITING_PAYMENT,
            'method' => Order::METHOD_PAYU,
            'currency' => Yii::$app->formatter->currencyCode,
            'vta_rate' => 0.2,
            'date' => date('Y-m-d H:i:s'),
            'amount' => 0
        ]);

        $item = Item::findOne($itemId);
        
        //Vérifications
        if(!Yii::$app->user->can(\frontend\components\WebUser::PERMISSION_ITEM_UPDATE, ['item' => $item])) {
            return $this->goBack();
        }
        if(!array_key_exists($order->type, Order::getTypes())) {
            return $this->goBack();
        }
        if(!array_key_exists($subscriptionType, Subscription::getTypes())) {
            return $this->goBack();
        }
        
        $subscription = new Subscription([
            'user_id' => $user->getPrimaryKey(),
            'item_id' => $item->getPrimaryKey(),
            'type' => $subscriptionType,
            'status' => Subscription::STATUS_PENDING,
            'period' => Subscription::PERIOD_1_DAY,
        ]);
        $subscription->initStartDate();
        
        $subscriptionPrices = $subscription->getPrices($item);
        
        if(Yii::$app->getRequest()->getIsPost()) 
        {
            $subscription->load(Yii::$app->getRequest()->post());
            if($subscription->validate(['period']))
            {
                $subscription->setDates();
                $order->amount = $subscription->getPrice($item);
                $payment->amount = $order->amount;
                if($order->save())
                {
                    $subscription->order_id = $order->getPrimaryKey();
                    $subscription->save(false);
                    $payment->order_id = $order->getPrimaryKey();
                    $payment->save(false);
                    return $this->redirect(['/payment/payu', 'id' => $payment->getPrimaryKey()]);
                }
            }
        }

        return $this->render('create', [
            'order' => $order,
            'subscription' => $subscription,
            'subscriptionPrices' => $subscriptionPrices,
            'item' => $item
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
}
