<?php

namespace frontend\controllers;

use Yii;
use common\models\Notification;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Description of Notification
 *
 * @author Timothee
 */
class NotificationController extends \frontend\components\Controller
{
    
    public function actionLoad()
    {
        $html = '';
        
        $user = $this->getUser()->getIdentity();
        
        $offset = Yii::$app->getRequest()->post('offset', 0);
        
        $notifications = $user->getNotifs(null, $offset);
        
        if($notifications)
        {
            foreach($notifications as $notification) {
                $html .= '<li data-id="'.$notification->getPrimaryKey().'" class="notification '.($notification->is_read ? 'read' : 'unread').'">
                            <a href="'.Url::to($notification->getUrl()).'">
                                '.$notification->getText().'
                                <small class="text-right text-muted">'.$notification->getNiceDate().'</small>
                            </a>
                        </li>';
            }
        }
        else {
            $html = '0';
        }
        
        return $html;
    }
    
    public function actionRead()
    {
        $id = Yii::$app->getRequest()->post('id');
        
        $notification = Notification::find()->where(['id' => $id, 'user_id' => $this->getUser()->getId()])->one();
        
        if($notification && !$notification->is_read) {
            $notification->is_read = 1;
            $notification->update(false, ['is_read']);
        }
        
        return 1;
    }
    
    
}
