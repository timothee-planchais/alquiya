<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\Item;
use common\models\Booking;
use frontend\components\WebUser;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Description of UserController
 *
 * @author Timothee
 */
class UserController extends \frontend\components\Controller
{
    
    public function actionView($id)
    {
        $user = $this->findModel($id);
        $userPro = $user->isPro() ? $user->userPro : null;
        
        $items = $user->getItems()->where(['status' => Item::STATUS_ACTIVE, 'locked' => 0])->all();
        
        $reviews = $user->receivedReviews;
        
        return $this->render('view', [
            'user' => $user,
            'userPro' => $userPro,
            'items' => $items,
            'reviews' => $reviews
        ]);
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
}
