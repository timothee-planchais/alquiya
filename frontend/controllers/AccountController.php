<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\Item;
use common\models\Booking;
use frontend\components\WebUser;

/**
 * Description of AccountController
 *
 * @author Timothee
 */
class AccountController extends \frontend\components\Controller
{
    public $layout = 'account';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    /*
     * Dashboard
     */
    public function actionIndex()
    {
        $user = $this->getUser()->getIdentity();
        
        if(!$user->isAddressCompleted()) {
            Yii::$app->session->setFlash('warning', '<a href="'.\yii\helpers\Url::to(['update']).'">'.Yii::t('app', "Your profile is not completed. You must complete your address before publishing a product.").'</a>');
        }
        
        $nbOwnerRents = $user->getBookings2()->andWhere(['booking.status' => Booking::STATUS_COMPLETED])->count();
        $nbRents = $user->getBookings()->andWhere(['booking.status' => Booking::STATUS_COMPLETED])->count();
        $nbItems = $user->getItems()->andWhere(['status' => Item::STATUS_ACTIVE, 'locked' => 0])->count();
        $totalEarnings = $user->getBookings2()->andWhere(['booking.status' => Booking::STATUS_COMPLETED])->sum('booking.total');
        
        $stats = [
            1 => ['label' => Yii::t('app', "Rents as owner"), 'text' => $nbOwnerRents, 'color' => '#2ecc71', 'icon' => 'fa fa-thumbs-o-up'],
            2 => ['label' => Yii::t('app', "Rents as renter"), 'text' => $nbRents, 'color' => '#e74c3c', 'icon' => 'fa fa-thumbs-o-up'],
            3 => ['label' => Yii::t('app', "Items published"), 'text' => $nbItems, 'color' => '#3498db', 'icon' => 'fa fa-file-text-o'],
            4 => ['label' => Yii::t('app', "Earnings"), 'text' => Yii::$app->formatter->asCurrency($totalEarnings ? : 0), 'color' => '#f39c12', 'icon' => 'glyphicon glyphicon-piggy-bank'],
        ];
        
        return $this->render('index', [
            'user' => $user,
            'stats' => $stats
        ]);
    }
    
    public function actionUpdate()
    {
        $model = $this->getUser()->getIdentity();
        $model->scenario = User::SCENARIO_FRONTEND_UPDATE;
        
        if($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            Yii::$app->getSession()->setFlash('success', "Account correctly updated");
            return $this->redirect(['index']);
        }
        elseif($model->hasErrors()) {
            //var_dump($model->getErrors());die;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdatePro()
    {
        $user = $this->getUser()->getIdentity();
        
        $model = $user->userPro;
        
        if($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            Yii::$app->getSession()->setFlash('success', "Company informations correctly updated");
            return $this->redirect(['index']);
        }
        elseif($model->hasErrors()) {
            //var_dump($model->getErrors());die;
        }

        return $this->render('update-pro', [
            'model' => $model,
            'categories' => $this->getDropdownCategories()
        ]);
    }
    
    public function actionUpdatePassword()
    {
        $model = $this->findModel($this->getUser()->getId());
        $model->scenario = User::SCENARIO_UPDATE_PASSWORD;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', "Password correctly updated");
            return $this->redirect(['index']);
        }
        elseif($model->hasErrors()) {
            //var_dump($model->getErrors());die;
        }

        return $this->render('update-password', [
            'model' => $model,
        ]);
    }
    
    public function actionCalendar($itemId = null)
    {
        $user = $this->getUser()->getIdentity();
        
        /* @var $bookings Booking[] */
        $bookings = $user->getBookings2()/*->andWhere(['booking.status' => Booking::STATUS_VALIDATED])*/->all();
        
        return $this->render('calendar', [
            'bookings' => $bookings
        ]);
    }
    
    public function actionWishlist()
    {
        $user = $this->getUser()->getIdentity();
        
        $items = Item::find()->where(['in', 'id', $user->aWishlist])->andWhere(['status' => Item::STATUS_ACTIVE])->all();
        
        return $this->render('wishlist', [
            'items' => $items,
            'user' => $user
        ]);
    }
    
    public function actionUpdateWishlist()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
       
        $response = [
            'status' => 0
        ];
        
        $user = $this->getUser()->getIdentity();
        
        if(Yii::$app->getRequest()->getIsAjax())
        {
            $item = Item::findOne(Yii::$app->getRequest()->post('id'));
            if($item && Yii::$app->getUser()->can(WebUser::PERMISSION_ITEM_VIEW, ['item' => $item]))
            {
                if(Yii::$app->getRequest()->post('remove', false)) {
                    $user->removeFromWishList($item->getPrimaryKey());
                }
                else {
                    $user->addToWishList($item->getPrimaryKey());
                }
                $user->update(false, ['wishlist']);
                $response['status'] = 1;
            }
        }
        
        return $response;
    }
    
     /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
