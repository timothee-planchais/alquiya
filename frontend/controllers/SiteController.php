<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\User;
use common\models\Item;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends \frontend\components\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new \frontend\models\SearchItem();
        
        $cache = Yii::$app->cache;
        
        //Derniers produits ajoutés
        $keyLast = 'home_last_items';
        if($cache->exists($keyLast)) {
            $lastItems = $cache->get($keyLast);
        }
        else {
            $lastItems = Item::findActive()->orderBy('bump_date DESC')->limit(12)->all();
            $cache->set($keyLast, $lastItems, 15*60);
        }
        
        //Featured items
        $keyFeatured = 'home_featured_items';
        if($cache->exists($keyFeatured)) {
            $featuredItems = $cache->get($keyFeatured);
        }
        else {
            $featuredItems = Item::findFeatured()->orderBy('RAND()'/*'bump_date DESC'*/)->limit(12)->all();
            $cache->set($keyFeatured, $featuredItems, 3600);
        }
        
        //Top locations
        /*$keyTop = 'home_top_items';
        if($cache->exists($keyTop)) {
            $topItems = $cache->get($keyTop);
        }
        else {
            $topItems = Item::findActive()->orderBy('created_at DESC')->limit($limit)->all();
            $cache->set($keyTop, $topItems, 3600);
        }*/
        
        $allItems = [
            'featuredItems' => ['title' => Yii::t('app', "Featured products"), 'items' => $featuredItems],
            'lastItems' => ['title' => Yii::t('app', "Last published products"), 'items' => $lastItems],
            //'topItems' => ['title' => Yii::t('app', "Top products"), 'items' => $topItems],
        ];
        
        //Categories
        $categories = \common\models\Category::find()->where('parent_id IS NULL')->orderBy('position ASC')->limit(8)->all();
        
        return $this->render('index', [
            'model' => $searchModel,
            'categories' => $categories,
            'allItems' => $allItems
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'login_signup';
        
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup($pro = 0)
    {
        $this->layout = 'login_signup';
        
        $model = new SignupForm([
            'type' => !empty($pro) ? User::TYPE_USER_PRO : User::TYPE_USER,
            'gender' => User::GENDER_MALE,
            'country' => Yii::$app->params['country']
        ]);
        
        $categories = $this->getDropdownCategories();
        
        if ($model->load(Yii::$app->request->post())) 
        {
            if($user = $model->signup()) 
            {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', "You are successfully registered. You must valid your email adress"));
                return $this->redirect(['login']);
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'categories' => $categories
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('app', "Check your email for further instructions"));
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', "Error"));//Sorry, we are unable to reset password for the provided email address
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app', "New password saved"));

            return $this->redirect(['login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new \frontend\models\VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Your account have been validated !'));
                return $this->redirect(['/account/index']);
            }
        }
        
        /*
         * if($user->status != User::STATUS_ACTIVE) 
            {
                $user->status = User::STATUS_ACTIVE;
                $user->update(false, ['status']);
                Yii::$app->getSession()->setFlash('success', "Your account have been validated.");
            }
            else {
                Yii::$app->getSession()->setFlash('error', "You already have validated your account");
            }
         */

        Yii::$app->session->setFlash('error', Yii::t('app', 'An error occured when validating your account'));
        return $this->goHome();
    }
    
    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new \frontend\models\ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
    
    public function onAuthSuccess($client)
    {
        (new \frontend\components\AuthHandler($client))->handle();
    }
    
    /**
     * @see https://www.sitemaps.org/protocol.html
     */
    public function actionSitemap()
    {
        if(Yii::$app->cache->exists('sitemap'))
        {
            $xml = Yii::$app->cache->get('sitemap');
        }
        else
        {
            $xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

            $pages = ['/site/index','/site/login','/site/signup','/page/contact', '/page/about','/page/help','/page/terms','/page/privacy','/item/index'];
            
            foreach($pages as $url) {
                $xml .= '<url>'
                            .'<loc>'.\yii\helpers\Url::to([$url], true).'</loc>'
                            .'<lastmod>'.date('Y-m-d').'</lastmod>'
                            .'<changefreq>weekly</changefreq>'
                            .'<priority>0.8</priority>'
                        .'</url>';
            }
            
            $categories = $this->allCategories;

            if($categories)
            {
                foreach($categories as $category)
                {
                    $xml .= '<url>'
                                .'<loc>'.\yii\helpers\Url::to($category->getUrl(), true).'</loc>'
                                .'<lastmod>'.date('Y-m-d').'</lastmod>'
                                .'<changefreq>weekly</changefreq>'
                                .'<priority>0.8</priority>'
                            .'</url>';
                }
            }
            
            $xml .= '</urlset>';
            Yii::$app->cache->set('sitemap', $xml, 3600*24*7);
        }
        
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        Yii::$app->response->headers->add('Content-Type', 'text/xml');
        return $xml;
    }
}
