<?php

namespace frontend\controllers;

use Yii;
use common\models\Contact;

/**
 * Description of PageController
 *
 * @author Timothee
 */
class PageController extends \frontend\components\Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact($category = null)
    {
        $model = new Contact([
            'category' => $category,
            'is_read' => 0
        ]);
        if(Yii::$app->getUser()->getIsGuest()) {
            $model->scenario = Contact::SCENARIO_GUEST;
        }
        else {
            //$model->scenario = Contact::SCENARIO_LOGGED;
            $model->user_id = Yii::$app->getUser()->getId(); 
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Thank you for contacting us. We will respond to you as soon as possible.'));
            return $this->refresh();
        }
        
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionTerms()
    {
        return $this->render('terms');
    }
    
    public function actionPrivacy()
    {
        return $this->render('privacy');
    }
    
    public function actionHelp()
    {
        return $this->render('help');
    }
    
    public function actionPro()
    {
        return $this->render('pro');
    }
    
    public function actionRentContract()
    {
        return $this->render('rent-contract');
    }
    
    public function actionAbout()
    {
        return $this->render('about');
    }
    
}
