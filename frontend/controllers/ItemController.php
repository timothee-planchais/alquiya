<?php

namespace frontend\controllers;

use Yii;
use common\models\Item;
use frontend\models\SearchItem;
use frontend\models\Booking;
use common\models\ReportItem;
use frontend\components\WebUser;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Description of ItemController
 *
 * @author Timothee
 */
class ItemController extends \frontend\components\Controller
{
    
    /*
     * ITEM VIEW
     */
    public function actionView($id, $start = null, $end = null)      
    {
        $item = $this->findModel($id);
        
        if(!Yii::$app->user->can(WebUser::PERMISSION_ITEM_VIEW, ['item' => $item])) {
            throw new \yii\web\NotFoundHttpException(Yii::t('app', "Page not found"));
        }
        
        $item->incrementNbViews();
        
        $model = new Booking([
            'item_id' => $item->getPrimaryKey(),
            'status' => Booking::STATUS_PENDING,
            'price' => $item->price,
            'date_start' => $start ? : date('d/m/Y'),
            'date_end' => $end ? : date('d/m/Y'/*, strtotime('tomorrow')*/)
        ]);
        $model->date_range = $model->date_start.Yii::$app->params['datarange.separator'].$model->date_end;
        $model->scenario = Booking::SCENARIO_ITEM;
        
        /*
         * FORM
         */
        if($model->load(Yii::$app->getRequest()->post()))
        {
            if($model->validate())
            {
                return $this->redirect(['/booking/create', 'id' => $item->getPrimaryKey(), 'date_start' => $model->date_start, 'date_end' => $model->date_end]);
            }
        }
        
        
        $reviews = $item->getReviews()->limit(10)->all();
        
        //Similar items
        $query = Item::find()->select('*, RAND()');
        if($item->subcategory_id) {
            $query->where(['subcategory_id' => $item->subcategory_id]);
        }
        else {
            $query->where(['category_id' => $item->category_id]);
        }
        $similarItems = $query->andWhere(['not', ['id' => $item->getPrimaryKey()]])->limit(3)->all();
        
        if(empty($similarItems)) {
            $similarItems = Item::find()->select('*, RAND()')->andWhere(['not', ['id' => $item->getPrimaryKey()]])->limit(3)->all();
        }
        
        return $this->render('view', [
            'item' => $item,
            'model' => $model,
            'similarItems' => $similarItems,
            'reviews' => $reviews
        ]);
    }
    
    /*
     * ITEMS LIST
     */
    public function actionIndex($bigCategoryId = null, $categoryId = null, $subCategoryId = null)
    {
        $searchModel = new SearchItem([
            'date_range' => null//date('d/m/Y').'               '.date('d/m/Y')
        ]);
        $searchModel->load(Yii::$app->request->queryParams);
        
        $bigCategory = null;
        $category = null;
        $subCategory = null;
        $subCategories = [];
        
        /*
         * Enregistrement de la recherche en BDD
         */
        \common\models\StatSearch::addTerm($searchModel->search);
        
        /*
         * subCategory
         */
        if($searchModel->subcategory_id) {
            $subCategoryId = $searchModel->subcategory_id;
        }
        else {
            $searchModel->subcategory_id = $subCategoryId;
        }
        
        if($subCategoryId) {
            $subCategory = $this->getCategory($subCategoryId);
        }
        
        /*
         * category
         */
        if(empty($categoryId) && $subCategory) {
            $categoryId = $subCategory->parent_id;
        }
        
        if($searchModel->category_id) {
            $categoryId = $searchModel->category_id;
        }
        else {
            $searchModel->category_id = $categoryId;
        }
        
        if($categoryId) {
            $category = $this->getCategory($categoryId);
            if($category) {
                $subCategories = $category->subCategories;
            }
        }
        
        /*
         * bigCategory
         */
        if(empty($bigCategoryId) && !empty($category)) {
            $bigCategoryId = $category->parent_id;
        }
        
        if($bigCategoryId) {
            $bigCategory = $this->getCategory($bigCategoryId);
            $searchModel->bigCategoryId = $bigCategoryId;
        }
        
        $dataProvider = $searchModel->search();
        
        /*
         * Items featured
         */
        $searchModelFeatured = clone $searchModel;
        $searchModelFeatured->isFeatured = true;
        $dataProviderFeatured = $searchModelFeatured->search();
        $featuredItems = $dataProviderFeatured->getModels();
        if($featuredItems) {
            $dataProvider->query->andWhere(['not in', 'item.id', array_keys(ArrayHelper::index($featuredItems, 'id'))]);
        }
        $items = $dataProvider->getModels();
        if($featuredItems) 
        {
            $items = array_merge($featuredItems, $items);
        }
        
        return $this->render('index', [
            'bigCategory' => $bigCategory,
            'category' => $category,
            'subCategories' => $subCategories,
            'subCategory' => $subCategory,
            'dataProvider' => $dataProvider,
            'model' => $searchModel,
            'items' => $items,
            'featuredItems' => $featuredItems
            
        ]);
    }
    
    public function actionReport($id)
    {
        $item = $this->findModel($id);
        
        $model = new ReportItem([
            'item_id' => $item->getPrimaryKey(),
            'is_read' => 0,
        ]);
        
        if(Yii::$app->getUser()->getIsGuest()) {
            $model->scenario = ReportItem::SCENARIO_GUEST;
        }
        else {
            //$model->scenario = ReportItem::SCENARIO_LOGGED;
            $model->user_id = Yii::$app->getUser()->getId(); 
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your message has been sent !'));
            return $this->redirect($item->getUrl());
        }
        
        return $this->render('report', [
            'item' => $item,
            'model' => $model
        ]);
    }
    
    /*
     * Redirection pour pretty url
     */
    public function actionSearch()
    {
        
        $params = Yii::$app->request->post();
        unset($params['_csrf-frontend']);
        
        $url = array_merge(['index'], $params);
        
        return $this->redirect($url);
    }
    
    /*
     * SUBCATEGORIES FROM CATEGORY
     */
    public function actionSubcategories()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        $response = [
            'status' => 0,
            'categories' => null
        ];
        
        $id = Yii::$app->getRequest()->post('id');
        
        $category = $this->getCategory($id);
        
        if($category)
        {
            $subcategories = $category->subCategories;
            $response['categories'] = $subcategories ? \yii\helpers\ArrayHelper::map($subcategories, 'id', 'title') : null;
            $response['status'] = 1;
        }
        
        return $response;
    }
    
    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    
}
