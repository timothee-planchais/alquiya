<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => "AlquiYa",
    'language' => 'es',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'class' => 'frontend\components\WebUser',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'sitemap.xml' => 'site/sitemap',
                //'alquiler/search/<SearchItem[search]>' => '/item/index',
                //'alquiler' => 'item/index',
                '<bigCategorySlug:[-a-zA-Z-0-9]+>/<categorySlug:[-a-zA-Z-0-9]+>/<subCategorySlug:[-a-zA-Z-0-9]+>/alquiler/<id:\d+>-<slug:[-a-zA-Z-0-9]+>' => '/item/view',
                '<bigCategorySlug:[-a-zA-Z-0-9]+>/<categorySlug:[-a-zA-Z-0-9]+>/alquiler/<id:\d+>-<slug:[-a-zA-Z-0-9]+>' => '/item/view',
                'alquiler/<slug:[-a-zA-Z-0-9]+>-<id:\d+>' => '/item/view',
                '<bigCategorySlug:[-a-zA-Z-0-9]+>/<categorySlug:[-a-zA-Z-0-9]+>/<subCategorySlug:[-a-zA-Z-0-9]+>-<subCategoryId:\d+>' => '/item/index',
                '<bigCategorySlug:[-a-zA-Z-0-9]+>/<categorySlug:[-a-zA-Z-0-9]+>-<categoryId:\d+>' => '/item/index',
                '<bigCategorySlug:[-a-zA-Z-0-9]+>-<bigCategoryId:\d+>' => '/item/index',
                'i/<id:\d+>' => '/item/view',
                'i/<id:\d+>/booking' => '/booking/create',
                'u/<id:\d+>' => 'user/view',
                'conversation/<id:\d+>' => '/account/message/conversation',
                'items' => '/item/index',
                'validate/u/<id:\d+>/<token>' => 'site/validate-signup',
                '' => '/site/index',
                '/<action>.html' => '/page/<action>',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en',
                    'forceTranslation' => true,
                    'fileMap' => [
                        'app' => 'app.php',
                        'app/error' => 'error.php',
                    ],
                    //'on missingTranslation' => ['frontend\components\TranslationEventHandler', 'handleMissingTranslation']
                ],
                'mail*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en',
                    'forceTranslation' => true,
                    'fileMap' => [
                        'mail' => 'mail.php',
                    ],
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '1966537283377570',
                    'clientSecret' => '35085f6fae20567429201d3e86dbfc25',
                    'version' => '2.8',
                    'attributeNames' => [
                        'id',
                        'name',
                        'email',
                        'picture',
                        'last_name',
                        'first_name',
                        'birthday',
                        'gender',
                        'age_range'
                    ]
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => 'OchxrovTkUhIPGd3WwDakqPTJ',
                    'consumerSecret' => 'imVbmyed4ZvhCYOiCmzoy1s4ViS5xxUoOJcpUuFdVwCbeQZfAP',
                    'attributeParams' => [
                        'include_email' => 'true',
                        'skip_status' => 1
                    ],
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '52303728919-20rdb5ip71n7h30ihieboekpsfpihija.apps.googleusercontent.com',
                    'clientSecret' => 'phKclJ3vokHJvFoC5qAMAn1N',
                ],
            ],
        ],
        'view' => [
            'class' => 'frontend\components\View'
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
