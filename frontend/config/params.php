<?php
return [
    
    'datarange.separator' => '               ',
    
    'bootstrapFormConfig' => [
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => 'col-sm-offset-3',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ],
    'formRadioItem' => function ($index, $label, $name, $checked, $value) {
        return '<label class="radio radio-inline">'
                    .'<input type="radio" name="'.$name.'" value="'.$value.'" '.($checked ? 'checked' : '').'> '
                    .$label
                .'</label>';
    },
];
