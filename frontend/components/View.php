<?php

namespace frontend\components;

use Yii;
use common\models\Category;
use yii\helpers\ArrayHelper;

/**
 * Description of View
 *
 * @author Timothee
 */
class View extends \yii\web\View
{
    
    public $showTitle = true;
    
    public $useContainer = true;
    
    public $usePageContent = true;
    
    /** @var Category[] */
    public $allCategories = [];
    
    /** @var Category[] */
    public $bigCategories = [];
    
    /** @var Category[] */
    public $categories = [];
    
    /** @var Category[] */
    public $subCategories = [];
    
    public $dropDownCategories = [];
    
    public $nbNewMessages = 0;
    
    
    public function beforeRender($viewFile, $params)
    {
        /*
         * MESSAGES
         */
        if(!Yii::$app->getUser()->getIsGuest()) 
        {
            $cache = Yii::$app->cache;
            $key = 'user_nb_messages';
            if($cache->exists($key)) {
                $this->nbNewMessages = $cache->get($key);
            }
            else {
                $this->nbNewMessages = Yii::$app->getUser()->getIdentity()->getNewMessages()->count();
                $cache->set($key, $this->nbNewMessages, 3600);
            }
        }
        
        return parent::beforeRender($viewFile, $params);
    }
    
    /**
     * 
     * @param integer $id
     * @return Category
     */
    public function getCategory($id)
    {
        return Yii::$app->controller->getCategory($id);
    }
    
}
