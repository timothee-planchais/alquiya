<?php

namespace frontend\components;

use Yii;
use common\models\Booking;
use common\models\Item;
use common\models\Review;
use common\models\Payment;
use common\models\Order;
use common\models\Subscription;

/**
 * Description of WebUser
 *
 * @author Timothee
 */
class WebUser extends \yii\web\User
{
    const PERMISSION_BOOKING_VIEW = 'booking_view';
    const PERMISSION_BOOKING_CANCEL = 'booking_cancel';//Locataire ou loueur
    const PERMISSION_BOOKING_VALIDATE = 'booking_validate';
    //const PERMISSION_BOOKING_COMPLETE = 'booking_complete';
    const PERMISSION_BOOKING_REFUSE = 'booking_refuse';
    const PERMISSION_BOOKING_REVIEW = 'booking_review';
    const PERMISSION_BOOKING_PAY = 'booking_pay';
    
    const PERMISSION_ITEM_VIEW = 'item_view';
    const PERMISSION_ITEM_UPDATE = 'item_update';
    
    const PERMISSION_PAYMENT = 'payment';
    
    
    
    public function can($permissionName, $params = array(), $allowCaching = true)
    {
        
        /* @var $booking Booking */
        $booking = null;
        if(!empty($params['booking'])) {
            $booking = $params['booking'];
        }
        
        /* @var $item Item */
        $item = null;
        if(!empty($params['item'])) {
            $item = $params['item'];
        }
        
        /* @var $payment Payment */
        $payment = null;
        if(!empty($params['payment'])) {
            $payment = $params['payment'];
        }
        
        /* @var $order Order */
        $order = null;
        if(!empty($params['order'])) {
            $order = $params['order'];
        }
        
        /* @var $subscription Subscription */
        $subscription = null;
        if(!empty($params['subscription'])) {
            $subscription = $params['subscription'];
        }
        
        $controllerId = null;
        $actionId = null;
        if(strpos($permissionName, '/') !== false) 
        {
            $explode = explode('/', ltrim($permissionName, '/'));
            $controllerId = $explode[0];
            $actionId = $explode[1];
            if(strpos($actionId, '?')) {
                $explode = explode('?', $actionId);
                $actionId = $explode[0];
            }
        }
        
        
        
        if($permissionName == self::PERMISSION_ITEM_VIEW && !empty($item))
        {
            return ($item->status == Item::STATUS_ACTIVE && !$item->locked) || $item->user_id == $this->getId();
        }
        
        if($this->getIsGuest()) {
            return false;
        }
        
        if($permissionName == self::PERMISSION_ITEM_UPDATE && !empty($item))
        {
            return $item->user_id == $this->getId();
        }
        
        /*
         * REVIEW A BOOKING
         */
        if($permissionName == self::PERMISSION_BOOKING_REVIEW && !empty($booking))
        {
            if($this->can(self::PERMISSION_BOOKING_VIEW, ['booking' => $booking]) && $booking->status == Booking::STATUS_COMPLETED)
            {
                if(!$booking->isReviewed($this->getIdentity())) {
                    return true;
                }
            }
        }
        
        /*
         * VIEW A BOOKING
         */
        if($permissionName == self::PERMISSION_BOOKING_VIEW && !empty($booking))
        {
            if($booking->user_id == $this->getId()) {
                return true;
            }
            if(empty($item)) {
                $item = $booking->item;
                return $item->user_id == $this->getId();
            }
        }
        
        /*
         * CANCEL A BOOKING
         */
        if($permissionName == self::PERMISSION_BOOKING_CANCEL && !empty($booking))
        {
            //Si locataire
            if($booking->user_id == $this->getId())
            {
                return ($booking->status == Booking::STATUS_PENDING || $booking->status == Booking::STATUS_VALIDATED)
                        /*&& $booking->status != Booking::STATUS_REFUSED*/ 
                        && $booking->isUpcoming()
                        /*&& $booking->oStart->diff(new \DateTime())->days <= Yii::$app->params['booking.delay']*/
                        && $booking->oStart->diff(new \DateTime())->days > Yii::$app->params['booking.cancel.min_days'];
            }
            //Si loueur
            /*elseif($booking->item->user_id == $this->getId())
            {
                return $booking->status == Booking::STATUS_VALIDATED
                        && $booking->isUpcoming();
            }*/
        }
        
        /*
         * VALIDATE A BOOKING
         */
        if($permissionName == self::PERMISSION_BOOKING_VALIDATE && !empty($booking))
        {
            return $booking->status == Booking::STATUS_PENDING && strtotime($booking->oStart->format('Y-m-d')) > time() && $booking->item->user_id == $this->getId();
        }
        
        /*
         * REFUSE A BOOKING
         */
        if($permissionName == self::PERMISSION_BOOKING_REFUSE && !empty($booking))
        {
            return $booking->status == Booking::STATUS_PENDING 
                    && $booking->item->user_id == $this->getId()
                    && $booking->isUpcoming()
                    /*&& $booking->oStart->diff(new \DateTime())->days <= Yii::$app->params['booking.delay']*/;
        }
        
        /*
         * PAYMENT
         */
        if($controllerId == 'payment' && !empty($payment))
        {
            return $payment->user_id == $this->getId();
        }
        
        /*
         * ORDER
         */
        if($controllerId == 'account/order' && !empty($order))
        {
            return $order->user_id == $this->getId();
        }
        
        /*
         * SUBSCRIPTION
         */
        if($controllerId == 'account/subscription' && !empty($subscription))
        {
            return $subscription->user_id == $this->getId();
        }
        
        return false;
    }
    
    /**
     * 
     * @param type $autoRenew
     * @return \common\models\User
     */
    public function getIdentity($autoRenew = true)
    {
        return parent::getIdentity($autoRenew);
    }
    
}
