<?php

namespace frontend\components;

use Yii;
use common\models\User;
use common\components\Utils;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * Description of AuthHandler
 *
 * @author Timothee
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
    
    /**
     * 
     */
    public function handle()
    {
        $email = null;
        $source = $this->client->getId();
        
        $attributes = $this->client->getUserAttributes();
        
        if($source == 'twitter') {
            $id = ArrayHelper::getValue($attributes, 'id_str');
        }
        else {
            $id = ArrayHelper::getValue($attributes, 'id');
        }
        
        if($source == 'google' && !empty($attributes['emails'])) {
            $emails = ArrayHelper::getValue($attributes, 'emails');
            if($emails && !empty($emails[0])) {
                $email = $emails[0]['value'];
            }
        }
        else {
            $email = ArrayHelper::getValue($attributes, 'email');
        }
        
        //var_dump($source, $attributes, $email);die;
        
        //Non connecté et facebook_id
        if(Yii::$app->user->isGuest && !empty($email)) 
        {
            //On cherche s'il existe
            $user = User::findByEmail($email);
            
            //Si déjà enregistré en BDD, on le connecte
            if($user)
            {
                Yii::$app->user->login($user);
            } 
            //Sinon on l'enregistre
            else 
            {
                $user = new User([
                    'admin' => 0,
                    'status' => User::STATUS_PENDING,
                    'email' => $email,
                    'country' => Yii::$app->params['country']
                ]);
                
                //Facebook
                if($source == 'facebook') 
                {
                    $user->facebook_id = $id;
                    $user->firstname = ArrayHelper::getValue($attributes, 'first_name');
                    $user->lastname = ArrayHelper::getValue($attributes, 'last_name');
                    $user->username = ArrayHelper::getValue($attributes, 'screen_name', $user->firstname.' '.$user->lastname);
                    $user->gender = ArrayHelper::getValue($attributes, 'gender') == 'male' ? User::GENDER_MALE : User::GENDER_FEMALE;
                    $user->image = 'https://graph.facebook.com/'.$id.'/picture?type=large';
                }
                //Twitter
                elseif($source == 'twitter') 
                {
                    $username = ArrayHelper::getValue($attributes, 'screen_name');
                    $explodeName = explode(' ', ArrayHelper::getValue($attributes, 'name'));
                    $user->gender = User::GENDER_MALE;
                    $user->twitter_id = $id;
                    $user->firstname = $explodeName[0];
                    $user->lastname = $explodeName[1];
                    $user->username = ArrayHelper::getValue($attributes, 'screen_name', $user->firstname.' '.$user->lastname);
                    $user->image = 'https://twitter.com/'.$username.'/profile_image?size=original';
                    $user->website = ArrayHelper::getValue($attributes, 'url');
                    $user->lang = ArrayHelper::getValue($attributes, 'lang', 'es');
                }
                //Google+
                elseif($source == 'google') 
                {
                    $name = ArrayHelper::getValue($attributes, 'name');
                    $image = ArrayHelper::getValue($attributes, 'image');
                    $user->google_id = $id;
                    $user->gender = ArrayHelper::getValue($attributes, 'gender') == 'male' ? User::GENDER_MALE : User::GENDER_FEMALE;
                    $user->firstname = ArrayHelper::getValue($name, 'givenName');
                    $user->lastname = ArrayHelper::getValue($name, 'familyName');
                    $user->username = ArrayHelper::getValue($attributes, 'displayName', $user->firstname.' '.$user->lastname);
                    $user->image = ArrayHelper::getValue($image, 'url');
                    if($user->image) {
                        $user->image = str_replace('?sz=50', '?sz=300', $user->image);
                    }
                    $user->lang = ArrayHelper::getValue($attributes, 'language', 'es');
                }
                
                $user->generateAuthKey();
                $user->status = User::STATUS_ACTIVE;
                
                //var_dump($user->getAttributes());die;
                
                //Puis on le connecte
                if($user->insert(false)) {
                    //Yii::$app->getSession()->setFlash('success', Yii::t('app', "You are successfully registered. You must valid your adress email"));
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', "You are successfully registered !"));
                    Yii::$app->user->login($user);
                }
                else {
                    //print_r($user->getErrors());
                }
                
            }
        }
        elseif(empty($email)) {
            
        }
    }
    
}
