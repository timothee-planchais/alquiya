<?php

namespace frontend\components;

use Yii;
use common\models\Category;
use yii\helpers\ArrayHelper;

/**
 * Description of Controller
 *
 * @author Timothee
 */
class Controller extends \yii\web\Controller
{
    /** @var Category[] */
    public $allCategories;
    
    public $isMobile = false;
    public $isTablet = false;
    
    public function init()
    {
        if(Yii::$app->request->get('lg') && array_key_exists(Yii::$app->request->get('lg'), Yii::$app->params['languages'])) {
            Yii::$app->session->set('lg', Yii::$app->request->get('lg'));
        }
        if(Yii::$app->session->has('lg')) {
            Yii::$app->language = Yii::$app->session->get('lg');
        }
        
        /*
         * CURRENCY
         */
        $this->view->registerJs('var CURRENCY_CODE = "'.Yii::$app->formatter->currencyCode.'";', \yii\web\View::POS_HEAD);
        
        /*
         * COUNTRIES
         */
        $this->view->registerJs('var COUNTRIES = '.\yii\helpers\Json::encode(array_keys(Yii::$app->params['countries2'])).';', \yii\web\View::POS_HEAD);
        $this->view->registerJs('var COUNTRY = "'.Yii::$app->params['country2'].'";', \yii\web\View::POS_HEAD);
        
        /*
         * FEES
         */
        $this->view->registerJs('var FEES = '.Yii::$app->params['item.fees'].';', \yii\web\View::POS_HEAD);
        
        return parent::init();
    }
    
    public function beforeAction($action)
    {
        /*
         * ------------
         *  CATEGORIES
         */
        
        $key = 'categories';
        
        if(Yii::$app->cache->exists($key))
        {
            $allCategories = Yii::$app->cache->get($key);
        }
        else
        {
            $allCategories = \yii\helpers\ArrayHelper::index(Category::find()->where(['active' => 1])->orderBy('parent_id ASC, position ASC')->all(), 'id');

            if($allCategories) 
            {
                
                foreach($allCategories as $category)
                {
                    if($category->parent_id) 
                    {
                        $parent = ArrayHelper::getValue($allCategories, $category->parent_id);
                        if($parent) {
                            $category->populateRelation('parent', $parent);
                        }
                    }
                }
            }
            
            Yii::$app->cache->set($key, $allCategories, 6*3600);
        }
        
        $bigCategories = [];
        
        if($allCategories)
        {
            foreach($allCategories as $category) 
            {
                if(!$category->parent_id) {
                    $bigCategories[$category->id] = $category;
                }
            }
        }
        
        $this->allCategories = $this->view->allCategories = $allCategories;
        $this->view->bigCategories = $bigCategories;
        
        return parent::beforeAction($action);
    }
    
    public function afterAction($action, $result)
    {
        /*
         * Return URL
         */
        $actions = ['login', 'logout', 'signup', 'verify-email'];
        if(!in_array($action->id, $actions) /*$action->controller->id !== 'site'*/) {
            Yii::$app->user->setReturnUrl(Yii::$app->getRequest()->getUrl());
        } 
        
        return parent::afterAction($action, $result);
    }
    
    public function getDropdownCategories()
    {
        $dropDownCategories = [];
        if($this->allCategories)
        {
            if($this->allCategories)
            {
                foreach($this->allCategories as $category) 
                {
                    if(!$category->parent_id) {
                        $dropDownCategories[$category->title] = ArrayHelper::map($category->subCategories, 'id', 'title');
                    }
                }
            }
        }
        
        return $dropDownCategories;
    }
    
    protected function checkPermission($permissionName, $params = [])
    {
        //Permission par défaut : actionController
        if($permissionName === null) {
            $permissionName = Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
        }
        
        if(!Yii::$app->user->can($permissionName, $params)) {
            $this->denyAccess();
        }
    }
    
    protected function denyAccess($msg = null)
    {
        throw new \yii\web\ForbiddenHttpException($msg !== null ? $msg : Yii::t('yii', 'You are not allowed to perform this action.'));
    }
    
    /**
     * 
     * @return WebUser
     */
    protected function getUser()
    {
        return Yii::$app->getUser();
    }
    
    /**
     * 
     * @param integer $id
     * @return Category
     */
    public function getCategory($id)
    {
        return ArrayHelper::getValue($this->allCategories, $id);
    }
    
}
