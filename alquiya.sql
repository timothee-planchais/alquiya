-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 10 Juin 2019 à 19:35
-- Version du serveur :  5.6.17
-- Version de PHP :  5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `alquiya`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `password_hash` varchar(120) DEFAULT NULL,
  `auth_key` varchar(100) DEFAULT NULL,
  `role` varchar(15) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `password_hash` (`password_hash`),
  KEY `auth_key` (`auth_key`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`id`, `email`, `firstname`, `lastname`, `password_hash`, `auth_key`, `role`, `created_at`, `updated_at`) VALUES
(1, 'timothee@umcgroup.fr', 'Timothée', 'Planchais', '$2y$13$dAqu.rxHGzRyD5RW2M/DyOJlRbOuXEd746NAGzOJTl/o8zc6sYPPO', NULL, 'super_admin', '2019-04-27 20:53:40', '2019-04-27 20:53:40'),
(2, 'alexis@umcgroup.fr', 'Alexis', 'Bou', '$2y$13$vR71.U9rsxvefbgjzEqoN.XvqXiAOUnGhAFHf8g9.RVUK/WM.4lnW', NULL, 'super_admin', '2019-04-27 20:53:40', '2019-04-27 15:22:54'),
(4, 'jose@umcgroup.fr', 'José', 'Lou', '$2y$13$CcFWBLR0KrD/5sca.ztaIOZJscm7uJ8mkIFl2cCuTV7RLNZEHTwWu', NULL, 'super_admin', '2019-04-27 20:59:54', '2019-04-27 19:42:38');

-- --------------------------------------------------------

--
-- Structure de la table `amazon_item`
--

CREATE TABLE IF NOT EXISTS `amazon_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `amazon_item`
--

INSERT INTO `amazon_item` (`id`, `category_id`, `url`, `title`, `description`, `image`) VALUES
(1, 66, 'https://www.amazon.fr/gp/product/B00XKSBMQA?ie=UTF8&tag=usoastuto-21&camp=1642&linkCode=xm2&creativeASIN=B00XKSBMQA', 'Canon EF STM - Objectif Reflex 50 mm / f 1,8 - Noir', 'Mise au point rapide et quasi silencieuse\r\nDistance minimale de mise au point : 0,35 m\r\nAgrandissement maximum : 0,21 x\r\nMoteur AF : STM\r\nDiamètre du filtre : 49 mm\r\nBouchon anti-poussière de l''objectif E\r\nPerformances exceptionnelles en basse lumière\r\nQualité d''image ultra-nette\r\nDesign compact idéal pour tous vos déplacements\r\nGarantie fabricant : 1 an', '//ws-eu.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=FR&ASIN=B00KAQX66Y&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=_SL250_&tag=usoastuto-21');

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `status` varchar(30) NOT NULL,
  `refusal_reason` varchar(255) DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `price` decimal(10,3) NOT NULL,
  `total` decimal(10,3) NOT NULL,
  `fees` decimal(10,3) NOT NULL DEFAULT '0.000',
  `deposit_value` decimal(10,3) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`item_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `booking`
--

INSERT INTO `booking` (`id`, `user_id`, `item_id`, `status`, `refusal_reason`, `date_start`, `date_end`, `price`, `total`, `fees`, `deposit_value`, `created_at`, `updated_at`) VALUES
(1, 2, 9, 'refused', NULL, '2018-04-01', '2018-04-04', '50000.000', '200000.000', '40000.000', NULL, '2018-03-01 14:00:00', '2018-03-04 14:00:00'),
(2, 2, 9, 'completed', NULL, '2018-04-18', '2018-04-20', '60000.000', '120000.000', '24000.000', NULL, '2018-03-18 10:00:00', '2018-03-20 10:00:00'),
(3, 2, 9, 'completed', NULL, '2018-04-05', '2018-04-11', '60000.000', '420000.000', '84000.000', NULL, '2018-03-27 10:00:00', '2018-03-27 10:00:00'),
(4, 1, 8, 'completed', NULL, '2018-04-18', '2018-04-20', '100000.000', '300000.000', '60000.000', NULL, '2018-03-28 12:04:53', '2018-03-28 12:04:53'),
(5, 2, 1, 'completed', NULL, '2018-03-18', '2018-03-20', '50000.000', '150000.000', '30000.000', NULL, '2018-03-04 11:37:15', '2018-03-04 11:37:15'),
(6, 1, 8, 'completed', NULL, '2018-04-25', '2018-04-28', '100000.000', '400000.000', '0.000', NULL, '2018-04-04 12:26:58', '2019-06-10 12:04:24'),
(7, 2, 12, 'refused', 'Lorem ipsum', '2018-04-25', '2018-04-28', '60000.000', '240000.000', '48000.000', NULL, '2018-04-06 15:21:19', '2018-04-06 15:42:20'),
(9, 1, 8, 'pending', NULL, '2018-05-15', '2018-05-18', '100000.000', '400000.000', '80000.000', NULL, '2018-04-06 18:32:33', '2018-04-06 18:32:33'),
(11, 1, 14, 'completed', NULL, '2018-05-01', '2018-05-02', '60000.000', '120000.000', '0.000', NULL, '2018-04-10 17:30:23', '2019-06-10 12:05:30'),
(16, 1, 15, 'pending', NULL, '2018-05-01', '2018-05-01', '50000.000', '50000.000', '7500.000', NULL, '2018-04-20 18:03:19', '2018-04-20 18:03:19'),
(18, 1, 15, 'pending', NULL, '2018-05-01', '2018-05-01', '50000.000', '50000.000', '7500.000', NULL, '2018-04-20 18:12:06', '2018-04-20 18:12:06'),
(19, 1, 15, 'pending', NULL, '2018-04-22', '2018-04-22', '50000.000', '50000.000', '7500.000', NULL, '2018-04-23 10:00:37', '2018-04-23 10:00:37'),
(20, 1, 15, 'pending', NULL, '2018-04-22', '2018-04-22', '50000.000', '50000.000', '7500.000', '150000.000', '2018-04-23 10:02:48', '2018-04-23 15:15:44'),
(21, 1, 13, 'pending', NULL, '2018-05-15', '2018-05-15', '45000.000', '45000.000', '6750.000', NULL, '2018-05-14 17:04:57', '2018-05-14 17:04:57'),
(22, 1, 13, 'pending', NULL, '2018-05-23', '2018-05-23', '45000.000', '45000.000', '6750.000', NULL, '2018-05-15 12:24:48', '2018-05-15 12:24:48'),
(23, 1, 13, 'pending', NULL, '2018-05-23', '2018-05-23', '45000.000', '45000.000', '6750.000', NULL, '2018-05-15 12:27:31', '2018-05-15 12:27:31'),
(26, 1, 13, 'validated', NULL, '2019-07-02', '2019-07-05', '45000.000', '90000.000', '13500.000', NULL, '2018-06-18 17:06:37', '2018-06-18 17:06:37'),
(29, 1, 15, 'pending', NULL, '2019-05-14', '2019-05-30', '50000.000', '850000.000', '127500.000', '150.000', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 2, 24, 'refused', 'No estaré disponible a esa fecha.\r\n\r\nLo siento', '2019-06-11', '2019-06-12', '180000.000', '360000.000', '0.000', '300000.000', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 2, 24, 'completed', NULL, '2019-06-09', '2019-06-10', '180000.000', '360000.000', '0.000', '300000.000', '0000-00-00 00:00:00', '2019-06-10 12:21:21');

-- --------------------------------------------------------

--
-- Structure de la table `bump_log`
--

CREATE TABLE IF NOT EXISTS `bump_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `bump_log`
--

INSERT INTO `bump_log` (`id`, `user_id`, `item_id`, `created_at`) VALUES
(1, 1, 23, '2019-05-16 19:39:37'),
(2, 1, 17, '2019-05-16 22:37:30');

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text,
  `group` varchar(1) DEFAULT NULL,
  `position` tinyint(2) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `menu_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `parent_id_2` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=721 ;

--
-- Contenu de la table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `title`, `slug`, `description`, `group`, `position`, `active`, `menu_active`) VALUES
(1, NULL, 'Vehículos', 'vehiculos', NULL, 'C', 1, 1, 1),
(2, NULL, 'Bricolaje', 'bricolaje', NULL, 'A', 2, 1, 1),
(3, NULL, 'Evento', 'evento', NULL, 'A', 3, 1, 1),
(4, NULL, 'Electrónica', 'electronica', NULL, 'A', 4, 1, 1),
(5, NULL, 'Diversión', 'diversion', NULL, 'A', 5, 1, 1),
(6, NULL, 'Vacaciones', 'vacaciones', NULL, 'C', 6, 1, 1),
(7, NULL, 'Hogar', 'hogar', NULL, 'A', 7, 1, 1),
(42, 1, 'Accesorios', 'accesorios', NULL, 'A', 6, 1, 1),
(43, 1, 'Carro de lujo', 'carro-de-lujo', NULL, 'C', 5, 1, 1),
(44, 1, 'Motos', 'motocicleta-y-scooter', 'Motocicleta y scooter', 'C', 3, 1, 1),
(45, 1, 'Remolque', 'remolque', NULL, 'C', 4, 1, 1),
(46, 1, 'Camión', 'camion', 'Camión & Utilitario', 'C', 2, 1, 1),
(47, 1, 'Carro', 'carro', NULL, 'C', 1, 1, 1),
(48, 2, 'Manejo de material', 'manejo-de-material', NULL, NULL, 9, 1, 1),
(49, 2, 'Andamio y escalera', 'andamio-y-escalera', 'Maquinaria de carga', NULL, 8, 1, 1),
(50, 2, 'Calefacción y aire acondicionado', 'calefaccion-y-aire-acondicionado', NULL, NULL, 7, 1, 1),
(51, 2, 'Construcción', 'construccion', NULL, NULL, 6, 1, 1),
(52, 2, 'Medición y detección', 'medicion-y-deteccion', NULL, NULL, 5, 1, 1),
(53, 2, 'Mantenimiento', 'mantenimiento', NULL, NULL, 4, 1, 1),
(54, 2, 'Jardinería', 'jardineria', NULL, NULL, 3, 1, 1),
(55, 2, 'Herramientas', 'herramientas', NULL, NULL, 1, 1, 1),
(57, 2, 'Remodelación / Reforma', 'remodelacion-reforma', NULL, NULL, 2, 1, 1),
(58, 3, 'Animaciones', 'animaciones', NULL, 'B', 4, 1, 1),
(59, 3, 'Mobiliario', 'mobiliario', NULL, 'B', 1, 1, 1),
(60, 3, 'Recepción', 'recepcion', NULL, NULL, 2, 1, 1),
(61, 3, 'Salas', 'salas', NULL, 'C', 6, 1, 1),
(62, 3, 'Sonido y luz', 'sonido-y-luz', NULL, 'B', 5, 1, 1),
(63, 4, 'Audio', 'audio', NULL, NULL, 4, 1, 1),
(64, 4, 'Consolas y Videojuegos', 'consolas-y-videojuegos', NULL, NULL, 6, 1, 1),
(65, 4, 'Informática', 'informatica', NULL, NULL, 2, 1, 1),
(66, 4, 'Foto', 'foto', NULL, NULL, 1, 1, 1),
(67, 4, 'Móviles, tabletas y PDA', 'moviles-tabletas-pda', NULL, NULL, 5, 1, 1),
(68, 4, 'Vídeo', 'video', NULL, NULL, 3, 1, 1),
(69, 5, 'Bote', 'bote', 'Barco, lancha', 'B', 5, 1, 1),
(70, 5, 'Montaña', 'montana', NULL, 'B', 4, 1, 1),
(71, 5, 'Caza y pesca', 'caza-y-pesca', NULL, 'B', 8, 1, 1),
(72, 5, 'Deporte de tabla', 'deporte-de-tabla', NULL, 'B', 1, 1, 1),
(73, 5, 'Deporte extremo', 'deporte-extremo', NULL, 'B', 6, 1, 1),
(74, 5, 'Deporte individual', 'deporte-individual', NULL, 'B', 2, 1, 1),
(75, 5, 'Bicicleta', 'bicicleta', NULL, 'B', 3, 1, 1),
(76, 5, 'Deporte de equipo', 'deporte-de-equipo', NULL, 'B', 7, 1, 1),
(78, 5, 'Juegos varios', 'juegos-varios', NULL, NULL, 10, 1, 1),
(79, 5, 'DVD y CD', 'dvd-y-cd', NULL, NULL, 11, 1, 0),
(80, 5, 'Libros', 'libros', NULL, NULL, 12, 1, 0),
(81, 5, 'Instrumentos de música', 'instrumentos-de-musica', NULL, NULL, 9, 1, 1),
(82, 5, 'Otro', 'otro', NULL, NULL, 14, 1, 0),
(83, 6, 'Camping', 'camping', NULL, 'C', 1, 1, 1),
(84, 6, 'Bed and breakfast', 'bed-and-breakfast', 'Bed and breakfast, Hogar y habitación de huéspedes', 'C', 2, 1, 1),
(85, 6, 'Finca', 'finca', NULL, 'C', 3, 1, 1),
(86, 6, 'Apartamento', 'apartamento', NULL, 'C', 4, 1, 1),
(87, 6, 'Autocaravana', 'autocaravana', 'casa rodante, casa autoportante o motorhome', 'C', 5, 1, 1),
(88, 7, 'Mobiliario de interior', 'mobiliario-de-interior', NULL, 'B', 3, 1, 1),
(89, 7, 'Electrodomésticos', 'electrodomesticos', NULL, 'B', 1, 1, 1),
(90, 7, 'Niños', 'ninos', NULL, NULL, 2, 1, 1),
(91, 7, 'Cuidado corporal', 'cuidado-corporal', NULL, NULL, 4, 1, 1),
(92, 7, 'Equipamiento medico', 'equipamiento-medico', NULL, 'B', 8, 1, 1),
(93, 7, 'Ropa hombres', 'ropa-hombres', NULL, NULL, 6, 1, 1),
(94, 7, 'Ropa mujeres', 'ropa-mujeres', NULL, NULL, 5, 1, 1),
(95, 7, 'Ropa niños', 'ropa-ninos', NULL, NULL, 7, 1, 1),
(96, 7, 'Accesorios', 'accesorios', NULL, NULL, 9, 1, 1),
(97, 4, 'GPS', 'gps', NULL, NULL, 7, 1, 1),
(98, 55, 'Sierra, motosierra y caladora', 'sierra-motosierra-caladora', NULL, NULL, 1, 1, 1),
(99, 55, 'Taladro', 'taladro', NULL, NULL, 2, 1, 1),
(100, 55, 'Rotomartillo y demoledor', 'rotomartillo-y-demoledor', NULL, NULL, 3, 1, 1),
(101, 55, 'Albañilería', 'albanileria', NULL, NULL, 4, 1, 1),
(102, 64, 'Nintendo', 'nintendo', NULL, NULL, 2, 1, 1),
(103, 64, 'PlayStation', 'playstation', NULL, NULL, 1, 1, 1),
(104, 64, 'Xbox', 'xbox', NULL, NULL, 3, 1, 1),
(105, 47, 'Económicos', 'economicos', NULL, NULL, 1, 1, 1),
(106, 47, 'Medianos', 'medianos', NULL, NULL, 2, 1, 1),
(107, 47, 'Grandes', 'grandes', NULL, NULL, 3, 1, 1),
(108, 47, 'Compactos', 'compactos', NULL, NULL, 4, 1, 1),
(109, 47, 'Camionetas & pickups', 'camionetas', NULL, NULL, 5, 1, 1),
(111, 47, 'Van y Minivan', 'van-y-minivan', NULL, NULL, 7, 1, 1),
(113, 47, 'Lujoso', 'lujoso', NULL, NULL, 9, 1, 1),
(114, 47, 'Antiguo y clásico', 'antiguo-y-clasico', NULL, NULL, 10, 1, 1),
(115, 43, 'Limusina', 'limusina', NULL, NULL, 1, 1, 1),
(116, 43, 'Carro deportivo', 'carro-deportivo', NULL, NULL, 2, 1, 1),
(117, 43, 'Carro antiguo', 'carro-antiguo', NULL, NULL, 3, 1, 1),
(118, 43, 'Carro especial evento', 'carro-especial-evento', NULL, NULL, 4, 1, 1),
(119, 43, 'Carro con conductor', 'carro-con-conductor', NULL, NULL, 5, 1, 1),
(120, 44, 'Moto 125cc y más', 'moto-125cc', NULL, NULL, 1, 1, 1),
(121, 44, 'Scooter 125cc y más', 'scooter-125cc', NULL, NULL, 2, 1, 1),
(122, 44, 'Scooter menos de 125cc', 'scooter-menos-de-125cc', NULL, NULL, 3, 1, 1),
(123, 44, 'Accessorios moto', 'accesorios-moto', NULL, NULL, 4, 1, 1),
(124, 44, 'Moto menos de 125cc', 'moto-menos-de-125cc', NULL, NULL, 5, 1, 1),
(125, 44, 'Ciclomotor', 'ciclomotor', NULL, NULL, 6, 1, 1),
(126, 44, 'Moto de coleccion', 'moto-de-coleccion', NULL, NULL, 7, 1, 1),
(127, 44, 'Trimoto', 'trimoto', NULL, NULL, 8, 1, 1),
(128, 44, 'Otro', 'otro', NULL, NULL, 9, 1, 1),
(132, 55, 'Cortador', 'cortador', NULL, NULL, 5, 1, 1),
(133, 55, 'Atornillador', 'atornillador', NULL, NULL, 6, 1, 1),
(134, 55, 'Llaves', 'llaves', NULL, NULL, 7, 1, 1),
(135, 55, 'Alicates', 'alicates', NULL, NULL, 8, 1, 1),
(136, 55, 'Garlopa', 'garlopa', NULL, NULL, 9, 1, 1),
(137, 55, 'Escalera', 'escalera', NULL, NULL, 10, 1, 1),
(138, 55, 'Mesa de trabajo', 'Mesa-de-trabajo', NULL, NULL, 11, 1, 0),
(139, 55, 'Perforación', 'perforacion', NULL, NULL, 12, 1, 0),
(140, 55, 'Martillo', 'martillo', NULL, NULL, 13, 1, 0),
(141, 55, 'Pulidora y torno', 'pulidora-torno', NULL, NULL, 14, 1, 0),
(142, 55, 'Tirfor', 'tirfor', NULL, NULL, 15, 1, 0),
(143, 55, 'Pala', 'pala', NULL, NULL, 16, 1, 0),
(144, 55, 'Cortador', 'cortador', NULL, NULL, 17, 1, 0),
(145, 55, 'Cúter y bisturí', 'cuter-bisturi', NULL, NULL, 18, 1, 0),
(146, 55, 'Hacha', 'hacha', NULL, NULL, 19, 1, 0),
(147, 55, 'Bieldo', 'bieldo', NULL, NULL, 20, 1, 0),
(148, 55, 'Balde', 'balde', NULL, NULL, 21, 1, 0),
(149, 55, 'Otro', 'otro', NULL, NULL, 22, 1, 0),
(165, 57, 'Lijadora', 'lijadora', NULL, NULL, 1, 1, 1),
(166, 57, 'Pistola de pintura', 'pistola-de-pinture', NULL, NULL, 2, 1, 1),
(167, 57, 'Desempapeladora a vapor', 'desempapeladora-a-vapor', NULL, NULL, 3, 1, 1),
(168, 57, 'Pistola de calor', 'pistola-de-calor', NULL, NULL, 4, 1, 1),
(169, 57, 'Pistola de clavos', 'pistola-de-clavos', NULL, NULL, 5, 1, 1),
(170, 57, 'Grapadora', 'grapadora', NULL, NULL, 6, 1, 1),
(171, 57, 'Pulidora', 'pulidora', NULL, NULL, 7, 1, 1),
(172, 57, 'Allanadora', 'allanadora', NULL, NULL, 8, 1, 1),
(173, 57, 'Pincel', 'pincel', NULL, NULL, 9, 1, 1),
(177, 2, 'Soldadura y fontanería', 'soldadura-y-fontaneria', NULL, NULL, 10, 1, 1),
(178, 54, 'Cortaseto', 'cortaseto', NULL, NULL, 1, 1, 1),
(179, 54, 'cortacésped', 'cortacesped', NULL, NULL, 2, 1, 1),
(180, 54, 'Motosierra', 'motosierra', NULL, NULL, 3, 1, 1),
(181, 54, 'Desbrozadora', 'desbrozadora', NULL, NULL, 4, 1, 1),
(182, 54, 'Motocultor', 'motocultor', NULL, NULL, 5, 1, 1),
(183, 54, 'Trituradora', 'trituradora', NULL, NULL, 6, 1, 1),
(184, 54, 'Escarificador', 'escarificador', NULL, NULL, 7, 1, 1),
(185, 54, 'Soplador de hojas', 'soplador-de-hojas', NULL, NULL, 8, 1, 1),
(186, 54, 'Equipamiento de sembradora', 'equipamiento-de-sembradora', NULL, NULL, 9, 1, 1),
(187, 54, 'Barreno', 'barreno', NULL, NULL, 10, 1, 1),
(188, 54, 'Tractor de césped', 'tractor-de-cesped', NULL, NULL, 11, 1, 0),
(189, 54, 'Zanjadora', 'zanjadora', NULL, NULL, 12, 1, 0),
(190, 54, 'Pulverizador', 'pulverizador', NULL, NULL, 13, 1, 0),
(191, 54, 'Aspersión', 'aspersion', NULL, NULL, 14, 1, 0),
(192, 54, 'Eliminación térmica de malas hierbas', 'eliminacion-termica-de-malas-hierbas', NULL, NULL, 15, 1, 0),
(193, 53, 'Hidrolimpiadoras', 'hidrolimpiadoras', 'Limpiadoras de alta presión', NULL, 1, 1, 1),
(194, 53, 'Aspiradora', 'aspiradora', NULL, NULL, 2, 1, 1),
(195, 53, 'Bomba', 'bomba', NULL, NULL, 3, 1, 1),
(196, 53, 'Desatascador', 'desatascador', NULL, NULL, 4, 1, 1),
(197, 53, 'Lava-aspiradore', 'lava-aspiradore', NULL, NULL, 5, 1, 1),
(198, 53, 'Tubería', 'tubiera', NULL, NULL, 6, 1, 1),
(199, 53, 'Limpiadora y aspiradora de vapor', 'limpiadora-y-aspiradora-de-vapor', NULL, NULL, 7, 1, 1),
(200, 53, 'Cisterna', 'cisterna', NULL, NULL, 8, 1, 1),
(201, 52, 'Dectector', 'dectector', NULL, NULL, 1, 1, 1),
(202, 52, 'Láser', 'laser', NULL, NULL, 2, 1, 1),
(203, 52, 'Sensores, multímetro y luxómetro', 'sensores-y-multimetro-y-lucometro', NULL, NULL, 3, 1, 1),
(204, 52, 'Probador de compresión', 'probador-de-compresion', NULL, NULL, 4, 1, 1),
(205, 52, 'Sonómetro', 'sonometro', NULL, NULL, 5, 1, 1),
(206, 52, 'Higrómetro', 'higrometro', NULL, NULL, 6, 1, 1),
(207, 52, 'Teodolito', 'teodolito', NULL, NULL, 7, 1, 1),
(208, 51, 'Hormigonera', 'hormigonera', NULL, NULL, 1, 1, 1),
(209, 51, 'Martillo mecánico', 'martillo-mecanico', NULL, NULL, 2, 1, 1),
(210, 51, 'Compresor', 'compresor', NULL, NULL, 3, 1, 1),
(211, 51, 'Pala excavadora', 'pala-excavadora', NULL, NULL, 4, 1, 1),
(212, 51, 'Apisonadora', 'apisonadora', NULL, NULL, 5, 1, 1),
(213, 51, 'Pala cargadora', 'pala-cargadora', NULL, NULL, 6, 1, 1),
(214, 51, 'Accesorios de obra', 'accesorios-de-obra', NULL, NULL, 7, 1, 1),
(215, 51, 'Aguja vibradora', 'aguja-vibradora', NULL, NULL, 8, 1, 1),
(216, 51, 'Autovolquete', 'autovolquete', NULL, NULL, 9, 1, 1),
(217, 51, 'Zanjadora', 'Zanjadora', 'Zanjadora de piso', NULL, 10, 1, 1),
(218, 51, 'Arenado', 'arenado', NULL, NULL, 11, 1, 0),
(219, 51, 'Regla, escuadra y nivel', 'regla-escuadra-nivel', NULL, NULL, 12, 1, 0),
(220, 51, 'Tractor', 'tractor', NULL, NULL, 13, 1, 0),
(221, 51, 'Pala mixta y miniexcavadora', 'pala-mixta-y-miniexcavadora', NULL, NULL, 14, 1, 0),
(222, 51, 'Otro', 'otro', NULL, NULL, 15, 1, 0),
(223, 50, 'Calefacción', 'calefaccion', NULL, NULL, 1, 1, 1),
(224, 50, 'Grupo electrógeno', 'grupo-electrogeno', NULL, NULL, 2, 1, 1),
(225, 50, 'Deshumidificador', 'deshumidificador', NULL, NULL, 3, 1, 1),
(231, 50, 'Ventilador', 'ventilador', NULL, NULL, 4, 1, 1),
(232, 50, 'Aire acondicionado', 'aire-acondicionado', NULL, NULL, 5, 1, 1),
(233, 50, 'Aire comprimido', 'aire-comprimido', NULL, NULL, 6, 1, 1),
(234, 50, 'Generador', 'generador', NULL, NULL, 7, 1, 1),
(236, 49, 'Escalera', 'escalera', NULL, NULL, 1, 1, 1),
(237, 49, 'Andamio', 'andamio', NULL, NULL, 2, 1, 1),
(238, 49, 'Plataforma elevadora', 'plataforma-elevadora', NULL, NULL, 3, 1, 1),
(239, 49, 'Carretilla elevadora', 'carretilla-elevadora', NULL, NULL, 4, 1, 1),
(240, 49, 'Camión de cubo', 'camion-de-cubo', NULL, NULL, 5, 1, 1),
(241, 49, 'Plataforma', 'plataforma', NULL, NULL, 6, 1, 1),
(242, 49, 'Carretilla telescopica', 'carretilla-telescopica', NULL, NULL, 7, 1, 1),
(243, 49, 'Otro', 'otro', NULL, NULL, 8, 1, 1),
(244, 48, 'Carretilla de carga', 'carretilla-de-carga', NULL, NULL, 1, 1, 1),
(245, 48, 'Apilador', 'apilador', NULL, NULL, 2, 1, 1),
(246, 48, 'Carrito de plataforma', 'carrito-de-plataforma', NULL, NULL, 3, 1, 1),
(247, 48, 'Carretilla', 'carretilla', NULL, NULL, 4, 1, 1),
(248, 48, 'Grúa', 'grua', NULL, NULL, 5, 1, 1),
(249, 48, 'Transpaleta', 'transpaleta', NULL, NULL, 6, 1, 1),
(250, 48, 'Cinta transportadora', 'cinta-transportadora', NULL, NULL, 7, 1, 1),
(251, 48, 'Torno, Cabrestante', 'torno-cabrestante', 'cabrestante, malacate o árgano', NULL, 8, 1, 1),
(252, 48, 'Polipasto', 'Polipasto', NULL, NULL, 9, 1, 1),
(253, 48, 'Otro', 'otro', NULL, NULL, 10, 1, 1),
(254, 177, 'Soldador', 'soldador', NULL, NULL, 1, 1, 1),
(255, 177, 'Soplete', 'soplete', NULL, NULL, 2, 1, 1),
(256, 177, 'Curvador', 'curvador', NULL, NULL, 3, 1, 1),
(257, 177, 'Plegadora', 'plegadora', NULL, NULL, 4, 1, 1),
(258, 177, 'Obturador de tubería', 'obturador-de-tuberia', NULL, NULL, 5, 1, 1),
(259, 177, 'Congelador de tubos', 'congelador-de-tubos', NULL, NULL, 6, 1, 1),
(260, 177, 'Cortatubos', 'cortatubos', NULL, NULL, 7, 1, 1),
(261, 177, 'Soldadura de zinc', 'soldadura-de-zinc', NULL, NULL, 8, 1, 1),
(262, 177, 'Descalcificación', 'descalcificacion', NULL, NULL, 9, 1, 1),
(263, 177, 'Otro', 'otro', NULL, NULL, 10, 1, 1),
(264, 3, 'Cocina', 'cocina', NULL, NULL, 3, 1, 1),
(265, 3, 'Seguridad', 'seguridad', NULL, NULL, 7, 1, 1),
(266, 3, 'Ropa', 'ropa', NULL, NULL, 8, 1, 1),
(267, 59, 'Mobiliario vario', 'mobiliario-vario', NULL, NULL, 1, 1, 1),
(268, 59, 'Carpa de recepción', 'carpa-de-recepcion', 'Tienda de campaña', NULL, 2, 1, 1),
(269, 59, 'Mesa', 'mesa', NULL, NULL, 3, 1, 1),
(270, 59, 'Silla', 'silla', NULL, NULL, 4, 1, 1),
(271, 59, 'Planta', 'planta', NULL, NULL, 6, 1, 1),
(272, 59, 'Basura', 'basura', NULL, NULL, 7, 1, 1),
(273, 59, 'Sombrilla', 'sombrilla', NULL, NULL, 8, 1, 1),
(274, 59, 'Muebles de jardín', 'muebles-de-jardin', NULL, NULL, 9, 1, 1),
(275, 59, 'Bar', 'bar', NULL, NULL, 10, 1, 1),
(276, 59, 'Feria', 'feria', NULL, NULL, 11, 1, 1),
(277, 59, 'Entarimado', 'entarimado', '', NULL, 12, 1, 0),
(278, 59, 'Lámpara', 'lampara', NULL, NULL, 13, 1, 0),
(279, 59, 'Caballete', 'caballete', NULL, NULL, 14, 1, 0),
(280, 59, 'Carrito de servicio', 'carrito-de-servicio', NULL, NULL, 15, 1, 0),
(281, 59, 'Grada', 'grada', NULL, NULL, 16, 1, 0),
(282, 60, 'Vajilla', 'vajilla', NULL, NULL, 1, 1, 1),
(283, 60, 'Plato de frotamiento', 'plato-de-frotamiento', 'calentador de platos', NULL, 2, 1, 1),
(284, 60, 'Decoración y Servicio de mesa', 'decoracion-y-servicio-de-mesa', NULL, NULL, 3, 1, 1),
(285, 60, 'Organización', 'organizacion', NULL, NULL, 4, 1, 1),
(286, 60, 'Mantel', 'mantel', NULL, NULL, 5, 1, 1),
(287, 60, 'Bandejas', 'bandeja', NULL, NULL, 6, 1, 1),
(288, 60, 'Platos', 'platos', NULL, NULL, 7, 1, 1),
(289, 60, 'Guardarropa', 'guardarropa', NULL, NULL, 8, 1, 1),
(290, 264, 'Diverso', 'diverso', NULL, NULL, 1, 1, 1),
(291, 264, 'Cocción', 'coccion', NULL, 'B', 2, 1, 1),
(292, 264, 'Refrigerador', 'refrigerador', NULL, 'B', 3, 1, 1),
(293, 264, 'Utensilio de cocina', 'utensilio-de-cocina', 'Artículo de cocina', NULL, 4, 1, 1),
(294, 264, 'Maquina de crepes', 'maquina-de-crepes', NULL, 'B', 10, 1, 0),
(295, 264, 'Asado y parrilla', 'asado-y-parrilla', 'Plancha de cocina', 'B', 5, 1, 1),
(296, 264, 'Algodón de azúcar', 'algodon-de-azucar', '', 'B', 6, 1, 1),
(297, 264, 'Crispetas', 'crispetas', 'Pop-corn, crispetas, cotufas', 'B', 7, 1, 1),
(298, 264, 'Fuente', 'fuente', NULL, 'B', 8, 1, 1),
(299, 264, 'Tirador de cerveza', 'tirador-de-cerveza', NULL, 'B', 9, 1, 1),
(300, 264, 'Rebanadora de carne', 'rebanadora-de-carne', NULL, 'B', 11, 1, 0),
(301, 264, 'Máquina para perros calientes', 'maquina-para-perros-calientes', NULL, 'B', 12, 1, 0),
(302, 264, 'Máquina para helados', 'maquina-para-helados', NULL, 'B', 13, 1, 1),
(303, 264, 'Salamandra y tostadora', 'salamandra-y-tostadora', NULL, 'B', 14, 1, 0),
(304, 264, 'Percolador', 'percolador', NULL, 'B', 15, 1, 0),
(305, 264, 'Congelador', 'congelador', NULL, 'B', 16, 1, 0),
(306, 264, 'Molde', 'molde', NULL, NULL, 17, 1, 0),
(307, 264, 'Máquina para hielo', 'maquina-para-hielo', NULL, 'B', 18, 1, 0),
(308, 58, 'Castillo hinchable', 'castillo-hinchable', NULL, 'B', 1, 1, 1),
(309, 58, 'Maquinas de espuma y humo', 'maquinas-de-espuma-y-humo', NULL, 'B', 2, 1, 1),
(310, 58, 'Fútbol de mesa humano', 'futbol-de-mesa-humano', NULL, 'B', 3, 1, 1),
(311, 58, 'Cama elástica', 'cama-elastica', NULL, 'B', 4, 1, 1),
(312, 58, 'Tobogán', 'tobogan', NULL, 'B', 5, 1, 1),
(313, 58, 'Carpa de circo', 'carpa-de-circo', NULL, 'B', 7, 1, 1),
(314, 58, 'Otro', 'otro', NULL, NULL, 8, 1, 1),
(316, 62, 'Material de sonido', 'material-de-sonido', NULL, NULL, 1, 1, 1),
(317, 62, 'Material iluminación', 'material-iluminacion', NULL, NULL, 2, 1, 1),
(318, 62, 'Altavoz', 'altavoz', NULL, NULL, 3, 1, 1),
(319, 62, 'Efectos especiales', 'efectos-especiales', NULL, NULL, 4, 1, 1),
(320, 62, 'Micrófono', 'microfono', NULL, NULL, 5, 1, 1),
(321, 62, 'Material de DJ', 'material-de-dj', NULL, NULL, 6, 1, 1),
(322, 62, 'Proyector', 'proyector', NULL, NULL, 7, 1, 1),
(323, 62, 'Mesa de mezclas de audio', 'mesa-de-mezclas-de-audio', NULL, NULL, 8, 1, 1),
(324, 62, 'Estructura luz', 'estructura-luz', NULL, NULL, 9, 1, 1),
(325, 62, 'Kit de audio', 'kit-audio', NULL, NULL, 10, 1, 1),
(326, 62, 'Cuadro de distribución', 'cuadro-de-distribucion', NULL, NULL, 11, 1, 0),
(327, 62, 'Etapa de potencia', 'etapa-de-potencia', NULL, NULL, 12, 1, 0),
(328, 62, 'Estroboscopio', 'estroboscopio', NULL, NULL, 13, 1, 0),
(329, 62, 'Bola de espejos', 'bola-de-espejos', NULL, NULL, 14, 1, 0),
(330, 62, 'Luz negra', 'luz-negra', NULL, NULL, 15, 1, 0),
(331, 62, 'Karaoke', 'karaoke', NULL, NULL, 16, 1, 0),
(332, 62, 'Guirnaldas de luz', 'guirnaldas-de-luz', NULL, NULL, 17, 1, 0),
(333, 62, 'Walkie-talkie', 'walkie-talkie', NULL, NULL, 18, 1, 0),
(334, 62, 'Megáfono', 'megafono', NULL, NULL, 19, 1, 0),
(335, 62, 'Neón', 'neon', NULL, NULL, 20, 1, 0),
(336, 62, 'Tocadiscos', 'tocadiscos', NULL, NULL, 21, 1, 0),
(337, 265, 'Equipamiento de seguridad', 'equipamiento-de-seguridad', NULL, NULL, 1, 1, 1),
(338, 265, 'Señalización', 'senalizacion', NULL, 'B', 2, 1, 1),
(339, 265, 'Barrera', 'barrera', NULL, 'B', 3, 1, 1),
(340, 265, 'Extintor', 'extintor', NULL, 'B', 4, 1, 1),
(341, 265, 'Sistema de alarma', 'sistema-de-alarma', NULL, 'B', 5, 1, 1),
(342, 265, 'Cámaras de vigilancia', 'camaras-de-vigilancia', NULL, 'B', 6, 1, 1),
(343, 265, 'Cola de espera', 'cola-de-espera', NULL, NULL, 7, 1, 1),
(344, 266, 'Disfraz', 'disfraz', NULL, NULL, 1, 1, 1),
(345, 266, 'Vestido de noche', 'vestido-de-noche', NULL, NULL, 2, 1, 1),
(346, 266, 'Vestido de novia', 'vestido-de-novia', NULL, NULL, 3, 1, 1),
(347, 266, 'Ropa de ceremonia', 'ropa-de-ceremonia', NULL, NULL, 4, 1, 1),
(348, 266, 'Esmoquin', 'esmoquin', NULL, NULL, 5, 1, 1),
(349, 266, 'Alta costura', 'alta-costura', NULL, NULL, 6, 1, 1),
(350, 266, 'Frac', 'frac', NULL, NULL, 7, 1, 1),
(354, 61, 'Sala de recepción', 'sala-de-recepcion', NULL, NULL, 1, 1, 1),
(355, 61, 'Autoalmacenaje ', 'autoalmacenaje', 'Self storage', NULL, 2, 1, 1),
(356, 61, 'Sala atípica', 'sala-atipica', NULL, NULL, 3, 1, 1),
(357, 61, 'Sala de Seminario', 'sala-de-seminario', NULL, NULL, 4, 1, 1),
(358, 61, 'Sala de exposición', 'sala-de-exposicion', NULL, NULL, 5, 1, 1),
(359, 61, 'Oficina', 'oficina', NULL, NULL, 6, 1, 1),
(360, 61, 'Sala de fiestas', 'sala-de-fiestas', NULL, NULL, 7, 1, 1),
(361, 61, 'Estudio', 'estudio', NULL, NULL, 8, 1, 1),
(362, 61, 'Discoteca', 'discoteca', NULL, NULL, 9, 1, 1),
(363, 61, 'Sala de conferencia', 'sala-de-conferencia', NULL, NULL, 10, 1, 1),
(365, 46, 'Furgoneta', 'furgoneta', 'Camioneta', NULL, 1, 1, 1),
(366, 46, 'Camión estacas', 'camion-estaca', NULL, NULL, 2, 1, 1),
(367, 46, 'Volqueta', 'volqueta', 'Camión volquete', NULL, 3, 1, 1),
(368, 46, 'Camión', 'camion', NULL, NULL, 4, 1, 1),
(369, 46, 'Camión refrigerado', 'camion-refrigerado', NULL, NULL, 5, 1, 1),
(370, 712, 'Microbus', 'microbus', 'Minibus', NULL, 3, 1, 1),
(371, 712, 'Grúa', 'grua', NULL, NULL, 6, 1, 1),
(372, 46, 'Utilitario convertido', 'utilitario-convertido', NULL, NULL, 8, 1, 1),
(375, 45, 'Remolque de equipaje', 'remolque-para-carro', NULL, NULL, 1, 1, 1),
(376, 45, 'Remolque para moto', 'remolque-para-moto', NULL, NULL, 3, 1, 1),
(377, 45, 'Remolque para bicicleta', 'remolque-para-bicicleta', NULL, NULL, 4, 1, 1),
(378, 45, 'Transporte de carro', 'transporte-de-carro', NULL, NULL, 5, 1, 1),
(379, 45, 'Remolque para animales', 'remolque-para-animales', NULL, NULL, 6, 1, 1),
(380, 45, 'Remolque para bote', 'remolque-para-bote', NULL, NULL, 7, 1, 1),
(381, 45, 'Semirremolque', 'semirremolque', NULL, NULL, 8, 1, 1),
(382, 45, 'Remolque utilitario', 'remolque-utilitario', NULL, NULL, 2, 1, 1),
(383, 66, 'Objetivo', 'objectivo', NULL, NULL, 1, 1, 1),
(384, 66, 'Cámara réflex digital', 'Camara-reflex-digital', NULL, 'B', 2, 1, 1),
(385, 66, 'Accesorio luz', 'accessorio-luz', NULL, NULL, 3, 1, 1),
(386, 66, 'Soporte/Trípode', 'soporte-tripode', NULL, NULL, 4, 1, 1),
(387, 66, 'Cámara de cine', 'camara-de-cine', NULL, 'B', 5, 1, 1),
(388, 66, 'Filtro fotográfico', 'filtro-fotografico', NULL, NULL, 6, 1, 1),
(389, 66, 'Cámara compacta digital', 'compacta-digital', NULL, 'B', 7, 1, 1),
(390, 66, 'Cámara instantánea', 'camara-instantanea', NULL, 'B', 8, 1, 1),
(391, 66, 'Fuente de alimentación', 'Fuente-de-alimentacion', NULL, NULL, 9, 1, 1),
(392, 66, 'Material de iluminación', 'material-de-iluminacion', 'Reflector', NULL, 10, 1, 1),
(393, 66, 'Tarjeta de memoria', 'tarjeta-de-memoria', NULL, NULL, 11, 1, 0),
(394, 66, 'Visor', 'visor', NULL, NULL, 12, 1, 0),
(395, 66, 'Medida', 'medida', NULL, NULL, 13, 1, 0),
(396, 66, 'Motor', 'motor', NULL, NULL, 14, 1, 0),
(397, 66, 'Prismáticos, Catalejo, Telescopio', 'prismaticos-catalejo-telescopio', NULL, NULL, 15, 1, 0),
(398, 66, 'Caja y caso', 'caja-y-caso', NULL, NULL, 16, 1, 0),
(399, 66, 'Otro', 'otro', NULL, NULL, 17, 1, 0),
(401, 65, 'Portatil', 'portatil', NULL, 'B', 1, 1, 1),
(402, 65, 'Impresora', 'impresora', NULL, 'B', 2, 1, 1),
(403, 65, 'Router y Conmutador', 'router-conmutador', NULL, NULL, 3, 1, 1),
(404, 65, 'Computadora', 'computadora', NULL, 'B', 4, 1, 1),
(405, 65, 'Monitor', 'monitor', NULL, NULL, 5, 1, 1),
(406, 65, 'Escáner', 'escaner', NULL, 'B', 6, 1, 1),
(407, 65, 'Disco duro externo', 'disco-duro-externo', NULL, NULL, 7, 1, 1),
(408, 65, 'Grabadora y Reproductor', 'grabadora-y-reproductor', 'Unidad de disco óptico', NULL, 8, 1, 1),
(409, 65, 'Lápiz USB', 'lapiz-usb', 'Memoria USB, lápiz de memoria, memoria externa, pen drive o pendrive', NULL, 9, 1, 1),
(410, 65, 'Fotocopia', 'fotocopia', NULL, NULL, 10, 1, 1),
(411, 65, 'Ratón', 'raton', NULL, NULL, 11, 1, 0),
(412, 65, 'Cámara web', 'camara-web', NULL, NULL, 12, 1, 0),
(413, 65, 'Módem', 'modem', NULL, NULL, 13, 1, 0),
(414, 65, 'Teclado', 'teclado', NULL, NULL, 14, 1, 0),
(415, 65, 'Auricular', 'auricular', NULL, NULL, 15, 1, 0),
(416, 65, 'Antena', 'antena', NULL, NULL, 16, 1, 0),
(417, 65, 'Micrófono', 'microfono', NULL, NULL, 17, 1, 0),
(418, 65, 'Otro', 'otro', NULL, NULL, 18, 1, 0),
(419, 68, 'Videocámara', 'videocamara', 'Cámara de vídeo', 'B', 1, 1, 1),
(420, 68, 'Cámara HD', 'camara', NULL, 'B', 2, 1, 1),
(421, 68, 'Proyector de vídeo', 'proyector-de-video', NULL, 'B', 3, 1, 1),
(422, 68, 'Vídeo digital', 'video-digital', NULL, 'B', 5, 1, 1),
(423, 68, 'Televisión', 'television', NULL, 'B', 6, 1, 1),
(424, 68, 'Monitor', 'monitor', NULL, NULL, 7, 1, 1),
(425, 68, 'Reproductor de DVD', 'reproductor-de-dvd', NULL, NULL, 8, 1, 1),
(426, 68, 'Pantalla de proyección', 'pantalla-de-proyyeccion', NULL, NULL, 9, 1, 1),
(427, 68, 'Accesorios', 'accesorios', NULL, NULL, 10, 1, 1),
(428, 68, 'Conector eléctrico', 'conector-electrico', NULL, NULL, 11, 1, 0),
(429, 68, 'Videograbadora', 'videograbadora', NULL, 'B', 12, 1, 0),
(430, 68, 'Cámara de acción', 'camara-de-accion', NULL, 'B', 13, 1, 0),
(431, 68, 'Grabador de vídeo digital', 'grabador-de-video-digital', NULL, 'B', 14, 1, 0),
(432, 68, 'Teatro En Casa', 'teatro-en-casa', 'Teatro En Casa / Cine hogareño', NULL, 15, 1, 0),
(433, 68, 'Televisión satelital', 'television-satelital', NULL, NULL, 16, 1, 0),
(434, 68, 'Cámara de cine', 'camara-de-cine', NULL, 'B', 17, 1, 0),
(435, 68, 'Otro', 'otro', NULL, NULL, 18, 1, 0),
(436, 63, 'Audio digital', 'audio-digital', NULL, 'B', 1, 1, 1),
(437, 63, 'Audio analógica', 'audio-analogica', NULL, 'B', 2, 1, 1),
(438, 63, 'Auricular', 'auricular', NULL, 'B', 3, 1, 1),
(439, 63, 'Sistema De Sonido', 'sistem-de-sonido', 'Microcomponente', 'B', 4, 1, 1),
(440, 63, 'Radio Reproductor', 'radio-reproductor', NULL, 'B', 5, 1, 1),
(441, 63, 'Lector de CD', 'lectro-de-cd', NULL, 'B', 6, 1, 1),
(442, 63, 'Reproductor mp3/mp4', 'reproductor-mp3-mp4', NULL, 'B', 7, 1, 1),
(443, 63, 'Dictáfono', 'dictafono', NULL, 'B', 8, 1, 1),
(444, 63, 'Otro', 'otro', NULL, NULL, 9, 1, 1),
(447, 67, 'Celulare', 'celulare', NULL, 'B', 1, 1, 1),
(448, 67, 'Tableta', 'tableta', NULL, 'B', 2, 1, 1),
(449, 67, 'PDA', 'pda', NULL, 'B', 3, 1, 1),
(450, 67, 'Batería externa', 'bateria-externa', NULL, NULL, 4, 1, 1),
(451, 67, 'Accesorios', 'accesorios', NULL, NULL, 5, 1, 1),
(452, 67, 'Teléfono satelital', 'telefono-satelital', NULL, 'B', 6, 1, 1),
(453, 67, 'Walkie-talkie', 'walkie-talkie', NULL, 'B', 7, 1, 1),
(454, 67, 'Fax', 'fax', NULL, 'B', 8, 1, 1),
(455, 67, 'Teléfono fijo', 'telefono-fijo', NULL, 'B', 9, 1, 1),
(456, 64, 'PC', 'pc', NULL, NULL, 4, 1, 1),
(457, 64, 'Realidad virtual\r\n', 'realidad-virtual', NULL, NULL, 5, 1, 1),
(458, 64, 'Sega', 'sega', NULL, NULL, 6, 1, 1),
(459, 64, 'Accesorios', 'accesorios', NULL, NULL, 7, 1, 1),
(460, 64, 'Otro', 'otro', NULL, NULL, 8, 1, 1),
(461, 97, 'GPS carro', 'gps-carro', NULL, NULL, 1, 1, 1),
(462, 97, 'GPS montaña', 'gps-montana', NULL, NULL, 2, 1, 1),
(463, 97, 'GPS moto y bicileta', 'gps-moto-bicileta', NULL, NULL, 3, 1, 1),
(464, 97, 'Accesorios', 'accesorios', NULL, NULL, 4, 1, 1),
(465, 68, 'Dron', 'dron', NULL, 'B', 4, 1, 1),
(466, 72, 'Esquí', 'esqui', NULL, NULL, 1, 1, 1),
(467, 72, 'Snowboard', 'snowboard', '', NULL, 2, 1, 1),
(468, 72, 'Deportes acuáticos', 'deportes-acuaticos', NULL, NULL, 6, 1, 1),
(469, 72, 'Patín', 'patin', NULL, NULL, 4, 1, 1),
(470, 72, 'Surf', 'surf', NULL, NULL, 5, 1, 1),
(471, 72, 'Monopatín', 'monopatin', 'Skateboard', NULL, 6, 1, 1),
(472, 72, 'Kitesurf', 'kitesurf', NULL, NULL, 7, 1, 1),
(473, 72, 'Surf a vela', 'surf-a-vela', 'Windsurf', NULL, 8, 1, 1),
(474, 72, 'Wakeboard', 'wakeboard', 'Esquí acuático sobre tabla', NULL, 9, 1, 1),
(475, 72, 'Patinaje sobre hielo', 'patinaje-sobre-hielo', NULL, NULL, 10, 1, 1),
(476, 72, 'Otro', 'otro', NULL, NULL, 11, 1, 0),
(477, 74, 'Fitness', 'fitness', NULL, NULL, 1, 1, 1),
(478, 74, 'Gym', 'gym', 'Entrenamiento con pesas/cargas', NULL, 2, 1, 1),
(479, 74, 'Tenis', 'tenis', NULL, NULL, 4, 1, 1),
(480, 74, 'Deportes de motor', 'deportes-de-motor', NULL, NULL, 5, 1, 1),
(481, 74, 'Golf', 'golf', NULL, NULL, 6, 1, 1),
(482, 74, 'Bicicleta estática', 'bicicleta-estatica', 'bicicleta fija', NULL, 7, 1, 1),
(483, 74, 'Buceo', 'buceo', 'Submarinismo, escafandrismo', NULL, 8, 1, 1),
(484, 74, 'Bádminton', 'badminton', NULL, NULL, 9, 1, 1),
(486, 74, 'Raqueta', 'raqueta', NULL, NULL, 10, 1, 0),
(487, 74, 'Baile', 'baile', NULL, NULL, 11, 1, 0),
(488, 74, 'Deporte de combate', 'deporte-de-combate', NULL, NULL, 12, 1, 0),
(489, 74, 'Otro', 'otro', NULL, NULL, 13, 1, 0),
(490, 75, 'Bicicleta de montaña', 'bicicleta-de-montana', NULL, NULL, 1, 1, 1),
(491, 75, 'Bicicleta doméstica', 'bicicleta-domestica', 'Bicicleta urbana', NULL, 2, 1, 1),
(492, 75, 'Bicicleta de carreras', 'bicicleta-de-carreras', NULL, NULL, 3, 1, 1),
(493, 75, 'Bicicleta de turismo', 'bicicleta-de-turismo', NULL, NULL, 4, 1, 1),
(494, 75, 'Accesorios', 'accesorios', NULL, NULL, 5, 1, 1),
(495, 75, 'Bicicleta eléctrica', 'bicicleta-electrica', NULL, NULL, 6, 1, 1),
(496, 75, 'Tándem', 'tandem', NULL, NULL, 7, 1, 1),
(497, 75, 'BMX', 'bmx', NULL, NULL, 8, 1, 1),
(498, 75, 'Otro', 'otro', NULL, NULL, 9, 1, 1),
(500, 70, 'Equipamiento  de senderismo', 'equipamiento-de-senderismo', NULL, NULL, 1, 1, 1),
(501, 70, 'Carpa', 'carpa', NULL, NULL, 2, 1, 1),
(502, 70, 'Equipamiento de montaña', 'equipamiento-de-montana', NULL, NULL, 3, 1, 1),
(503, 70, 'Crampónes & zapatos', 'crampones-y-zapatos', NULL, NULL, 4, 1, 1),
(504, 70, 'Otro', 'otro', NULL, NULL, 5, 1, 1),
(505, 69, 'Lancha motora', 'lancha-motora', NULL, 'C', 1, 1, 1),
(506, 69, 'Piragüismo', 'piraguismo', 'Canoaje, canotaje o canoa kayak', 'B', 2, 1, 1),
(507, 69, 'Velero', 'velero', 'Embarcación de vela', 'C', 3, 1, 1),
(508, 69, 'Moto acuática', 'moto-acuatica', 'Jet ski, moto de agua', 'C', 4, 1, 1),
(509, 69, 'Lancha neumática/semirrígida', 'lancha-neumatica-semirrigida', 'Lancha neumática, bote inflable, bote neumático, gomón, Zodiac, Embarcación semirrígida, lancha semirrígida', 'C', 5, 1, 1),
(510, 69, 'Barca', 'barca', NULL, 'C', 6, 1, 1),
(511, 69, 'Lancha eléctrica', 'lancha-electrica', NULL, 'C', 7, 1, 1),
(512, 69, 'Hidropedal', 'hidropedal', NULL, 'C', 8, 1, 1),
(513, 69, 'Otro', 'otro', NULL, 'C', 9, 1, 1),
(514, 73, 'Barranquismo', 'barranquismo', NULL, NULL, 1, 1, 1),
(515, 73, 'Escalada', 'escalada', NULL, NULL, 2, 1, 1),
(516, 73, 'Cuatrimoto', 'cuatrimoto', NULL, NULL, 3, 1, 1),
(517, 73, 'Buggy', 'buggy', NULL, NULL, 4, 1, 1),
(518, 73, 'Parapente', 'parapente', NULL, NULL, 5, 1, 1),
(519, 73, 'Otro', 'otro', NULL, NULL, 6, 1, 1),
(522, 76, 'Fútbol', 'futbol', NULL, NULL, 1, 1, 1),
(523, 76, 'Voleibol', 'voleibol', NULL, NULL, 2, 1, 1),
(524, 76, 'Baloncesto', 'baloncesto', 'basquetbol, básquetbol, básquet', NULL, 3, 1, 1),
(525, 76, 'Airsoft', 'airsoft', NULL, NULL, 4, 1, 1),
(526, 76, 'Paintball', 'paintball', NULL, NULL, 5, 1, 1),
(527, 76, 'Otro', 'otro', NULL, NULL, 6, 1, 1),
(528, 74, 'Equitación', 'equitacion', NULL, NULL, 3, 1, 1),
(529, 71, 'Equipamiento de pesca', 'equipamiento-de-pesca', NULL, NULL, 1, 1, 1),
(530, 71, 'Equipamiento de caza', 'equipamiento-de-caza', NULL, NULL, 2, 1, 1),
(531, 71, 'Caña de pesca', 'cana-de-pesca', NULL, NULL, 3, 1, 1),
(532, 71, 'Otro', 'otro', NULL, NULL, 4, 1, 1),
(533, 81, 'Guitarra y bajo', 'guitarra-y-bajo', NULL, NULL, 1, 1, 1),
(534, 81, 'Piano y teclado', 'piano-y-teclado', NULL, NULL, 2, 1, 1),
(535, 81, 'Batería y percusión', 'bateria-y-percusion', NULL, NULL, 3, 1, 1),
(536, 81, 'Instrumento de viento', 'instrumento-de-viento', NULL, NULL, 4, 1, 1),
(537, 81, 'Instrumento de cuerda', 'instrumento-de-cuerda', 'Cordófonos ', NULL, 5, 1, 1),
(538, 81, 'Instrumento de viento metal', 'instrumento-de-viento-metal', NULL, NULL, 6, 1, 1),
(539, 81, 'Acordeón y armónica', 'acordeon-y-armonica', NULL, NULL, 7, 1, 1),
(540, 81, 'Partitura - método', 'partitura-metodo', NULL, NULL, 8, 1, 1),
(541, 81, 'Otro', 'otro', NULL, NULL, 9, 1, 1),
(542, 78, 'Juego de mesa', 'juego-de-mesa', NULL, NULL, 1, 1, 1),
(543, 78, 'Juego al aire libre', 'juego-al-aire-libre', NULL, NULL, 2, 1, 1),
(544, 78, 'Juego electrónico', 'juego-electronico', NULL, NULL, 3, 1, 1),
(545, 78, 'Casino y Póquer', 'casino-y-poquer', 'Póker', NULL, 4, 1, 1),
(546, 78, 'Billar', 'billar', NULL, NULL, 5, 1, 1),
(547, 78, 'Fútbol de mesa', 'futbol-de-mesa', 'Futbolito, metegol, fulbito, futmesa, taca-taca, fulbatin, tacatocó, futillo, fulbacho x, tiragol o fulbote', NULL, 6, 1, 1),
(548, 78, 'Arcade', 'arcade', NULL, NULL, 7, 1, 1),
(549, 78, 'Dardos, Tiro con arco', 'dardos-tiro-con-arco', NULL, NULL, 8, 1, 1),
(550, 78, 'Juegos para niños', 'juegos-para-ninos', NULL, NULL, 9, 1, 1),
(551, 78, 'Dron', 'dron', NULL, NULL, 10, 1, 1),
(552, 78, 'Rompecabezas', 'rompecabezas', NULL, NULL, 11, 1, 0),
(553, 78, 'Otro', 'Otro', NULL, NULL, 12, 1, 0),
(558, 83, 'Carpa', 'Carpa', NULL, 'A', 1, 1, 1),
(559, 83, 'Caravana', 'caravana', NULL, 'C', 2, 1, 1),
(560, 83, 'Mobile home', 'mobile-home', 'caravanas estáticas', 'C', 3, 1, 1),
(561, 83, 'Bungaló y Chalé', 'bungalo-y-chale', NULL, 'C', 4, 1, 1),
(564, 84, 'Bed and breakfast', 'bed-and-breakfast', 'habitación de huéspedes', 'C', 1, 1, 1),
(565, 84, 'Posada', 'posada', NULL, 'C', 2, 1, 1),
(566, 84, 'Albergue juvenil', 'albergue-juvenil', NULL, 'C', 3, 1, 1),
(567, 84, 'Otro', 'otro', NULL, 'C', 4, 1, 1),
(568, 85, '1 a 3 habitaciones', '1-a-3-habitaciones', NULL, NULL, 1, 1, 1),
(569, 85, '4 a 5 habitaciones', '4-a-5-habitaciones', NULL, NULL, 2, 1, 1),
(570, 85, '+ de 5 habitaciones', 'mas-de-5-habitaciones', NULL, NULL, 3, 1, 1),
(572, 86, 'Estudio', 'Estudio', NULL, NULL, 1, 1, 1),
(573, 86, '2 a 3 cuartos', '2-a-3-cuartos', NULL, NULL, 2, 1, 1),
(574, 86, '4 a 5 cuartos', '4-a-5-cuartos', NULL, NULL, 3, 1, 1),
(575, 86, '+ de 5 cuartos', 'mas-de-5-cuartos', NULL, NULL, 4, 1, 1),
(576, 87, 'Integral', 'integral', NULL, NULL, 1, 1, 1),
(577, 87, 'Perfilada', 'perfilada', NULL, NULL, 2, 1, 1),
(578, 87, 'Capuchina', 'capuchina', NULL, NULL, 3, 1, 1),
(579, 87, 'Cámper', 'camper', NULL, NULL, 4, 1, 1),
(580, 89, 'Robot de cocina', 'robot-de-cocina', 'Procesador de alimentos, multiprocesadora simplemente procesadora o máquina de cocina toda-en-un', NULL, 1, 1, 1),
(581, 89, 'Cafetera', 'cafetera', NULL, NULL, 2, 1, 1),
(582, 89, 'Cocina', 'cocina', 'Cocina de inducción, Cocina eléctrica,Cocina vitrocerámica', NULL, 3, 1, 1),
(583, 89, 'Máquina de coser/punto', 'maquina-de-coser-punto', NULL, NULL, 4, 1, 1),
(584, 89, 'Lavadora', 'lavadora', 'Lavarropas', NULL, 5, 1, 1),
(585, 89, 'Máquina de waffles', 'maquina-de-waffles', NULL, NULL, 6, 1, 1),
(586, 89, 'Máquina de arepas', 'maquina-de-arepas', NULL, NULL, 7, 1, 1),
(587, 89, 'Limpiadora de vapor', 'limpiador-de-vapor', NULL, NULL, 8, 1, 1),
(588, 89, 'Tostadora', 'tostadora', NULL, NULL, 9, 1, 1),
(589, 89, 'Planchado', 'planchado', NULL, NULL, 10, 1, 1),
(590, 89, 'Aspiradora', 'aspiradora', NULL, NULL, 11, 1, 0),
(591, 83, 'Lugar para acampar', 'lugar-para-acampar', NULL, 'C', 5, 1, 1),
(592, 89, 'Lavavajillas', 'lavavajillas', 'Lavaplatos', NULL, 12, 1, 0),
(593, 89, 'Olla vaporera', 'olla-vaporera', NULL, NULL, 13, 1, 0),
(594, 89, 'Licuadora', 'licuadora', NULL, NULL, 14, 1, 0),
(595, 89, 'Exprimidor', 'exprimidor', 'Juguera ', NULL, 15, 1, 0),
(596, 89, 'Centrifugadora', 'centrifugadora', NULL, NULL, 16, 1, 0),
(597, 89, 'Otro', 'Otro', NULL, NULL, 17, 1, 0),
(598, 90, 'Coche de niño', 'coche-de-nino', 'carricoche, cochecito, carriola', NULL, 1, 1, 1),
(599, 90, 'Cama bébé', 'cama-bebe', NULL, NULL, 2, 1, 1),
(600, 90, 'Sillita de coche', 'sillita-de-coche', 'sistema de retención infantil,1 asientos de seguridad, asiento infantil o silla infantil', NULL, 3, 1, 1),
(601, 90, 'Portabebé', 'portabebe', NULL, NULL, 4, 1, 1),
(602, 90, 'Baño de bébé', 'bano-de-bebe', NULL, NULL, 5, 1, 1),
(603, 90, 'Juego para bébé', 'juego-para-bebe', NULL, NULL, 6, 1, 1),
(604, 90, 'Biberón', 'biberon', NULL, NULL, 7, 1, 1),
(605, 90, 'Tumbona bébé', 'tumbona-bebe', 'Reposera ', NULL, 8, 1, 1),
(606, 90, 'Trona', 'trona', NULL, NULL, 9, 1, 1),
(607, 90, 'Accesorios', 'accesorios', NULL, NULL, 10, 1, 1),
(608, 90, 'Silla elevadora y de mesa', 'silla-elevadora', NULL, NULL, 11, 1, 0),
(609, 90, 'Cocina bébés', 'cocina-bebes', NULL, NULL, 12, 1, 0),
(610, 90, 'Columpio, móvil, andador', 'columpio-movil-andador', NULL, NULL, 13, 1, 0),
(611, 90, 'Corralito', 'corralito', NULL, NULL, 14, 1, 0),
(612, 90, 'Capazo de bébé', 'capazo-de-bebe', NULL, NULL, 15, 1, 0),
(613, 90, 'Ropa de maternidad', 'ropa-de-maternidad', NULL, NULL, 16, 1, 0),
(614, 90, 'Monitor de bebé y seguridad', 'monitor-de-bebe-y-seguridad', NULL, NULL, 17, 1, 0),
(615, 90, 'Cuna', 'cuna', NULL, NULL, 18, 1, 0),
(616, 90, 'Sacaleches', 'sacaleches', NULL, NULL, 19, 1, 0),
(617, 90, 'Otro', 'otro', NULL, NULL, 20, 1, 0),
(618, 88, 'Silla y mesa', 'silla-y-mesa', NULL, NULL, 2, 1, 1),
(619, 88, 'Lámpara', 'lampara', NULL, NULL, 3, 1, 1),
(620, 88, 'Mobiliario', 'mobiliario', NULL, NULL, 1, 1, 1),
(621, 88, 'Pintura', 'pintura', NULL, NULL, 4, 1, 1),
(622, 88, 'Escultura', 'escultura', NULL, NULL, 5, 1, 1),
(623, 88, 'Decoración', 'decoracion', NULL, NULL, 6, 1, 1),
(624, 88, 'Cama', 'cama', NULL, NULL, 7, 1, 1),
(625, 88, 'Otro', 'otro', NULL, NULL, 8, 1, 1),
(626, 91, 'Depilador', 'depilador', NULL, NULL, 1, 1, 1),
(627, 91, 'Plancha', 'plancha', 'Alisadora de pelo', NULL, 2, 1, 1),
(628, 91, 'Masaje', 'masaje', NULL, NULL, 3, 1, 1),
(629, 91, 'Secador de pelo', 'secador-de-pelo', NULL, NULL, 4, 1, 1),
(630, 91, 'Tenacillas', 'tenacillas', 'Para razar', NULL, 5, 1, 1),
(631, 91, 'Balanza/báscula', 'balanza-bascula', NULL, NULL, 6, 1, 1),
(632, 91, 'Yacuzzi ', 'yacuzzi', NULL, NULL, 7, 1, 1),
(633, 91, 'maquinilla de corte', 'manquinilla-de-corte', 'Maquinilla de cortar pelo, maquinilla de cortar cabello', NULL, 8, 1, 1),
(634, 91, 'Otro', 'otro', NULL, NULL, 9, 1, 1),
(641, 94, 'Vestido', 'vestido', NULL, NULL, 1, 1, 1),
(643, 94, 'Zapatos', 'zapatos', NULL, NULL, 3, 1, 1),
(644, 94, 'Chaqueta', 'chaqueta', NULL, NULL, 4, 1, 1),
(645, 94, 'Pantalón, short, falda', 'pantalon-short-falda', NULL, NULL, 5, 1, 1),
(646, 94, 'Camiseta, blusa, top', 'camiseta-blusa-top', NULL, NULL, 6, 1, 1),
(648, 94, 'Deporte', 'deporte', NULL, NULL, 7, 1, 1),
(649, 94, 'Otro', 'otro', NULL, NULL, 8, 1, 1),
(651, 93, 'Camiseta, camisa, sudadera, suéter', 'camiseta-camisa-sudadera-sueter', NULL, NULL, 1, 1, 1),
(652, 93, 'Deporte', 'deporte', NULL, NULL, 2, 1, 1),
(653, 93, 'Zapatos', 'zapatos', NULL, NULL, 3, 1, 1),
(654, 93, 'Traje', 'traje', NULL, NULL, 4, 1, 1),
(655, 93, 'Pantalón, short', 'pantalon-short', NULL, NULL, 5, 1, 1),
(656, 93, 'Otro', 'otro', NULL, NULL, 6, 1, 1),
(657, 95, 'Vestido', 'vestido', NULL, NULL, 1, 1, 1),
(658, 95, 'Accesorios', 'accesorios', NULL, NULL, 2, 1, 1),
(659, 95, 'Matrimonio', 'matrimonio', NULL, NULL, 3, 1, 1),
(660, 95, 'Chaqueta', 'chaqueta', NULL, NULL, 4, 1, 1),
(661, 95, 'Pantalón, short, falda', 'pantalon-short-falda', NULL, NULL, 5, 1, 1),
(662, 95, 'Camiseta, blusa, camisa', 'camiseta-blusa-camisa', NULL, NULL, 6, 1, 1),
(663, 95, 'Zapatos', 'zapatos', NULL, NULL, 7, 1, 1),
(664, 95, 'Otro', 'otro', NULL, NULL, 8, 1, 1),
(665, 92, 'Terapia', 'terapia', NULL, NULL, 1, 1, 1),
(666, 92, 'Férula', 'ferula', NULL, NULL, 2, 1, 1),
(667, 92, 'Muleta', 'mutela', NULL, NULL, 3, 1, 1),
(668, 92, 'Humidificador', 'humidificador', NULL, NULL, 4, 1, 1),
(669, 92, 'Silla de ruedas', 'silla-de-ruedas', NULL, NULL, 5, 1, 1),
(670, 92, 'Andador', 'andador', 'Andador para discapacidad', NULL, 6, 1, 1),
(671, 92, 'Sillón', 'sillon', NULL, NULL, 7, 1, 1),
(672, 92, 'Estimulación', 'estimulacion', NULL, NULL, 8, 1, 1),
(673, 92, 'Mobiliario', 'mobiliario', NULL, NULL, 9, 1, 1),
(674, 92, 'Cama', 'cama', NULL, NULL, 10, 1, 1),
(675, 92, 'Otro', 'otro', NULL, NULL, 11, 1, 0),
(676, 96, 'Bolsa de lujo', 'bolsa-de-lujo', NULL, NULL, 1, 1, 1),
(677, 96, 'Maleta', 'maleta', NULL, NULL, 2, 1, 1),
(678, 96, 'Bolsa de mano', 'bolsa-de-mano', NULL, NULL, 3, 1, 1),
(679, 96, 'Jaula transportadora', 'jaula-transportadora', NULL, NULL, 4, 1, 1),
(680, 96, 'Bolso de viaje', 'bolso-de-viaje', NULL, NULL, 5, 1, 1),
(681, 96, 'Reloj de pulsera hombre', 'reloj-de-pulsera-hombre', NULL, NULL, 6, 1, 1),
(682, 96, 'Sombrero', 'sombrero', NULL, NULL, 7, 1, 1),
(683, 96, 'Pendiente de oreja', 'pendiente-de-oreja', NULL, NULL, 8, 1, 1),
(684, 96, 'Mochila', 'mochila', 'Morral', NULL, 9, 1, 1),
(685, 96, 'Collar', 'collar', NULL, NULL, 10, 1, 1),
(686, 96, 'Gafas de sol', 'gafas-de-sol', NULL, NULL, 11, 1, 0),
(687, 96, 'Foulard, bufanda, estola', 'foulard-bufanda-estola', NULL, NULL, 12, 1, 0),
(688, 96, 'Reloj de pulsera mujer', 'reloj-de-pulsera-mujer', NULL, NULL, 13, 1, 0),
(689, 96, 'Anillo', 'anillo', NULL, NULL, 14, 1, 0),
(690, 96, 'Cinturón', 'cinturon', 'correa, cinto, cincho', NULL, 15, 1, 0),
(691, 96, 'Pulsera', 'pulsera', NULL, NULL, 16, 1, 0),
(692, 96, 'Otro', 'otro', NULL, NULL, 17, 1, 0),
(693, 42, 'Caja de techo y portaequipajes', 'caja-de-techo', 'Baca, parrilla, portaequipaje', 'B', 1, 1, 1),
(694, 42, 'Herramientas carro', 'herramientas-carro', NULL, NULL, 2, 1, 1),
(695, 42, 'Portabicicletas', 'portabicicletas', 'Soporte para bicicletas', 'B', 3, 1, 1),
(696, 42, 'Silla carro', 'silla-carro', NULL, NULL, 4, 1, 1),
(697, 42, 'Barras portaequipajes', 'barras-portaequipajes', NULL, NULL, 5, 1, 1),
(698, 42, 'Accesorios de batería', 'accesorios-de-bateria', NULL, NULL, 6, 1, 1),
(699, 42, 'Barra para remolcar', 'barra-para-remolcar', NULL, NULL, 7, 1, 1),
(700, 42, 'Diagnóstico', 'diagnostico', NULL, NULL, 8, 1, 1),
(701, 42, 'Parqueadero', 'parqueadero', 'Estacionamiento', 'C', 9, 1, 1),
(702, 42, 'Vídeo y pantalla', 'video-pantalla', NULL, NULL, 10, 1, 1),
(703, 42, 'Advertencia de radar', 'advertencia-de-radar', NULL, NULL, 11, 1, 0),
(705, 42, 'Portaesquís', 'portaesquis', NULL, NULL, 13, 1, 0),
(706, 42, 'Cadenas para la nieve', 'cadenas-para-la-nieve', NULL, NULL, 12, 1, 0),
(707, 42, 'Otro', 'otro', NULL, NULL, 14, 1, 0),
(708, 47, 'Carro sin permiso', 'carro-sin-permiso', NULL, NULL, 11, 1, 0),
(709, 47, 'Vehículo atípico', 'vehiculo-atipico', NULL, NULL, 12, 1, 0),
(710, 46, 'Furgón', 'furgon', NULL, NULL, 9, 1, 1),
(711, 46, 'Camión chasis', 'camion-chasis', NULL, NULL, 10, 1, 1),
(712, 1, 'Otros', 'otros', NULL, 'C', 7, 1, 1),
(713, 46, 'Ninera', 'ninera', 'Camión ninera', NULL, 11, 1, 0),
(714, 46, 'Tanque', 'tanque', NULL, NULL, 12, 1, 0),
(715, 46, 'Tractocamion', 'tractocamion', NULL, NULL, 13, 1, 0),
(716, 46, 'Otros', 'otros', NULL, NULL, 14, 1, 1),
(717, 712, 'Buseta', 'buseta', NULL, NULL, 2, 1, 1),
(718, 712, 'Autobus', 'autobus', 'Bus & Pullman', NULL, 1, 1, 1),
(719, 712, 'Ambulancia', 'ambulancia', NULL, NULL, 4, 1, 1),
(720, 712, 'Food Truck', 'food-truck', NULL, NULL, 5, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `category_old`
--

CREATE TABLE IF NOT EXISTS `category_old` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text,
  `position` tinyint(2) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `parent_id_2` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Contenu de la table `category_old`
--

INSERT INTO `category_old` (`id`, `parent_id`, `title`, `slug`, `description`, `position`, `active`) VALUES
(1, NULL, 'Fotografía y vídeo', 'fotografia-y-video', NULL, 1, 1),
(2, NULL, 'Electrónica', 'electronica', NULL, 2, 1),
(3, NULL, 'Deportes', 'deportes', '', 3, 1),
(4, NULL, 'Herramientas', 'herramientas', '', 4, 1),
(5, NULL, 'Eventos', 'eventos', '', 5, 1),
(6, NULL, 'Instrumentos musicales', 'instrumentos-musicales', '', 6, 1),
(7, NULL, 'Campers', 'campers', '', 7, 1),
(8, NULL, 'Iluminación', 'iluminacion', '', 8, 1),
(9, NULL, 'Otros', 'otros', '', 9, 1),
(10, 1, 'Cámaras de fotos', 'camaras-de-fotos', '', 1, 1),
(11, 1, ' Cámaras de vídeo y cine', 'camaras-de-video-y-cine', '', 2, 1),
(12, 1, 'Objetivos', 'objetivos', '', 3, 1),
(13, 1, 'Drones', 'drones', '', 4, 1),
(14, 1, 'Cámaras de acción', 'camaras-de-accion', '', 5, 1),
(15, 1, 'Estabilizadores y soportes', 'estabilizadores-y-soportes', '', 6, 1),
(16, 1, 'Iluminación', 'iluminacion', '', 7, 1),
(17, 1, 'Sonido', 'sonido', '', 8, 1),
(18, 1, 'Cámaras instantáneas', 'camaras-instantaneas', '', 9, 1),
(19, 1, '360º', '360', '', 10, 1),
(20, 1, 'Kits', 'kits', '', 11, 1),
(21, 1, 'Estudios fotográficos', 'estudios-fotograficos', '', 12, 1),
(22, 1, 'Otros', 'otros', '', 13, 1),
(23, 2, 'Proyectores', 'proyectores', '', 1, 1),
(24, 2, 'Pantallas de LED y plasma', 'pantallas-de-led-y-plasma', '', 2, 1),
(25, 2, 'Sonido', 'sonido', '', 3, 1),
(26, 2, 'Equipo de DJ', 'equipo-de-dj', '', 4, 1),
(27, 2, 'Efectos especiales', 'efectos-especiales', '', 5, 1),
(28, 2, 'Informática', 'informatica', '', 6, 1),
(29, 2, 'Videojuegos y consolas', 'videojuegos-y-consolas', '', 7, 1),
(30, 2, 'Otros', 'otros', '', 8, 1),
(31, 7, 'Autocaravanas', 'autocaravanas', '', 1, 1),
(32, 7, 'Furgonetas camper', 'furgonetas-camper', '', 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `category` tinyint(2) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`id`, `user_id`, `email`, `name`, `category`, `subject`, `message`, `is_read`, `created_at`, `updated_at`) VALUES
(1, NULL, 'titi@gmail.com', 'Test', 2, 'Truc', 'Voilà', 1, '2019-05-24 10:27:58', '2019-05-24 10:39:36'),
(3, 1, NULL, NULL, 1, 'Hola', 'Pouet', 0, '2019-05-24 10:43:09', '2019-05-24 10:45:28');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `image` varchar(255) NOT NULL,
  `position` int(2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `item_id`, `image`, `position`) VALUES
(13, 1, 'El-costo-de-fabricar-una-Nintendo-Wii.jpg', 0),
(14, 8, '17-4.jpg', 0),
(15, 9, '71dr9-mowwl__sl1500_.jpg', 0),
(16, 9, '511s219u1ql__sl1044_.jpg', 0),
(17, 9, '71hrxl-aiil__sl1500_.jpg', 0),
(18, 10, 'imag3074_1.jpg', 0),
(19, 11, 'silla.png', 1),
(21, 12, 'poupee-gonflable-femme.jpg', 0),
(23, 14, 'playstation3.jpg', 1),
(24, 13, 'ps3_slim_640_ars-thumb-640xauto-7741.jpg', 0),
(25, 15, '41god7kwsol.jpg', 1),
(26, 16, '410333.jpg', 1),
(27, 17, 'FOTO 15.jpg', 1),
(28, 17, 'motosierra-echo-cs-680-espada-50-cm-japon-en-12-cuotas--D_NQ_NP_892820-MLA27376490311_052018-F.jpg', 2),
(29, 18, 'atornillador-de-impacto-vvr-635-mm-1-4-luz-le.jpg', 1),
(30, 19, '6CXN2_AS01.jpg', 1),
(31, 20, 'pistola-de-calor-makita-1500-watts-hg6020-D_NQ_NP_939876-MCO27380415821_052018-F.jpg', 1),
(32, 21, 'rBVaVlv98dqAGTXgAAGyLqjjRPg131.jpg', 1),
(33, 22, '00105310309132____1__640x640.jpg', 1),
(34, 23, 'Finca-lifestyle-casacol-medellin-real-estate-investment-1.jpg', 1),
(35, 24, 'CAT3-1.png', 1),
(36, 25, '20m3Profil.png', 1),
(37, 26, 's20.jpg', 1),
(38, 26, 's22.jpg', 2),
(39, 26, 's24.jpg', 3),
(40, 26, 's31.jpg', 4);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `status` varchar(30) NOT NULL,
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `highlighted` tinyint(1) NOT NULL DEFAULT '0',
  `bump_date` datetime NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `subcategory_id` int(10) unsigned DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `full_address` varchar(500) DEFAULT NULL,
  `short_address` varchar(255) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `neighborhood` varchar(200) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `full_city` varchar(200) DEFAULT NULL,
  `state` varchar(200) DEFAULT NULL,
  `region` varchar(200) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `country_code` varchar(5) DEFAULT NULL,
  `lat` varchar(50) NOT NULL,
  `lng` varchar(50) NOT NULL,
  `on_quote` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pricing_type` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(10,3) DEFAULT NULL,
  `price_weekend` decimal(10,3) DEFAULT NULL,
  `price_week` decimal(10,3) DEFAULT NULL,
  `price_month` decimal(10,3) DEFAULT NULL,
  `deposit_value` decimal(10,3) DEFAULT NULL,
  `price_new` decimal(10,3) DEFAULT NULL,
  `delivery` varchar(255) DEFAULT NULL,
  `condition` tinyint(1) unsigned DEFAULT NULL,
  `nb_rents` int(3) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`category_id`,`subcategory_id`),
  KEY `category` (`category_id`,`subcategory_id`),
  KEY `subcategory` (`subcategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `item`
--

INSERT INTO `item` (`id`, `user_id`, `status`, `locked`, `featured`, `highlighted`, `bump_date`, `title`, `slug`, `category_id`, `subcategory_id`, `subtitle`, `description`, `full_address`, `short_address`, `address`, `neighborhood`, `zipcode`, `city`, `full_city`, `state`, `region`, `country`, `country_code`, `lat`, `lng`, `on_quote`, `pricing_type`, `price`, `price_weekend`, `price_week`, `price_month`, `deposit_value`, `price_new`, `delivery`, `condition`, `nb_rents`, `created_at`, `updated_at`) VALUES
(1, 1, 'active', 0, 0, 0, '2018-03-20 00:00:00', 'Nintendo Wii', 'nintendo-wii', 64, 102, 'Lorem ipsummmm', 'Lorem ipsum', 'Carrera 70 #30a-45, Medellín, Antioquia, Colombie', 'Medellín, 050030, Antioquia', 'Carrera 70 #30a-45', NULL, '050029', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', 'Colombia', 'COL', '6.2326691', '-75.5916881', 0, 1, '50000.000', NULL, NULL, NULL, NULL, NULL, '', NULL, 2, '2018-03-20 00:00:00', '2018-04-04 17:48:57'),
(8, 2, 'active', 0, 0, 0, '2018-03-21 15:47:20', 'Canon EF 17-40mm f/4 L', 'canon-ef-17-40mm-f4-l', 66, 383, '', 'Objetivo Canon EF 17-40mm f/4 L\r\n\r\nObjetivo zoom anglular', 'Carrera 71 #30-15, Medellín, Antioquia, Colombie', 'Medellín, 050030, Antioquia', 'Carrera 71 #30-15', NULL, NULL, 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', NULL, NULL, '6.231740899999999', '-75.59215510000001', 0, 1, '100000.000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2018-03-21 15:47:20', '2018-03-23 12:34:39'),
(9, 1, 'active', 0, 0, 0, '2019-03-21 16:15:08', 'Proyector OPTOMA HD25', 'proyector-optoma-hd25', 68, 421, '', ':) *SE ENTREGA Y SE DEVUELVE EN ANTON MARTIN* :)\r\n\r\n- Sistema de formato de la señal analógica: NTSC, PAL, SECAM\r\n\r\n- Modos de vídeo admitidos: 1080i, 1080p, 480i, 480p, 720p\r\n\r\n- Conector USB: USB B\r\n\r\n- Tipo de interfaz serie: RS-232\r\n\r\n- Versión HDMI: 1.4 a\r\n\r\n- Brillo de proyector: 3200 ANSI Lúmenes\r\n\r\n- Tecnología de proyección: DLP\r\n\r\n- Resolución nativa del proyector: 1080p (1920 x 1080)\r\n\r\n- Potencia de la lámpara: 240 W\r\n\r\n- Longitud focal: 22,37 - 26,73 mm\r\n\r\n- Amplitud de apertura: 2.55 - 2.86', 'Calle 57 #14-23, Bogotá, Bogotá, Colombie', 'San Luis, Bogotá, Bogotá', 'Calle 57 #14-23', 'Bogotá', '110621', 'Bogotá', 'Bogotá, Bogotá, Colombia', NULL, 'Bogotá', NULL, NULL, '4.6441127', '-74.0659329', 0, 1, '60000.000', '140000.000', '280000.000', '400000.000', '300000.000', NULL, 'pickup,meeting', NULL, 0, '2018-03-21 16:15:08', '2018-04-06 21:09:40'),
(10, 1, 'active', 0, 0, 0, '2018-03-21 16:17:54', 'Bicicleta Carretera Battaglin', 'bicicleta-carretera-battaglin', 75, 492, '', 'Bicicleta de carretera de la marca italiana Battaglin. Talla 51 (1.70cm).\r\n\r\n- Componentes alta gama Shimano Dura-ace.\r\n\r\n- Horquilla y vainas traseras de carbono.\r\n\r\nLa bicicleta está en buen estado.', 'Calle 39d #73-106, Medellín, Antioquia, Colombie', 'Laureles, Medellín, Antioquia', 'Calle 39d #73-106', 'Laureles', '050031', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', NULL, NULL, '6.246111', '-75.59287789999996', 0, 1, '80000.000', NULL, NULL, NULL, NULL, NULL, '', NULL, 0, '2018-03-21 16:17:54', '2018-04-06 21:10:24'),
(11, 1, 'active', 0, 0, 0, '2018-03-27 16:58:19', 'Silla de ruedas', 'silla-de-ruedas', 92, 669, 'Lorem ipsum', 'Silla de ruedas marca Sunrise Medical\r\n\r\nCómoda, muy fácil de transportar. Con tapicería de nylon, reposapiés a 70º desmontables y giratorios, reposabrazos abatibles hacia atrás y desmontables y distancia entre ruedas modificable para alargar o acortar la longitud de la silla, y 3 posibles alturas de asiento al suelo.\r\n\r\nPreguntar precio para alquileres con duración.', 'Prado, Medellín, Antioquia, Colombie', 'Prado, Medellín, Antioquia', 'Prado', 'Prado', '050012', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', NULL, NULL, '6.259861199999999', '-75.5611485', 0, 1, '12000.000', NULL, NULL, NULL, NULL, NULL, 'pickup', NULL, 0, '2018-03-27 16:58:19', '2018-04-06 23:39:30'),
(12, 1, 'active', 0, 0, 0, '2018-04-02 19:01:42', 'Poupée gonflable', 'poupee-gonflable', 78, NULL, '', 'Bueno sexo', 'Rosales, Medellín, Antioquia, Colombie', 'Rosales, Medellín, Antioquia', 'Rosales, Colombie', 'Rosales', '050030', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', '', '', '6.2348523', '-75.59025409999998', 0, 1, '60000.000', NULL, NULL, NULL, NULL, NULL, 'send,pickup,flexible,meeting', NULL, 0, '2018-04-02 19:01:42', '2019-05-09 17:20:07'),
(13, 2, 'active', 0, 0, 0, '2018-04-04 17:12:21', 'PlayStation 3 Slim con juegos', 'play-station-3-slim-con-juegos', 64, 103, '', 'Play Station 3 Slim.\r\n\r\nLa tarjeta de red de la consola no funciona muy bien por lo que es necesario enchufarla con cable al router.\r\n\r\nSe incluye un mando y juego Call of Duty Modern Warfare 3', 'Buenos Aires, Medellín, Antioquia, Colombie', 'Medellín, 050030, Antioquia', 'Buenos Aires', 'Buenos Aires', NULL, 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', NULL, NULL, '6.237128999999999', '-75.5560544', 0, 1, '45000.000', '95000.000', '25000.000', NULL, NULL, NULL, 'flexible', NULL, 0, '2018-04-04 17:12:21', '2018-04-04 17:12:21'),
(14, 2, 'active', 0, 0, 0, '2018-04-04 17:16:31', 'PlayStation 3', 'playstation-3', 64, 103, '', 'Alquiler de videoconsola Sony PlayStation 3 Slim, disfruta del mejor entretenimiento.\r\n\r\nIdeal para cualquier evento, bodas, comuniones, fiestas de cumpleaños, torneos gamming. . .\r\n\r\nIncluye 2 mandos inalámbricos DULASHOCK 3\r\n\r\nDisponemos de pantallas, sonido, proyectores...\r\n\r\nTenemos toda clase de equipos audiovisuales para hacer de tu evento un recuerdo inolvidable.', 'Robledo, Medellín, Antioquia, Colombie', 'Medellín, 050030, Antioquia', 'Robledo', 'Robledo', NULL, 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', NULL, NULL, '6.276998799999999', '-75.59680200000003', 0, 1, '60000.000', '120000.000', NULL, NULL, NULL, NULL, 'flexible', NULL, 0, '2018-04-04 17:16:31', '2018-04-04 17:16:31'),
(15, 2, 'active', 0, 0, 0, '2018-04-04 17:26:01', 'XBOX 360 mando y juegos', 'xbox-360-mando-y-juegos', 64, 104, '', 'CONDICIONES GENERALES DE ALBERTO ZARZOSA (RELENDO)\r\n\r\n1º Haces la reserva del día o días que necesitas el producto y Relendo te inmoviliza el dinero. Si el día está libre yo te aceptaré la reserva, por supuesto puedes contactar conmigo. Relendo solo cobra el dinero cuando hayas disfrutado del alquiler y no hace falta dejar fianza.\r\n\r\n2º Se recoge el artículo en LAS TABLAS el día de antes de empezar el alquiler de 19h a 24h y se devuelve al día último del alquiler antes de las 19h. Cuando hagas la reserva te mando la dirección.\r\n\r\nEsto es un poco flexible si nadie alquila el mismo producto en días contiguos. Si hay un conflicto no pactado se debe abonar la tardanza.\r\n\r\nHay conserjería 24 horas por si no se puede coincidir o se necesita recoger o devolver el producto en horas difíciles).\r\n\r\n3º Yo te ayudo, si lo necesitas, a usar los equipos prestados.\r\n\r\n4º Si el alquiler es fuera de Madrid la persona que lo alquila debe abonar los gastos de envío por mensajería urgente tanto de ida como de vuelta. SOLO SE ALQUILA FUERA DE MADRID SI LOS ALQUILERES SON MAYORES A TRES DÍAS.\r\n\r\n5º Además de los precios jugosos de Relendo hay dos promociones especiales en mi perfil: 3x2 y 7x4. esto hace que ahorres mucho en alquileres largos.\r\n\r\nNOTA: Si no puedes venir a buscar el producto puedes pagar para que te lleve el equipo donde quieras o para que te lo lleve un taxi.\r\n\r\nDAÑOS: Si haces un desperfecto a un producto alquilado debes abonar la reparación del mismo. Si no hay solución amistosa Relendo posee un seguro para solucionar los conflictos.\r\n\r\nCONDICIONES DEL ARTÍCULO (si alquilas varios productos se hacen precios especiales)\r\n\r\nXBOX 360\r\n\r\n1 mando\r\n\r\ny los siguientes juegos:\r\n\r\nassassins creed ii\r\n\r\ncall of duty black ops\r\n\r\ncall of duty black ops II\r\n\r\ncall of duty mw3\r\n\r\ngears of war 3\r\n\r\nhalo reach\r\n\r\nred dead redemption', 'Carrera 70 #48-10, Medellín, Antioquia, Colombie', 'Medellín, 050030, Antioquia', 'Carrera 70 #48-10', 'Suramericana', '050031', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', NULL, NULL, '6.2532486', '-75.5878017', 0, 1, '50000.000', '100000.000', '250000.000', '750000.000', '150.000', NULL, 'flexible', NULL, 0, '2018-04-04 17:26:01', '2018-04-04 17:26:01'),
(16, 1, 'active', 0, 0, 0, '2019-05-14 22:32:06', 'Scarificateur Thermique Pro', 'scarificateur-thermique-pro', 54, 184, NULL, 'Loue scarificateur Thermique pro de marque Pubert.\r\nAvec panier de ramassage.', 'Carrera 80 #30-24, Medellín, Antioquia, Colombia', 'Belén, Medellín, Antioquia', 'Av. 80 #30-24', 'Belén', '050030', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', 'Colombia', 'CO', '6.2316731', '-75.60193229999999', 0, 1, '120000.000', NULL, NULL, NULL, '600000.000', '800000.000', 'pickup', 1, 0, '2019-05-14 15:32:06', '2019-05-16 16:45:26'),
(17, 1, 'active', 0, 0, 0, '2019-05-16 22:37:30', 'MOTOSIERRA CS 680 S PROFESIONAL MARCA ECHO', 'motosierra-cs-680-s-profesional-marca-echo', 55, 98, NULL, 'moto sierra marca ECHO de fabricación japonesa para trabajo profesional a gasolina.\r\n\r\n- cadena de 3/8 \r\n- espada de 60 cm ( 24") marca OREGON \r\n- motor de 2 tiempos de 66.8 cm cúbicos\r\n-capacidad deposito de combustible 640 ml', 'Carrera 69 #50-21, Medellín, Antioquia, Colombia', 'Carlos E. Restrepo, Medellín, Antioquia', 'Cra. 69 #50-21', 'Carlos E. Restrepo', '050034', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', 'Colombia', 'CO', '6.258418300000001', '-75.58494359999997', 0, 1, '90000.000', '190000.000', '460000.000', '1000000.000', NULL, NULL, 'pickup,flexible,meeting', 2, 0, '2019-05-14 15:40:40', '2019-05-16 14:58:18'),
(18, 1, 'active', 0, 0, 0, '2019-05-14 22:44:41', 'ATORNILLADOR MAKITA TD0101 IMPACTO VVR', 'atornillador-makita-td0101-impacto-vvr', 55, 133, NULL, 'Permite atornillar incluso sin pre-perforado\r\nCarbones externos accesibles\r\nNingún daño a la muñeca cuando un tornillo es asentado\r\nPermite atornillar incluso sin pre-perforado\r\n\r\nEspecificaciones\r\n\r\n-Potencia de entrada: 230W\r\n-Capacidad: Tornillo: M4 - M8 (5/32 -5/16 pulg) \r\nTornillo estándar: M5 - M14 (3/16 - 9/16 pulg) \r\nTornillo de alta resistencia: M5 - M10 (3/16 - 3/8 pulg) \r\nTornillo grueso (en longitud): 22 - 90mm (7/8 - 3-1/2 pulg)\r\n-Zanco (Hex): 6,35mm (1/4 pulg)\r\n-Impactos por minuto: 0-3.200\r\n-Velocidad sin carga: 0-3.600\r\n-Torque máximo: 100N•m (885in.lbs)\r\n-Doble Aislamiento\r\n-Velocidad Variable\r\n-Reversible\r\n-Peso neto: 1 kg\r\n-Dimensiones: 18.4 cm largo x 6.7 cm ancho x 19 cm alto.', 'Carrera 10 #70-33, Bogotá, Colombia', 'Alameda, Bogotá, Bogotá', 'Cra. 10 #23-70', 'Alameda', '110311', 'Bogotá', 'Bogotá, Bogotá, Colombia', '', 'Bogotá', 'Colombia', 'CO', '4.6104442', '-74.07155610000001', 0, 1, '30000.000', '60000.000', NULL, NULL, '400000.000', '500000.000', 'pickup', 3, 0, '2019-05-14 15:44:41', '2019-05-14 15:44:41'),
(19, 1, 'active', 0, 0, 0, '2019-05-14 22:46:40', 'Taladro Inalambrico Dewalt 12v', 'taladro-inalambrico-dewalt-12v', 55, 99, NULL, 'Nope', 'Carrera 10 #20-15, Manizales, Caldas, Colombia', 'Manizales, Caldas', 'Cra. 10', '', '170002', 'Manizales', 'Manizales, Caldas, Colombia', 'Manizales', 'Caldas', 'Colombia', 'CO', '5.0695324', '-75.48906160000001', 0, 1, '20000.000', '40000.000', '99000.000', '250000.000', '150000.000', '250000.000', 'pickup,meeting', 3, 0, '2019-05-14 15:46:40', '2019-05-14 15:46:40'),
(20, 1, 'active', 0, 0, 0, '2019-05-14 22:49:15', 'PISTOLA CALOR MAKITA 1500W HG6020 6 NIVELES', 'pistola-calor-makita-1500w-hg6020-6-niveles', 57, 168, NULL, 'Pistola de calor con temperatura variable electrónica.\r\nPoderosa pistola de calor con temperatura variable y termopar electrónica para un uso flexible y máxima fiabilidad.\r\nUso para soldadura de plástico, envases, mantenimiento del automóvil, y una amplia gama de aplicaciones, que requieren de temperatura variable.\r\n3 configuraciones de temperatura.\r\n\r\nEspecificaciones\r\n\r\n-Voltaje: 110V 5060 HZ \r\n-Potencia de entrada: 2000 W \r\n-3 posiciones flujo de aire: \r\n150 l/min \r\n300 l/min \r\n500 l/min\r\n\r\n-Ajuste de temperatura: 9 pasos con rueda de mando \r\n-Potencia: 1500W \r\n-Temperatura de aire 50-600 grados\r\n-Flujo de aire 150 – 500 L/min\r\n-Peso: 1 kg\r\n-Dimensiones: 23 cm largo x 8 cm ancho x 27 cm alto.', 'Carrera 5 #15-5, Villavicencio, Meta, Colombia', 'Villavicencio, Meta', 'Carrera 5', '', '500003', 'Villavicencio', 'Villavicencio, Meta, Colombia', 'Villavicencio', 'Meta', 'Colombia', 'CO', '4.135620999999999', '-73.60691400000002', 0, 1, '22000.000', '40000.000', '88000.000', '250000.000', '400000.000', '800000.000', 'flexible', 4, 0, '2019-05-14 15:49:15', '2019-05-14 15:49:15'),
(21, 1, 'active', 0, 0, 1, '2019-05-14 22:52:03', 'Traje de boda hombre', 'traje-de-boda-hombre', 266, 347, NULL, 'traje de boda masculino, 1 sola postura.', 'Calle 30 #70-5, Barranquilla, Atlántico, Colombia', 'Barranquilla, Atlántico', 'Centro Comercial Panorama', '', '', 'Barranquilla', 'Barranquilla, Atlántico, Colombia', '', 'Atlántico', 'Colombia', 'CO', '10.9453007', '-74.78512619999998', 0, 1, '55000.000', '100000.000', '270000.000', '800000.000', '300000.000', '350000.000', 'flexible,meeting', 2, 0, '2019-05-14 15:52:03', '2019-05-14 15:52:03'),
(22, 1, 'active', 1, 0, 0, '2019-05-14 22:54:37', 'Coche para Niño Marca Bugaboo Bee Plus', 'coche-para-nino-marca-bugaboo-bee-plus', 90, NULL, '', 'GANGA - USADO EN EXCELENTE ESTADO\r\nBugaboo Bee Plus Capota de color Azul Eléctrico mas adaptador para moises marca Graco -imagen de referencia -y adiciono capota impermeable.', 'Calle 25 #55-25, Bogotá, Colombia', 'Bogotá, Cundinamarca', 'VÍA BALOTO CAFE INTERNET RICARDO BOGOTA DC', '', '', 'Bogotá', 'Bogotá, Cundinamarca, Colombia', '', 'Cundinamarca', 'Colombia', 'CO', '4.62015', '-74.11870599999997', 0, 1, '60000.000', NULL, NULL, NULL, '800000.000', '1000000.000', 'pickup,meeting', 2, 0, '2019-05-14 15:54:37', '2019-05-16 22:46:32'),
(23, 1, 'active', 0, 1, 0, '2019-05-16 19:39:37', 'Finca en Girardota', 'finca-en-girardota', 85, 570, NULL, 'Fincas para Alquilar en Girardota, Sector Limonar, cuenta con seis habitaciones, camas dobles, sencilla, kiosco, baños, piscina, cancha y más. \r\nUbicada a 30 minutos de Medellín. ', 'Girardota, Antioquia, Colombia', 'Girardota, Antioquia', 'Girardota', '', '', 'Girardota', 'Girardota, Antioquia, Colombia', 'Girardota', 'Antioquia', 'Colombia', 'CO', '6.378322', '-75.44951700000001', 0, 1, '400000.000', '800000.000', '1800000.000', '5500000.000', NULL, NULL, '', NULL, 0, '2019-05-14 15:58:03', '2019-05-14 15:58:03'),
(24, 1, 'active', 0, 0, 0, '2019-05-20 00:54:54', 'Utilitario 3m3', 'utilitario-3m3', 46, 365, NULL, 'Utilitario', 'Calle 42 #70-15, Medellín, Antioquia, Colombia', 'Bolivariana, Medellín, Antioquia', 'Cl. 42 #70-15', 'Bolivariana', '050031', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', 'Colombia', 'CO', '6.2478831', '-75.5890971', 0, 1, '180000.000', NULL, '1000000.000', NULL, '300000.000', NULL, 'pickup', 2, 2, '2019-05-19 17:54:54', '2019-05-19 17:54:54'),
(25, 12, 'active', 0, 0, 0, '2019-05-21 00:04:27', 'Camion 20m3 avec hayon (Ducato)', 'camion-20m3-avec-hayon-ducato', 46, 368, NULL, 'Avec ce modèle, nous vous proposons la location d’un véhicule utilitaire grand volume équipé d’un hayon élévateur et d''un intérieur adapté aux déménagements de type F3 ou petite maison', 'Carrera 80 #30-15, Medellín, Antioquia, Colombia', 'Belén, Medellín, Antioquia', 'Av. 80 #30-15', 'Belén', '050030', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', 'Colombia', 'CO', '6.2314349', '-75.60204390000001', 0, 2, '200000.000', NULL, NULL, NULL, '2000000.000', NULL, 'pickup', 2, 0, '2019-05-20 17:04:27', '2019-05-24 14:51:01'),
(26, 12, 'active', 0, 0, 0, '2019-05-24 21:20:14', 'Limousina', 'limousina', 43, 115, NULL, 'Limousina', 'Parque Lleras, Medellín, Antioquia, Colombia', 'Zona Rosa, Medellín, Antioquia', 'Parque Lleras', 'Zona Rosa', '050021', 'Medellín', 'Medellín, Antioquia, Colombia', 'Medellín', 'Antioquia', 'Colombia', 'CO', '6.2089814', '-75.56795669999997', 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 0, '2019-05-24 14:20:14', '2019-05-24 15:01:39');

-- --------------------------------------------------------

--
-- Structure de la table `item_nb_views`
--

CREATE TABLE IF NOT EXISTS `item_nb_views` (
  `item_id` int(10) unsigned NOT NULL,
  `nb_views` int(6) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `item_nb_views`
--

INSERT INTO `item_nb_views` (`item_id`, `nb_views`) VALUES
(1, 9),
(8, 7),
(9, 312),
(11, 6),
(12, 18),
(13, 78),
(14, 35),
(15, 70),
(16, 10),
(17, 13),
(18, 4),
(19, 1),
(20, 2),
(21, 1),
(22, 8),
(23, 27),
(24, 13),
(25, 18),
(26, 32);

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(30) DEFAULT NULL,
  `author_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `booking_id` int(10) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` varchar(500) NOT NULL,
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`,`user_id`,`booking_id`,`item_id`),
  KEY `user_id` (`user_id`),
  KEY `book_id` (`booking_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id`, `type`, `author_id`, `user_id`, `item_id`, `booking_id`, `subject`, `message`, `is_read`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 2, 8, NULL, 'Question sobre Canon EF 17-40mm f/4 L', 'Lorem ipsum !', 1, '2018-03-29 17:16:34', '2018-03-29 17:16:34'),
(2, NULL, 2, 1, NULL, NULL, 'Re: Question sobre Canon EF 17-40mm f/4 L', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 1, '2018-03-30 10:00:00', '2018-03-30 10:00:00'),
(4, NULL, 1, 2, NULL, NULL, 'Re: Re: Question sobre Canon EF 17-40mm f/4 L', 'Oui oui oui !', 1, '2018-03-30 11:22:45', '2018-03-30 11:22:45'),
(5, NULL, 2, 1, NULL, NULL, 'Pouet', 'Lorem ipsum', 1, '2018-04-02 10:00:00', '2018-04-02 10:00:00'),
(6, NULL, 3, 1, NULL, NULL, 'Hola', 'Como estas ?', 1, '2018-04-02 10:00:00', '2018-04-02 10:00:00'),
(8, NULL, 1, 2, 9, 3, 'Adresse', 'Quelle est votre adresse ?', 1, '2018-04-04 11:35:59', '2018-04-04 11:35:59'),
(9, NULL, 1, 2, 8, NULL, 'Question sobre Canon EF 17-40mm f/4 L', 'Holq bebè:', 1, '2018-04-04 12:26:30', '2018-04-04 12:26:30'),
(13, NULL, 1, 2, NULL, NULL, 'blabladed', 'dededed', 0, '2018-04-05 11:51:11', '2018-04-05 11:51:11'),
(14, NULL, 1, 12, 26, NULL, 'Hola', 'Voilà', 1, '2019-05-24 16:44:08', '2019-05-24 16:44:08'),
(15, NULL, 1, 12, 26, NULL, 'Question sobre Limousina', 'coucou', 1, '2019-05-24 16:44:26', '2019-05-24 16:44:26'),
(16, NULL, 1, 12, NULL, NULL, 'Hello', 'How''s going ?', 1, '2019-05-24 16:44:39', '2019-05-24 16:44:39'),
(17, NULL, 12, 1, NULL, NULL, 'Re: Hello', 'Hey !', 1, '2019-05-24 16:50:24', '2019-05-24 16:50:24'),
(18, NULL, 1, 2, NULL, NULL, 'Re: blabladed', 'Ok', 0, '2019-05-24 17:28:28', '2019-05-24 17:28:28'),
(19, NULL, 2, 1, 24, NULL, 'Question sobre Utilitario 3m3', 'Hola ! \r\n\r\nDeseo saber...', 0, '2019-06-10 10:08:53', '2019-06-10 10:08:53');

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type` varchar(50) NOT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `booking_id` int(10) unsigned DEFAULT NULL,
  `message_id` int(10) unsigned DEFAULT NULL,
  `subscription_id` int(10) unsigned DEFAULT NULL,
  `text` varchar(255) NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `send_email` tinyint(1) NOT NULL DEFAULT '1',
  `email_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `item_id` (`item_id`),
  KEY `message_id` (`message_id`),
  KEY `booking_id` (`booking_id`),
  KEY `subscription_id` (`subscription_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Contenu de la table `notification`
--

INSERT INTO `notification` (`id`, `user_id`, `type`, `item_id`, `booking_id`, `message_id`, `subscription_id`, `text`, `is_read`, `send_email`, `email_sent`, `created_at`) VALUES
(4, 2, 'new_message', NULL, NULL, 13, NULL, '', 0, 1, 0, '2018-05-10 10:00:00'),
(5, 1, 'new_booking', NULL, 7, NULL, NULL, '', 0, 1, 0, '2018-05-10 10:00:00'),
(7, 2, 'new_booking', NULL, 9, NULL, NULL, '', 0, 1, 0, '2018-05-10 10:00:00'),
(9, 2, 'new_booking', NULL, 11, NULL, NULL, '', 0, 1, 0, '2018-05-10 10:00:00'),
(10, 1, 'booking_validated', NULL, 11, NULL, NULL, '', 0, 1, 0, '2019-05-10 10:00:00'),
(11, 2, 'new_booking', NULL, 20, NULL, NULL, '', 0, 1, 0, '2019-05-10 10:00:00'),
(14, 1, 'new_booking', NULL, 26, NULL, NULL, '', 1, 1, 0, '2019-05-10 10:00:00'),
(15, 2, 'booking_cancelled', NULL, 26, NULL, NULL, '', 0, 1, 0, '2019-05-10 10:00:00'),
(16, 2, 'booking_cancelled', NULL, 26, NULL, NULL, '', 0, 1, 0, '2019-05-10 10:00:00'),
(17, 1, 'booking_refused', NULL, 26, NULL, NULL, '', 1, 1, 0, '2019-05-10 10:00:00'),
(18, 1, 'booking_validated', NULL, 26, NULL, NULL, '', 1, 1, 0, '2019-05-10 10:00:00'),
(21, 2, 'new_booking_owner', NULL, 29, NULL, NULL, '', 0, 1, 0, '2019-05-10 10:00:00'),
(22, 12, 'new_message', NULL, NULL, 14, NULL, '', 0, 1, 0, '2019-05-24 16:44:08'),
(23, 12, 'new_message', NULL, NULL, 15, NULL, '', 0, 1, 0, '2019-05-24 16:44:26'),
(24, 12, 'new_message', NULL, NULL, 16, NULL, '', 1, 1, 0, '2019-05-24 16:44:39'),
(25, 1, 'new_message', NULL, NULL, 17, NULL, '', 1, 1, 0, '2019-05-24 16:50:24'),
(26, 2, 'new_message', NULL, NULL, 18, NULL, '', 1, 1, 0, '2019-05-24 17:28:28'),
(30, 1, 'subscription_finished', NULL, NULL, NULL, 6, '', 1, 1, 0, '2019-05-24 22:09:55'),
(31, 1, 'new_message', NULL, NULL, 19, NULL, '', 1, 1, 0, '2019-06-10 10:08:53'),
(32, 1, 'new_booking_owner', NULL, 30, NULL, NULL, '', 0, 1, 0, '2019-06-10 10:25:23'),
(33, 2, 'booking_refused', NULL, 30, NULL, NULL, '', 1, 1, 0, '2019-06-10 10:37:21'),
(34, 1, 'new_booking_owner', NULL, 31, NULL, NULL, '', 0, 1, 0, '2019-06-10 10:45:53'),
(35, 2, 'booking_validated', NULL, 31, NULL, NULL, '', 1, 1, 0, '2019-06-10 10:46:23'),
(36, 1, 'booking_cancelled', NULL, 31, NULL, NULL, '', 0, 1, 0, '2019-06-10 11:27:24'),
(44, 2, 'booking_completed', NULL, 31, NULL, NULL, '', 1, 1, 0, '2019-06-10 12:20:49'),
(45, 1, 'booking_completed_owner', NULL, 31, NULL, NULL, '', 1, 1, 0, '2019-06-10 12:21:21');

-- --------------------------------------------------------

--
-- Structure de la table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` varchar(3) NOT NULL DEFAULT 'COP',
  `vta_rate` decimal(10,2) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`),
  KEY `ref` (`code`),
  KEY `method` (`method`),
  KEY `status` (`status`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `order`
--

INSERT INTO `order` (`id`, `parent_id`, `user_id`, `type`, `code`, `method`, `amount`, `currency`, `vta_rate`, `status`, `date`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'subscription', 'frfndandoi', 'payu', '15000.00', 'COP', '0.20', 'waiting_payment', '2019-05-13 00:00:00', '2019-05-13 15:56:35', '2019-05-13 22:22:35'),
(2, NULL, 1, 'subscription', 'iiPqvWPZXQ7Q3vW', 'payu', '25000.00', 'COP', '0.20', 'paid', '2019-05-15 22:44:38', '2019-05-15 15:44:38', '2019-05-15 17:29:11'),
(3, NULL, 1, 'subscription', 'hnWWHMGkqqV0nyc', 'payu', '1000.00', 'COP', '0.20', 'paid', '2019-05-16 22:34:11', '2019-05-16 15:34:11', '2019-05-16 15:34:11'),
(4, NULL, 1, 'subscription', 'G8Zl-yS8yNYB-mJ', 'payu', '40000.00', 'COP', '0.20', 'paid', '2019-05-17 00:00:28', '2019-05-16 17:00:28', '2019-05-16 17:00:28'),
(5, NULL, 1, 'subscription', 'qdSPC59mro1D4BP', 'payu', '40000.00', 'COP', '0.20', 'paid', '2019-05-17 18:41:58', '2019-05-17 11:41:58', '2019-05-17 11:44:37'),
(6, NULL, 1, 'subscription', 'W9wxdVwglLHiVfP', 'payu', '7500.00', 'COP', '0.20', 'paid', '2019-05-17 18:47:23', '2019-05-17 11:47:23', '2019-05-17 11:47:23'),
(7, NULL, 12, 'subscription', 'U6UJwR0rkTFX6Lh', 'payu', '16100.00', 'COP', '0.20', 'waiting_payment', '2019-05-23 23:52:53', '2019-05-23 16:52:53', '2019-05-23 16:52:53');

-- --------------------------------------------------------

--
-- Structure de la table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `order_id` int(10) unsigned DEFAULT NULL,
  `reference_code` varchar(255) NOT NULL,
  `transaction_id` varchar(36) DEFAULT NULL,
  `state_pol` int(2) DEFAULT NULL,
  `amount` decimal(14,2) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `response_code_pol` varchar(64) DEFAULT NULL,
  `reference_pol` varchar(255) DEFAULT NULL,
  `payment_method_id` varchar(255) DEFAULT NULL,
  `payment_method_type` int(2) DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `response_message_pol` varchar(64) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `payment`
--

INSERT INTO `payment` (`id`, `user_id`, `order_id`, `reference_code`, `transaction_id`, `state_pol`, `amount`, `currency`, `response_code_pol`, `reference_pol`, `payment_method_id`, `payment_method_type`, `transaction_date`, `response_message_pol`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '1234', NULL, NULL, '15000.00', 'COP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-13 15:58:18', NULL),
(3, 1, 2, 'nLyWH7m1Cc', '2ad7c077-7b0a-46a7-8b0d-7ced63341347', 4, '25000.00', 'COP', '1', '845636784', '1', 1, '2019-05-15 00:00:00', 'APPROVED', '2019-05-15 15:44:38', '2019-05-16 12:14:38'),
(4, 1, 3, 'WBtmduhkK_', NULL, NULL, '1000.00', 'COP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 15:34:11', '2019-05-16 15:34:11'),
(5, 1, 4, 'KhI7ImfxpQ', NULL, NULL, '40000.00', 'COP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-16 17:00:28', '2019-05-16 17:00:28'),
(6, 1, 5, '7o7CkattZV', NULL, NULL, '40000.00', 'COP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 11:41:58', '2019-05-17 11:41:58'),
(7, 1, 6, 'D6-819XMbe', NULL, NULL, '7500.00', 'COP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-17 11:47:23', '2019-05-17 11:47:23'),
(8, 12, 7, 'zvEzWswrro', NULL, NULL, '16100.00', 'COP', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-23 16:52:53', '2019-05-23 16:52:53');

-- --------------------------------------------------------

--
-- Structure de la table `report_item`
--

CREATE TABLE IF NOT EXISTS `report_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `reason` int(11) NOT NULL,
  `message` text NOT NULL,
  `is_read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `report_item`
--

INSERT INTO `report_item` (`id`, `item_id`, `user_id`, `email`, `reason`, `message`, `is_read`, `created_at`, `updated_at`) VALUES
(3, 12, 1, NULL, 4, 'Yep', 1, '2019-05-24 23:12:47', '2019-05-24 23:12:56'),
(4, 12, NULL, 'timothep@yahoo.fr', 4, 'Ome !', 1, '2019-05-24 23:13:56', '2019-05-24 23:14:35');

-- --------------------------------------------------------

--
-- Structure de la table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `booking_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `note` tinyint(1) NOT NULL,
  `text` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`,`booking_id`,`user_id`),
  KEY `reservation_id` (`booking_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `review`
--

INSERT INTO `review` (`id`, `author_id`, `booking_id`, `user_id`, `note`, `text`, `created_at`) VALUES
(1, 2, 2, 1, 4, 'Superrrr !\r\nBuen producto', '2018-03-07 10:00:00'),
(2, 1, 2, 2, 5, 'Nice guy', '2018-03-14 11:00:00'),
(4, 1, 5, 2, 5, 'Chico chevere !', '2018-04-05 16:41:34'),
(5, 2, 5, 1, 5, 'Objeto superr ! Bien entretenido', '2018-04-05 16:45:20'),
(6, 1, 4, 2, 5, 'Perfecto ! ', '2019-05-29 12:19:54'),
(8, 2, 31, 1, 5, 'Fue super ! Uyyy\r\nGracias !', '2019-06-10 12:30:51'),
(10, 1, 31, 2, 4, 'Bien.\r\nPersona puntual', '2019-06-10 12:34:44');

-- --------------------------------------------------------

--
-- Structure de la table `stat_search`
--

CREATE TABLE IF NOT EXISTS `stat_search` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(255) NOT NULL,
  `num_searches` int(10) unsigned DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `stat_search`
--

INSERT INTO `stat_search` (`id`, `term`, `num_searches`, `created_at`, `updated_at`) VALUES
(1, 'pistola calor ', 244, '2019-05-22 09:48:12', '2019-06-10 10:04:56'),
(2, 'camion', 3, '2019-05-22 09:48:39', '2019-05-22 15:05:11');

-- --------------------------------------------------------

--
-- Structure de la table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `period` varchar(45) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `subscription`
--

INSERT INTO `subscription` (`id`, `user_id`, `order_id`, `item_id`, `type`, `status`, `code`, `period`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 20, 'featured', 'pending', 'fezfdjezd', '1_month', '2019-05-13 00:00:00', '2019-06-13 00:00:00', '2019-05-13 15:52:18', NULL),
(2, 1, 2, 23, 'bumped', 'active', 'LO0JQZUa6fDbcB4', '1_month', '2019-05-15 15:44:38', '2019-06-15 15:44:38', '2019-05-15 15:44:38', '2019-05-15 17:29:11'),
(3, 1, 3, 17, 'bumped', 'finished', 'moEvZQGvpptdE8n', '1_day', '2019-05-16 15:34:11', '2019-05-16 15:35:11', '2019-05-16 15:34:11', '2019-05-16 15:43:45'),
(4, 1, 4, 23, 'featured', 'active', 'yx8lwadIsccu4-Q', '1_month', '2019-05-17 00:00:28', '2019-06-17 00:00:28', '2019-05-16 17:00:28', '2019-05-16 17:02:37'),
(5, 1, 5, 23, 'featured', 'active', 'oOfb5EJwBeN1e5k', '1_month', '2019-06-17 00:00:28', '2019-07-17 00:00:28', '2019-05-17 11:41:58', '2019-05-17 11:46:27'),
(6, 1, 6, 23, 'highlighted', 'finished', 'WwlNEofIePQ6uCG', '1_week', '2019-05-17 18:54:05', '2019-05-24 18:54:05', '2019-05-17 11:47:23', '2019-05-24 22:09:55'),
(7, 12, 7, 25, 'highlighted', 'pending', 'p22rt3SHF30hBLq', '1_week', '2019-05-23 23:52:53', '2019-05-30 23:52:53', '2019-05-23 16:52:53', '2019-05-23 16:52:53');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  `type` varchar(30) DEFAULT NULL,
  `gender` varchar(10) NOT NULL DEFAULT 'male',
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password_hash` varchar(2555) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `verification_token` varchar(250) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `mobile_phone` varchar(30) DEFAULT NULL,
  `work_phone` varchar(30) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zipcode` varchar(15) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(3) NOT NULL DEFAULT 'COL',
  `lang` varchar(2) NOT NULL DEFAULT 'es',
  `lat` varchar(50) DEFAULT NULL,
  `lng` varchar(50) DEFAULT NULL,
  `wishlist` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `show_email` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `show_phone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `security_email_verified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(100) DEFAULT NULL,
  `isp` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`,`username`,`password_reset_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `status`, `type`, `gender`, `email`, `username`, `firstname`, `lastname`, `password_hash`, `password_reset_token`, `auth_key`, `verification_token`, `image`, `phone`, `mobile_phone`, `work_phone`, `website`, `description`, `address`, `address2`, `zipcode`, `city`, `state`, `country`, `lang`, `lat`, `lng`, `wishlist`, `facebook_id`, `twitter_id`, `google_id`, `show_email`, `show_phone`, `security_email_verified`, `ip`, `isp`, `created_at`, `updated_at`) VALUES
(1, '1', 'user', 'male', 'timothee@umcgroup.fr', 'timmy', 'Timothée', 'Planchais', '$2y$13$Buz8kZ/iqnsEpDa8QXvfZu2JsHzi7C8rEVrSO3eJmiM663IsCOnBG', NULL, 'uylX0jPiwSMDg0LthbOKWJwYknaomNK6', NULL, 'malik3-600x600.jpg', '3016736471', '', '', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \r\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'carrera 70 #30a-45', '', '050030', 'Medellin', 'Antioquia', 'COL', 'es', '6.2326691', '-75.5916881', '9,10', NULL, NULL, NULL, 0, 0, 1, '2130706433', 'Timothee-PC', '2018-03-19 00:00:00', '2019-06-03 11:47:37'),
(2, '1', 'user', 'male', 'timothep@yahoo.fr', 'timtim', 'Timothéee', 'Planchais', '$2y$13$eHkYWoh48OGcMPrEV8Y8zORYYigQ4FTg514xuTjTNgf7GUzuqKBZm', NULL, '1g9CScswZ1JxqtUOf6m87ME8z4C1lFRB', NULL, NULL, '3016736471', NULL, NULL, NULL, NULL, 'carrera 76 #41-41', 'Café Cliché', '050030', 'Medellin', 'Antioquia', 'COL', 'es', '6.2484349', '-75.5935634', NULL, NULL, NULL, NULL, 1, 1, 1, '2130706433', 'Timothee-PC', '2018-03-21 13:59:55', '2018-03-21 13:59:55'),
(3, '1', 'user', 'male', 'timothee.planchais@gmailcom', 'vregergerg', 'vregergerg', 'Oolszodkokdaz', '$2y$13$SrXmSYY40fkvxjBbAuk/ZO.TFiDJYE5NMwV9RzIJXTfQf.9aoRFFS', NULL, '3Teud-O7pJA5Xu6qlBNiGAHGXB7eRkmp', NULL, NULL, '3016736471', NULL, NULL, NULL, NULL, 'Avenida Nutibara #70-181', 'Pimientos Pizza Gourmet', '050031', 'Medellín', 'Antioquia', 'COL', 'es', '6.2404238', '-75.5913414', NULL, NULL, NULL, NULL, 0, 0, 1, NULL, NULL, '2018-03-28 19:52:14', '2018-03-28 19:52:14'),
(4, '1', 'user', 'male', 'timothep78@gmail.com', 'Timothée Planchais', 'Timothéeee', 'Planchais', NULL, NULL, 'kJdqrDihhjMMBA2-FkGwbZdw6xFVTKlW', NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg?sz=300', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'COL', 'es', NULL, NULL, NULL, NULL, NULL, '106493900725605577098', 0, 0, 1, '2130706433', 'Timothee-PC', '2018-04-02 18:20:53', '2018-04-02 18:20:53'),
(5, '1', 'user', 'male', 'timothe1@yahoo.fr', 'timmy78180', 'Timothéeeeee', 'Planchais', NULL, NULL, '-omDVbec9033AjwlHpnq-yNt01hi3HJ8', NULL, 'https://pbs.twimg.com/profile_images/980949325944623109/s4YkZDqX.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'COL', 'fr', NULL, NULL, NULL, NULL, '401710202', NULL, 0, 0, 1, NULL, NULL, '2018-04-02 18:37:08', '2018-04-02 18:37:08'),
(8, '1', 'user', 'male', 'timothee2@umcgroup.fr', 'timothee2', 'Timothéeeeeee', 'Planchais', '$2y$13$SYVNcc6puxh5xalxfJ7pBebcBg1ZR/Isdv/jRiGJyHNhPQWGSdARW', NULL, 'veOZiVRB-QUiTL8GjUnqgcjPJQiR6HOl', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'COL', 'es', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, '2130706433', 'Timothee-PC', '2019-04-23 17:06:52', '2019-04-23 17:06:52'),
(10, '1', 'user', 'male', 'jose@umcgroup.fr', 'jose', 'Jose', 'Truc', '$2y$13$srtDZYEwkKuendZfPxSWouI36XmaOsvZPtv7A/huuxpHImKttPD7y', NULL, 'Kj0iVBm98Nnp4XT_lQtQGH8l1TKE46Ga', 'U6DAobgAimk1Aa_7p3ByZLs8NRNL2a5L_1558365586', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'COL', 'es', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, '2130706433', 'Timothee-PC', '2019-05-20 10:19:46', '2019-05-20 10:23:56'),
(12, '1', 'user_pro', 'male', 'contact@umcgroup.fr', 'umcgroup', 'Alexis', 'Bou', '$2y$13$xE7o.OadvfQdnlylEOojJeuM52Iy/1R1JMwvGcwKcznqdQX3iUJD.', NULL, 'QWeH1dDAhjNGbgtugvf89vflzBdI3z4F', 'm0EXfUVHaavp00Ulmtjmn5-_WNZ42yRc_1558388642', NULL, '0101010101', '', NULL, NULL, '', 'Carrera 80#30-15', '', '050030', 'Medellin', NULL, 'COL', 'es', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2130706433', 'Timothee-PC', '2019-05-20 16:44:02', '2019-05-24 14:37:57');

-- --------------------------------------------------------

--
-- Structure de la table `user_pro`
--

CREATE TABLE IF NOT EXISTS `user_pro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `website` varchar(250) DEFAULT NULL,
  `vat_num` varchar(50) DEFAULT NULL,
  `description` text,
  `nb_items` int(10) DEFAULT NULL,
  `available_days` varchar(250) DEFAULT NULL,
  `available_hours` varchar(250) DEFAULT NULL,
  `logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user_pro`
--

INSERT INTO `user_pro` (`id`, `user_id`, `company_name`, `categories`, `website`, `vat_num`, `description`, `nb_items`, `available_days`, `available_hours`, `logo`) VALUES
(1, 12, 'UMC Group', '46,45,48', 'http://www.google.fr', '1234', 'Test\r\nPouet', 50, '1,2,3,4,5,6', 'Du Lundi au Vendredi de 9h à 17h. Le Samedi de 9h à 13h.', 'alquiya-logo_squarred.png');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `amazon_item`
--
ALTER TABLE `amazon_item`
  ADD CONSTRAINT `amazon_item_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Contraintes pour la table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`);

--
-- Contraintes pour la table `category_old`
--
ALTER TABLE `category_old`
  ADD CONSTRAINT `category_old_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `category_old` (`id`);

--
-- Contraintes pour la table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `item_ibfk_3` FOREIGN KEY (`subcategory_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `item_ibfk_4` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

--
-- Contraintes pour la table `item_nb_views`
--
ALTER TABLE `item_nb_views`
  ADD CONSTRAINT `item_nb_views_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `message_ibfk_3` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `message_ibfk_4` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notification_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notification_ibfk_3` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notification_ibfk_4` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notification_ibfk_5` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `order` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `order_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `report_item`
--
ALTER TABLE `report_item`
  ADD CONSTRAINT `report_item_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_item_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `review_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscription_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `subscription_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_pro`
--
ALTER TABLE `user_pro`
  ADD CONSTRAINT `user_pro_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
