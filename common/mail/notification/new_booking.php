<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $booking Booking */
/* @var $user \common\models\User */
/* @var $item \common\models\Item */

if(empty($item)) {
    $item = $booking->item;
}

$owner = $booking->item->user;

$url = Yii::$app->urlManager->createAbsoluteUrl(['/booking/view', 'id' => $booking->getPrimaryKey()]);

?>

<p>
    Acaba de hacer una resa en Alquiya.com sobre el producto : <i><?= Html::encode($booking->item->title) ?></i>.
</p>

<p>Ahora depende de usted ponerse en contacto con el propietario para validar la disponibilidad del objeto y especificar las condiciones de retiro.</p>

<p><strong>¿Quién es el Arrendador?</strong></p>

<ul>
    <li><?= Html::encode($owner->getFullName()) ?></li>
    <li><a href="mailto:<?= $owner->email ?>"><?= Html::encode($owner->email) ?></a></li>
    <?php if($owner->mobile_phone): ?>
        <li><?= Html::encode($owner->mobile_phone) ?></li>
    <?php endif; ?>
    <?php if($owner->phone): ?>
        <li><?= Html::encode($owner->phone) ?></li>
    <?php endif; ?>
    <li><?= $owner->getOfficialAddress() ?></li>
</ul>

<p><strong>Detailles de la reservación :</strong></p>

<ul>
    <li>Número de reserva : <a href="<?= $url ?>"><?= $booking->getNum() ?></a></li>
    <li>Objeto : <a href="<?= Yii::$app->urlManager->createAbsoluteUrl($booking->getUrl()) ?>"><?= Html::encode($item->title) ?></a></li>
    <li>Fechas : <?= $booking->getFromTo() ?></li>
    <li>Monto a pagar directamente al propietario : <?= Yii::$app->formatter->asCurrency($booking->getTotalPrice()) ?></li>
    <?php if($booking->deposit_value): ?>
        <li>Monto del depósito solicitado : <?= Yii::$app->formatter->asCurrency($booking->deposit_value) ?></li>
    <?php endif; ?>
</ul>

<p>&nbsp;</p>

<p><strong>Alquiler de último minuto?</strong> Póngase en contacto rápidamente por teléfono y correo electrónico con el propietario para asegurarse de que el artículo esté disponible.</p>

<p>
    <strong>Como recordatorio, tiene un período de 3 días (siempre que el alquiler no haya comenzado) para cancelar su reservación.</strong> 
    Una vez que ha transcurrido este período de 3 días, ya no es posible cancelar.
</p>



