<?php

use common\models\Subscription;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $user common\models\User */
/* @var $subscription Subscription */

?>

<p>Su suscripción #<?= $subscription->code ?> por el objeto "<?= $subscription->item->title ?>" se acaba pronto.</p>

<p>
    <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/account/order/create', 'type' => \common\models\Order::TYPE_SUBSCRIPTION, 'itemId' => $subscription->item_id, 'subscriptionType' => $subscription->type]) ?>">
        Para reactivar una nueva suscripción haz clic aqui.
    </a>
</p>

<br/>

<p>Ver todas sus suscripciones :</p>

<p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/account/subscription/index']) ?>"><?= Yii::$app->urlManager->createAbsoluteUrl(['/account/subscription/index']) ?></a></p>



