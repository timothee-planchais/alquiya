<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $booking Booking */
/* @var $user \common\models\User */
/* @var $item \common\models\Item */

?>

<p>
    La reservación <?= $booking->getNum() ?> aún está pendiente para su validación.
</p>

<br/>

<p>Detailles de la reservación :</p>

<p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl($booking->getUrl()) ?>"><?= Html::encode($booking->getTitle()) ?></a></p>

<p>Ahora tiene que validar o rechazar la reservación.</p>


