<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $user common\models\User */

?>

<p>Tienes un nuevo mensaje en AlquiYa !</p>

<br/>

<p>Ver todos sus mensajes :</p>

<p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/account/message/index']) ?>"><?= Yii::$app->urlManager->createAbsoluteUrl(['/account/message/index']) ?></a></p>



