<?php

use common\models\Subscription;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $user common\models\User */
/* @var $subscription Subscription */

?>

<p>Su pago por la suscripción #<?= $subscription->code ?> fue validado.</p>

<p>Su suscripción fue activada.</p>

<br/>

<p>Ver todas sus suscripciones :</p>

<p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/account/subscription/index']) ?>"><?= Yii::$app->urlManager->createAbsoluteUrl(['/account/subscription/index']) ?></a></p>



