<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $booking Booking */

$user = $booking->user;

if(empty($item)) {
    $item = $booking->item;
}

$url = Yii::$app->urlManager->createAbsoluteUrl(['/booking/view', 'id' => $booking->getPrimaryKey()]);

?>

<p><?= $user->getFullName() ?> acaba de hacer una resa en Alquiya.com sobre su producto : <i><?= Html::encode($booking->item->title) ?></i>.</p>

<p><strong>¿Quién es el arrendatario?</strong></p>

<ul>
    <li><?= Html::encode($user->getFullName()) ?></li>
    <li><a href="mailto:<?= $user->email ?>"><?= Html::encode($user->email) ?></a></li>
    <?php if($user->mobile_phone): ?>
        <li><?= Html::encode($user->mobile_phone) ?></li>
    <?php endif; ?>
    <?php if($user->phone): ?>
        <li><?= Html::encode($user->phone) ?></li>
    <?php endif; ?>
    <li><?= $user->getOfficialAddress() ?></li>
</ul>

<p><strong>Detailles de la reservación :</strong></p>

<ul>
    <li>Número de reserva : <a href="<?= $url ?>"><?= $booking->getNum() ?></a></li>
    <li>Objeto : <a href="<?= Yii::$app->urlManager->createAbsoluteUrl($item->getUrl()) ?>"><?= Html::encode($booking->item->title) ?></a></li>
    <li>Fechas : <?= $booking->getFromTo() ?></li>
    <li>Monto a pagar directamente al propietario : <?= Yii::$app->formatter->asCurrency($booking->getTotalPrice()) ?></li>
    <li>Monto del depósito solicitado : <?= Yii::$app->formatter->asCurrency($booking->deposit_value) ?></li>
</ul>

<p><a href="<?= $url ?>">Ahora tiene que validar o rechazar la reservación.</a></p>



