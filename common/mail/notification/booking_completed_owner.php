<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $booking Booking */

$item = $booking->item;

?>

<p>
    La reserva <?= $booking->getNum() ?>  sobre el objeto <i><a href="<?= Yii::$app->urlManager->createAbsoluteUrl($booking->getUrl()) ?>"><?= Html::encode($booking->getTitle()) ?></a></i> termina hoy.
    Esperamos que todo haya ido bien y que el principio de alquilar de un particular te complazca.
</p>

<p>
    Para mejorar la experiencia en el sitio, lo invitamos a evaluar el arrendatario haciendo clic en el siguiente enlace
    o yendo a la pestaña "Mis reservaciónes" de su área de miembros.
</p>

<p><u><a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/booking/review', 'id' => $booking->getPrimaryKey()]) ?>">Evalúo la reservación</a></u></p>

<p>¡Gracias y buenos alquileres en alquiya.com!</p>




