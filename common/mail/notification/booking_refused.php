<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $booking Booking */

?>

<p>Su reservación <?= $booking->getNum() ?> fue rechazada por el Arrendador.</p>

<?php if($booking->refusal_reason): ?>
    <p><b style="font-weight:700;">Motivo de rechazo :</b> <?= nl2br(Html::encode($booking->refusal_reason)) ?></p>
<?php endif; ?>

<p>Ver su reservación :</p>

<p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl($booking->getUrl()) ?>"><?= Html::encode($booking->getTitle()) ?></a></p>



