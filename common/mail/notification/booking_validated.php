<?php

use common\models\Booking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $booking Booking */

?>

<p>Su reservación <?= $booking->getNum() ?> fue validada por el arrendador.</p>

<br/>

<p>Ver su reservación :</p>

<p><a href="<?= Yii::$app->urlManager->createAbsoluteUrl($booking->getUrl()) ?>"><?= Html::encode($booking->getTitle()) ?></a></p>



