<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
    <table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" bgcolor="#eceff1" style="border-collapse:collapse;">
        <tbody>
            <tr>
                <td>
                    <table bgcolor="#ffffff" border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="width:600px;margin:auto;background-color:#ffffff;">
                        <tbody>
                            <!-- HEADER LOGO -->
                            <tr>
                                <td bgcolor="#fff">
                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#fff" dir="ltr">
                                        <tbody>
                                            <!--<tr>
                                                <td height="10" colspan="2"></td>
                                            </tr>-->
                                            <tr>
                                                <td align="center">
                                                    <a href="<?= Yii::$app->params['frontend.url'] ?>" target="_blank" style="display:block;">
                                                        <img src="http://alquiya.com/img/logo.png" style="display:block;margin:0 auto;" width="250" alt="AlquiYa"/>
                                                    </a>
                                                </td>
                                            </tr>
                                            <!--<tr>
                                                <td height="10" colspan="2"></td>
                                            </tr>-->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td height="2" bgcolor="#2D3F50"></td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!-- CONTENT -->
                            <tr>
                                <td>
                                    <table style="width:600px;margin:auto;background-color:#ffffff;">
                                        <tbody>
                                            <tr>
                                                <td width="20" style="width:20px;"></td>
                                                <td style="background:#FFF;font-family:Arial, Helvetica, sans-serif;font-size:14px;line-height:16px;color:#333;">
                                                    <p>Hola,</p>

                                                    <br/>

                                                    <?= $content ?>

                                                    <br/>

                                                    <p>Hasta pronto,</p>

                                                    <p></p>

                                                    <p>El equipo alquiya.com</p>

                                                    <p>&nbsp;</p>
                                                </td>
                                                <td width="20" style="width:20px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="20"></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- FOOTER -->
                    <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="width:600px;margin:auto;">
                        <tbody>
                            <tr>
                                <td>
                                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td height="20"></td>
                                            </tr>
                                            <tr>
                                                <td align="center" style="text-align:center;color:#777777;font-family:Arial, sans-serif;font-size:13px!important;"> 
                                                    Por favor no respondas a este correo.<br>
                                                    Si tienes alguna pregunta, encuentra la información<br>
                                                    en nuestra <a rel="nofollow" target="_blank" href="<?= Yii::$app->params['frontend.url'] ?>/help.html" style="color:#777777;text-decoration:underline;">página de ayuda</a> 
                                                    o <a rel="nofollow" target="_blank" href="<?= Yii::$app->params['frontend.url'] ?>/contact.html" style="color:#777777;text-decoration:underline;">contactános</a>. 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
        
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
