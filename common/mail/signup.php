<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this frontend\components\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/verify-email', 'token' => $user->verification_token]);

?>

<p> Hemos registrado su registro en Oso Astuto !</p>

<br/>
 
<p>Atención, debe validar su dirección de correo electrónico haciendo clic en este enlace :</p>
<p><a href="<?= $verifyLink ?>"><?= $verifyLink ?></a></p>

<br/>

<p>Aquí está la información de inicio de sesión para acceder a su cuenta :</p>

<br/>

<p>Su dirección de inicio de sesión : <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['/site/login']) ?>"><?= Yii::$app->urlManager->createAbsoluteUrl(['/site/login']) ?></a></p>
<p><strong>Tu nombre de usuario es :</strong> <?= $user->email ?></p>


