<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "report_item".
 *
 * @property string $id
 * @property string $item_id
 * @property string $user_id
 * @property string $email
 * @property int $reason
 * @property string $message
 * @property integer $is_read
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property User $user
 * @property Item $item
 */
class ReportItem extends \yii\db\ActiveRecord
{
    const SCENARIO_GUEST = 'guest';
    const SCENARIO_LOGGED = 'logged';
    
    public $verifyCode;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report_item';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'reason', 'message',], 'required'],
            //[['user_id'], 'required', 'on' => self::SCENARIO_LOGGED],
            //[['user_id'], 'integer'],
            [['email'], 'required', 'on' => self::SCENARIO_GUEST],
            [['item_id', 'reason'], 'integer'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
            [['email'], 'string', 'max' => 250],
            
            ['verifyCode', 'captcha'],
        ];
    }
    
    public function __toString()
    {
        return '#'.$this->getPrimaryKey();
    }
    
    public static function getReasons()
    {
        return [
            1 => "Fraude",
            2 => "Duplicado",
            3 => "Categoría incorrecta",
            4 => "Otra"
        ];
    }
    
    public function getTextReason()
    {
        return ArrayHelper::getValue(self::getReasons(), $this->reason);
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item'),
            'user_id' => Yii::t('app', 'User'),
            'email' => Yii::t('app', 'Email'),
            'reason' => Yii::t('app', 'What is the problem with this publication ?'),
            'message' => Yii::t('app', 'Message'),
            'is_read' => Yii::t('app', "Read"),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'verifyCode' => Yii::t('app', "Verification code"),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
