<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stat_search".
 *
 * @property string $id
 * @property string $term
 * @property string $num_searches
 * @property string $created_at
 * @property string $updated_at
 */
class StatSearch extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stat_search';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['term'], 'required'],
            [['num_searches'], 'integer'],
            [['term'], 'string', 'max' => 255],
        ];
    }
    
    public static function addTerm($q)
    {
        $q = mb_strtolower($q, 'UTF-8');
        
        $model = self::find()->where(['like', 'term', $q])->one();
        if(!empty($model)) {
            $model->num_searches++;
            $model->update(false);
        }
        else {
            $model = new self([
                'term' => $q,
                'num_searches' => 1
            ]);
            $model->save(false);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'term' => Yii::t('app', 'Term'),
            'num_searches' => Yii::t('app', 'Num searches'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
