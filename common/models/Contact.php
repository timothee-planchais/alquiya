<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $email
 * @property string $name
 * @property integer $category
 * @property string $subject
 * @property string $message
 * @property integer $is_read
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property User $user
 */
class Contact extends \yii\db\ActiveRecord
{
    const SCENARIO_GUEST = 'guest';
    const SCENARIO_LOGGED = 'logged';
    
    public $verifyCode;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'subject', 'message'], 'required'],
            //[['user_id'], 'required', 'on' => self::SCENARIO_LOGGED],
            //[['user_id'], 'integer'],
            [['email', 'name'], 'required', 'on' => self::SCENARIO_GUEST],
            [['message'], 'string'],
            [['email'], 'string', 'max' => 250],
            [['name'], 'string', 'max' => 100],
            [['subject'], 'string', 'max' => 255],
            ['category', 'in', 'range' => array_keys(self::getCategories())],
            
            ['verifyCode', 'captcha'],
        ];
    }
    
    public function __toString()
    {
        return '#'.$this->getPrimaryKey();
    }
    
    public static function getCategories()
    {
        return [
            1 => "Mensaje privado",
            2 => "Problema tecnico",
            //3 => "Cuestión administrativa",
            4 => "Propuesta de funcionalidad.",
            5 => "Propuesta de nueva categoría",
            6 => "Contacto de la relación de prensa",
            //7 => "Solicitud de cotización",
            8 => "Solicitud de información",
            //9 => "Solicitud de reserva",
            10 => "Solicitud de asociación",//Asociación Alianza Cooperación Colaboración
            //11 => "Envíe su solicitud",//Candidature
            12 => "Felicitarnos :)",
            13 => "Enviar su testimonio!",
            14 => "No recibo el correo de confirmación.",
            15 => "Eliminar cuenta",
        ];
    }
    
    public function getTextCategory()
    {
        return ArrayHelper::getValue(self::getCategories(), $this->category);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Name'),
            'category' => Yii::t('app', 'Category'),
            'subject' => Yii::t('app', 'Subject'),
            'message' => Yii::t('app', 'Message'),
            'is_read' => Yii::t('app', "Read"),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'verifyCode' => Yii::t('app', "Verification code"),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
