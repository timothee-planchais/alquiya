<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bump_log".
 *
 * @property string $id
 * @property string $user_id
 * @property string $item_id
 * @property string $created_at
 */
class BumpLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bump_log';
    }
    
    /**
     * {@inheritdoc}
     */
    /*public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }*/

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id', 'created_at'], 'required'],
            [['user_id', 'item_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'item_id' => Yii::t('app', 'Item'),
            'created_at' => Yii::t('app', 'Bumped At'),
        ];
    }
}
