<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property string $id
 * @property string $user_id
 * @property string $type
 * @property string $item_id
 * @property string $booking_id
 * @property string $message_id
 * @property string $subscription_id
 * @property string $text
 * @property int $is_read
 * @property int $send_email
 * @property int $email_sent
 * @property string $created_at
 *
 * @property Booking $booking
 * @property User $user
 * @property Item $item
 * @property Message $message
 * @property Subscription $subscription
 */
class Notification extends \yii\db\ActiveRecord
{
    const TYPE_NEW_MESSAGE = 'new_message';
    const TYPE_NEW_BOOKING = 'new_booking';
    const TYPE_NEW_BOOKING_OWNER = 'new_booking_owner';
    const TYPE_BOOKING_VALIDATED = 'booking_validated';
    const TYPE_BOOKING_COMPLETED = 'booking_completed';
    const TYPE_BOOKING_COMPLETED_OWNER = 'booking_completed_owner';
    const TYPE_BOOKING_REFUSED = 'booking_refused';
    const TYPE_BOOKING_CANCELLED = 'booking_cancelled';
    const TYPE_BOOKING_PENDING = 'booking_pending';
    const TYPE_SUBSCRIPTION_PAID = 'subscription_paid';
    const TYPE_SUBSCRIPTION_ENDS_SOON = 'subscription_ends_soon';
    const TYPE_SUBSCRIPTION_FINISHED = 'subscription_finished';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
                'updatedAtAttribute' => false
            ],
        ];
    }
    
    /**
     * @param User $user
     * @return \self
     */
    public static function generateNotification($user)
    {
        $notif = new self([
            'user_id' => $user->getPrimaryKey(),
            'text' => '',
            'is_read' => 0,
            'send_email' => 1,
            'email_sent' => 0,
            'item_id' => null,
            'booking_id' => null,
            'message_id' => null,
        ]);
        $notif->populateRelation('user', $user);
        
        return $notif;
    }
    
    /*
     * Notification for new message
     */
    public static function newMessage(Message $message)
    {
        $notif = self::generateNotification($message->user);
        $notif->type = self::TYPE_NEW_MESSAGE;
        $notif->message_id = $message->getPrimaryKey();
        $notif->populateRelation('message', $message);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification for a new booking
     */
    public static function newBooking(Booking $booking)
    {
        $notif = self::generateNotification($booking->user);
        $notif->type = self::TYPE_NEW_BOOKING;
        $notif->booking_id = $booking->getPrimaryKey();
        $notif->populateRelation('booking', $booking);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            //return true;
        }
        
        $notifOwner = clone $notif;
        $notifOwner->user_id = $booking->item->user_id;
        $notifOwner->populateRelation('user', $booking->item->user);
        $notifOwner->type = self::TYPE_NEW_BOOKING_OWNER;
        
        if($notifOwner->save(false)) {
            $notifOwner->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification for a validated booking
     */
    public static function bookingValidated(Booking $booking)
    {
        $notif = self::generateNotification($booking->user);
        $notif->type = self::TYPE_BOOKING_VALIDATED;
        $notif->booking_id = $booking->getPrimaryKey();
        $notif->populateRelation('booking', $booking);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification for a cancelled booking
     */
    public static function bookingCancelled(Booking $booking)
    {
        $notif = self::generateNotification($booking->item->user);
        $notif->type = self::TYPE_BOOKING_CANCELLED;
        $notif->booking_id = $booking->getPrimaryKey();
        $notif->populateRelation('booking', $booking);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification for a refused booking
     */
    public static function bookingRefused(Booking $booking)
    {
        $notif = self::generateNotification($booking->user);
        $notif->type = self::TYPE_BOOKING_REFUSED;
        $notif->booking_id = $booking->getPrimaryKey();
        $notif->populateRelation('booking', $booking);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification for a completed booking
     */
    public static function bookingCompleted(Booking $booking)
    {
        $notif = self::generateNotification($booking->user);
        $notif->type = self::TYPE_BOOKING_COMPLETED;
        $notif->booking_id = $booking->getPrimaryKey();
        $notif->populateRelation('booking', $booking);
        
        $notifOwner = clone $notif;
        
        if($notif->save(false)) {
            $notif->sendEmail();
        }
        
        $notifOwner->user_id = $booking->item->user_id;
        $notifOwner->populateRelation('user', $booking->item->user);
        $notifOwner->type = self::TYPE_BOOKING_COMPLETED_OWNER;
        
        if($notifOwner->save(false)) {
            $notifOwner->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification for a booking still pending
     */
    public static function bookingPending(Booking $booking)
    {
        $notif = self::generateNotification($booking->item->user);
        $notif->type = self::TYPE_BOOKING_PENDING;
        $notif->booking_id = $booking->getPrimaryKey();
        $notif->populateRelation('booking', $booking);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification subscription paid
     */
    public static function subscriptionPaid(Subscription $subscription)
    {
        $notif = self::generateNotification($subscription->user);
        $notif->type = self::TYPE_SUBSCRIPTION_PAID;
        $notif->subscription_id = $subscription->getPrimaryKey();
        $notif->populateRelation('subscription', $subscription);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification subscription ends soon
     */
    public static function subscriptionEndsSoon(Subscription $subscription)
    {
        $notif = self::generateNotification($subscription->user);
        $notif->type = self::TYPE_SUBSCRIPTION_ENDS_SOON;
        $notif->subscription_id = $subscription->getPrimaryKey();
        $notif->populateRelation('subscription', $subscription);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    /*
     * Notification subscription finished
     */
    public static function subscriptionFinished(Subscription $subscription)
    {
        $notif = self::generateNotification($subscription->user);
        $notif->type = self::TYPE_SUBSCRIPTION_FINISHED;
        $notif->subscription_id = $subscription->getPrimaryKey();
        $notif->populateRelation('subscription', $subscription);
        
        if($notif->save(false)) {
            $notif->sendEmail();
            return true;
        }
        
        return false;
    }
    
    
    public function getNiceDate()
    {
        return Yii::$app->formatter->asDate($this->created_at, 'short').' - '.Yii::$app->formatter->asTime($this->created_at, 'short');
    }
    
    
    /* ----- */
    
    public function getText()
    {
        switch($this->type)
        {
            case self::TYPE_NEW_MESSAGE : return "Tiene un nuevo mensaje";
            case self::TYPE_NEW_BOOKING : return "Un objeto ha sido reservado";
            case self::TYPE_NEW_BOOKING_OWNER : return "Un objeto suyo ha sido reservado";
            case self::TYPE_BOOKING_VALIDATED : return "Su reservación ".$this->booking->getNum()." ha sido validada por el Arrendador";
            case self::TYPE_BOOKING_CANCELLED : return "Su reservación ".$this->booking->getNum()." ha sido cancelada por el Arrendatario";
            case self::TYPE_BOOKING_REFUSED : return "Su reservación ".$this->booking->getNum()." ha sido rechazada por el Arrendador";
            case self::TYPE_BOOKING_COMPLETED : return "Su reservación ".$this->booking->getNum()." este completa. Se puede dejar su opinion";
            case self::TYPE_BOOKING_COMPLETED_OWNER : return "La reservación ".$this->booking->getNum()." este completa. Se puede dejar su opinion";
            case self::TYPE_BOOKING_PENDING : return "La reserva ".$this->booking->getNum()." sigue esperando su validación.";
            case self::TYPE_SUBSCRIPTION_PAID : return "Su suscripción ha sido correctamente pagada y activada";
            case self::TYPE_SUBSCRIPTION_ENDS_SOON : return "Su suscripción termina pronto";
            case self::TYPE_SUBSCRIPTION_FINISHED : return "Su suscripción acaba de terminar";
        }
        
        return '';
    }
    
    public function getUrl()
    {
        switch($this->type)
        {
            case self::TYPE_NEW_MESSAGE : return ['/account/message/index'];
            case self::TYPE_NEW_BOOKING : ;
            case self::TYPE_NEW_BOOKING_OWNER : ;
            case self::TYPE_BOOKING_VALIDATED : ;
            case self::TYPE_BOOKING_CANCELLED : ;
            case self::TYPE_BOOKING_REFUSED : ;
            case self::TYPE_BOOKING_COMPLETED : ;
            case self::TYPE_BOOKING_COMPLETED_OWNER : 
            case self::TYPE_BOOKING_PENDING : return $this->booking->getUrl();
            case self::TYPE_SUBSCRIPTION_PAID :
            case self::TYPE_SUBSCRIPTION_ENDS_SOON :
            case self::TYPE_SUBSCRIPTION_FINISHED : return ['/account/subscription/index'];
        }
        
        return null;
    }
    
    protected function sendEmail()
    {
        $user = $this->user;
        if($user->receiveNotificationEmail($this->type))
        {
            $view = 'notification/'.$this->getEmailViewFromType($this->type);
            $viewParams = [
                'user' => $user,
                'item' => $this->isRelationPopulated('item') ? $this->item : null,
                'booking' => $this->isRelationPopulated('booking') ? $this->booking : null,
                'message' => $this->isRelationPopulated('message') ? $this->message : null,
                'subscription' => $this->isRelationPopulated('subscription') ? $this->subscription : null,
            ];
            $sent = Yii::$app
                    ->mailer
                    ->compose($view, $viewParams)
                    ->setFrom([Yii::$app->params['contactFromEmail'] => Yii::$app->params['contactFromName']])
                    ->setTo($user->email)
                    ->setSubject('AlquiYa - '.$this->getEmailSubjectFromType($this->type, $viewParams))
                    ->send();
            
            return $sent;
        }
        
        return false;
    }
    
    protected function getEmailViewFromType()
    {
        /*switch($this->type)
        {
            case self::TYPE_NEW_MESSAGE : return 'new_message';
        }*/
        return $this->type;
    }
    
    protected function getEmailSubjectFromType()
    {
        return $this->getText();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::className(), ['id' => 'message_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
}
