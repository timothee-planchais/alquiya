<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "payment".
 *
 * @property string $id
 * @property string $user_id
 * @property string $order_id
 * @property string $reference_code
 * @property float $amount
 * @property string $currency
 * @property int $state_pol
 * @property string $response_code_pol
 * @property string $reference_pol
 * @property string $payment_method_id
 * @property int $payment_method_type
 * @property string $transaction_date
 * @property string $response_message_pol
 * @property string $transaction_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Order $order
 */
class Payment extends \yii\db\ActiveRecord
{
    /*
     * http://developers.payulatam.com/en/web_checkout/variables.html
     */
    
    const RESPONSE_POL_APPROVED = 1;
    const RESPONSE_POL_DECLINED = 2;
    const RESPONSE_POL_EXPIRED = 5;
    
    const STATE_POL_APPROVED = 4;
    const STATE_POL_EXPIRED = 5;
    const STATE_POL_DECLINED = 6;
    const STATE_POL_PENDING = 7;
    const STATE_POL_ERROR = 104;
    
    /*
     * http://developers.payulatam.com/en/web_checkout/variables.html?opened_div=08
     * https://www.payulatam.com/medios-de-pago/
     */
    const METHOD_TYPE_CREDIT_CARD = 1;
    const METHOD_TYPE_PSE = 4;//Colombia
    const METHOD_TYPE_ACH = 5;//Colombia
    const METHOD_TYPE_DEBIT_CARD = 6;
    const METHOD_TYPE_CASH = 7;
    const METHOD_TYPE_REFERENCED = 8;
    const METHOD_TYPE_BANK_REFERENCED = 10;
    const METHOD_TYPE_SPEI = 14;//Mexique
    
    protected $signature;
    protected $signature_response;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }
    
    public function __toString()
    {
        return $this->reference_code;
    }
    
    public function isSuccess()
    {
        return $this->state_pol == self::STATE_POL_APPROVED;
    }
    
    public function getResponseDescription()
    {
        switch($this->response_code_pol)
        {
            case 1 : return Yii::t('app', "Transaction approved");
            //...
            
            default: return Yii::t('app', "Internal error");
        }
    }
    
    
    public function getSignature()
    {
        if(!$this->signature) {
            $this->generateSignature();
        }
        
        return $this->signature;
    }
    
    public function generateSignature()
    {
        $str = Yii::$app->params['payu.apiKey']
                .'~'.Yii::$app->params['payu.merchantId']
                .'~'.$this->getReferenceCode()
                .'~'.number_format($this->amount, 2, '.', '')
                .'~'.$this->currency;
        $this->signature = md5($str);
    }
    
    public function getSignatureResponse()
    {
        if(!$this->signature) {
            $this->generateSignatureResponse();
        }
        
        return $this->signature;
    }
    
    public function generateSignatureResponse()
    {
        $str = Yii::$app->params['payu.apiKey']
                .'~'.Yii::$app->params['payu.merchantId']
                .'~'.$this->getReferenceCode()
                .'~'.number_format($this->amount, 1, '.', '')
                .'~'.$this->currency
                .'~'.$this->state_pol;
        $this->signature = md5($str);
    }
    
    public function generateReferenceCode()
    {
        $this->reference_code = base64_encode($this->getPrimaryKey()).Yii::$app->security->generateRandomString(10);
    }
    
    public function getReferenceCode()
    {
        if(empty($this->reference_code)) {
            $this->generateReferenceCode();
        }
        
        return $this->reference_code;
    }
    
    public function getDescription()
    {
        return $this->order->getTitle(true);
    }
    
    public static function getStatesPol()
    {
        return [
            self::STATE_POL_APPROVED => Yii::t('app', 'Approved'),
            self::STATE_POL_DECLINED => Yii::t('app', "Declined"),
            self::STATE_POL_EXPIRED => Yii::t('app', "Expired")
        ];
    }
    
    public function getTextStatePol()
    {
        return ArrayHelper::getValue(self::getStatesPol(), $this->state_pol);
    }
    
    public static function getMethodTypes()
    {
        return [
            self::METHOD_TYPE_CREDIT_CARD => Yii::t('app', 'Credit card'),
            self::METHOD_TYPE_PSE => 'PSE',
            self::METHOD_TYPE_ACH => 'ACH',
            self::METHOD_TYPE_DEBIT_CARD => Yii::t('app', 'Debit card'),
            self::METHOD_TYPE_CASH => Yii::t('app', 'Cash'),
            self::METHOD_TYPE_REFERENCED => Yii::t('app', 'Referenced'),
            self::METHOD_TYPE_BANK_REFERENCED => Yii::t('app', 'Bank referenced'),
            self::METHOD_TYPE_SPEI => 'SPEI',

        ];
    }
    
    public function getTextMethodType()
    {
        return ArrayHelper::getValue(self::getMethodTypes(), $this->payment_method_type);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    
    public function attributeLabels()
    {
        return [
            
        ];
    }
}
