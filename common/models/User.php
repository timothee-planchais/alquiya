<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use common\components\Utils;

/**
 * User model
 *
 * @property integer $id
 * @property string $status
 * @property string $type
 * @property string $gender
 * @property string $email
 * @property string $username
 * @property string $firstname
 * @property string $lastname
 * @property string $image
 * @property string $mobile_phone
 * @property string $phone
 * @property string $work_phone
 * @property string $show_email
 * @property string $show_phone
 * @property string $description
 * @property string $lang
 * @property string $wishlist
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $google_id
 * @property string $ip
 * @property string $isp
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $verification_token
 * @property integer $security_email_verified
 * @property integer $show_email
 * @property integer $show_phone
 * @property integer $created_at
 * @property integer $updated_at
 * 
 * @property string $address
 * @property string $address2
 * @property string $zipcode
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $lat
 * @property string $lng
 * 
 * @property string $password write-only password
 * 
 * @property UserPro $userPro
 * @property Item[] $items
 * @property Booking[] $bookings En tant que locataire
 * @property Booking[] $bookings2 En tant que propriétaire
 * @property Review[] $receivedReviews Ses reviews (sans objet)
 * @property Review[] $postedReviews Reviews envoyées
 * @property Message[] $sendedMessages
 * @property Message[] $receivedMessages
 * @property Payment[] $payments
 * @property Order[] $orders
 * @property Subscription[] $subscriptions
 * @property Notification[] $notifications
 * 
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_BACKEND_INSERT = 'backend_insert';
    const SCENARIO_BACKEND_UPDATE = 'backend_update';
    const SCENARIO_FRONTEND_UPDATE = 'frontend_update';
    const SCENARIO_UPDATE_PASSWORD = 'password';
    
    const TYPE_USER = 'user';
    const TYPE_USER_PRO = 'user_pro';
    
    const STATUS_PENDING = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DESACTIVATED = 2;

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';
    
    public $password;//Contient le mdp non hashé
    public $password_repeat;//Mdp répété
    public $new_password;//Nouveau mdp
    public $new_password_repeat;//Répétition du nouveau mdp
    
    public $aWishlist = [];
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
            /*
             * https://github.com/2amigos/yii2-file-upload-widget
             * http://demos.krajee.com/widget-details/fileinput
             */
            'image' => [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    '300x300' => ['width' => 300, 'height' => 300],
                ],
                'filePath' => '@data/[[model]]/[[pk]]/[[basename]]',
                'fileUrl' => Yii::$app->params['nfs.url'].'/[[model]]/[[pk]]/[[basename]]',
                'thumbPath' => '@data/[[model]]/[[pk]]/[[filename]]_[[profile]].[[extension]]',
                'thumbUrl' => Yii::$app->params['nfs.url'].'/[[model]]/[[pk]]/[[filename]]_[[profile]].[[extension]]',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'status'], 'required', 'on' => [self::SCENARIO_BACKEND_INSERT,self::SCENARIO_BACKEND_UPDATE]],
            [['gender', 'username', 'email', 'firstname', 'lastname', 'phone', 'address', 'zipcode', 'city', 'country'], 'required', 'on' => [self::SCENARIO_BACKEND_INSERT,self::SCENARIO_BACKEND_UPDATE,self::SCENARIO_FRONTEND_UPDATE]],
            
            ['type', 'in', 'range' => array_keys(self::getTypes())],
            
            ['status', 'in', 'range' => array_keys(self::getStatuses()), 'on' => [self::SCENARIO_BACKEND_INSERT,self::SCENARIO_BACKEND_UPDATE]],
            
            ['gender', 'in', 'range' => array_keys(self::getGenders())],
            
            [['username', 'email'], 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This email address has already been taken.')],

            [['username', 'email', 'firstname', 'lastname', 'address', 'address2', 'city'], 'string', 'min' => 2, 'max' => 255],
            
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This username has already been taken.')],
            
            [['phone', 'mobile_phone', 'work_phone'], 'safe'],
            
            ['description', 'string', 'max' => 500],
            
            [['show_email', 'show_phone'], 'integer'],
            
            ['zipcode', 'string', 'min' => 5, 'max' => 7],
            
            ['country', 'in', 'range' => array_keys(self::getCountries())],
            
            //['zipcode', 'match', 'pattern' => Utils::REGEX_POSTAL_CODE],
            
            ['lang', 'in', 'range' => array_keys(self::getLangs())],
            
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => Yii::$app->params['file.max_size']],
            
            [['password', 'password_repeat'], 'required', 'on' => [self::SCENARIO_BACKEND_INSERT]],
            ['password', 'string', 'min' => 6, 'on' => [self::SCENARIO_BACKEND_INSERT]],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => [self::SCENARIO_BACKEND_INSERT]],
            
            [['new_password', 'new_password_repeat'], 'required', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['new_password', 'string', 'min' => 6, 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['new_password_repeat', 'compare', 'compareAttribute' => 'new_password', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
        ];
    }
    
    public function afterFind()
    {
        if($this->wishlist) {
            $this->aWishlist = explode(',', $this->wishlist);
        }
        
        return parent::afterFind();
    }
    
    /**
     * Sauvegarde des lat & lng
     * https://maps.googleapis.com/maps/api/geocode/json?address=Avenida+Nutibara+%2370-181%2C+Medellin%2C+050031%2C+Colombia&key=AIzaSyCAQmc0iWeBYUOcpqvPDdTHAM8fKzv0k5c&language=es
     */
    public function beforeSave($insert)
    {
        if(!empty($this->address) && ($insert/* || $this->address != $this->getOldAttribute('address')*/))
        {
            $address = $this->address.', '.$this->city.($this->zipcode ? ', '.$this->zipcode : '').', '.Utils::getTextCountry($this->country);
            $url = 'https://maps.googleapis.com/maps/api/geocode/json';
            $params = [
                'address' => $address,
                'key' => Yii::$app->params['google.api_key'],
                'language' => 'es'
            ];
            $data = Utils::curl_get($url, $params);
            $results = !empty($data['results']) ? $data['results'] : null;
            //var_dump($results);die;
            if($results && !empty($results[0]))
            {
                $result = $results[0];
                if(!empty($result['geometry'])) {
                    $this->lat = (string)$result['geometry']['location']['lat'];
                    $this->lng = (string)$result['geometry']['location']['lng'];
                }
                if(!empty($result['address_components'])) 
                {
                    foreach($result['address_components'] as $address_component) 
                    {
                        if(!empty($address_component['types']) && !empty($address_component['types'][0])) {
                            $value = $address_component['long_name'];
                            switch($address_component['types'][0])
                            {
                                case 'administrative_area_level_1' : $this->state = $value;break;
                                case 'locality' : $this->city = $value;break;
                            }
                        }
                    }
                }
            }
        }
        
        //Modification du mot de passe
        if(!$insert && !empty($this->new_password))
        {
            $this->setPassword($this->new_password);
        }
        
        if($this->aWishlist && is_array($this->aWishlist)) {
            $this->wishlist = implode(',', $this->aWishlist);
        }
        else {
            $this->wishlist = null;
        }
        
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        $this->password = $this->password_repeat = null;
    }
    
    public function isPro()
    {
        return $this->type == self::TYPE_USER_PRO/* && !empty($this->userPro)*/;
    }
    
    public function __toString()
    {
        return $this->username;
        //return $this->firstname.' '.$this->lastname/*$this->getFirstLetterLastname().'.'*/;
    }
    
    public function getTitle()
    {
        return $this->firstname.' '.$this->lastname.' ('.$this->email.')';
    }
    
    public function getFirstLetterLastname()
    {
        return substr($this->lastname, 0, 1);
    }
    
    public function isInWishList($itemId)
    {
        return in_array($itemId, $this->aWishlist);
    }
    
    public function addToWishList($itemId)
    {
        $this->aWishlist[$itemId] = $itemId;
    }
    
    public function removeFromWishList($itemId)
    {
        if (($key = array_search($itemId, $this->aWishlist)) !== false) {
            unset($this->aWishlist[$key]);
        }
    }
    
    public function getValidationToken()
    {
        return base64_encode($this->getPrimaryKey().$this->email);
    }
    
    /**
     * @param type $offset
     * @param type $limit
     * @return Notification[]
     */
    public function getNotifs($read = null, $offset = 0, $limit = 10)
    {
        $query = $this->getNotifications();
        
        if($read !== null) {
            $query->andWhere(['is_read' => $read]);
        }
        
        return $query->orderBy('id DESC')->offset($offset)->limit($limit)->all();
    }
    
    public function getCountUnreadNotifications()
    {
        return $this->getNotifications()->andWhere(['is_read' => 0])->count();
    }
    
    public function receiveNotificationEmail($type)
    {
        /*switch($type)
        {
            case Notification::TYPE_NEW_MESSAGE : return $this->notification_new_message;
            default: return true;
        }*/
        
        return true;
    }
    
    public function getReferralCode()
    {
        return base64_encode($this->getPrimaryKey());
    }
    
    public function getImageUrl($thumb = null)
    {
        if($this->image)
        {
            if(strpos($this->image, 'http://') !== false || strpos($this->image, 'https://') !== false) {
                return $this->image;
            }
            
            if($thumb) {
                return $this->getThumbFileUrl('image', $thumb);
            }

            return $this->getImageFileUrl('image');
        }
        
        return '@web/img/user/empty.png';
    }
    
    public function getProfileUrl($params = [])
    {
        $url = ['user/view', 'id' => $this->getPrimaryKey()];
        
        if($params) {
            $url = array_merge($url, $params);
        }
        return $url;
    }
    
    public function getFullName()
    {
        return $this->firstname.' '.$this->lastname;
    }
    
    public function getOfficialAddress($separator = ', ')
    {
        return /*$this->zipcode.' '.*/$this->city.( $this->state ? $separator.$this->state : '').$separator.strtoupper($this->getNiceCountry());
    }
    
    public function getNiceAddress($separator= "\n")
    {
        return $this->address.$separator.$this->zipcode.' '.$this->city.$separator.strtoupper($this->getNiceCountry());
    }
    
    public function getFullAddress()
    {
        return $this->address.($this->address2 ? "\n".$this->address2 : '')."\n".$this->zipcode.' '.$this->city.( $this->state ? ', '.$this->state : '')."\n".strtoupper($this->getNiceCountry());
    }
    
    public function getNiceCountry()
    {
        return ArrayHelper::getValue(self::getCountries(), $this->country);
    }
    
    public function getRating()
    {
        return Review::find()->where(['user_id' => $this->getPrimaryKey()])->average('note');
    }
            
    public static function getCountries()
    {
        return \common\components\Utils::getCountries();
    }
    
    public static function getLangs()
    {
        return \common\components\Utils::getLangs();
    }
    
    public function getPhoneNumber()
    {
        return $this->phone;
    }
    
    public function isAddressCompleted()
    {
        return !empty($this->address) && !empty($this->zipcode) && !empty($this->city);
    }
    
    public function getProfileCompleticity()
    {
        $value = 0;
        
        $attributes = ['gender','email','username','lastname','firstname','phone','description','address','zipcode','city'];
        
        foreach($attributes as $attribute) {
            if($this->getAttribute($attribute)) {
                $value++;
            }
        }
        
        return $value/count($attributes);
    }
    
    public function getProfileCompleticityColor($completicity = null)
    {
        if(!$completicity) {
            $completicity = $this->getProfileCompleticity();
        }
        
        if($completicity > 0.6) {
            return 'success';
        }
        
        return 'warning';
    }
    
    public static function getTypes()
    {
        return [
            self::TYPE_USER => Yii::t('app', "A particular"),
            self::TYPE_USER_PRO => Yii::t('app', "A professional renter")
        ];
    }
    
    public function getTexttype()
    {
        return ArrayHelper::getValue(self::getTypes(), $this->type);
    }
    
    public static function getStatuses()
    {
        return [
            self::STATUS_PENDING => Yii::t('app', "Pending"),
            self::STATUS_ACTIVE => Yii::t('app', "Active"),
            self::STATUS_DESACTIVATED => Yii::t('app', "Desactivated")
        ];
    }
    
    public function getTextStatus()
    {
        return ArrayHelper::getValue(self::getStatuses(), $this->status);
    }
    
    public static function getGenders()
    {
        return [
            self::GENDER_MALE => Yii::t('app', 'Male'),
            self::GENDER_FEMALE => Yii::t('app', 'Female')
        ];
    }
    
    public function getTextGender()
    {
        return ArrayHelper::getValue(self::getGenders(), $this->gender);
    }
    
    public function getTextCountry()
    {
        return ArrayHelper::getValue(Utils::getCountries(), $this->country);
    }
           
    public function attributeLabels()
    {
        return [
            'type' => Yii::t('app', "Type"),
            'status' => Yii::t('app', "Status"),
            'gender' => Yii::t('app', "Gender"),
            'username' => Yii::t('app', "Username"),
            'email' => Yii::t('app', "Email"),
            'firstname' => Yii::t('app', "Firstname"),
            'lastname' => Yii::t('app', "Lastname"),
            'phone' => Yii::t('app', "Phone"),
            'mobile_phone' => Yii::t('app', "Mobile phone"),
            'work_phone' => Yii::t('app', "Work phone"),
            'show_email' => Yii::t('app', "Show my email"),
            'show_phone' => Yii::t('app', "Show my phone number"),
            'description' => Yii::t('app', "Your description"),
            'address' => Yii::t('app', "Address"), 
            'address2' => Yii::t('app', "Address 2"),
            'zipcode' => Yii::t('app', "Zipcode"), 
            'city' => Yii::t('app', "City"), 
            'state' => Yii::t('app', "State"),
            'country' => Yii::t('app', "Country"),
            'lang' => Yii::t('app', "Language"),
            'show_email' => Yii::t('app', "Show my email"),
            'show_phone' => Yii::t('app', "Show my phone"),
            'updated_at' => "Updated at",
            'created_at' => "Created at",
            'ip' => 'IP',
            'isp' => 'ISP',
            'password' => Yii::t('app', "Password"),
            'password_repeat' => Yii::t('app', "Confirmation password"),
            'image' => Yii::t('app', "Profile picture"),
            
            'new_password' => Yii::t('app', "New password"),
            'new_password_repeat' => Yii::t('app', "Confirmation new password")
        ];
    }
    
    public function attributeInfos()
    {
        return [
            'type' => Yii::t('app', "If you act professionally, you need to create a professional account"),
            'address' => Yii::t('app', "Your exact address will not be shown"),
        ];
    }
    
    public function getAttributeInfo($attribute)
    {
        return \yii\helpers\ArrayHelper::getValue($this->attributeInfos(), $attribute);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings2()
    {
        return Booking::find()->leftJoin('item', 'booking.item_id = item.id')->where(['item.user_id' => $this->getPrimaryKey()]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivedReviews()
    {
        return $this->hasMany(Review::className(), ['user_id' => 'id'])->orderBy('id DESC');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getItemsReviews()
    {
        return Review::find()->leftJoin('booking', 'review.booking_id = booking.id')->leftJoin('item', 'item.id = booking.item_id')->where(['item.user_id' => $this->getPrimaryKey()]);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostedReviews()
    {
        return $this->hasMany(Review::className(), ['author_id' => 'id'])->orderBy('id DESC');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSendedMessages()
    {
        return $this->hasMany(Message::className(), ['author_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceivedMessages()
    {
        return $this->hasMany(Message::className(), ['user_id' => 'id']);
    }
    
    public function getNewMessages()
    {
        return $this->getReceivedMessages()->andWhere(['is_read' => 0]);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPro()
    {
        return $this->hasOne(UserPro::className(), ['user_id' => 'id']);
    }
    
    /*
     * --------------
     */

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email, $params = [])
    {
        $params['email'] = $email;
        return static::findOne($params);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    
    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_PENDING
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
}
