<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $parent_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property string $group
 * @property integer $position
 * @property integer $active
 * @property integer $menu_active
 * 
 * @property Category $parent
 * @property Category[] $subCategories
 * @property Item[] $items
 * @property AmazonItem[] $amazonItems
 */
class Category extends \yii\db\ActiveRecord
{
    const GROUP_A = 'A';
    const GROUP_B = 'B';
    const GROUP_C = 'C';
    
    const MAX_LEVEL = 3;
    
    public $menu_subCategories;
    
    public static function tableName()
    {
        return 'category';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'position', 'active', 'menu_active'], 'integer'],
            [['title', 'position', 'active', 'menu_active', 'group'], 'required'],
            [['description'], 'string'],
            [['title', 'slug'], 'string', 'max' => 255],
            ['group', 'in', 'range' => array_keys(self::getGroupes())],
        ];
    }

    /*
     * 
     */
    public function getUrl($params = [])
    {
        $url = ['item/index'];
        
        $parent = $this->parent_id && $this->parent ? $this->parent : null;
        $bigParent = null;
        if($parent) {
            $bigParent = $this->parent->parent ? $parent->parent : null;
        }
        
        
        //subCategory
        if($parent && $bigParent) {
            $url['bigCategorySlug'] = $bigParent->slug;
            $url['categorySlug'] = $parent->slug;
            $url['subCategoryId'] = $this->id;
            $url['subCategorySlug'] = $this->slug;
        }
        //category
        elseif($parent) {
            $url['bigCategorySlug'] = $parent->slug;
            $url['categoryId'] = $this->id;
            $url['categorySlug'] = $this->slug;
        }
        //bigCategory
        else {
            $url['bigCategoryId'] = $this->id;
            $url['bigCategorySlug'] = $this->slug;
        }
        
        if($params) {
            $url = array_merge($url, $params);
        }
        
        return $url;
    }
    
    public function findGroup()
    {
        if($this->group) {
            return $this->group;
        }
        elseif($this->parent_id && $this->parent)
        {
            if($this->parent->group) {
                return $this->parent->group;
            }
            elseif($this->parent->parent_id && $this->parent->parent) {
                return $this->parent->parent->group;
            }
        }
        
        return self::GROUP_A;
    }
    
    /*
     * Prix des groupes
     */
    public function getRatioPriceFromPeriod($period = Subscription::PERIOD_1_DAY)
    {
        $group = $this->findGroup();
        if($group == self::GROUP_A)
        {
            return 1;
        }
        elseif($group == self::GROUP_B)
        {
            switch($period)
            {
                case Subscription::PERIOD_1_DAY : return 1.5;
                case Subscription::PERIOD_1_WEEK : return 1.5;
                case Subscription::PERIOD_1_MONTH : return 1.3;
                case Subscription::PERIOD_6_MONTHS : return 1.2;
                case Subscription::PERIOD_1_YEAR : return 1.2;
            }
        }
        elseif($group == self::GROUP_C)
        {
            switch($period)
            {
                case Subscription::PERIOD_1_DAY : return 2.5;
                case Subscription::PERIOD_1_WEEK : return 2.3;
                case Subscription::PERIOD_1_MONTH : return 2.1;
                case Subscription::PERIOD_6_MONTHS : return 1.9;
                case Subscription::PERIOD_1_YEAR : return 1.7;
            }
        }
    }
    
    public static function getGroupes()
    {
        return [
            self::GROUP_A => 'A',
            self::GROUP_B => 'B',
            self::GROUP_C => 'C'
        ];
    }
    
    /*
     * Groupes de prix
     */
    public function getTextGroup()
    {
        return ArrayHelper::getValue(self::getGroupes(), $this->group);
    }
    
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'description' => Yii::t('app', 'Description'),
            'group' => Yii::t('app', 'Group'),
            'position' => Yii::t('app', 'Position'),
            'active' => Yii::t('app', 'Active'),
            'menu_active' => Yii::t('app', 'Menu visible'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id'])->orderBy('position ASC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), [ $this->parent_id ? 'subcategory' : 'category' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmazonItems()
    {
        return $this->hasMany(AmazonItem::className(), ['category_id' => 'id']);
    }

}
