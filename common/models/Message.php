<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property string $id
 * @property string $type
 * @property integer $author_id
 * @property integer $user_id
 * @property integer $booking_id
 * @property integer $item_id
 * @property string $subject
 * @property string $message
 * @property integer $is_read
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Item $item
 * @property User $author
 * @property User $user
 * @property Booking $booking
 */
class Message extends \yii\db\ActiveRecord
{
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'type', */ 'subject','message'], 'required'],
            //[['author_id', 'user_id', 'booking_id', 'item_id'], 'integer'],
            [['type'], 'string', 'max' => 30],
            [['subject'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 500],
            /*[['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::className(), 'targetAttribute' => ['booking_id' => 'id']],*/
        ];
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        if($insert)
        {
            //Envoyer notification
            Notification::newMessage($this);
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function isNotRead()
    {
        return $this->author_id != Yii::$app->getUser()->getId() && !$this->is_read;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'author_id' => Yii::t('app', 'Author'),
            'user_id' => Yii::t('app', 'User'),
            'booking_id' => Yii::t('app', 'Booking'),
            'item_id' => Yii::t('app', 'Item'),
            'subject' => Yii::t('app', 'Subject'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }
}
