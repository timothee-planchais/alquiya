<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property string $id
 * @property string $author_id
 * @property string $booking_id
 * @property string $user_id
 * @property int $note
 * @property string $text
 * @property string $created_at
 *
 * @property User $author Auteur du review
 * @property Booking $booking Booking en question
 * @property User $user Cible du review
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'booking_id', 'note', 'text'], 'required'],
            [['author_id', 'booking_id', 'user_id'], 'integer'],
            [['note'], 'in', 'range' => self::getPossibleNotes()],
            [['text'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => Booking::className(), 'targetAttribute' => ['booking_id' => 'id']],
        ];
    }
    
    public static function getPossibleNotes()
    {
        return [0,1,2,3,4,5];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'author_id' => Yii::t('app', 'Author'),
            'booking_id' => Yii::t('app', 'Booking'),
            'user_id' => Yii::t('app', 'User'),
            'note' => Yii::t('app', 'Note'),
            'text' => Yii::t('app', "Review"),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['id' => 'booking_id']);
    }
}
