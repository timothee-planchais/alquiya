<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use DateTime;

/**
 * This is the model class for table "subscription".
 *
 * @property string $id
 * @property string $user_id
 * @property string $order_id
 * @property string $item_id
 * @property string $type
 * @property string $code
 * @property string $period
 * @property string $status
 * @property string $start_date
 * @property string $end_date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Order $order
 * @property Item $item
 */
class Subscription extends \yii\db\ActiveRecord
{
    const SCENARIO_BACKEND = 'backend';
    const SCENARIO_FRONTEND = 'frontend';
    
    const TYPE_ITEM_BUMPED = 'bumped';//Remonter mon annonce en tête de liste
    const TYPE_ITEM_FEATURED = 'featured';//Item featured
    const TYPE_ITEM_HIGHLIGHTED = 'highlighted';//Item highlighted
    const TYPE_ITEM_PICTURES = 'pictures';//Ajout de photos
    
    const PERIOD_1_DAY = '1_day';
    const PERIOD_1_WEEK = '1_week';
    const PERIOD_1_MONTH = '1_month';
    const PERIOD_6_MONTHS = '6_months';
    const PERIOD_1_YEAR = '1_year';
    
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_FINISHED = 'finished';
    const STATUS_UPCOMING = 'upcoming';
    
    /** @var DateTime */
    public $oStartDate;
    /** @var DateTime */
    public $oEndDate;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscription';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'item_id', 'type', 'code', 'status'], 'required', 'on' => [self::SCENARIO_BACKEND]],
            [['user_id', 'order_id', 'item_id'], 'integer', 'on' => [self::SCENARIO_BACKEND]],
            [['start_date', 'end_date'], 'safe', 'on' => [self::SCENARIO_BACKEND]],
            [['type', 'code', 'status'], 'string', 'max' => 45, 'on' => [self::SCENARIO_BACKEND]],
            ['period', 'in', 'range' => array_keys(self::getPeriods())],
            [['period'], 'required', 
                'when' => function($model) { return $model->type != self::TYPE_ITEM_PICTURES; },
                'whenClient' => 'function(attribute, value) { return $("#subscription-type").val() != "'.self::TYPE_ITEM_PICTURES.'";}'
            ],
            [['start_date', 'end_date'], 'required', 
                'when' => function($model) { return $model->type != self::TYPE_ITEM_PICTURES; },
                'whenClient' => 'function(attribute, value) { return $("#subscription-type").val() != "'.self::TYPE_ITEM_PICTURES.'";}'
            , 'on' => [self::SCENARIO_BACKEND]]
        ];
    }
    
    public function afterFind()
    {
        if($this->start_date) {
            $this->oStartDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->start_date);
        }
        if($this->end_date) {
            $this->oEndDate = DateTime::createFromFormat('Y-m-d H:i:s', $this->end_date);
        }
        
        
        return parent::afterFind();
    }
    
    public function beforeSave($insert)
    {
        if($insert) {
            $this->code = Yii::$app->security->generateRandomString(15);
        }
        
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        /*
         * Abonnement payé
         */
        if(($insert && $this->status == self::STATUS_ACTIVE) || (isset($changedAttributes['status']) && $changedAttributes['status'] == self::STATUS_PENDING && $this->status == self::STATUS_ACTIVE))
        {
            Notification::subscriptionPaid($this);
            
            //Si bump, on le bump direct
            if($this->type == self::TYPE_ITEM_BUMPED)
            {
                $item = $this->item;
                if($item)
                {
                    $item->bump_date = date('Y-m-d H:i:s');
                    $item->update(false, ['bump_date']);
                    $bumpLog = new BumpLog([
                        'user_id' => $item->user_id,
                        'item_id' => $item->getPrimaryKey(),
                        'created_at' => $item->bump_date
                    ]);
                    $bumpLog->save(false);
                }
            }
            //Si feature, on le featrured
            elseif($this->type == self::TYPE_ITEM_FEATURED)
            {
                $item = $this->item;
                if($item)
                {
                    $item->featured = 1;
                    $item->update(false, ['featured']);
                }
            }
            //Si highlight, on le highlighted
            elseif($this->type == self::TYPE_ITEM_HIGHLIGHTED)
            {
                $item = $this->item;
                if($item)
                {
                    $item->highlighted = 1;
                    $item->update(false, ['highlighted']);
                }
            }
        }
        /*
         * Abonnement terminé
         */
        elseif(!$insert && (isset($changedAttributes['status']) && $changedAttributes['status'] == self::STATUS_ACTIVE && $this->status == self::STATUS_FINISHED))
        {
            if($this->type != self::TYPE_ITEM_BUMPED && $this->period != self::PERIOD_1_DAY) {
                Notification::subscriptionFinished($this);
            }
            
            if($this->type == self::TYPE_ITEM_FEATURED)
            {
                $item = $this->item;
                if($item)
                {
                    $item->featured = 0;
                    $item->update(false, ['featured']);
                }
            }
            elseif($this->type == self::TYPE_ITEM_HIGHLIGHTED)
            {
                $item = $this->item;
                if($item)
                {
                    $item->highlighted = 0;
                    $item->update(false, ['highlighted']);
                }
            }
        }
        
        
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function __toString()
    {
        return $this->getNiceCode();
    }
    
    public function getTitle()
    {
        if($this->type == self::TYPE_ITEM_PICTURES) {
            return Yii::t('app', "More pictures");
        }
        else {
            return /*Yii::t('app', "Subscription").' : '.*/$this->getTextType(true).' ('.$this->getTextPeriod().')';
        }
    }
    
    public function getNiceCode()
    {
        return '#'.$this->code; 
    }
    
    public function isUpComing()
    {
        return !empty($this->start_date) && strtotime($this->start_date) > time();
    }
    
    public function isFinished()
    {
        return $this->status == self::STATUS_FINISHED || (!empty($this->end_date) && strtotime($this->end_date) <= time());
    }
    
    public function isInProgress()
    {
        return !$this->isUpComing() && !$this->isFinished();
    }
    
    public function getTotalDays()
    {
        return $this->oEndDate->diff($this->oStartDate)->format("%a");
    }
    
    public function getPastDays()
    {
        return (new DateTime())->diff($this->oStartDate)->format("%a");
    }
    
    public function getRemainingDays()
    {
        return $this->oEndDate->diff(new DateTime())->format("%a");
    }
    
    public function getNiceEndDate()
    {
        return str_replace(':','h', Yii::$app->formatter->asDate($this->end_date, 'short').' - '.Yii::$app->formatter->asTime($this->end_date, 'short'));
    }
    
    public static function getPricesFromType($type, Item $item = null)
    {
        $category = null;
        if(!empty($item)) {
            $category = $item->subcategory ? : $item->category;
        }
        
        $prices = [];
        if($type != self::TYPE_ITEM_PICTURES)
        {
            foreach(self::getPeriods() as $period => $name) 
            {
                $ratio = 1;
                if(!empty($category)) {
                    $ratio = $category->getRatioPriceFromPeriod($period);
                }
                $prices[$period] = Yii::$app->params['subscription.'.$type.'.'.$period] * $ratio;
            }
        }
        
        return $prices;
    }
    
    public function getPrices(Item $item = null)
    {
        return self::getPricesFromType($this->type, $item);
    }
    
    public function getPriceFromPeriod($period = self::PERIOD_1_DAY, Item $item = null)
    {
        $prices = $this->getPrices($item);
        foreach($prices as $p => $price) {
            if($p == $period) {
                return $price;
            }
        }
        
        return null;
    }
    
    public function getPrice(Item $item = null)
    {
        return $this->getPriceFromPeriod($this->period, $item);
    }
    
    public function setDates()
    {
        if($this->period)
        {
            $this->initStartDate();
            switch($this->period)
            {
                case self::PERIOD_1_DAY : $this->end_date = date('Y-m-d H:i:s', strtotime('today', strtotime($this->start_date))+60);break;
                case self::PERIOD_1_WEEK : $this->end_date = date('Y-m-d H:i:s', strtotime('+1 week', strtotime($this->start_date)));break;
                case self::PERIOD_1_MONTH : $this->end_date = date('Y-m-d H:i:s', strtotime('+1 month', strtotime($this->start_date)));break;
                case self::PERIOD_6_MONTHS : $this->end_date = date('Y-m-d H:i:s', strtotime('+6 months', strtotime($this->start_date)));break;
                case self::PERIOD_1_YEAR: $this->end_date = date('Y-m-d H:i:s', strtotime('+1 year', strtotime($this->start_date)));break;
            }
        }
    }
    
    public function initStartDate()
    {
        //On cherche s'il y a déjà un abonnement du même type en cours
        $lastActiveSubscription = $this->getLastActiveSubscription();
        if($lastActiveSubscription) {
            $this->start_date = $lastActiveSubscription->end_date;
        }
        else {
            $this->start_date = date('Y-m-d H:i:s');
        }
    }
    
    /**
     * @return Subscription
     */
    public function getLastActiveSubscription()
    {
        return $this->user->getSubscriptions()->andWhere(['status' => self::STATUS_ACTIVE, 'type' => $this->type, 'item_id' => $this->item_id])->andWhere(['not', ['id' => $this->getPrimaryKey()]])->orderBy('end_date DESC')->one();
    }
    
    public function needsPeriod()
    {
        return $this->type != self::TYPE_ITEM_PICTURES;
    }
    
    public static function getTypes($ed = false)
    {
        return [
            self::TYPE_ITEM_BUMPED => Yii::t('app', $ed ? 'Bumped' : 'Bump'),
            self::TYPE_ITEM_FEATURED => Yii::t('app', $ed ? 'Featured' : 'Feature'),
            self::TYPE_ITEM_HIGHLIGHTED => Yii::t('app', $ed ? 'Highlighted' : 'Highlight'),
            self::TYPE_ITEM_PICTURES => Yii::t('app', 'Pictures')
        ];
    }
    
    public function getTextType($ed = false)
    {
        return ArrayHelper::getValue(self::getTypes($ed), $this->type);
    }
    
    public function getIconType()
    {
        switch($this->type)
        {
            case self::TYPE_ITEM_BUMPED : return 'fa fa-arrow-up';
            case self::TYPE_ITEM_FEATURED : return 'fa fa-certificate';
            case self::TYPE_ITEM_HIGHLIGHTED : return 'fa fa-star';
            case self::TYPE_ITEM_PICTURES : return 'fa fa-picture-o';
        }
    }
    
    public static function getStatuses()
    {
        return [
            self::STATUS_PENDING => Yii::t('app', "Pending"),
            self::STATUS_ACTIVE => Yii::t('app', 'Activated'),
            self::STATUS_FINISHED => Yii::t('app', "Finished"),
            self::STATUS_UPCOMING => Yii::t('app', "Upcoming")
        ];
    }
    
    public function getTextStatus()
    {
        return ArrayHelper::getValue(self::getStatuses(), $this->status);
    }
    
    public static function getPeriods()
    {
        return [
            //self::PERIOD_1_DAY => Yii::t('app', "One day"),
            self::PERIOD_1_WEEK => Yii::t('app', "One week"),
            self::PERIOD_1_MONTH => Yii::t('app', "One month"),
            self::PERIOD_6_MONTHS => Yii::t('app', "Six months"),
            self::PERIOD_1_YEAR => Yii::t('app', "One year")
        ];
    }
    
    public function getTextPeriod()
    {
        return ArrayHelper::getValue(self::getPeriods(), $this->period);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'order_id' => Yii::t('app', 'Order'),
            'item_id' => Yii::t('app', 'Item'),
            'type' => Yii::t('app', 'Type'),
            'code' => Yii::t('app', 'Code'),
            'period' => Yii::t('app', 'Period'),
            'status' => Yii::t('app', 'Status'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
