<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "image".
 *
 * @property string $id
 * @property string $item_id
 * @property string $image
 *
 * @property Item $item
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            /*
             * https://github.com/2amigos/yii2-file-upload-widget
             * http://demos.krajee.com/widget-details/fileinput
             */
            'image' => [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    '300x300' => ['width' => 300, 'height' => 300],
                    '300x200' => ['width' => 300, 'height' => 200],
                ],
                'filePath' => '@data/item/[[model]]/[[pk]]/[[basename]]',
                'fileUrl' => Yii::$app->params['nfs.url'].'/item/[[model]]/[[pk]]/[[basename]]',
                'thumbPath' => '@data/item/[[model]]/[[pk]]/[[filename]]_[[profile]].[[extension]]',
                'thumbUrl' => Yii::$app->params['nfs.url'].'/item/[[model]]/[[pk]]/[[filename]]_[[profile]].[[extension]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'integer'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['image'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => Yii::$app->params['file.max_size']],
        ];
    }
    
    public function getImageUrl($thumb = null)
    {
        if($thumb) {
            return $this->getThumbFileUrl('image', $thumb);
        }
        
        return $this->getImageFileUrl('image');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
