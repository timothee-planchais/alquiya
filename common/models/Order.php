<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property string $id
 * @property string $parent_id
 * @property string $user_id
 * @property string $type
 * @property string $code
 * @property string $method
 * @property string $amount
 * @property string $vta_rate
 * @property string $currency
 * @property string $status
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $parent
 * @property Order[] $orders
 * @property User $user
 * @property Subscription[] $subscriptions
 * @property Subscription $subscription
 * @property Payment[] $payments
 * @property Payment $payment
 */
class Order extends \yii\db\ActiveRecord
{
    const TYPE_SUBSCRIPTION = 'subscription';
    
    const STATUS_WAITING_PAYMENT = 'waiting_payment';
    const STATUS_PAID = 'paid';
    const STATUS_PAYMENT_CANCELLED = 'payment_cancelled';
    const STATUS_PAYMENT_ERROR = 'payment_error';
    
    const METHOD_PAYU = 'payu';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'status'], 'required'],
            [['parent_id', 'user_id'], 'integer'],
            [['amount'], 'number'],
            [['date'], 'safe'],
            [['type', 'code', 'method', 'status'], 'string', 'max' => 45],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            
        ];
    }
    
    public function beforeSave($insert)
    {
        if($insert)
        {
            $this->code = Yii::$app->security->generateRandomString(15);
            
            if(empty($this->date)) {
                $this->date = date('Y-m-d');
            }
        }
        
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        //Si la facture est payée
        if(($insert && $this->status == self::STATUS_PAID) || (isset($changedAttributes['status']) && $changedAttributes['status'] == self::STATUS_WAITING_PAYMENT && $this->status == self::STATUS_PAID))
        {
            $this->paid();
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function paid()
    {
        if($this->type == self::TYPE_SUBSCRIPTION)
        {
            $subscriptions = $this->subscriptions;
            foreach($subscriptions as $subscription) 
            {
                $subscription->setDates();
                $subscription->status = Subscription::STATUS_ACTIVE;
                $subscription->update(false, ['status', 'updated_at']);
            }
        }
    }
    
    public function __toString()
    {
        return '#'.$this->code;
    }
    
    public function getTitle($full = true)
    {
        if($this->type == self::TYPE_SUBSCRIPTION) 
        {
            return $this->subscription->getTitle();
        }
        
        return $this->__toString();
    }
    
    public function getSubscriptionDates()
    {
        if($this->type == self::TYPE_SUBSCRIPTION)
        {
            return Yii::$app->formatter->asDate($this->subscription->start_date, 'short').' - '.Yii::$app->formatter->asDate($this->subscription->end_date, 'short');
        }
        
        return null;
    }
    
    public function isPaid()
    {
        return $this->status == self::STATUS_PAID;
    }
    
    public static function getTypes()
    {
        return [
            self::TYPE_SUBSCRIPTION => Yii::t('app', "Subscription"),
        ];
    }
    
    public function getTextType()
    {
        return ArrayHelper::getValue(self::getTypes(), $this->type);
    }
    
    public static function getStatuses()
    {
        return [
            self::STATUS_WAITING_PAYMENT => Yii::t('app', "Waiting for the payment"),
            self::STATUS_PAID => Yii::t('app', "Validated"),
            self::STATUS_PAYMENT_CANCELLED => Yii::t('app', "Cancelled/Refused"),
            self::STATUS_PAYMENT_ERROR => Yii::t('app', "Error"),
        ];
    }
    
    public function getTextStatus()
    {
        return ArrayHelper::getValue(self::getStatuses(), $this->status);
    }
    
    public function getColorStatus()
    {
        switch($this->status)
        {
            case self::STATUS_WAITING_PAYMENT : return 'warning';
            case self::STATUS_PAID : return 'success';
            case self::STATUS_PAYMENT_CANCELLED : return 'danger';
            case self::STATUS_PAYMENT_ERROR : return 'danger';
        }
    }
    
    public static function getMethods()
    {
        return [
            self::METHOD_PAYU => "PayU"
        ];
    }
    
    public function getTextMethod()
    {
        return ArrayHelper::getValue(self::getMethods(), $this->method);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => Yii::t('app', 'Parent'),
            'user_id' => Yii::t('app', "User"),
            'type' => Yii::t('app', 'Type'),
            'code' => Yii::t('app', 'Code'),
            'method' => Yii::t('app', 'Method'),
            'amount' => Yii::t('app', 'Amount'),
            'vta_rate' => Yii::t('app', "VTA rate"),
            'currency' => Yii::t('app', "Currency"),
            'status' => Yii::t('app', 'Status'),
            'date' => Yii::t('app', 'Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Order::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['order_id' => 'id']);
    }
}
