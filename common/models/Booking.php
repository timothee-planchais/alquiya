<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use DateTime;

/**
 * This is the model class for table "booking".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $item_id
 * @property string $status
 * @property string $refusal_reason
 * @property string $date_start
 * @property string $date_end
 * @property float $price
 * @property float $total
 * @property float $fees
 * @property float $deposit_value
 * @property string $created_at
 * @property string $updated_at
 * 
 * @property Item $item
 * @property User $user
 * @property Review[] $reviews
 * @property Payment $payment
 */
class Booking extends \yii\db\ActiveRecord
{
    const SCENARIO_REFUSE = 'refuse';
    const SCENARIO_CANCEL = 'refuse';
    const SCENARIO_CREATE = 'create';
    
    //const STATUS_WAITING_PAYMENT = 'waiting_payment';//Pas encore payé
    const STATUS_PENDING = 'pending';//En attente de validation par lo loueur
    const STATUS_VALIDATED = 'validated';//Validé par le loueur
    const STATUS_REFUSED = 'refused';//Refusé par le loueur
    const STATUS_CANCELLED = 'cancelled';//Annulé par locataire
    const STATUS_WAITING_REVIEW = 'waiting_review';//En attente de l'avis du locataire
    const STATUS_COMPLETED = 'completed';//Terminé
    
    /** @var DateTime */
    public $oStart;
    /** @var DateTime */
    public $oEnd;
    
    public $duration = null;
    
    public $totalWithFees;
    
    public $accept_terms;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'booking';
    }
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id', 'status', 'date_start', 'date_end', 'price', 'total'], 'required'],
            [['user_id', 'item_id'], 'integer'],
            [['date_start', 'date_end'], 'date', 'format' => 'php:d/m/Y'],
            [['price', 'total'], 'number'],
            [['status'], 'in', 'range' => array_keys(self::getStatuses())],
            ['refusal_reason', 'string', 'max' => 255, 'on' => [self::SCENARIO_REFUSE, self::SCENARIO_CANCEL]],
            ['accept_terms', 'required', 'requiredValue' => 1, 'on' => self::SCENARIO_CREATE]
        ];
    }
    
    public function afterFind()
    {
        $this->initDates();
        
        $this->duration = $this->getDuration();
        //$this->priceWithFees = $this->price + ($this->price * Yii::$app->params['item.fees']);
        $this->totalWithFees = $this->total + ($this->total * Yii::$app->params['item.fees']);
        
        return parent::afterFind();
    }
    
    public function validate($attributeNames = null, $clearErrors = true)
    {
        $ok = parent::validate($attributeNames, $clearErrors);
        
        if($ok && $this->scenario != self::SCENARIO_REFUSE && $this->scenario != self::SCENARIO_CANCEL && $this->scenario != self::SCENARIO_CREATE)
        {
            $this->initDatesFromInputs();
            
            //Compare start and end
            if($this->oStart > $this->oEnd)
            {
                $this->addError('date_range', Yii::t('app', "The end date cannot be anterior to the start date"));
                return false;
            }
            
            //Inferior date_start
            /*if($this->oStart < new DateTime()) {
                $this->addError('date_range', Yii::t('app', "The start date cannot be anterior to tomorrow"));
                return false;
            }*/
            
            //Disponible ?
            if(!$this->isDisponible())
            {
                $this->addError('date_range', Yii::t('app', "The period isn't available"));
                return false;
            }
            
            //Owner item ?
            if($this->isRelationPopulated('item') && $this->item->user_id == Yii::$app->getUser()->getId()) {
                $this->addError('date_range', Yii::t('app', "You cannot renter your own item"));
                return false;
            }
            
        }
        
        return $ok;
    }
    
    public function beforeSave($insert)
    {
        $this->date_start = $this->oStart->format('Y-m-d');
        $this->date_end = $this->oEnd->format('Y-m-d');
        
        $this->total = $this->getTotalPrice();
        $this->fees = 0;//$this->getFees();
        
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        if($insert && $this->status == self::STATUS_PENDING) {
            Notification::newBooking($this);
        }
        //Envoyer un email de notification à la personne
        elseif(!empty($changedAttributes) && array_key_exists('status', $changedAttributes))
        {
            
            //Envoyer un email de notification au loueur
            if($this->status == self::STATUS_PENDING)
            {
                Notification::newBooking($this);
            }
            elseif($this->status == self::STATUS_VALIDATED)
            {
                Notification::bookingValidated($this);
            }
            elseif($this->status == self::STATUS_CANCELLED) {
                Notification::bookingCancelled($this);
            }
            elseif($this->status == self::STATUS_REFUSED) {
                Notification::bookingRefused($this);
            }
            elseif($this->status == self::STATUS_COMPLETED)
            {
                //Notif booking completed
                Notification::bookingCompleted($this);
                $item = $this->item;
                $item->nb_rents++;
                $item->update(false, ['nb_rents']);
            }
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
    
    /*
     * Return duration in days
     */
    public function getDuration($dateInterval = false)
    {
        if($this->duration === null) {
            if($dateInterval) {
                $this->duration = $this->oStart->diff($this->oEnd);
            }

            $this->duration = $this->oStart->diff($this->oEnd)->days + 1;
        }
        
        return $this->duration;
    }
    
    public function initDuration()
    {
        $this->duration = $this->getDuration();
    }
    
    public function getDurationPrice()
    {
        return $this->getDuration() * $this->getDailyPrice();
    }
    
    /**
     * @return float The total price
     */
    public function getTotalPrice()
    {
        if($this->total === null) 
        {
            $duration = $this->getDuration();
            $dailyPrice = $this->item->price;

            $priceMonth = $this->item->price_month;
            $priceWeek = $this->item->price_week;
            $priceWeekend = $this->item->price_weekend;

            if($priceMonth && $duration >= 30) {
                $dailyPrice = $priceMonth / 30;
            }
            if($priceWeek && $duration >= 7) {
                $dailyPrice = $priceWeek / 7;
            }
            if($priceWeekend && $duration >= 3) {
                $dailyPrice = $priceWeekend / 3;
            }

            $this->total = $duration * $dailyPrice;
        }
        
        return $this->total;
    }
    
    public function getDailyPrice()
    {
        //return round($this->getTotalPrice() / $this->getDuration(), 0);
        return $this->item->price;
    }
    
    public function getDiscountAmount()
    {
        return $this->getDurationPrice() - $this->getTotalPrice();
    }
    
    public function isDisponible()
    {
        $count = self::find()
                ->where(['item_id' => $this->item_id])
                ->andWhere(['status' => self::STATUS_VALIDATED])
                ->andWhere(['or', 'date_start BETWEEN :start AND :end', 'date_end BETWEEN :start AND :end'])
                ->params(['start' => $this->oStart->format('Y-m-d'), 'end' => $this->oEnd->format('Y-m-d')])
                ->count();
        
        return $count <= 0;
    }
    
    public function initDatesFromInputs()
    {
        if($this->date_start && $this->date_end)
        {
            $this->oStart = DateTime::createFromFormat('d/m/Y', $this->date_start);
            $this->oEnd = DateTime::createFromFormat('d/m/Y', $this->date_end);
        }
    }
    
    public function initDates()
    {
        if($this->date_start && $this->date_end)
        {
            $this->oStart = DateTime::createFromFormat('Y-m-d', $this->date_start);
            $this->oEnd = DateTime::createFromFormat('Y-m-d', $this->date_end);
        }
    }
    
    public function __toString()
    {
        return $this->getNum();
    }
    
    public function getNum()
    {
        return '#'.$this->getPrimaryKey();
    }
    
    public function getFromTo()
    {
        return Yii::t('app', "From").' '.Yii::$app->formatter->asDate($this->date_start)
                .' '.Yii::t('app', "to").' '.Yii::$app->formatter->asDate($this->date_end);
    }
    
    public function getTitle()
    {
        return $this->__toString().' / '.$this->getFromTo();
    }
    
    public function getIntervalDates()
    {
        $dates = [];
        if($this->oStart && $this->oEnd)
        {
            $start = clone $this->oStart;
            $end = clone $this->oEnd;
            $end = $end->modify( '+1 day' );
            $datePeriod = new \DatePeriod($start, new \DateInterval('P1D') , $end);
            foreach($datePeriod as $i => $date) {
                $dates[] = $date->format('d/m/Y');
            }
        }
        return $dates;
    }
    
    public function isUpcoming()
    {
        return strtotime($this->date_start) > time();
    }
    
    public function isPassed()
    {
        return strtotime($this->date_start) <= time();
    }
    
    public function isReviewed(User $user = null)
    {
        if(empty($user)) {
            $user = Yii::$app->getUser()->getIdentity();
        }
        return Review::find()->where(['booking_id' => $this->getPrimaryKey(), 'author_id' => $user->getPrimaryKey()])->count() > 0;
    }
    
    public function getUrl($params = [])
    {
        $url = ['booking/view', 'id' => $this->getPrimaryKey()];
        
        if($params) {
            $url = array_merge($url, $params);
        }
        return $url;
    }

    public static function getStatuses()
    {
        return [
            //self::STATUS_WAITING_PAYMENT => Yii::t('app', "Waiting for the payment"),
            self::STATUS_PENDING => Yii::t('app', "Must be confirmed"),
            self::STATUS_VALIDATED => Yii::t('app', "Confirmed"),
            self::STATUS_REFUSED => Yii::t('app', "Refused"),
            self::STATUS_CANCELLED => Yii::t('app', "Cancelled"),
            self::STATUS_WAITING_REVIEW  => Yii::t('app', "Waiting for review"),
            self::STATUS_COMPLETED => Yii::t('app', "Completed")
        ];
    }
    
    public function getTextStatus()
    {
        return ArrayHelper::getValue(self::getStatuses(), $this->status);
    }
    
    public function getStatusColor()
    {
        switch($this->status)
        {
            case self::STATUS_PENDING : return 'info';
            case self::STATUS_VALIDATED : ;
            case self::STATUS_WAITING_REVIEW : ;
            case self::STATUS_COMPLETED : return 'success';
            default: return 'danger';
        }
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => Yii::t('app', "User"),
            'item_id' => Yii::t('app', "Item"),
            'status' => Yii::t('app', "Status"),
            'refusal_reason' => Yii::t('app', "Reason of cancellation"),
            'date_start' => Yii::t('app', "From"),
            'date_end' => Yii::t('app', "To"),
            'price' => Yii::t('app', "Price"),
            'total' => Yii::t('app', "Total"),
            'fees' => Yii::t('app', "Fees"),
            'deposit_value' => Yii::t('app', "Deposit value"),
            'created_at' => Yii::t('app', "Created at"),
            'updated_at' => Yii::t('app', "Updated at"),
            'accept_terms' => Yii::t('app', "I accept the terms and conditions")
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['booking_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['booking_id' => 'id']);
    }
    
    
}
