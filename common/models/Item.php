<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use DateTime;

/**
 * This is the model class for table "item".
 *
 * @property string $id
 * @property string $user_id
 * @property string $status
 * @property integer $locked
 * @property integer $featured
 * @property integer $highlighted
 * @property string $bump_date
 * @property string $title
 * @property string $slug
 * @property string $category_id
 * @property string $subcategory_id
 * @property string $subtitle
 * @property string $description
 * @property string $delivery
 * @property integer $condition
 * @property string $full_address
 * @property string $short_address
 * @property string $address
 * @property string $neighborhood
 * @property string $zipcode
 * @property string $city
 * @property string $full_city
 * @property string $region
 * @property string $state
 * @property string $country
 * @property string $country_code
 * @property string $lat
 * @property string $lng
 * @property integer $on_quote Seulement sur devis
 * @property integer $pricing_type Type de prix
 * @property string $price
 * @property string $price_weekend
 * @property string $price_week
 * @property string $price_month
 * @property string $deposit_value
 * @property string $price_new
 * @property int $nb_views
 * @property int $nb_rents
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Category $category
 * @property Category $subcategory
 * @property Image[] $images
 * @property Image $firstImage
 * @property Booking[] $bookings
 * @property Review[] $reviews
 * @property Subscription[] $subscription
 */
class Item extends \yii\db\ActiveRecord
{
    const SCENARIO_BACKEND = 'backend';
    const SCENARIO_FRONTEND_CREATE = 'frontend_create';
    const SCENARIO_FRONTEND_UPDATE = 'frontend_update';
    
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    
    const PRICING_TYPE_FIX = 1;
    const PRICING_TYPE_FROM = 2;
    const PRICING_TYPE_REQUEST = 3;
    
    const DELIVERY_SEND = 'send';
    const DELIVERY_PICKUP = 'pickup';
    const DELIVERY_FLEXIBLE = 'flexible';
    const DELIVERY_MEETING = 'meeting';
    
    public $priceWithFees;
    public $priceWeekendWithFees;
    public $priceWeekWithFees;
    public $priceMonthWithFees;
    
    /** @var UploadedFile */
    public $tmp_images;
    
    public $distance;
    
    protected $_nb_views = null;
    
    public $is_pro = null;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => \yii\behaviors\SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'category_id', 'description', 'full_address'], 'required'],
            [['user_id', 'status', 'featured', 'highlighted', 'lat', 'lng'], 'required', 'on' => [self::SCENARIO_BACKEND]],
            [['featured', 'highlighted', 'user_id', 'locked'], 'integer', 'on' => [self::SCENARIO_BACKEND]],
            ['short_address', 'required', 'message' => Yii::t('app', "Please select an address")],
            [['category_id', 'subcategory_id'], 'integer'],
            [['description'], 'string'],
            [['price', 'price_weekend', 'price_week', 'price_month', 'deposit_value', 'price_new'], 'number', 'min' => 1, 'max' => 100000000],
            [['status'], 'in', 'range' => array_keys(self::getStatuses())],
            [['title', 'subtitle', 'full_address', 'short_address', 'address', 'neighborhood', 'zipcode', 'city', 'full_city', 'region', 'state', 'country', 'country_code', 'lat', 'lng'], 'string', 'max' => 255],
            ['delivery', 'in', 'range' => array_keys(self::getDeliveries()), 'allowArray' => true],
            ['condition', 'in', 'range' => array_keys(self::getConditions())],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['subcategory_id' => 'id']],
            [['tmp_images'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => Yii::$app->params['item.max_files'], 'maxSize' => Yii::$app->params['file.max_size']],
            [['tmp_images'], 'required', 'on' => self::SCENARIO_FRONTEND_CREATE],
            
            [['price'], 'required', 'when' => function($model){
                return $model->pricing_type != self::PRICING_TYPE_REQUEST;
            }, 'whenClient' => 'function(attribute, value) { console.log($("#item-pricing_type input:checked").val());return $("#item-pricing_type input:checked").val() != "'.self::PRICING_TYPE_REQUEST.'"; }'],
            [['pricing_type'], 'required',  'when' => function($model){ return $model->is_pro; }],
            ['pricing_type', 'in', 'range' => array_keys(self::getPricingTypes()),  'when' => function($model){ return $model->is_pro; }],
        ];
    }
    
    public function afterFind()
    {
        if($this->delivery) {
            $this->delivery = explode(',', $this->delivery);
        }
        
        $this->priceWithFees = $this->price + ($this->price * Yii::$app->params['item.fees']);
        $this->priceWeekendWithFees = $this->price_weekend + ($this->price_weekend * Yii::$app->params['item.fees']);
        $this->priceWeekWithFees = $this->price_week + ($this->price_week * Yii::$app->params['item.fees']);
        $this->priceMonthWithFees = $this->price_month + ($this->price_month * Yii::$app->params['item.fees']);
        
        return parent::afterFind();
    }
    
    /*
     * On récupère les images envoyées
     */
    public function beforeValidate()
    {
        $this->tmp_images = UploadedFile::getInstances($this, 'tmp_images');
        
        return parent::beforeValidate();
    }
    
    public function validate($attributeNames = null, $clearErrors = true)
    {
        if(empty($this->lat) || empty($this->lng)) {
            $this->addError('address', Yii::t('app', "Please select an address"));
        }
        
        return parent::validate($attributeNames, $clearErrors);
    }
    
    /*
     * Lors de l'update, si image NULL, on renvoie l'ancienne valeur
     */
    public function afterValidate()
    {
        if(!$this->isNewRecord)
        {
            if($this->tmp_images == null) {
                $this->tmp_images = $this->getOldAttribute('tmp_images');
            }
        }
        
        return parent::afterValidate();
    }
    
    public function beforeSave($insert)
    {
        if($insert)
        {
            $this->bump_date = date('Y-m-d H:i:s');
        }
        
        if($this->pricing_type == self::PRICING_TYPE_REQUEST) {
            $this->price = $this->price_weekend = $this->price_week = $this->price_month = null;
        }
        
        if($this->delivery && is_array($this->delivery)) {
            $this->delivery = implode(',', $this->delivery);
        }
        
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        if($this->tmp_images)
        {
            foreach($this->tmp_images as $i => $instance) {
                $image = new Image([
                    'item_id' => $this->getPrimaryKey(),
                    'image' => $instance,
                    'position' => $i+1
                ]);
                $image->save();
            }
        }
        
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function beforeDelete()
    {
        if($this->images) {
            foreach($this->images as $image) {
                $image->delete();
            }
        }
        
        return parent::beforeDelete();
    }
    
    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public static function findActive()
    {
        return self::find()->where(['item.status' => self::STATUS_ACTIVE, 'locked' => 0]);
    }
    
    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public static function findFeatured()
    {
        return self::findActive()->andWhere(['featured' => 1]);
    }
    
    public function isPro()
    {
        return $this->user->isPro();
    }
    
    public function isInactivated()
    {
        return $this->status == self::STATUS_INACTIVE;
    }
    
    public function isLocked()
    {
        return !empty($this->locked);
    }
    
    public function getUrl($params = [])
    {
        $url = ['/item/view', 'id' => $this->getPrimaryKey(), 'slug' => $this->slug];
        
        if($params) {
            $url = array_merge($url, $params);
        }
        return $url;
    }
    
    public function getPermalink()
    {
        return ['/item/view', 'id' => $this->getPrimaryKey()];
    }
    
    public function getOficialAddress()
    {
        return $this->short_address;
    }
    
    public function getDailyPrice($duration)
    {
        $dailyPrice = $this->price;
        
        if($this->price_month && $duration >= 30) {
            $dailyPrice = $this->price / 30;
        }
        elseif($this->price_week && $duration >= 7) {
            $dailyPrice = $this->price / 7;
        }
        elseif($this->price_weekend && $duration >= 3) {
            $dailyPrice = $this->price / 3;
        }
        
        return round($dailyPrice, 0);
    }
    
    /**
     * @return Booking[]
     */
    public function getUpcomingBookings()
    {
        return $this->getBookings()->where('date_end >= NOW()')->andWhere(['status' => Booking::STATUS_VALIDATED])->all();
    }
    
    public function getUpcomingBookingDates()
    {
        $dates = [];
        
        $upcomingBookings = $this->getUpcomingBookings();
        
        if($upcomingBookings)
        {
            foreach($upcomingBookings as $upcomingBooking) {
                $dates += $upcomingBooking->getIntervalDates();
            }
        }
        
        return $dates;
    }
    
    public function getJsUpcomingBookingDateRanges()
    {
        $ranges = [];
        
        $upcomingBookings = $this->getUpcomingBookings();
        
        if($upcomingBookings)
        {
            foreach($upcomingBookings as $upcomingBooking) {
                $ranges[] = [
                    'start' => new \yii\web\JsExpression("moment('".$upcomingBooking->oStart->format('Y-m-d')."')"), 
                    'end' => new \yii\web\JsExpression("moment('".$upcomingBooking->oEnd->format('Y-m-d')."')")
                    ];
            }
        }
        
        return $ranges;
    }
    
    public function getImage()
    {
        return $this->getFirstImage()->one();
    }
    
    public function getNiceImages($thumb = null)
    {
        $niceImages = [];
        
        $images = $this->images;
        
        if($images) {
            foreach($images as $image) {
                $niceImages[] = $image->getImageUrl($thumb);
            }
        }
        
        return $niceImages;
    }
    
    public function getImageUrl($thumb = null)
    {
        $image = $this->firstImage;
        
        if($image) {
            return $image->getImageUrl($thumb);
        }
        
        return '@web/img/item/empty_200x200.png';
    }
    
    public function getNiceCategories($separator = ' - ')
    {
        $html = $this->category->parent->title.$separator.$this->category->title;
        
        if($this->subcategory_id) {
            $html .= $separator.$this->subcategory->title;
        }
        
        return $html;
    }
    
    public function getNiceActiveSubscriptions($separator = ', ')
    {
        $html = '';
        
        $activeSubscriptions = $this->getSubscriptions()->andWhere(['status' => Subscription::STATUS_ACTIVE])->orderBy('start_date ASC')->all();
        
        if($activeSubscriptions)
        {
            foreach($activeSubscriptions as $i => $subscription) {
                $html .= ($i>0 ? $separator : '').'<span><i class="'.$subscription->getIconType().'"></i> '.$subscription->getTitle().'</span>';
            }
        }
        
        return $html;
    }
    
    public function getNbViews()
    {
        if($this->_nb_views == null) {
            $result = Yii::$app->db->createCommand('SELECT nb_views FROM item_nb_views WHERE item_id= :id')->bindValue('id', $this->getPrimaryKey())->queryOne();
            if($result && isset($result['nb_views'])) {
                $this->_nb_views = $result['nb_views'];
            }
            else {
                $this->_nb_views = 0;
            }
        }
        
        return $this->_nb_views;
    }
    
    public function incrementNbViews()
    {
        if($this->getNbViews() == 0) {
            Yii::$app->db->createCommand('INSERT INTO item_nb_views VALUES (:id, 1)')->bindValue('id', $this->getPrimaryKey())->execute();
        }
        else {
            Yii::$app->db->createCommand('UPDATE item_nb_views SET nb_views = nb_views  + 1 WHERE item_id=:id')->bindValue('id', $this->getPrimaryKey())->execute();
        }
    }
    
    public function getReviewsBars()
    {
        $bars = [
            5 => 0, 
            4 => 0, 
            3 => 0, 
            2 => 0, 
            1 => 0
        ];
        
        $total = $this->getReviews()->count();
        foreach($bars as $note => $nb)
        {
            /*$bars[$note] = (new \yii\db\Query)->select('id')->from('review r')->leftJoin('booking b', 'r.booking_id = b.id')
                ->where(['b.item_id' => $this->getPrimaryKey()])->andWhere('ROUND(r.note) = :note')->addParams(['note' => $note])->count;*/
            $totalForNote = $this->getReviews()->andWhere('ROUND(review.note) = :note')->addParams(['note' => $note])->count();
            $bars[$note] = $totalForNote / $total;
        }
        
        return $bars;
    }
    
    public function getReviewsAvg()
    {
        $result = (new \yii\db\Query)->select('AVG(note) AS avg')->from('review')->leftJoin('booking', 'review.booking_id = booking.id')
                ->where(['booking.item_id' => $this->getPrimaryKey()])
                ->andWhere('review.author_id != :id')
                ->params(['id' => $this->user_id])->one();
        
        return $result && isset($result['avg']) ? $result['avg'] : null;
    }
    
    public static function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', "Activated"),
            self::STATUS_INACTIVE => Yii::t('app', "Inactivated")
        ];
    }
    
    public function getTextStatus()
    {
        return \yii\helpers\ArrayHelper::getValue(self::getStatuses(), $this->status);
    }
    
    public static function getPricingTypes()
    {
        return [
            self::PRICING_TYPE_FIX => Yii::t('app', 'Fixed price'),
            self::PRICING_TYPE_FROM => Yii::t('app', "From"),
            self::PRICING_TYPE_REQUEST => Yii::t('app', "Upon request")
        ];
    }
    
    public function getTextPricingType()
    {
        return \yii\helpers\ArrayHelper::getValue(self::getPricingTypes(), $this->pricing_type);
    }
    
    public static function getDeliveries()
    {
        return [
            self::DELIVERY_SEND => Yii::t('app', "I can send it to you"),
            self::DELIVERY_PICKUP => Yii::t('app', "You pick up"),
            self::DELIVERY_FLEXIBLE => Yii::t('app', "I'm flexible"),
            self::DELIVERY_MEETING => Yii::t('app', "We meet"),
        ];
    }
    
    public function getTextDelivery()
    {
        $text = null;
        
        if($this->delivery) {
            foreach($this->delivery as $i => $delivery) {
                $text .= ($i > 0 ? ', ' : '').\yii\helpers\ArrayHelper::getValue(self::getDeliveries(), $delivery);
            }
        }
        
        return $text;
    }
    
    public static function getConditions()
    {
        return [
            1 => Yii::t('app', "New"),
            2 => Yii::t('app', "Almost new"),
            3 => Yii::t('app', "Nice condition"),
            4 => Yii::t('app', "Can still serve")
        ];
    }
    
    public function getTextCondition()
    {
        return \yii\helpers\ArrayHelper::getValue(self::getConditions(), $this->condition);
    }
    
    public function attributeInfos()
    {
        return [
            'category_id' => Yii::t('app', "You will not be able to update the category"),
            //'address' => Yii::t('app', "You exact address will not be shown"),
            'price_weekend' => Yii::t('app', "This price per day will apply to all rentals of {nb} days or more", ['nb' => 3]),
            'price_week' => Yii::t('app', "This price per day will apply to all rentals of {nb} days or more", ['nb' => 7]),
            'price_month' => Yii::t('app', "This price per day will apply to all rentals of {nb} days or more", ['nb' => 30]),
            'deposit_value' => Yii::t('app', "Set a coherent deposit (usually the price of the new -20%)"),
            'price_new' => Yii::t('app', "This price will be hidden"),
            'tmp_images' => Yii::t('app', "You can upload hasta 4 pictures")
        ];
    }
    
    public function getAttributeInfo($attribute)
    {
        return \yii\helpers\ArrayHelper::getValue($this->attributeInfos(), $attribute);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'status' => Yii::t('app', 'Status'),
            'locked' => Yii::t('app', "Locked"),
            'featured' => Yii::t('app', "Featured"),
            'highlighted' => Yii::t('app', "Highlighted"),
            'bump_date' => Yii::t('app', "Bumped at"),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'category_id' => Yii::t('app', 'Category'),
            'subcategory_id' => Yii::t('app', 'Subcategory'),
            'subtitle' => Yii::t('app', 'Subtitle'),
            'description' => Yii::t('app', 'Description'),
            'delivery' => Yii::t('app', "Delivery options"),
            'condition' => Yii::t('app', "Condition"),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'full_address' => Yii::t('app', 'Address'),
            'short_address' => Yii::t('app', "Visible address"),
            'address' => Yii::t('app', "Address"),
            'neighborhood' => Yii::t('app', "Neighborhood"),
            'city' => Yii::t('app', "City"),
            'full_city' => Yii::t('app', "City (full)"),
            'zipcode' => Yii::t('app', "Zipcode"),
            'state' => Yii::t('app', "State"),
            'region' => Yii::t('app', "Region"),
            'country' => Yii::t('app', "Country"),
            'country_code' => Yii::t('app', "Country code"),
            'priceWithFees' => Yii::t('app', "Price displayed"),
            'price' => Yii::t('app', 'Price for a day'),
            'price_weekend' => Yii::t('app', 'Price for a weekend (3 days)'),
            'price_week' => Yii::t('app', 'Price for a week (7 days)'),
            'price_month' => Yii::t('app', 'Price for a month (30 days)'),
            'deposit_value' => Yii::t('app', 'Deposit value').' ('.Yii::$app->formatter->currencyCode.')',
            'price_new' => Yii::t('app', 'Price of the product').' ('.Yii::$app->formatter->currencyCode.')',
            'nb_views' => Yii::t('app', 'Nb Views'),
            'nb_rents' => Yii::t('app', 'Nb Rents'),
            'created_at' => Yii::t('app', 'Published at'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'tmp_images' => Yii::t('app', "Images"),
            
            'pricing_type' => Yii::t('app', "Pricing type"),
            'on_quote' => Yii::t('app', "Only upon quote")
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'subcategory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['item_id' => 'id'])->orderBy('image.id ASC');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstImage()
    {
        //return $this->getImages()->limit(1);
        return $this->hasOne(Image::className(), ['item_id' => 'id'])->orderBy('image.id ASC')->limit(1);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['item_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return Review::find()->leftJoin('booking', 'review.booking_id = booking.id')
                ->where(['booking.item_id' => $this->getPrimaryKey()])
                ->andWhere('review.author_id != :id')
                ->params(['id' => $this->user_id])
                ->orderBy('review.created_at DESC');
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['item_id' => 'id']);
    }
}
