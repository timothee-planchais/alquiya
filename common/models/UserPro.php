<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user_pro".
 *
 * @property string $id
 * @property string $user_id
 * @property int $company_name
 * @property string $categories
 * @property string $website
 * @property string $vat_num
 * @property string $description
 * @property string $nb_items
 * @property string $available_days
 * @property string $available_hours
 * @property UploadedFile $logo
 *
 * @property User $user
 */
class UserPro extends \yii\db\ActiveRecord
{
    const SCENARIO_BACKEND = 'backend';
    
    /** @var Category[] */
    public $oCategories;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_pro';
    }
    
    public function behaviors()
    {
        return [
            /*
             * https://github.com/2amigos/yii2-file-upload-widget
             * http://demos.krajee.com/widget-details/fileinput
             */
            'logo' => [
                'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                'attribute' => 'logo',
                'thumbs' => [
                    '300x300' => ['width' => 300, 'height' => 300],
                ],
                'filePath' => '@data/[[model]]/[[pk]]/[[basename]]',
                'fileUrl' => Yii::$app->params['nfs.url'].'/[[model]]/[[pk]]/[[basename]]',
                'thumbPath' => '@data/[[model]]/[[pk]]/[[filename]]_[[profile]].[[extension]]',
                'thumbUrl' => Yii::$app->params['nfs.url'].'/[[model]]/[[pk]]/[[filename]]_[[profile]].[[extension]]',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required', 'on' => self::SCENARIO_BACKEND],
            [['user_id', 'company_name', 'categories'], 'required'],
            [['user_id'], 'integer', 'on' => self::SCENARIO_BACKEND],
            [['nb_items'], 'integer'],
            [['categories', ], 'safe'],
            ['company_name', 'string', 'max' => 100],
            [['website', 'available_hours'], 'string', 'max' => 250],
            [['vat_num'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['description'], 'string', 'max' => 500],
            ['available_days', 'in', 'range' => array_keys(self::getAvailableDays()), 'allowArray' => true],
            [['logo'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => Yii::$app->params['file.max_size']],
        ];
    }
    
    public function afterFind()
    {
        parent::afterFind();
        
        $this->categories = explode(',', $this->categories);
        $this->available_days = explode(',', $this->available_days);
    }
    
    public function beforeSave($insert)
    {
        if(is_array($this->categories)) {
            $this->categories = implode(',', $this->categories);
        }
        if(is_array($this->available_days)) {
            $this->available_days = implode(',', $this->available_days);
        }
        
        return parent::beforeSave($insert);
    }
    
    public function __toString()
    {
        return $this->company_name;
    }
    
    public function getNiceCategories($separator = ' - ')
    {
        $html = '';
        
        $categories = $this->getAllCategories();
        
        if($categories)
        {
            foreach($categories as $i => $category) {
                $html .= ($i>0 ? $separator : '').$category->title;
            }
        }
        
        return $html;
    }
    
    /**
     * @return Category[]
     */
    public function getAllCategories()
    {
        if($this->oCategories === null) {
            $this->oCategories = Category::find()->where(['in', 'id', $this->categories])->all();
        }
        
        return $this->oCategories;
    }
    
    public function getLogoUrl($thumb = null)
    {
        if($this->logo)
        {
            if($thumb) {
                return $this->getThumbFileUrl('logo', $thumb);
            }

            return $this->getImageFileUrl('logo');
        }
        
        return '@web/img/user-pro/empty.png';
    }
    
    public static function getAvailableDays()
    {
        return [
            1 => Yii::t('app', "Monday"),
            2 => Yii::t('app', "Tuesday"),
            3 => Yii::t('app', "Wednesday"),
            4 => Yii::t('app', "Thursday"),
            5 => Yii::t('app', "Friday"),
            6 => Yii::t('app', "Saturday"),
            7 => Yii::t('app', "Sunday"),
        ];
    }
    
    public function getNiceAvailableDays()
    {
        $html = '';
        
        if($this->available_days && is_array($this->available_days)) {
            $days = self::getAvailableDays();
            foreach($this->available_days as $i => $day) {
                $html .= ($i > 0 ? ',' : '').ArrayHelper::getValue($days, $day);
            }
        }
        
        return $html;
    }

    public function attributeInfos()
    {
        return [
            'description' => Yii::t('app', "This description will appear on your company page"),
            'nb_items' => Yii::t('app', "Please indicate the approximate number of references you have in your catalog")
        ];
    }
    
    public function getAttributeInfo($attribute)
    {
        return \yii\helpers\ArrayHelper::getValue($this->attributeInfos(), $attribute);
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'company_name' => Yii::t('app', 'Company name'),
            'categories' => Yii::t('app', 'Sector of activity'),
            'website' => Yii::t('app', 'Website'),
            'vat_num' => Yii::t('app', 'VAT number'),
            'description' => Yii::t('app', 'Decription'),
            'nb_items' => Yii::t('app', "Num of referencies in catalog"),
            'available_days' => Yii::t('app', "Opening days"),
            'available_hours' => Yii::t('app', 'Opening hours'),
            'logo' => Yii::t('app', 'Company logo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
