<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Yii;
use NumberFormatter;

/**
 * Description of Formatter
 *
 * @author Timothee
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * @var bool whether the [PHP intl extension](https://secure.php.net/manual/en/book.intl.php) is loaded.
     */
    private $_intlLoaded = false;
    /**
     * @var \ResourceBundle cached ResourceBundle object used to read unit translations
     */
    private $_resourceBundle;
    /**
     * @var array cached unit translation patterns
     */
    private $_unitMessages = [];
    
    public function init()
    {
        parent::init();
        
        $this->_intlLoaded = extension_loaded('intl');
        if (!$this->_intlLoaded) {
            if ($this->decimalSeparator === null) {
                $this->decimalSeparator = '.';
            }
            if ($this->thousandSeparator === null) {
                $this->thousandSeparator = ',';
            }
        }
    }
    
    public function asCurrency($value, $currency = null, $options = array(), $textOptions = array())
    {
        $currency = $currency ?: $this->currencyCode;
        
        if ($value === null) {
            return $this->nullDisplay;
        }

        $normalizedValue = $this->normalizeNumericValue($value);

        if ($this->isNormalizedValueMispresented($value, $normalizedValue)) {
            return $this->asCurrencyStringFallback((string) $value, $currency);
        }

        if ($this->_intlLoaded) {
            $currency = $currency ?: $this->currencyCode;
            // currency code must be set before fraction digits
            // https://secure.php.net/manual/en/numberformatter.formatcurrency.php#114376
            if ($currency && !isset($textOptions[NumberFormatter::CURRENCY_CODE])) {
                $textOptions[NumberFormatter::CURRENCY_CODE] = $currency;
            }
            $formatter = $this->createNumberFormatter(NumberFormatter::CURRENCY, null, $options, $textOptions);
            if ($currency === null) {
                $result = $formatter->format($normalizedValue);
            } else {
                
                /*
                 * On remplace pour COL :
                 *  '#,##0 ¤' par '$#,##0'
                 */
                if($currency == 'COP') {
                    $formatter->setPattern('$#,##0');
                }
                $result = $formatter->formatCurrency($normalizedValue, $currency);
            }
            if ($result === false) {
                throw new InvalidArgumentException('Formatting currency value failed: ' . $formatter->getErrorCode() . ' ' . $formatter->getErrorMessage());
            }

            return $result;
        }

        return parent::asCurrency($value, $currency, $options, $textOptions);
    }
    
    
}
