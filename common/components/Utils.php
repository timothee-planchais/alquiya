<?php

namespace common\components;

use Yii;
use yii\helpers\Json;

/**
 * Description of Utils
 *
 * @author Timothee
 */
class Utils
{
    const FORMAT_DATE = 'Y-m-d';
    const FORMAT_TIME = 'H:i:s';
    const FORMAT_DATETIME = 'Y-m-d H:i:s';
    
    const COMMON = '/^[0-9A-Za-z_ \.-]*$/';
    const CHARAC_COMMON = '0-9A-Za-z_ \.-';
    const COMMON_NO_SPACE = '/^[0-9A-Za-z_\.-]*$/';
    const REGEX_NICE_DATE = '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/([0-9]{4})$/';
    const REGEX_PHONE = '#^0[1-9]([-. ]?[0-9]{2}){4}$#';
    const REGEX_POSTAL_CODE = "/^\d{2}[ ]?\d{3}$/";
    
    const PAGINATION_LIMIT = 10;
    
    /** @var \linslin\yii2\curl\Curl */
    public static $curl;
    
    public static $countries;
    
    
    /**
     * Instance de Curl
     * @return \linslin\yii2\curl\Curl
     */
    public static function getCurl()
    {
        if(!static::$curl instanceof \linslin\yii2\curl\Curl)
            static::$curl = new \linslin\yii2\curl\Curl();
        
        return static::$curl->setOption(CURLOPT_SSL_VERIFYPEER, false)/*->setOption(CURLOPT_HTTPHEADER, ['Connection_Token: '.Yii::$app->params['connexionToken']])*/;
    }
    
    /**
     * 
     * @param string $url
     * @param array $params
     * @return array
     */
    public static function curl_post($url, $params = [])
    {
        return Json::decode(self::getCurl()->setOption(CURLOPT_POSTFIELDS, http_build_query($params))->post($url));
    }
    
    /**
     * 
     * @param string $url
     * @param array $params
     * @return array
     */
    public static function curl_get($url, $params = [])
    {
        if($params && is_array($params)) {
            $url = $url.'?'.http_build_query($params);
        }
        $result = self::getCurl()->get($url);
        
        return $result ? \yii\helpers\Json::decode($result) : null;
    }
    
    public static function curl_put($url, $params = [])
    {
        return Json::decode(self::getCurl()->setOption(CURLOPT_POSTFIELDS, http_build_query($params))->put($url));
    }
    
    public static function limitWords($str, $limit = 50, $limiter = '...')
    {
        return mb_strlen($str) > $limit ? mb_substr(str_replace(["’"],["'"],$str), 0, $limit).$limiter : $str;
    }
    
    public static function combine($tab)
    {
        return array_combine($tab, $tab);
    }
    
    public static function getUserIP()
    {
        if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = $_SERVER['HTTP_X_FORWARDED_FOR'];

           $explode = explode(',',$ips);
            if(!empty($explode[0]))
                return ip2long($explode[0]);
        }
        
        return ip2long(Yii::$app->getRequest()->getUserIP());
    }
    
    public static function getUserISP()
    {
        return gethostbyaddr(long2ip(self::getUserIP()));
    }
    
    public static function getNiceDatetime($datetime)
    {
        return $datetime ? self::getNiceDate($datetime).' '.Yii::t('app', 'à').' '.str_replace(':','h',Yii::$app->formatter->asTime($datetime, 'short')) : null;
    }
    
    public static function getNiceDate($date)
    {
        return $date ? Yii::$app->formatter->asDate($date, 'medium') : null;
    }
    
    public static function getCountries()
    {
        /*if(empty(self::$countries)) {
            self::$countries = require(Yii::getAlias('@common/data/countries.php'));
        }
        
        return self::$countries;*/
        return Yii::$app->params['countries'];
    }
    
    public static function getLangs()
    {
        return Yii::$app->params['languages'];
    }
    
    public static function getTextCountry($code)
    {
        $countries = static::getCountries();
        
        return array_key_exists($code, $countries) ? $countries[$code] : '';
    }
    
    public static function getDays()
    {
        $days = [];
        
        for($i=1;$i<=31;$i++) {
            $days[sprintf("%02d", $i)] = $i;
        }
        
        return $days;
    }
    
    public static function getMonths()
    {
        $months = [];
        for($i=1;$i<=12;$i++) {
            $months[sprintf("%02d", $i)] = ucfirst(Yii::$app->formatter->asDate(mktime(0,0,0,$i, 1, date('Y')), 'php:F'));
        }
        
        return $months;
    }
    
    public static function getYears()
    {
        $range = range(date('Y')-18, date('Y')-120);
        
        return array_combine($range, $range);
    }
    
    public static function getYesNo()
    {
        return [
            1 => Yii::t('app', "Yes"),
            0 => Yii::t('app', "No")
        ];
    }
    
    public static function randColor() {
        return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
    }
}
