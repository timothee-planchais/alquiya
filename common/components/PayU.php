<?php

namespace common\components;

use Yii;

/**
 * Description of PayU
 *
 * @author Timothee
 */
class PayU extends \yii\base\Component
{
    
    public function init()
    {
        require_once(Yii::getAlias('@common/libs/PayU.php'));
        
        \PayU::$apiKey = Yii::$app->params['payu.apiKey']; //Enter your own apiKey here.
        \PayU::$apiLogin = Yii::$app->params['payu.apiLogin']; //Enter your own apiLogin here.
        \PayU::$merchantId = Yii::$app->params['payu.merchantId']; //Enter your commerce Id here.
        \PayU::$language = \SupportedLanguages::EN; //Select the language.
        \PayU::$isTest = true; //Leave it True when testing.
        
        /*
         * PROD
         */
        /*\Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi"); 
        \Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi"); 
        \Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.3/"); */
        
        /*
         * TEST
         */
        \Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi"); 
        \Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi"); 
        \Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.3/"); 
        
        return parent::init();
    }
    
}
