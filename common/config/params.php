<?php
return [
    
    'contactFromEmail' => 'notificacion@alquiya.com',
    'contactFromName' => 'AlquiYA',
    
    'user.passwordResetTokenExpire' => 3600,
    'file.max_size' => 10000000,
    
    //NFS
    'nfs.url' => 'http://data.alquiya.com',
    
    //ISO 4217 code
    'currency' => 'COP',
    
    'frontend.url' => 'http://alquiya.com', 
    'backend.url' => 'http://admin.alquiya.com', 
    
    /*
     * PAYU
     * http://developers.payulatam.com/en/web_checkout/integration.html
     */
    'payu.apiKey' => '4Vj8eK4rloUd272L48hsrarnUA',//MCAkzMno6scjq9W820uje5Lg4o
    'payu.apiLogin' => 'pRRXKOl8ikMmt9u',//0IdU7Tk4y3y8wfS
    'payu.action' => 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu',//https://checkout.payulatam.com/ppp-web-gateway-payu/
    'payu.merchantId' => '508029',//716691
    'payu.accountId' => '512321',//721539
    'payu.currency' => 'COP',
    'payu.test' => '1',//0
    
    /*
     * GOOGLE
     */
    'google.api_key' => 'AIzaSyCAQmc0iWeBYUOcpqvPDdTHAM8fKzv0k5c',//'AIzaSyAM-xDgiVCnrJvWZFHxh3UPrjaPAAIAWt9aPAAI
    'google.api_language' => 'es',
    
    /*
     * FACEBOOK, TWITTER, Instagram
     */
    'facebook.app_id' => '1966537283377570',
    'facebook.url' => 'https://www.facebook.com/AlquiYa-281954749355916',
    'instagram.url' => 'https://www.instagram.com/alquiya/',
    'twitter.account' => 'alquiya',
    'twitter.url' => 'https://twitter.com/alquiy',
     
    /*
     * ITEM
     */
    'item.fees' => 0.15,
    'item.max_files' => 4,
    
    /*
     * SUBSCRIPTION PRICES
     * Réduction : 25%
     */
    'subscription.bumped.1_day' => 2000,
    'subscription.bumped.1_week' => 10500,//x5
    'subscription.bumped.1_month' => 31500,
    'subscription.bumped.6_months' => 140000,
    'subscription.bumped.1_year' => 210000,
    
    'subscription.featured.1_day' => 4000,
    'subscription.featured.1_week' => 21000,//x5
    'subscription.featured.1_month' => 63000,
    'subscription.featured.6_months' => 283000,
    'subscription.featured.1_year' => 425000,
    
    'subscription.highlighted.1_day' => 1500,
    'subscription.highlighted.1_week' => 7000,//x5
    'subscription.highlighted.1_month' => 21000,
    'subscription.highlighted.6_months' => 90000,
    'subscription.highlighted.1_year' => 135000,
    
    'subscription.pictures' => 20000,//Illimité
    
    /*
     * BOOKING
     */
    'booking.min_total' => 10000,//En COP
    'booking.delay' => 3,//Days
    'booking.cancel.min_days' => 1,//Max days before cancel
    'booking.price_min' => 1000,
    
    /*
     * FOOTER CITIES
     */
    'footer.cities' => [
        'Bogotá' => [
            'lat' => '4.6486259',
            'lng' => '74.2478917',
            'radius' => 30
        ],
        'Medellín' => [
            'lat' => '6.2530408',
            'lng' => '-75.56457369999998',
            'radius' => 15
        ],
        'Cali' => [
            'lat' => '3.3952332',
            'lng' => '-76.595704',
            'radius' => 15
        ],
        'Barranquilla' => [
            'lat' => '10.9838942',
            'lng' => '-74.853037',
            'radius' => 15
        ],
        'Cartagena de Indias' => [
            'lat' => '10.4002813',
            'lng' => '-75.5435449',
            'radius' => 15
        ],
        /*'Cúcuta' => [
            'lat' => '',
            'lng' => '',
            'radius' => ''
        ],
        'Bucaramanga' => [
            'lat' => '',
            'lng' => '',
            'radius' => ''
        ],*/
        
    ],
    
    /*
     * CURRENT COUNTRY
     */
    'country' => 'COL',
    'country2' => 'CO',
    
    /*
     * COUNTRIES
     */
    'countries' => [
        /*'ARG' => "Argentina",
        'BOL' => "Bolivia",
        'BRA' => 'Brazil',
        'CHL' => "Chile",*/
        'COL' => "Colombia",
        /*'ECU' => "Ecuador",
        'MEX' => 'Mexico',
        'PAN' => "Panama",
        'PER' => "Peru",
        'PRY' => "Paraguay",
        'URY' => "Uruguay",
        'VEN' => "Venezuela",*/
    ],
    
    'countries2' => [
        /*'AR' => "Argentina",
        'BO' => "Bolivia",
        'BR' => 'Brazil',
        'CL' => "Chile",*/
        'CO' => "Colombia",
        /*'EC' => "Ecuador",
        'MX' => 'Mexico',
        'PA' => "Panama",
        'PE' => "Peru",
        'PY' => "Paraguay",
        'UY' => "Uruguay",
        'VE' => "Venezuela",*/
    ],
    
    'languages' => [
        'es' => 'Español',
        'en' => "English",
        'fr' => "Français",
        'pt_BR' => "Portuges",
    ]
    
];
