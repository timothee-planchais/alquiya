<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@data' => '/var/www/alquiya/data',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'payu' => [
            'class' => 'common\components\PayU'
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=164.132.192.27;dbname=alquiya',
            'username' => 'root',
            'password' => 'rootpwd00',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            //'htmlLayout' => '@common/mail/layouts/html',
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'email-smtp.us-west-2.amazonaws.com',
                'username' => 'AKIAI2NBOYYMQNMKIAHA',
                'password' => 'AtDOLrrgeybpVfEsyYasu2ZoRQrz1rOdudb3CckzpNn0',
                'port' => '587',
                'encryption' => 'tls',
            ]*/
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'in-v3.mailjet.com',
                'username' => 'ecbbba56553145dba0b3f4d7f7d83eb0',
                'password' => '1865db22d5e586e6b6875822abbde125',
                'port' => '587',
                'encryption' => 'tls',
            ]
        ],
        'formatter' => [
            'class' => 'common\components\Formatter',
            'currencyCode' => 'COP',
            'nullDisplay' => '',
            //'numberFormatterTextOptions' => []
        ],
    ],
];
