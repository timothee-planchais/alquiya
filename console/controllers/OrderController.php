<?php


namespace console\controllers;

use Yii;
use common\models\Order;
use common\models\Payment;

/**
 * Description of OrderController
 *
 * @author Timothee
 */
class OrderController extends \yii\console\Controller
{
    
    /*
     * - Suppression après 48h des commandes sans méthode [DATE(date) <= DATE(NOW() - INTERVAL 2 DAY)]
     * - Suppression après 48h des commandes PayU non abouties via CB [DATE(date) <= DATE(NOW() - INTERVAL 2 DAY)]
     * - Suppression après 30jours des commandes PayU non reçues via autre moyen de paiement [DATE(date) <= DATE(NOW() - INTERVAL 30 DAY)]
     */
    public function actionClean()
    {
        /* @var $orders Order[] */
        $orders = Order::find()
                ->where(['status' => Order::STATUS_WAITING_PAYMENT])
                ->all();
        
        if($orders)
        {
            foreach($orders as $order)
            {
                $payment = $order->payment;
                //Vérifier si paiement via CB, ou autre type
                //...
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
}
