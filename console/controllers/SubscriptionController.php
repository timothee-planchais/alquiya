<?php

namespace console\controllers;

use Yii;
use common\models\Subscription;

/**
 * Description of SubscriptionController
 *
 * @author Timothee
 */
class SubscriptionController extends \yii\console\Controller
{
    /*
     * On vérifie que la subscription est à jour.
     * Si outdated, on la met en finished
     */
    public function actionCheckActive()
    {
        /* @var $subscription Subscription[] */
        $subscriptions = Subscription::find()
                ->where(['status' => Subscription::STATUS_ACTIVE])
                ->andWhere('end_date <= NOW()')
                ->all();
        
        if($subscriptions)
        {
            foreach($subscriptions as $subscription)
            {
                $subscription->status = Subscription::STATUS_FINISHED;
                $subscription->update(false, ['status', 'updated_at']);
                echo "\r\n".'Subscription #'.$subscription->id.' FINISHED';
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
    public function actionEndsSoon()
    {
        /* @var $subscription Subscription[] */
        $subscriptions = Subscription::find()
                ->where(['status' => Subscription::STATUS_ACTIVE])
                ->andWhere('end_date > NOW()')
                ->andWhere('end_date BETWEEN NOW() - INTERVAL 3 DAY AND NOW()')
                ->all();
        
        if($subscriptions)
        {
            foreach($subscriptions as $subscription)
            {
                \common\models\Notification::subscriptionEndsSoon($subscription);
                echo "\r\n".'Subscription #'.$subscription->id.' ENDS SOON';
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
    
}
