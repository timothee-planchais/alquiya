<?php

namespace console\controllers;

use Yii;
use common\models\Booking;
use common\models\Item;
use common\models\User;
use common\models\Notification;

/**
 * Description of BookingController
 *
 * @author Timothee
 */
class BookingController extends \yii\console\Controller
{
    /*
     * On envoie un rappel aux loueurs qui n'ont pas encore validé ou refusé des réservations
     */
    public function actionPending()
    {
        /* @var $bookings Booking[] */
        $bookings = Booking::find()
                ->where(['status' => Booking::STATUS_PENDING])
                ->andWhere('date_start > NOW()')
                ->andWhere('DATEDIFF(NOW(), created_at) >= 1')
                //->andWhere('DATEDIFF(NOW(), created_at) <= :delay')
                //->params(['delay' => Yii::$app->params['booking.delay']])
                ->all();
        
        if($bookings)
        {
            foreach($bookings as $booking) {
                echo "\n".'Booking #'.$booking->getPrimaryKey();
                Notification::bookingPending($booking);
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
    /*
     * On met à complete toutes les réservations qui sont terminées
     */
    public function actionComplete()
    {
        /* @var $bookings Booking[] */
        $bookings = Booking::find()
                ->where(['status' => Booking::STATUS_VALIDATED])
                ->andWhere('date_end <= NOW()')
                ->all();
        
        if($bookings)
        {
            foreach($bookings as $booking) {
                echo "\n".'Booking #'.$booking->getPrimaryKey();
                $booking->status = Booking::STATUS_COMPLETED;
                $booking->save(false);
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
    /*
     * - Suppression des bookings en attente et dont la date est dépassée
     */
    public function actionClean()
    {
        /* @var $bookings Booking[] */
        $bookings = Booking::find()
                ->where(['status' => Booking::STATUS_PENDING])
                ->andWhere('date_end <= NOW()')
                ->all();
        
        if($bookings)
        {
            foreach($bookings as $booking) {
                echo "\n".'Booking #'.$booking->getPrimaryKey();
                $booking->delete();
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
    
    
    /*
     * Suppression des bookings dont le paiement n'a pas été fait (un jour de décalage)
     * - Vérification du statut du paiement
     * - Si le payment n'a pas de transaction_id ou de state_pol, c'est que le paiement n'a pas été effectué, on supprime le booking
     * - On supprime aussi les booking qui n'ont pas de paiement
     */
    public function actionWaitingPayment()
    {
        /* @var $payu \common\components\PayU */
        $payu = Yii::$app->payu;
        
        //$response = \PayUReports::doPing();
        //echo $response->code;
        
        /* @var $bookings Booking[] */
        $bookings = Booking::find()
                ->where(['status' => Booking::STATUS_WAITING_PAYMENT])
                ->andWhere('DATE(created_at) < DATE(NOW())')
                ->all();
        
        if($bookings)
        {
            foreach($bookings as $booking) 
            {
                echo "\n".'Booking #'.$booking->getPrimaryKey();
                $payment = $booking->payment;
                if($payment)
                {
                    if($payment->transaction_id)
                    {
                        /*$parameters = array(\PayUParameters::TRANSACTION_ID => $payment->transaction_id);

                        $response = \PayUReports::getTransactionResponse($parameters);

                        try
                        {
                            //var_dump($response);die;
                            if ($response){
                                echo "\n STATE : ".$response->state;
                                //echo ", TRAZABILITY CODE : ".$response->trazabilityCode;
                                //echo ", AUTHORIZATION CODE : ".$response->authorizationCode;
                                echo ", RESPONSE CODE : ".$response->responseCode;
                                //echo ", OPERATION DATE : ".$response->operationDate;
                            }
                        }
                        catch(\PayUException $e)
                        {
                            echo $e->getMessage();
                        }*/
                    }
                    else {
                        $booking->delete();
                    }
                }
                else {
                    $booking->delete();
                }
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
            
            
}
