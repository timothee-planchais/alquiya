<?php


namespace console\controllers;

use Yii;
use common\models\Item;
use common\models\Subscription;
use common\models\BumpLog;
use DateTime;

/**
 * Description of ItemController
 *
 * @author Timothee
 */
class ItemController extends \yii\console\Controller
{
    
    /*
     * - Vérification, chaque heure, s'il y a des items à bumper
     * - On bump une fois par jour par item
     */
    public function actionBump()
    {
        /* @var $subscription Subscription[] */
        $subscriptions = Subscription::find()
                ->where(['type' => Subscription::TYPE_ITEM_BUMPED, 'status' => Subscription::STATUS_ACTIVE])
                ->all();
        
        if($subscriptions)
        {
            foreach($subscriptions as $subscription)
            {
                $item = $subscription->item;
                if($item) 
                {
                    //On vérifie s'il n'a pas déjà été bumpé dans les dernières 24h
                    $oBumpDate = DateTime::createFromFormat('Y-m-d H:i:s', $item->bump_date);
                    $oCurrentDate = new DateTime();
                    $diff = $oBumpDate->diff($oCurrentDate);
                    $hours = $diff->h;
                    $hours = $hours + ($diff->days*24);
                    if($hours >= 24)
                    {
                        $item->bump_date = date('Y-m-d H:i:s');
                        $item->update(false, ['bump_date']);
                        $bumpLog = new BumpLog([
                            'user_id' => $item->user_id,
                            'item_id' => $item->getPrimaryKey(),
                            'created_at' => $item->bump_date
                        ]);
                        $bumpLog->save(false);
                        echo "\r\n".'ITEM #'.$item->id.' BUMPED AT '.$item->bump_date;
                    }
                }
            }
        }
        
        return \yii\console\ExitCode::OK;
    }
    
}
